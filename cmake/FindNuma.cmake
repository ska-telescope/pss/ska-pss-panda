#=======================================================================
# FindNuma
# --------
#
# Find the NUMA library libnuma
#
# Result Variables
# ^^^^^^^^^^^^^^^^
#
#     ``NUMA_FOUND``
#       Indicates that the NUMA library has been found.
#
#     ``NUMA_INCLUDE_DIR``
#       Points to the libnuma include directory.
#
#     ``NUMA_LIBRARY_DIRS``
#       Points to the directories that contains the libraries.
#       The content of this variable can be passed to link_directories.
#
#     ``NUMA_LIBRARY``
#       Points to the libnuma that can be passed to target_link_libararies.
#
#=======================================================================
# Copyright (c) 2013-2017 MulticoreWare, Inc

if(NOT SKA_PANDA_NUMA_GUARD_VAR)
    set(SKA_PANDA_NUMA_GUARD_VAR TRUE)
else()
    return()
endif()

include(FindPackageHandleCompat)

find_path(numa_root_dir
          NAMES include/numa.h
          PATHS ENV NUMA_ROOT
          DOC "NUMA root directory"
)

find_path(NUMA_INCLUDE_DIR
          NAMES numa.h
          HINTS ${numa_root_dir}
          PATH_SUFFIXES include
          DOC "NUMA include directory"
)

find_library(NUMA_LIBRARY
             NAMES numa
             HINTS ${numa_root_dir}
             DOC "NUMA library"
)

if(NUMA_LIBRARY)
    get_filename_component(NUMA_LIBRARY_DIRS ${NUMA_LIBRARY} PATH)
endif()

mark_as_advanced(NUMA_INCLUDE_DIR NUMA_LIBRARY_DIRS NUMA_LIBRARY)

find_package_handle_standard_args(NUMA FOUND_VAR NUMA_FOUND REQUIRED_VARS numa_root_dir NUMA_INCLUDE_DIR NUMA_LIBRARY)

if(NUMA_FOUND)
    set(CMAKE_CXX_FLAGS "-DENABLE_NUMA ${CMAKE_CXX_FLAGS}")
endif()
