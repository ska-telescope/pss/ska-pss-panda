if(NOT SKA_PANDA_BUILD_TYPES_GUARD_VAR)
    set(SKA_PANDA_BUILD_TYPES_GUARD_VAR TRUE)
else()
    return()
endif()

include(cmake/dependency_register.cmake)

if(CMAKE_BUILD_TYPE MATCHES documentation)
    # Only building doc
    include(cmake/doxygen.cmake)
else()
    # Normal build operations
    include(cmake/dependencies.cmake)
    include(cmake/compiler_settings.cmake)
endif()
