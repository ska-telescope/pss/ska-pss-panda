# The MIT License (MIT)
#
# Copyright (c) 2016-2020 The SKA organisation
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Adapted from https://github.com/bilke/cmake-modules/blob/master/CodeCoverage.cmake

if(NOT SKA_PANDA_CODE_COVERAGE_GUARD_VAR)
    set(SKA_PANDA_CODE_COVERAGE_GUARD_VAR TRUE)
else()
    return()
endif()

if(CMAKE_BUILD_TYPE MATCHES profile)

    # Find the coverage generators
    find_program(GCOV_EXE gcov)
    find_program(LCOV_EXE lcov)
    find_program(GENHTML_EXE genhtml)

    # Sanity checking
    if(NOT GCOV_EXE)
        message(FATAL_ERROR "gcov not found; specify with GCOV_EXE")
    endif()

    if(NOT LCOV_EXE)
        message(FATAL_ERROR "lcov not found; specify with LCOV_EXE")
    endif()

    if(NOT GENHTML_EXE)
        message(FATAL_ERROR "genhtml not found; specify with GENHTML_EXE")
    endif()

    file(MAKE_DIRECTORY ${CMAKE_BINARY_DIR}/coverage)

endif()

#============================================
# coverage_target
#
# brief: generate a coverage report of the specified target
#
# usage: coverage_target(target)
#============================================
macro(coverage_target _targetname)

    set(_coverage_target coverage_report_${PROJECT_NAME}_${_targetname})

    #  The target will be executed (e.g. 'make targetname' and a coverage report generated for that launch
    if(CMAKE_BUILD_TYPE MATCHES profile)

        set(_outputname coverage_${_targetname})
        set(_outputdir coverage)
        add_custom_target(${_coverage_target}
                          COMMAND ${LCOV_EXE} --directory ${_outputdir} --zerocounters # Clean up lcov
                          COMMAND ${CMAKE_MAKE_PROGRAM} ${_targetname} ${ARGN} # Run the target
                          COMMAND ${LCOV_EXE} --directory . --capture --output-file ${_outputdir}/${_outputname}.info # Capture lcov counters and generate report
                          COMMAND ${LCOV_EXE} --remove ${_outputdir}/${_outputname}.info 'panda/*' 'test/*' '/usr/*' --output-file ${_outputdir}/${_outputname}.info.cleaned
                          COMMAND ${GENHTML_EXE} -o ${_outputdir}/${_outputname} ${_outputdir}/${_outputname}.info.cleaned
                          COMMAND ${CMAKE_COMMAND} -E remove ${_outputdir}/${_outputname}.info ${_outputdir}/${_outputname}.info.cleaned
                          WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
        )

        # Show info on where to find the report
        add_custom_command(TARGET coverage_report_${_targetname} POST_BUILD
                           COMMAND ;
                           COMMENT "see ${_outputdir}/${_outputname}/index.html for the coverage report."
        )

    else()
        # If coverage is not available, output a suitable message if someone tries to build the target
        add_custom_target(${_coverage_target}
                          COMMAND ${CMAKE_COMMAND} -E echo
                          COMMENT "target ${_coverage_target} requires a profile build. Did you forget to set CMAKE_BUILD_TYPE=profile?"
        )
    endif()

    set(PROJECT_TARGETS_VAR PROJECT_COVERAGE_TARGETS_${_targetname})
    list(APPEND ${PROJECT_TARGETS_VAR} ${_coverage_target})
    get_directory_property(has_parent PARENT_DIRECTORY)
    if(has_parent)
        set(${PROJECT_TARGETS_VAR} ${${PROJECT_TARGETS_VAR}} PARENT_SCOPE)
    else()
        set(${PROJECT_TARGETS_VAR} ${${PROJECT_TARGETS_VAR}})
    endif()

    # Add top-level coverage_report_testname target only when there can be no clashes with subprojects using the same macro
    if("${PROJECT_NAME}" STREQUAL "${CMAKE_PROJECT_NAME}")
        add_custom_target(coverage_report_${_targetname} DEPENDS ${${PROJECT_TARGETS_VAR}})
    endif()

endmacro(coverage_target)

coverage_target(test)
