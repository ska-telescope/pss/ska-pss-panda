#=======================================================================
# CUDA development toolkit and configuration
#
# Result Variables
# ^^^^^^^^^^^^^^^^
#
#     ``CUDA_NVCC_FLAGS``
#       Additional flags to send to the NVCC compiler.
#
#     ``CUDA_NVCC_FLAGS_DEBUG``
#       Additional flags to send to the NVCC compiler in debug build.
#
#=======================================================================
option(ENABLE_CUDA "Enable CUDA algorithms. Requires the CUDA toolkit to be installed." OFF)

if(NOT SKA_PANDA_CUDA_GUARD_VAR)
    set(SKA_PANDA_CUDA_GUARD_VAR TRUE)
else()
    return()
endif()

if(ENABLE_CUDA)

    cmake_minimum_required(VERSION 3.13) # CUDA package only available from this version
    find_package(CUDA REQUIRED)
    include_directories(${CUDA_TOOLKIT_INCLUDE})

    set(CUDA_HOST_COMPILER ${CMAKE_CXX_COMPILER})
    set(CUDA_PROPAGATE_HOST_FLAGS OFF)

    # Options for NVCC
    list(APPEND CUDA_NVCC_FLAGS --std c++17)

    if(CUDA_VERSION_MAJOR EQUAL "11")
        add_definitions(-DTHRUST_IGNORE_DEPRECATED_CPP_DIALECT)
    endif()

    if(CUDA_VERSION_MAJOR LESS "9")
        list(APPEND CUDA_NVCC_FLAGS_DEBUG --generate-line-info)
    endif()

    list(APPEND CUDA_NVCC_FLAGS_DEBUG --debug; --device-debug; -Xcompiler "-Werror")

    #========================================================
    # cuda_generate_link_file
    #
    # brief: generate a link file based on the CUDA objects that need to be compiled
    #
    # usage: cuda_generate_link_file(output_link_file_name cuda_target)
    #========================================================
    function(cuda_generate_link_file cuda_link_file cuda_target)
        # Compute the file name of the intermedate link file used for separable compilation
        cuda_compute_separable_compilation_object_file_name(cuda_link_file ${cuda_target} "${${cuda_target}_SEPARABLE_COMPILATION_OBJECTS}")

        # Add a link phase for the separable compilation if it has been enabled.  If
        # it has been enabled, then the ${cuda_target}_SEPARABLE_COMPILATION_OBJECTS
        # variable will have been defined.
        cuda_link_separable_compilation_objects("${cuda_link_file}" ${cuda_target} "${_options}" "${${cuda_target}_SEPARABLE_COMPILATION_OBJECTS}")

        set(CUDA_LINK_FILE ${cuda_link_file} PARENT_SCOPE)
    endfunction(cuda_generate_link_file)

    #========================================================
    # cuda_subpackage_compile
    #
    # brief: specify that certain objects should be compiled with
    #        alternative flags (e.g. a specific architecture)
    #
    # usage: cuda_subpackage_compile(args)
    #
    # examples: cuda_subpackage_compile(${my_cuda_files} OPTIONS "-arch compute_35")
    #========================================================
    function(cuda_subpackage_compile)
        file(APPEND ${SUBPACKAGE_FILENAME}
             "cuda_add_cuda_include_once()\n"
             "cuda_compile(cuda_objects "
        )
        foreach(arg ${ARGN})
            if(EXISTS "${arg}")
                set(file "${arg}")
            else()
                set(file "${CMAKE_CURRENT_SOURCE_DIR}/${arg}")
                if(NOT EXISTS "${file}")
                    set(file ${arg})
                endif()
            endif()
            file(APPEND ${SUBPACKAGE_FILENAME}
                 "${file}\n"
            )
        endforeach()
        file(APPEND ${SUBPACKAGE_FILENAME}
             ")\n"
             "list(APPEND LIB_OBJ_CUDA \${cuda_objects})\n"
        )
    endfunction(cuda_subpackage_compile)

else(ENABLE_CUDA)
    #========================================================
    # cuda_subpackage_compile
    #
    # brief: empty dummy function to be called if CUDA is not present
    #
    # usage: cuda_subpackage_compile(...)
    #========================================================
    function(cuda_subpackage_compile)
    endfunction(cuda_subpackage_compile)
endif(ENABLE_CUDA)
