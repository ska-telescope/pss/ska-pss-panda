include(cmake/thread_affinities.cmake)
if(CMAKE_THREAD_LIBS_INIT)
    list(APPEND DEPENDENCY_LIBRARIES ${CMAKE_THREAD_LIBS_INIT})
endif()
include(cmake/googletest.cmake)

# Essential dependencies
required_dependency(boost)

# Optional dependencies
optional_dependency(cuda)
optional_dependency(opencl)
optional_dependency(intel_fpga)

#find_package(Numa)
include(cmake/doxygen.cmake)

# Generate configuration-dependent products
generate_configuration_header("SKA_PANDA_")
generate_export_project_configuration()
