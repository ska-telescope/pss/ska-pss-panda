#================================================================================
# Intel multi-channel DMA libraries for use with Intel FPGAs (e.g. Agilex).
#
# This will create a target which will build these libraries using an external
# Makefile while building panda.
#================================================================================

option(ENABLE_INTEL_FPGA "Enable Intel FPGA architecture." OFF)

if(NOT SKA_PANDA_INTEL_FPGA_GUARD_VAR)
    set(SKA_PANDA_INTEL_FPGA_GUARD_VAR TRUE)
else()
    return()
endif()

if(ENABLE_INTEL_FPGA)

    # Include ExternalProject.cmake (provided with CMake) so that we can
    # automatically build the Intel libraries using the provided Makefile
    include(ExternalProject)

    set(_libmqdma_dir "${CMAKE_CURRENT_SOURCE_DIR}/thirdparty/intel_agilex_fpga/intel_lib/libmqdma")
    set(_fdas_dir     "${CMAKE_CURRENT_SOURCE_DIR}/thirdparty/intel_agilex_fpga/intel_lib/fdas")
    set(_libmqdma_lib_list
        ${_libmqdma_dir}/../common/src/ifc_qdma_utils.o
        ${_libmqdma_dir}/obj/ifc_mcdma.o
        ${_libmqdma_dir}/obj/ifc_env.o
        ${_libmqdma_dir}/obj
    )

    set(_fdas_lib_list
        ${_fdas_dir}/obj/fdas_setup.o
        ${_fdas_dir}/obj/fdas_test.o
        ${_fdas_dir}/obj/fdas_functions.o
        ${_fdas_dir}/obj/fdas_find.o
        ${_fdas_dir}/obj/fdas_operate.o
        ${_fdas_dir}/obj
    )

    # Create a target to build the mqdma library by running the Makefile in the
    # directory we found above, using the CMake ExternalProject functionality
    ExternalProject_Add(LIBMQDMA
                        SOURCE_DIR "${_libmqdma_dir}"
                        UPDATE_COMMAND ""
                        CONFIGURE_COMMAND ""
                        BUILD_IN_SOURCE TRUE
                        INSTALL_COMMAND ""
                        BUILD_BYPRODUCTS ${_libmqdma_lib_list}
                        BUILD_ALWAYS TRUE
    )

    # Include all C functions that interface with FDAS algorithm on the FPGA
    # those functions depends of mqdma library
    ExternalProject_Add(LIBFDAS
                        SOURCE_DIR "${_fdas_dir}"
                        UPDATE_COMMAND ""
                        CONFIGURE_COMMAND ""
                        BUILD_IN_SOURCE TRUE
                        INSTALL_COMMAND ""
                        BUILD_BYPRODUCTS ${_fdas_lib_list}
                        BUILD_ALWAYS TRUE
                        DEPENDS ${LIBMQDMA}
    )


    # include files are located in the same source directory
    set(_libmqdma_include_dir
        ${_libmqdma_dir}
       "${_libmqdma_dir}/../common/include"
    )

    set(_fdas_include_dir ${_fdas_dir})

    include_directories(${_libmqdma_include_dir} ${_fdas_include_dir})

    # we temporary need fdas and libmqdma include files available to compile Cheetah
    # this is just a workaround and it will removed once AT4-1506 be done.
    file(GLOB _fdas_mqdma_include_files
    "${_fdas_include_dir}/*.h"
    "${_libmqdma_dir}/../common/include/*.h"
    "${_libmqdma_dir}/*.h")

    install(FILES ${_fdas_mqdma_include_files} DESTINATION ${INCLUDE_INSTALL_DIR}/panda/arch/intel_agilex_fpga)
    install(DIRECTORY ${_libmqdma_dir}/../common/include/regs/ DESTINATION ${INCLUDE_INSTALL_DIR}/panda/arch/intel_agilex_fpga/regs)

endif(ENABLE_INTEL_FPGA)
