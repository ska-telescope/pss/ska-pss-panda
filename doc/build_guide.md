# Build Guide

## Exportable cmake Modules

To ensure consistency Panda can export cmake modules for use by other projects that make use of Panda (eg Cheetah).

This is done by a version of `FindPanda` which sets a variable `PANDA_MODULE_PATH` if a distribution of Panda is found.

From the Panda side, to make a module exportable you need to add the files that the module needs to export (the cmake file for example and any other required files - eg DoxygenAPI.in in the case of Doxygen) to the `INSTALLABLE_CMAKE_FILES` variable. Do this in the export_modules.cmake file.

From within the cmake module you can determine if the module is being run from Panda or as an installed Module from outside of Panda by checking if `PANDA_MODULE_PATH` is set. This can be important when you need to look for input files for example.

For an example see the `doxygen.cmake` file.
