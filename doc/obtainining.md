@page download Obtaining Panda

# Source Code
Panda is released as open source code under the MIT licence.

The source code for panda is maintained in a gitlab repository: @ref https://gitlab.com/ska-telescope/pss/ska-pss-panda

To obtain a copy you can either:
  - use the download button on the gitlab project page
  - install a git client on your system

## Stable Version
The default @b master branch of this repository is the @b stable production version.

To obtain a copy of the master branch using the git command-line tool:
@code
git clone --single-branch https://gitlab.com/ska-telescope/pss/ska-pss-panda.git
@endcode
Which will create a directory @b panda with the source code.

Anytime you wish to update your copy of the src code you can do so by changing to this directory and using git to pull the latest version e.g.
@code
cd panda
git pull
@endcode

To use panda you will have to build the library and executables from this source code as descirbed here: @ref build.

## Developer Version
If you intend to contribute to the panda src code you should clone the whole repository.
@code
git clone https://gitlab.com/ska-telescope/pss/ska-pss-panda.git
@endcode
