@page ProducersTutorial Producer Tutorial - How to Create a Producer

This tutorial assumes you understand how a Producer fits in to the overall framework.
If not go here: @ref PDPPipeline, and come back when your done.

A Producers job is to take data from an external source and to fill data structures provided by the @ref DataManager class.
The @ref ProducerViewSequenceDiagram "Producer View" shows this interaction.

This section introduces you to the panda classes that can aid you in writing your own Producer classes
and shows some examples of Producers actually deployed with their associated DataManger.

## The Curiously Recurring Template Pattern (CRTP)##
@anchor CRTP
The CRTP idiom is used frequently in panda. It is a technique of polymorphism and that allows for compile
time binding. This in contrast as to the more familiar inheritance of a base class with a virtual table
that gives you runtime polymorphism. The advantage is that there are no need for any indirection and
the opportunities for optimisation are greater, leading to more perfomrant code (at the price of flexibility).

The syntax for CRTP is a bit esoteric, but you should soon get used to it. The trick is in passing the derived type
in as a template parameter, and then calling any polymorphic method via the derived class.

For example, we can define a BaseClass with some default behavior
~~~~~~~~~~~{.cpp}
template<class DerivedType>
class BaseClass {
    public:
        ...
        // -- interface API methods
        inline std::string do_something const {
            return static_cast<DerivedType&>(*this).foo();
        }

    protected:
        // optionally add a default implemtation for the call
        std::string foo const {
            // a default implementation of foo
            return "BaseClass::foo";
        }
};
~~~~~~~~~~~

With such a base class you then inherit to get Polymorphism.
Here we define two classes, one that uses the default implementation
of ```foo()``` and one that redefines it.

~~~~~~~~~~~{.cpp}
class DerivedClassA : public BaseClass<DerivedClassA>
{
    ...
};

class DerivedClassB : public BaseClass<DerivedClassB>
{
        ...
    protected:
        // override the default foo method
        std::string foo const {
            // an alternative  implementation of foo
            return "DerivedClassB::foo";
        }
};

int main(int argc, char** argv)
{
    DerivedClassA a;
    DerivedClassB b;
    std::cout << a.do_something(); \\ outputs BaseClass::foo
    std::cout << b.do_something(); \\ outputs DerivedClassB::foo
}
~~~~~~~~~~~

## The Producer Concept #
The Producer should be a class that supports the @ref ProducerConcept.
That is to say it has to implement all the methods documented.
@ref ska::panda::ProducerConcept

Best practice to create a producer class, is to define a suitiable CRTP template and
inherit from this to create a concrete class.

Not doing so is equivalent to marking your class ```final```, and prevents you combining
Producers together as we shall see later.

## An Example File Reading Producer #
In this example we read each line of a comma seperated file into a vector.

@ref panda/examples/CsvFileProducer.h
@ref panda/examples/CsvFileProducer.cpp

Each file starts with headers describing each column.
e.g.
@verbatim
R1, R2, R3, X, Y, Z
8, 9, 19, 22, 1, 0
9, 13, 12, 1, 7
97, 134, 2, 8, 47
@endverbatim

We want our producer to read from the files provided, producing a single std::vector<int> for
each line of data, and a std::vector<std::string> for the header labels.

The DataManager will supply these types to your data processing pipeline together.
So you could write a pipeline with the signature something like:
~~~~~~~~~~~{.cpp}
void MyPipeline::operator()(std::vector<int>& data, std::vector<std::string>& headers)
{
    // do something with the data based on what the header info is
}
~~~~~~~~~~~
and this would be called for each line of every file.

### Declarations ###
Inherit from the panda::Producer template class. The first template Parameter should be
the name of your class following the CRTP rules. The remaing parameters should describe the data structure that
the producer will fill.

Notice that our data type for storing the headers is wrapped with a
ServiceData template. This tells the DataManager to treat it slightly differently from normal.
Without the ServiceData wrapper then the DataManager would wait until each data type has been supplied
with fresh data. Only when fresh data for both types is available would it be released for processing.
Marking data as ServiceData tells the DataManager that it should not try and sync but, if no new service data
is forthcoming, to continue to use the exsiting service data without waiting.

~~~~~~~~~~~{.cpp}
template<class DerivedType>
class CsvFileProducerCRTP : public ska::panda::Producer<DerivedType, std::vector<int>, ska::panda::ServiceData<std::vector<std::string>>>
{
    typedef ska::panda::Producer<DerivedType, std::vector<int>, ska::panda::ServiceData<std::vector<std::string>>> BaseT;

    typedef std::vector<int> Data;
    typedef std::vector<std::string> Headers;
    ...
~~~~~~~~~~~
Note that we define ```BaseT``` to refer to the base type of out class. This is good practice and allows us easily to refer to the complex syntax of the base clsss name later in the code. The first instance of this is in declaring the DataSetType as required by the ProducerConcept.

~~~~~~~~~~~{.cpp}
        typedef typename BaseT::DataSetType DataSetType; // Producer requirement
~~~~~~~~~~~
or equivalently
~~~~~~~~~~~{.cpp}
        using BaseT::DataSetType; // Producer requirement
~~~~~~~~~~~

### Initialising the Producer ###
Any Producer goes through two initialisation stages, the first on construction and the second when
a suitable DataManager becomes available to provide the data structures to fill.
As soon as this happens the init() method is called, and your producer is then free to call the
@ref `Producer::get_chunk` method.

In this class we will initialise some internal variables to track
which file we should open next in the constructor.

When we have a DataManager available init() is called and we can open our first file.
Note in the implementation that the ```next_file``` also causes the header information
to be sent down to the DataManager.
~~~~~~~~~~~{.cpp}
void CsvFileProducer::init()
{
    next_file();
}
~~~~~~~~~~~

### Requesting Data Structures from the DataManager ###
When we have data, we need to ask the DataManager for a suitable structure to fill (the DataManager manages memory).
This is done through a call to the get_chunk<DataType> templated method of the Producer base class. Not this can only
be called in the init() method, or after the init() method has been called. Don't try calling it in the constructor!

This method returns the object as a std::shared_ptr. As soon as this object goes out of scope it will be returned to the
DataManager which will pass it on to the rest of the pipeline.
~~~~~~~~~~~{.cpp}
template<class DerivedType>
bool CsvFileProducerCRTP<DerivedType>::next_file()
{
    // get the headers data structure to fill from the DataManager
    std::shared_ptr<Headers> headers=this->template get_chunk<Headers>();

    // now fill this structure from the next open file
    return read_headers(*headers);

    // the headers object will go out of scope at the end of this function
    // which will allow it to propagate to the DataManager as a filled data set
}
~~~~~~~~~~~

### The process method ###
The process method will be called any time the data queues in the DataManager are empty.
In a single threaded model it should do the work of filling the data structures. This
is not necessary if you have launched background threads to do the work instead. In our
example we keep it as a simple single threaded model and implement the method. Its role is
to read in the next line of the currently open file (or open the next one when we run out of lines),
and fill in the Data structure. Again we see the use of the ```get_chunk``` method and once again when the
```shared_ptr``` goes out of scope it is released to the rest of the pipeline.
~~~~~~~~~~~{.cpp}
template<class DerivedType>
bool CsvFileProducerCRTP<DerivedType>::process()
{
    // make sure we have a file to read from
    if(!_file_stream.is_open() || _file_stream.eof() || _file_stream.peek() == decltype(_file_stream)::traits_type::eof()) {
        if(!next_file()) return true; // tell the DataManager not to expect any more data
    }

    // ask the DataManager for a data structure to fill
    std::shared_ptr<Data> data = this->template get_chunk<Data>();

    // read in the CSV from the next line of the file into data
    std::string line;
    std::getline(_file_stream, line);
    std::size_t pos, last_pos = 0;
    while((pos = line.find_first_of(',', last_pos)) != std::string::npos) {
        std::string const token=line.substr(last_pos, pos - last_pos);
        data->emplace_back(std::stoi(token));
        last_pos = pos + 1;
    }
    return false;

    // data goes out of scope here, propagating to the DataManager
}
~~~~~~~~~~~

Note that it is OK to throw from this function, but keep in mind that as soon as the
shared_ptr goes out of scope it is released for processing, so steps should be taken
to keep a copy in scope, and recycled.

## Integrating with the DataManager#
Now you have a Producer you need to link it in with a suitable DataManager.
This is simple enough, the DataManager class takes a concrete Producer class as a template parameter.

first is to define our concrete class
~~~~~~~~~~~{.cpp}
class CsvFileProducer : public examples::CsvFileProducerCRTP<CsvFileProducer>
{
    using CsvFileProducerCRTP<CsvFileProducer>::CsvFileProducerCRTP;
};
~~~~~~~~~~~
And then the DataManager would be defined and instantiated like this
~~~~~~~~~~~{.cpp}
typedef ska::panda::DataManager<CsvfileProducer> DataManagerType;

// Prepare the CsvFileProducer object
std::list<std::string> some_list_of_files;

      .... add entries to the file list ...

CsvFileProducer csv_file_producer(some_list_of_files);

// instantiate the DataManager
DataManagerType data_manager(csv_file_producer);
~~~~~~~~~~~

## Creating Multiple Stream Producers #
You can combine different Producers together giving an object that produces all the data types.
To do this use the @ref ska::panda::ProducerAggregator template class.

~~~~~~~~~~~{.cpp}
typedef ska::panda::ProducerAggregator<CsvFileProducerCRTP, MyOtherStreamCRTP> ProducerType;
~~~~~~~~~~~

Note the template parameters to the ProducerConcept must be CRTP style templates, and not concrete classes.

## Other Panda Producer Helper Classes #
For more complex data sources these classes may simplify things
- @ref ska::panda::PacketStream  use this for multi-threaded handling of packet stream data such as UDP streams.
- @ref ska::panda::Connection     makes handling network connections easier
