## Architecture Tags

We have a resource managing class (panda/ResoucrcePool), that we use for managing access to compute accelerators such as a GPU.

Each resource is of a certain type, and the system matches resources to the user of the resource
based on this type.

e.g.

template<typename ResourceTag>
PoolResource<ResourceTag>;


The User of the resource also carries a set of tags to indicate which resources will be suitable and we can therfore guarantee at compile
time that only resources of the correct type can be allocated.

This is all good but now we have more complex sceanarios that need to be addressed where there are different versions of a resource type.
In this case we may want to specify a minimum, or a maximum version to be used, a range, or even multiple ranges.

We need a taggig schema that is clear and is easy to implement a compiler-time matching algorithm in the resource manager.

template<typename ResourceTag>
ResourceUser<ResourceTag>;

The tag on the Resource side is likely to be a specific card, or compute capability identifier (currently a generic nvidia::Cuda).

Users tags might be something like (assuming version here is compute capability revision number):
~~~{.cpp}
 nvidia::Cuda // anything that supports Cuda
 minimum<nvidia::Cuda::version4>; // only cards greater or equal  than some version (or version set)
 maximum<nvidia::Cuda::version4>; // only cards <= than some version (or version set)
 range<mnvidia::Cuda::version2, nvidia::Cuda::version4>;
 version<range<mnvidia::Cuda::version2, nvidia::Cuda::version4>, range<mnvidia::Cuda::version6, nvidia::Cuda::version8>>;
 version<range<mnvidia::Cuda::version2, nvidia::Cuda::version4>, minimum<mnvidia::Cuda::version6>>;
~~~
