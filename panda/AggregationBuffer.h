/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_AGGREGATIONBUFFER_H
#define SKA_PANDA_AGGREGATIONBUFFER_H

#include "panda/Buffer.h"
#include <vector>
#include <type_traits>

namespace ska {
namespace panda {

/**
 * @brief
 *    Aggregate together data chunks into a larger Buffer.
 *
 * @details
 *    Aggregates the data from several different DataType objects whilst maintaining references to the
 *    original DataType objects.
 *    The Aggregated buffer is contiguous in memory and will be equal or a subset of the data blocks in the
 *    composition obects.
 *
 * @verbatim
   Inserted Data(composition())  [ DataType #0 ][ DataType #1 ][ DataType #2 ][ DataType #3 ] ....
   Buffer                             [              BufferType                  ]
                                      ^
                                      |
                                    data()

                                 |----|------------------------------------------|
                                   ^                       ^
                                   |                       |
                                   |                    data_size()
                              offset_first_block()
  @endverbatim
 *
 *
 * @param BufferType the underlying type of the data in the Buffer e.g. float, double etc
 * \ingroup config
 * @{
 */

template<typename DataType, typename BufferType>
class AggregationBuffer
{
    public:
        typedef std::vector<std::shared_ptr<const DataType>> Composition;
        typedef typename BufferType::iterator iterator;
        typedef typename BufferType::const_iterator const_iterator;
        typedef decltype(&*std::declval<iterator>()) value_type;
        typedef value_type Rep;

    public:
        template<typename Arg, typename... Args
               , typename std::enable_if<!std::is_same<typename std::decay<Arg>::type, AggregationBuffer<DataType, BufferType>>::value, bool>::type = true>
        AggregationBuffer(Arg&&, Args&& ...);
        AggregationBuffer(AggregationBuffer&&);
        AggregationBuffer(AggregationBuffer const&) = delete;
        ~AggregationBuffer();

        AggregationBuffer& operator=(AggregationBuffer&&) = default;

        /**
         * @brief transfers size bytes of data, and assosiated composition information from the end of the
         *        buffer to the destination_buffer
         */
        void transfer(std::size_t size, AggregationBuffer<DataType, BufferType>& destination_buffer) const;

        /**
         * @brief return a pointer to the  position of the underlying data buffer
         * @param offset from the position
         */
        iterator data(unsigned offset=0);
        const_iterator data(unsigned offset=0) const;

        /**
         * @brief iterators for beginning to end of the data
         */
        iterator begin();
        const_iterator begin() const;
        const_iterator cbegin() const;
        iterator end();
        const_iterator end() const;
        const_iterator cend() const;

        /**
         * @brief return a pointer to the  buffer object
         */
        BufferType& buffer();
        BufferType const& buffer() const;
        void buffer(BufferType const& buffer);

        /**
         * @brief restore the current insertion pointer to the beginning of the current buffer
         *        and remove any references to associated objects
         */
        void reset();

        /**
         * @brief copy the data into the current buffer at the current insertion location
         *        The insertion pointer is then updated to point to the end of this data.
         */
        template<typename IteratorType>
        void insert( IteratorType& begin_iter, IteratorType const& end_iter, std::shared_ptr<const DataType> const& object );

        /**
         * @brief return the objects that make up the current buffer
         */
        Composition const& composition() const;

        /**
         * @brief return the unused number of elements in the buffer
         */
        std::size_t remaining_capacity() const;

        /**
         * @brief return the current number of elements inserted in the buffer
         */
        std::size_t data_size() const;

        /**
         * @brief return the total number of elements that can be stored in the buffer.
        */
        std::size_t capacity() const;

        /**
         *  @brief swap the contents of this buffer with that of the provided
         */
        void swap(AggregationBuffer<DataType, BufferType>&);

        /**
         *  @brief return the offset of the buffer start from the first composition object start
         */
        std::size_t offset_first_block() const;

        /**
         *  @brief set an offset of the first insert from the begin() of the first chunk
         */
        void offset_first_block(std::size_t offset);

    private:
        std::unique_ptr<BufferType> _buffer;
        Composition _objects;

        iterator _current_ptr;
        iterator _end_ptr;
        iterator _previous_ptr;

        std::vector<iterator> _data_boundary;
        std::size_t _first_chunk_offset;
};

} // namespace panda
} // namespace ska

#include "panda/detail/AggregationBuffer.cpp"

#endif // SKA_PANDA_AGGREGATIONBUFFER_H
/**
 *  @}
 *  End Document Group
 **/
