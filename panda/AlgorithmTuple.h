/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_ALGORITHMTUPLE_H
#define SKA_PANDA_ALGORITHMTUPLE_H
#include "panda/ConceptChecker.h"
#include "panda/AlgorithmConcept.h"
#include "panda/AlgorithmInfo.h"
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#pragma GCC diagnostic ignored "-Wpragmas"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wunused-variable"
#include <boost/mpl/map.hpp>
#include <boost/mpl/pair.hpp>
#include <boost/mpl/at.hpp>
#pragma GCC diagnostic pop
#include <tuple>


namespace ska {
namespace panda {

/**
 * @brief
 *   Extends a std::tuple by adding Architecture type information
 *   Easily allows you to find algos in the container by architecture type.
 *
 */
template<typename... Algorithms>
class AlgorithmTuple : public std::tuple<Algorithms...>
{
        typedef std::tuple<Algorithms...> BaseT;

        typedef ConceptChecker<AlgorithmConcept>::Check<Algorithms...> AlgorithmConceptCheckAlgorithms;

        typedef boost::mpl::map<boost::mpl::pair<typename Algorithms::Architecture, Algorithms>...> AlgoArchMapType;

    public:
        typedef std::tuple<typename Algorithms::Architecture...> Architectures;

    public:
        AlgorithmTuple(Algorithms&&...);
        AlgorithmTuple(AlgorithmTuple<Algorithms...>&&);
        AlgorithmTuple(std::tuple<Algorithms...>&&);
        ~AlgorithmTuple();

        /**
         * @brief return the algorithm that matches the @param Arch architecture
         */
        template<typename Arch>
        typename boost::mpl::at<AlgoArchMapType,Arch>::type& get();

        /**
         * @brief return the algorithm that matches the @param Arch architecture
         */
        template<typename Arch>
        typename boost::mpl::at<AlgoArchMapType,Arch>::type const& get() const;

};

template<typename... Algos>
struct MixedAlgoTraits<AlgorithmTuple<Algos...>>
    : public MixedAlgoTraits<std::tuple<Algos...>>
{};

} // namespace panda
} // namespace ska
#include "panda/detail/AlgorithmTuple.cpp"

#endif // SKA_PANDA_ALGORITHMTUPLE_H
