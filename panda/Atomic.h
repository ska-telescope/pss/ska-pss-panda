/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_ATOMIC_H
#define SKA_PANDA_ATOMIC_H
#include <atomic>


namespace ska {
namespace panda {

/**
 * @brief a wrapper around std::atomic that uses the template provided memory_order
 * @details This allows us to keep the operator type interface for any memory ordering requirement.
 *          rather than having to use the explicit load, store operators
 * @tparam T a type supported by std::atomic<T>. pointer types not yet implemented
 * @tparam MemoryOrder must be one of std::memory_order_relaxed, std::memory_order_release or std::memory_order_seq_cst.
 */
template<typename T, std::memory_order MemoryOrder>
class Atomic
{
        typedef std::atomic<T> AtomicT;

    public:
        typedef T value_type;

    public:
        Atomic() noexcept = default;
        Atomic(T val) noexcept;
        Atomic(const Atomic&) = delete;

        T operator=(T) noexcept;
        bool is_lock_free() const;
        operator T() const noexcept;

        void store( T desired, std::memory_order order = MemoryOrder ) noexcept;
        T load(std::memory_order order = MemoryOrder) const noexcept;

        T operator++() noexcept;
        T operator++(int) noexcept;
        T operator--() noexcept;
        T operator--(int) noexcept;
        T operator+=(T) noexcept;
        T operator-=(T) noexcept;
        T operator&=(T) noexcept;
        T operator|=(T) noexcept;
        T operator^=(T) noexcept;

    private:
        AtomicT _atom;
};

} // namespace panda
} // namespace ska
#include "detail/Atomic.cpp"

#endif // SKA_PANDA_ATOMIC_H
