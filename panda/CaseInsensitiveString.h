/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 The SKA organisation
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_CASEINSENSITIVESTRING_H
#define SKA_PANDA_CASEINSENSITIVESTRING_H

#include <string>
#include <iostream>

namespace ska {
namespace panda {

/**
 * @brief
 *    Defines a std::string type that performs case insensitive matching
 * @details
 * @exaple
 * CaseInsensitiveString s1("AbcD");
 * CaseInsensitiveString s2("aBcD");
 * assert(s1 == s2);
 * @endexample
 */
struct CaseInsensitiveStringCharTraits : public std::char_traits<char>
{
    static bool eq(char c1, char c2);
    static bool ne(char c1, char c2);
    static bool lt(char c1, char c2);
    static int compare(const char* s1, const char* s2, size_t n);
    static const char* find(const char* s, int n, char a);
};

//typedef std::basic_string<char, CaseInsensitiveStringCharTraits> CaseInsensitiveString;
struct CaseInsensitiveString : public std::basic_string<char, CaseInsensitiveStringCharTraits>
{
    private:
        typedef std::basic_string<char, CaseInsensitiveStringCharTraits> BaseT;

    public:
        using BaseT::BaseT;

        CaseInsensitiveString();
        CaseInsensitiveString(std::string const& s);
        CaseInsensitiveString& operator+=(std::string const&);

        operator std::string& () { return reinterpret_cast<std::basic_string<char>&>(*this); }
};


} // namespace panda
} // namespace ska
std::string operator+(std::string const&, ska::panda::CaseInsensitiveString const&);
std::istream& operator>>(std::istream&, ska::panda::CaseInsensitiveString&);
std::ostream& operator<<(std::ostream& os, ska::panda::CaseInsensitiveString const& str);

#endif // SKA_PANDA_CASEINSENSITIVESTRING_H 
