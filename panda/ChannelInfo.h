/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 The SKA organisation
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_CHANNELINFO_H
#define SKA_PANDA_CHANNELINFO_H
#include "panda/ConfigModule.h"
#include "panda/ChannelId.h"
#include <vector>


namespace ska {
namespace panda {

/**
 * @brief
 *    Configuration information for a nindividual channel
 * @details
 * 
 */
class ChannelInfo : public ConfigModule
{
        typedef std::vector<std::string> SinkInfoType;

    public:
        ChannelInfo(ChannelId id);
        ~ChannelInfo();

        void activate(bool active);
        bool active() const;

        panda::ChannelId const& id() const;
        SinkInfoType const& sinks() const;

        /**
         * @brief add a sink identifier to the channel
         */
        void sink(std::string const& id);

    protected:
        void add_options(OptionsDescriptionEasyInit& add_options) override;

    private:
        panda::ChannelId _id;
        bool _active;
        mutable bool _sinks_init;
        mutable SinkInfoType _sinks;
};

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_CHANNELINFO_H 
