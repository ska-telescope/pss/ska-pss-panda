/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_CONFIGFILE_H
#define SKA_PANDA_CONFIGFILE_H

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#pragma GCC diagnostic ignored "-Wpragmas"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wunused-local-typedefs"
#include <boost/property_tree/ptree.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/filesystem.hpp>
#pragma GCC diagnostic pop

namespace ska {
namespace panda {

/**
 * @brief
 *    Configuration file parser
 *
 * @details
 *    The object will take a filename and parse it using an appropriate parser into 
 *    a boost::property_tree::tree object.
 *    This data structure can then be used to access the properties.
 *
 *    Use the parse_propert_tree function to convert to boost::program_options data structures
 *    which will allow you to doc the config file as you go also using the option descriptions.
 *    e.g.
 *    @code
 *    ConfigFile config(my_file);
 *    config.parse();
 *    boost::program_options::options_description description; // fill this
 *    boost::program_options::variable_map vm;
 *    boost::program_options::store(panda::parse_property_tree<char>(config.property_tree(), description), vm);
 *    @endcode
 * \ingroup config
 * @{
 */
class ConfigFile
{
    public:
        enum class FileType { None, Xml, Json, Ini };

    public:
        /**
         * @brief provide the filename of the configuration file
         * @details will attempt to determine the type of configuration file from the extension
         *          or f no extension will assume XML
         */
        ConfigFile(std::string filename);
        ~ConfigFile();

        /**
         * @brief determine the type of file and parse appropriately
         */
        void parse();
        void parse(std::istream&);

        /**
         * @brief force interpretation as a json file
         */
        void set_json();

        /**
         * @brief force interpretation as an xml file
         */
        void set_xml();

        /**
         * @brief force interpretation as an ini file
         */
        void set_ini();

        /**
         * @brief return the type associated with this file
         */
        FileType type() const;

        /**
         * @brief force interpretation as an ini file
         */
        boost::filesystem::path const& path() const;

        /**
         * @brief return the config file property_tree representation
         */
        boost::property_tree::ptree const& property_tree() const;

    private:
        boost::filesystem::path _filename;
        FileType _type;
        boost::property_tree::ptree _dom;
};

/**
 * @brief program_options compatible parser for ptree 
template<class charT>
boost::program_options::basic_parsed_options<charT>
parse_property_tree(boost::property_tree::ptree const&, const boost::program_options::options_description&);
 */

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_CONFIGFILE_H 
/**
 *  @}
 *  End Document Group
 **/
