/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_CONFIGMODULE_H
#define SKA_PANDA_CONFIGMODULE_H


#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#pragma GCC diagnostic ignored "-Wpragmas"
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#include <boost/program_options.hpp>
#include <boost/property_tree/ptree.hpp>
#pragma GCC diagnostic pop

#include <string>
#include <map>
#include <vector>
#include <set>
#include <functional>
#include <memory>

namespace ska {
namespace panda {

/**
 * @brief
 *    Hierarchial configuration parameters
 *
 * @details
 * Inherit from ths class, overriding the add_options() virtual method
 * See boost::program_options to see the options you can add.
 * @code
 * void MyConfigModule::add_options(OptionsDescriptionEasyInit& options)
 * {
 *      options
 *      ("my_option", boost::program_options::value<int>(&_my_int), "my option");
 * }
 * @endcode
 *
 * Each module can be added as a node in a tree structure with the method add(ConfigNode&)
 * This will then be filled according to your defined options either from a ptree structure
 * Note that the commanf line options will also be generated, with the defined option name being prefixed
 * by the names of the modules above it in the structure.
 *
 * @code
 * MyConfigModule top;
 * MySubModule sub;
 * top.add(sub); // add the submodule options to the top level
 * @endcode
 *
 * Once the parameters are defined we can parse various
 * various inputs for these parameters into the variables_map structure
 * e.g. for the command line options
 * @code
 * boost::program_options::variables_map vm;
 * boost::program_options::store(boost::program_options::command_line_parser(argc, argv)
 *                                    .options(m.command_line_options()).run(), vm);
 * @endcode
 *
 * We provide a method to allow the easy parsing of boost:;propert_tree::ptree structures (use for XML, JSON config files for example)
 * @code
 * boost::property_tree::ptree p_tree;
 * boost::property_tree::json_parser::read_json(input_stream, p_tree);
 * top.parse_property_tree(p_tree, vm);
 * @endcode
 *
 * @section dynamic module generation
 * in cases where you want many identical types of objects to be inserted according to the ptree, you can assign
 * factories with method add_factory to generate a suitable module whenever the associated tag is found.
 * Note command lines will also be generated, with the object id() inserted.
 * To access these generated nodes you can use the submodule(std:;string const& tag_name) method.
 * e.g.
 * @code
 * ConfigModuleIteratorWrapper<MySubType> it = config.submodule("my_tag");
 * ConfigModuleIteratorWrapper<MySubType> end_it = config.submodule_end();
 * while(it!=end_it)
 * {
 *      MySubType const& object = *it;
 *      ...
 * }
 * @endcode
 * \ingroup config
 * @{
 */
class ConfigModule
{
    private:
        typedef std::map<std::string, std::vector<ConfigModule*>> ContainerType;

        template<typename T> friend class ConfigModuleIteratorWrapper;

        // iterator type for accessing subsections
        class ConstIterator {
            public:
                ConstIterator();
                ConstIterator(std::vector<ConfigModule*> const* v);

                ConstIterator& operator++();
                ConstIterator operator++(int);
                bool operator==(ConstIterator const& i) const;
                bool operator!=(ConstIterator const& i) const;
                ConfigModule const& operator*() const;
                ConfigModule const* operator->() const;

            private:
                std::vector<ConfigModule*> const* _v;
                std::vector<ConfigModule*>::const_iterator _it;
        };

        /**
         * @brief allow boost::program_options initialisation semantics
         */
    protected:
        class OptionsDescriptionEasyInit {
          public:
            OptionsDescriptionEasyInit(boost::program_options::options_description* opts, std::string prefix);

            OptionsDescriptionEasyInit& operator()(const char* name, const char* description);

            OptionsDescriptionEasyInit& operator()(const char* name, const boost::program_options::value_semantic* s);

            OptionsDescriptionEasyInit& operator()(const char* name, const boost::program_options::value_semantic* s, const char* description);

        private:
            std::string ext_name(const char*) const;

        private:
            boost::program_options::options_description* _opts;
            std::string _prefix;
        };

    public:
        typedef ConstIterator const_iterator;

    public:
        /**
         * @param name is the name of the module
         */
        ConfigModule(std::string name);
        ConfigModule(ConfigModule const&) = delete; // very dangerous due to difficulty of ensuring notifiers are consistent
        ConfigModule(ConfigModule&&) = delete;      //
        virtual ~ConfigModule();

        /**
         * @return the name to be associated with the configuration
         */
        std::string const& name() const;

        /**
         * @brief implement in derived class to set available options (see boost::program_options doc for examples)
         */
        virtual void add_options(OptionsDescriptionEasyInit&) {};

        /**
         * @brief command line options associated with this configuration
         */
        boost::program_options::options_description command_line_options() const;
        boost::program_options::options_description command_line_options(typename boost::property_tree::ptree::path_type const& path) const;

        /**
         * @brief return the options of the named sub-section
         */
        boost::program_options::options_description get_subsection_options(std::string const& section_name) const;

        /**
         * @brief add a ststic subsection to integrate
         * @details command_line options will be passed up to the higher layer
         */
        void add(ConfigModule& config);

        /**
         * @brief add a factory to generate a new ConfigModule each tiem the @param section_name is encountered
         */
        void add_factory(std::string section_name, std::function<ConfigModule*()> const&);

        /**
         * @brief add a factory to generate a new ConfigModule each tiem an unknown label is encountered
         */
        void add_default_factory(std::function<ConfigModule*(std::string const& section_name)> const& fn);

        /**
         * @brief add a function to be called each tiem an unknown label is encountered
         * @details Note this will not be called if a default_factory is set
         */
        void add_default_function(std::function<void(std::string const& section_name)> const& fn);

        /**
         * @brief map the sections of the boost_property_tree onto the modules options
         */
        virtual void parse_property_tree(boost::property_tree::ptree const& tree, boost::program_options::variables_map& vm);

        /**
         * @brief return an empty ptree with the correct structure for parsing and the options description for values
         * @details use for documenting the config file - e.g. print out with xml_wrote, json_write etc.
         */
        boost::property_tree::ptree config_tree() const;

        /**
         * @brief list of viable section names
         */
        std::set<std::string> subsections() const;

        /**
         * @brief return iterator of sections that match the section_name
         */
        const_iterator subsection(std::string const& section_name) const;
        const_iterator subsection_end() const;

        /**
         * @return the unique id for the module
         * @details each module has its own id and the id option is reserved to enable it to be set.
         *          If it is not set then a random id string will be generated.
         *          This allows you to distinguish between different modules of the same type. e.g. when using config factories
         */
        std::string const& id() const;

       /**
        * @brief get the named section witht he specified id, a default constructed
        * @param section_name the subsection name
        * @param id the unique id associated with the section
        * @throw panda::Error if the requested section_name does not exist.
        */
        ConfigModule const& get_subsection(std::string const& section_name, std::string const& id) const;
        ConfigModule& get_subsection(std::string const& section_name, std::string const& id);


    protected:
       /**
        * @brief print out help for a submodule
        */
        std::string help(std::string const& module_path) const;

        /**
         * @return the option prefix for each configuration option
         */
        std::string const& prefix() const;

        /**
         * @return the option prefix for each configuration option
         */
        void set_name(std::string name);

        /**
         * @brief return the options object (initialised)
         */
        boost::program_options::options_description cli_options_description();

        /**
         * @brief return the options object prefixed with the provided string
         * @details will create an empty object if it does not exist
         */
        boost::program_options::options_description prefixed_cli_options_description(std::string const& prefix);

        /**
         * @brief return the options object for just the section (initialised)
         * @details will create an empty object if it does not exist
         */
        boost::program_options::options_description cli_options_description(std::string const& section_name) const;

        /**
         * @brief return the first named sub-module
         * @details throw panda::Error if the section does not exist
         */
        ConfigModule& get_subsection(std::string const& section_name) const;

        /**
         * @brief add a subsection to integrat
         * @param section_name use alternative section_name rather than the modules name
         *        n.b. if this is "" the node will not be added
         * @details command_line options will be passed up to the higher layer
         */
        void add(std::string section_name, ConfigModule& config);

        /**
         * @brief invoke the named factory to generate a node
         */
        ConfigModule* generate_section(std::string const& tag, boost::optional<std::string> const& id = boost::optional<std::string>());

        void parse_property_subtree(boost::property_tree::ptree const& tree
                                  , boost::program_options::variables_map& vm
                                  , std::string const& path);

    private:
        void add_subsection_options(boost::program_options::options_description& opts, std::string const& prefix) const;

        /**
         * @brief construct a default section object for the named type using the factory
         * @throws panda::Error if there is no suitable factory
         */
        ConfigModule& get_default_section(std::string const& section_name) const;

        /**
         * @brief returns the factory associated with the tag, or the default if none are set.
         * @details n.b. the default can be unset, and so the return value should always be checked
         */
        std::function<ConfigModule*()> get_factory(std::string const& name) const;

        /**
         * @brief returns a constructed object that represents an allowed node in the specified tree path
         * @returns nullptr if not found
         */
        ConfigModule const* get_representative_section(typename boost::property_tree::ptree::path_type const& path) const;

        /**
         * @brief the set of known tags for child nodes
         */
        std::set<std::string> const& tags() const;

    private:
        mutable bool _init;
        mutable std::string _id;
        std::string _name;
        std::string _prefix;
        std::map<std::string, std::function<ConfigModule*()>> _factory;
        std::function<ConfigModule*(std::string const&)> _default_factory;
        std::function<void(std::string const&)> _default_function;

        ContainerType _subsections;
        std::vector<std::unique_ptr<ConfigModule>> _dynamic_subs; // container of generated submodules
        mutable std::map<std::string, std::unique_ptr<ConfigModule>> _default_section; // container of default generated submodules

        static unsigned long _id_generator;
        std::set<std::string> _tags;
};
/**
 * @brief iterator wrapping class to provide an iterator that returnds ConfigModule objects cast to the type T
 */
template<typename T>
class ConfigModuleIteratorWrapper {
        typedef ConfigModuleIteratorWrapper<T> SelfType;

    public:
        ConfigModuleIteratorWrapper(ConfigModule::const_iterator&& it)
            : _it(std::move(it)) {}

        inline SelfType& operator++() { ++_it; return *this; }
        inline SelfType operator++(int) { SelfType tmp(*this); ++_it; return tmp;}
        inline bool operator==(ConfigModuleIteratorWrapper const& i) const { return i._it == _it; }
        inline bool operator!=(ConfigModuleIteratorWrapper const& i) const { return i._it != _it; }
        inline T const& operator*() const { return static_cast<T const&>(*_it); }
        inline T const* operator->() const { return static_cast<T const*>(&*_it); }

    private:
        ConfigModule::const_iterator _it;
};

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_CONFIGMODULE_H
/**
 *  @}
 *  End Document Group
 **/
