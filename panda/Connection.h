/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_CONNECTION_H
#define SKA_PANDA_CONNECTION_H

#include "panda/Buffer.h"
#include "panda/AbstractConnection.h"
#include "panda/ConnectionTraits.h"
#include "panda/DataRateMonitor.h"
#include "panda/ErrorPolicyConcept.h"
#include "panda/ConnectionTraitsConcept.h"
#include "panda/ClockTypeConcept.h"
#include "panda/detail/SocketConnectionImpl.h"
#include "panda/detail/ConnectionImpl.h"
#include <memory>

namespace ska {
namespace panda {
class IpAddress;

/**
 * @class Connection
 * @brief
 *    Manages a pre-defined network connection according to a specific Connection Policy.
 *
 * @details
 *    A templated class that allows you to specify network protocol (e.g. Tcp, Udp),
 *    and a sepecific error policy.
 *
 *    Hides all connection/handshaking details etc from the user of the class to
 *    provide a simple interface to get and send data.
 *
 * Instantiation
 * -------------
 *    e.g. set up a connection overriding the default error policy
 *
 *    @code
 *
 *      Engine engine;
 *      Connection<ConnectionTraits<Udp, MyErrorPolicy>> connection(engine);
 *      .
 *      .
 *      .
 *      engine.run(); // nothing happens until you start processing events
 *
 *    @endcode
 *
 * Error Handling Policies
 * -----------------------
 *    The connection uses a policy to inform it what to do in the case of an error.
 *    A policy class must provide the following methods:
 *
 *
 *      // will be called whenever an error attempting to connect occurs.
 *      // Should return true if an immediate reconnection attempt is desired, false otherwise.
 *
 *      bool connection_error(Error const&);
 *
 *
 *      // called whenever an other error condition occurs.
 *      // Should return true if an immediate reconnection attempt is desired, false otherwise.
 *
 *      bool error(Error const&);
 *
 *
 *      bool try_reconnect(); // Called before any attempt to connect.
 *                            // Return true to allow the attempt, false otherwise.
 *
 *
 *      void connected();     //  Called on a succesful connection.
 *
 */

// this class acts as a common interface and holder of a shared pointer
// to the underlying implementation class
template<class DerivedType, class ConnectionTraits>
class SocketConnection : public AbstractConnection
{
    public:
        typedef typename ConnectionTraits::EndPointType EndPointType;
        typedef typename ConnectionTraits::ErrorPolicyType ErrorPolicyType;
        typedef typename ConnectionTraits::SocketType SocketType;
        typedef typename ConnectionTraits::EngineType EngineType;
        using AbstractConnection::BufferType;

        BOOST_CONCEPT_ASSERT((ErrorPolicyConcept<ErrorPolicyType>));
        BOOST_CONCEPT_ASSERT((ConnectionTraitsConcept<ConnectionTraits>));

    protected:
        /**
         * @param engine the engine to run the socket connection
         * optional @param socket preconfiguraed socket
         */
        template<typename... Args>
        SocketConnection(Args&&...);

        virtual ~SocketConnection();

    public:
        /**
         * @brief set the remote endpoint
         */
        void set_remote_end_point(EndPointType const& end_point);

        /**
         * @brief set the remote endpoint
         */
        void set_remote_end_point(IpAddress const& end_point);

        /**
         * @brief return the remote endpoint
         */
        EndPointType const& remote_end_point() const;

        /**
         * @brief asyncronously send the data in the buffer through the connection.
         * @details The handler will be called after the data has been sent or an error occured.
         *          Notice that errors will be propagated according to the policy in place.
         */
        void send(BufferType const buffer, SendHandler const& handler) override;

        /**
         * @brief asyncronously send the data in the buffer through the connection.
         * @details The handler will be called after the data has been sent or an error occured.
         *          Notice that errors will be propagated according to the policy in place.
         */
        void receive(BufferType buffer, ReceiveHandler handler) override;

        /**
         * @brief close an open connection
         */
        void close() override;

        /**
         * @brief return the error handling policy
         */
        ErrorPolicyType& policy();

        /**
         * @brief return the underlying socket object
         */
        SocketType& socket();
        SocketType const& socket() const;

        /**
         * @brief return the underlying engine
         */
        EngineType& engine();

    private:
        typedef DerivedType PimplT;
        std::shared_ptr<PimplT> _pimpl;
};

/**
 * @brief The users interface to the SocketConnection object
 * @details
 * Use this class to define an internet Connection
 * @code
 * ProcessingEngine engine;
 *
 * Connection<ConnectionTraits<panda::Udp>> udp_connection(engine);
 * Connection<ConnectionTraits<panda::Tcp>> tcp_connection(engine);
 *
 * @endcode
 */
template<typename ConnectionTraits>
class Connection : public SocketConnection<detail::ConnectionImpl<ConnectionTraits>, ConnectionTraits>
{
        BOOST_CONCEPT_ASSERT((ConnectionTraitsConcept<ConnectionTraits>));

        typedef SocketConnection<detail::ConnectionImpl<ConnectionTraits>, ConnectionTraits> BaseT;

    public:
        template<typename... Args>
        Connection(Args&&...);
};

/**
 * @brief The users interface to the SocketConnection object
 *        where timing information is required
 * @details
 * Use this class to define an internet Connection
 * @code
 * ProcessingEngine engine;
 *
 * TimedConnection<ConnectionTraits<panda::Udp>> udp_connection(engine);
 * TimedConnection<ConnectionTraits<panda::Tcp>> tcp_connection(engine);
 *
 * @endcode
 */
template<typename ConnectionTraits, typename ClockType=std::chrono::high_resolution_clock>
class TimedConnection : public SocketConnection<detail::ConnectionImpl<ConnectionTraits>, ConnectionTraits>
{
        BOOST_CONCEPT_ASSERT((ConnectionTraitsConcept<ConnectionTraits>));
        BOOST_CONCEPT_ASSERT((ClockTypeConcept<ClockType>));

        typedef SocketConnection<detail::ConnectionImpl<ConnectionTraits>, ConnectionTraits> BaseT;

    public:
        template<typename... Args>
        TimedConnection(Args&&...);

        /**
         * @brief as in the baseclass, except taking timing data measurents
         */
        void send(typename BaseT::BufferType const buffer, typename BaseT::SendHandler const& handler) override;

        /**
         * 2brief return the object containg the timing statistics
         */
        DataRateMonitor<ClockType> const& monitor() const;

    private:
        std::shared_ptr<DataRateMonitor<ClockType>> _rate_monitor;
};

} // namespace panda
} // namespace ska
#include "panda/detail/Connection.cpp"

#endif // SKA_PANDA_CONNECTION_H
