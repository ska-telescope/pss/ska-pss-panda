/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_CONNECTIONTRAITSCONCEPT_H
#define SKA_PANDA_CONNECTIONTRAITSCONCEPT_H

#include "panda/ErrorPolicyConcept.h"
#include "panda/EndPointConcept.h"
#include "panda/EngineConcept.h"



/**
 * @brief The ConnectionTraitsConcept is used to check the requirements on a ConnectionTraits type
 *
 * @details A ConnectionTrait class must provide five types.
 *
 * The ErrorPolicyType dictates the connection behaviour when an error occurs. CumulativDelayErrorPolicy and PassiveErrorPolicy for specific
 * examples of behaviour.
 *
 * The EndPointType specifies the type of EndPoint. The default is taken from ProtocolType with Protocol::endpoint.
 * An example is boost::asio::ip::basic_endpoint<Protocol>
 *
 * The EngineType specifies a type that wraps the boost::io_service needed by the connection functionality
 * See Engine.h and ProcessingEngine.h for examples.
 *
 * A ProtocolType, specifying 'Tcp' or 'Udp' must be provided
 *
 * Finally, a SocketType must be provided, such as boost::asio::ip::udp::socket or boost::asio::ip::tcp::socket
 *
 * \ingroup concepts
 * @{
 */

namespace ska {
namespace panda {

template <class X>
struct ConnectionTraitsConcept: Concept<X> {
        typedef Concept<X> Base;

        BOOST_CONCEPT_ASSERT((ErrorPolicyConcept<typename X::ErrorPolicyType>));
        BOOST_CONCEPT_ASSERT((EndPointConcept<typename X::EndPointType>));
        BOOST_CONCEPT_ASSERT((EngineConcept<typename X::EngineType>));

    public:
        BOOST_CONCEPT_USAGE(ConnectionTraitsConcept) {
            Base::template type<typename X::ProtocolType>::exists();
            Base::template type<typename X::SocketType>::exists();
        }
};

} // namespace panda
} // namespace ska

/**
 *  @}
 *  End Document Group
 **/

#endif // SKA_PANDA_CONNECTIONTRAITSCONCEPT_H
