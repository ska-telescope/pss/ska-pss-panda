/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_DEVICEGENERATE_H
#define SKA_PANDA_DEVICEGENERATE_H

#include "panda/Configuration.h"
#include "panda/Architecture.h"

namespace ska {
namespace panda {

/**
 * @brief provides specialisations to extend panda::generate
 *
 * @details This class specialises on the architecture of iterators
 *
 */
template<class ArchT, typename GeneratorT, typename Enable=void>
class DeviceGenerate
{
    public:
        /**
         * @brief perform generate in memory on the Architecture
         */
        template<typename SrcIteratorT, typename EndIteratorT, typename GeneratorArgT>
        static inline
        void generate(SrcIteratorT&& begin, EndIteratorT&& end, GeneratorArgT&&);
};

// specialization to extract Architecture from the DeviceGenerateer type
template<class ArchT, typename GeneratorT, typename Enable>
struct ArchitectureType<DeviceGenerate<ArchT, GeneratorT, Enable>>
{
    typedef ArchT type;
};

} // namespace panda
} // namespace ska

#ifdef SKA_PANDA_ENABLE_CUDA
#include "panda/arch/nvidia/DeviceGenerate.h"
#endif // SKA_PANDA_ENABLE_CUDA
#ifdef SKA_PANDA_ENABLE_OPENCL
//#include "panda/arch/altera/DeviceGenerate.h"
#endif // SKA_PANDA_ENABLE_OPENCL
#include "detail/DeviceGenerate.cpp"
#endif // SKA_PANDA_DEVICEGENERATE_H
