/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_ENDPOINTCONFIG_H
#define SKA_PANDA_ENDPOINTCONFIG_H


#include "panda/ConfigModule.h"
#include "panda/IpAddress.h"
#include <string>

namespace ska {
namespace panda {

/**
 * @brief
 *    Configuration options for setting an ip address and port
 * 
 */
class EndpointConfig : public ConfigModule
{
    public:
        EndpointConfig(std::string const& tag_name="endpoint");
        ~EndpointConfig();

        IpAddress const& address() const;

        /**
         * @brief set the default address
         */
        void address(IpAddress const& address);

    protected:
        void add_options(OptionsDescriptionEasyInit& add_options);

    private:
        IpAddress _ip_address;
};

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_ENDPOINTCONFIG_H 
