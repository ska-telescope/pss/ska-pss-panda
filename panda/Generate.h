/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_GENERATE_H
#define SKA_PANDA_GENERATE_H

#include "panda/DeviceIterator.h"

namespace ska {
namespace panda {

/**
 * @brief device independent version of std::generate
 *
 * @details for each element referenced by the iterator assign its value
 *          with that returned by the generator
 *
 */

template<typename SrcIteratorT
       , typename EndIteratorT
       , typename GeneratorT>
inline void generate( SrcIteratorT&& begin
                    , EndIteratorT&& end
                    , GeneratorT&&
                    );


} // namespace panda
} // namespace ska

#include "panda/detail/Generate.cpp"

#endif // SKA_PANDA_GENERATE_H
