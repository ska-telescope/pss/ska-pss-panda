/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_MULTIITERATOR_H
#define SKA_PANDA_MULTIITERATOR_H
#include "panda/detail/MultiIteratorImpl.h"
#include "panda/TypeTraits.h"
#include "panda/Copy.h"
#include <iterator>
#include <type_traits>

namespace ska {
namespace panda {

/// helper classes to aid Configuring the MultiIterator
/**
 * @brief No opertion (i,e, do nothing) functor
 */
class NoOp {
    public:
        template<typename... Args>
        void operator()(Args...) {}
};

/**
 * @brief class that attempts to call std::begin of the type T
 *        if std::begin is not defined then the type of the operator
 *        will be void.
 */
template<typename T, class Enable=void>
struct DefaultBegin {
    // if theres no supported type return void to indicate it cant be done
    inline void operator()(T&) {};
};

// allow for shared_ptr wrapped types
template<typename T>
struct DefaultBegin<std::shared_ptr<T>, typename std::enable_if<std::is_compound<T>::value>::type>
{
    inline
    auto operator()(std::shared_ptr<T> const& a) -> decltype(DefaultBegin<T>()(*a))
    {
        if(a)
            return DefaultBegin<T>()(*a);
        return decltype(DefaultBegin<T>()(*a))();
    }
};

/**
 * @brief specialisation where std::begin is defined for type T
 */
template<typename T>
struct DefaultBegin<T, typename std::enable_if<!std::is_same<void, decltype(std::begin(std::declval<T&>()))>::value>::type>
{
    inline
    auto operator()(T& a) -> decltype(std::begin(a))
    {
        return std::begin(a);
    }
};

// --- begin sanity checks ---
static_assert(!std::is_same<void, decltype(DefaultBegin<std::vector<int>>()(std::declval<std::vector<int>&>()))>::value, "unexpected iterator type");
static_assert(std::is_same<void, decltype(DefaultBegin<int>()(std::declval<int&>()))>::value, "unexpected iterator type");
static_assert(std::is_same<decltype(DefaultBegin<std::vector<int>>()(std::declval<std::vector<int>&>()))
                         , decltype(DefaultBegin<std::shared_ptr<std::vector<int>>>()(std::declval<std::shared_ptr<std::vector<int>>&>()))>::value, "unexpected iterator type - shared_ptr is not being dereferenced");
// --- end sanity checks ---

template<typename T, class Enable=void>
struct DefaultEnd {
    // if theres no supported type return void to indicate it cant be done
    inline void operator()(T&) {};
};

template<typename T>
struct DefaultEnd<T, typename std::enable_if<!std::is_same<void, decltype(std::end(std::declval<T&>()))>::value>::type>
{
    inline
    auto operator()(T& a) -> decltype(std::end(a))
    {
        return std::end(a);
    }
};

// allow for shared_ptr wrapped types
template<typename T>
struct DefaultEnd<std::shared_ptr<T>>
{
    inline
    auto operator()(std::shared_ptr<T> const& a) -> decltype(DefaultEnd<T>()(*a))
    {
        if(a)
            return DefaultEnd<T>()(*a);
        return decltype(DefaultEnd<T>()(*a))();
    }
};

// Essential description of the iterators in each layer
template<typename T, typename HandlerType=NoOp
                   , typename BeginFunctor=DefaultBegin<typename std::remove_reference<T>::type>
                   , typename EndFunctor=DefaultEnd<typename std::remove_reference<T>::type>
                   , class Enable=void
        >
struct IteratorDepthTraits
{
    typedef BeginFunctor Begin;
    typedef EndFunctor End;
    typedef HandlerType Handler;

    public:
        // Constructors
        IteratorDepthTraits() {}

        IteratorDepthTraits(Handler h)
            : _handler(h) {}

        // return the begin iterator for the Type
        inline decltype(std::declval<Begin>()(std::declval<T&>())) begin(T& t) { return _begin(t); }

        // return the end iterator for the Type (if its not a void)
        inline decltype(std::declval<End>()(std::declval<T&>())) end(T& t) { return _end(t); }

        void handler() { _handler(); };

        std::size_t distance() const { return std::distance(_begin, _end); }

    private:
        Begin   _begin;
        End     _end;
        Handler _handler;
};

template<typename ContainerType> using DefaultIteratorDepthTraits = IteratorDepthTraits<ContainerType>;

/**
 * @brief
 *   Traits description for configuring the MultiIterator template.
 *   Should be passed as the parameter to MultiIterator.
 *
 * @tparam ContainerType the type of container/object you wish to iterate over
 * e.g. the following container will have two layers (max_depth=2). The convention
 * for depth is the outermost is depth 0, increasing as you move down through the layers
 * of the compound object.
 * @code
 * // depth 0 would refer to the object itself
 * // depth 1 would refer to objects of type std::vector<float>
 * // depth 2 would refer to objects of type float
 * typedef std::vector<std::vector<float>> MyContainerType;
 * @endcode
 *
 * @tparam IteratorType  the type of iterator at the top level (depth=0).
 * @tparam ConfigurationTemplateType A template class, taking the type at each depth in the Container as a parameter
 *                                  This allows you to specify he type of iterator function you wish to apply at
 *                                  any level by specialising the template for that type (see \ref DefaultIteratorDepthTraits)
 *                                  This allows you to specifu how to define, begin and end iterator functions.
 *                                  It also allow you to specify a callback handler to be called each time the end of a sequence in
 *                                  that dimension is reached.
 * @tparam MaxDepth  the maximum depth the MultiIterator should go down to, and therefore the type that the iterator will return
 *                  when it is dereferenced. A value of 0 will go to the maximum depth.
 *
 */
template<class ContainerType, typename IteratorType, template <typename> class ConfigurationTemplateType, int MaxDepth=0>
struct MultiIteratorTraits {
    typedef IteratorType Iterator;
    typedef ContainerType Container;
    template<typename T> using ConfigurationTemplate = ConfigurationTemplateType<T>;
    static constexpr std::size_t depth = MaxDepth; // 0 = assume max container depth possible
};


/**
 * @class MultiIterator
 * @brief
 *    Iterator of Multiple Dimension Structures.
 * @details
 *    - At the end of each dimension a callback can be set
 *    - Automaticly detect the max number of iterable layers
 *    - Choose any depth up to this max_depth limit (default=max_depth)
 *    - panda::copy can be used to copy from these iterators efficiently
 *      @code
 *      // assume data is a type that returns a MultiIterator with begin() and end()
 *      // and that the data contained within can be converted to float
 *      auto it = data.begin();
 *      std::vector<float> destination;
 *      panda::copy(it, data.end(), std::back_inserteor(destination));
 *      @endcode
 */
template<class MultiIteratorTraits>
class MultiIterator
{
    private:
        template<typename> friend class MultiIterator;
        typedef MultiIterator<MultiIteratorTraits> SelfType;
        typedef typename MultiIteratorTraits::Container Container;
        typedef detail::ContainerInspector<Container, MultiIteratorTraits::template ConfigurationTemplate, 0> ConfigurationTraits0;
        typedef typename MultiIteratorTraits::Iterator Iterator;

    protected:
        /// maximum depth(number of iteratable layers) that the Container can support
        static constexpr std::size_t max_depth = ConfigurationTraits0::max_depth;
        static_assert(max_depth != 0, "Not a Container");

    private:
        static constexpr std::size_t depth = (MultiIteratorTraits::depth==0)?max_depth:MultiIteratorTraits::depth;
        static_assert(max_depth >= depth, "depth specified greater than the Container will support");

        typedef typename detail::FindInspector<ConfigurationTraits0, depth - 1>::type ConfigurationTraits;
        typedef detail::MultiIteratorImpl<Iterator, ConfigurationTraits::depth, ConfigurationTraits> Impl;

    public:
        typedef typename Impl::ReferenceType ReferenceType;
        typedef typename Impl::Iterator InnerIteratorType;
        typedef typename Iterator::value_type value_type;

    public:
        template<typename... Configurations>
        explicit MultiIterator(Iterator begin, Iterator end, Configurations&&... configurations);

        ~MultiIterator();

        /**
         * @brief tests operator to see if its idnetical.
         * @details prefer != for testing as it is more efficient
         *          also this can lead to confusion. A MultiIterator
         *          initialised with end iterators will not be identical
         *          to an iterator that has arrived at the end through
         *          normal propagation through the iterator unless the
         *          end() function at every level is the same as the defualt
         *          initialised Iterator for that level.
         */
        template<typename TraitsType>
        inline bool operator==(MultiIterator<TraitsType> const&) const;

        /**
         * @brief tests operator for compatability
         */
        template<typename TraitsType>
        inline bool operator!=(MultiIterator<TraitsType> const&) const;

        /**
         * @brief compare depth 0 level iterator
         */
        inline bool operator==(Iterator const&) const;

        /**
         * @brief compare depth 0 level iterator
         */
        inline bool operator!=(Iterator const&) const;

        /** @brief dereference operator - retunr the value pointed to be the current position
         */
        inline ReferenceType operator*() const;

        /** @brief pre-increment operator
         */
        SelfType& operator++();

        /** @brief post-increment operator
         */
        SelfType operator++(int);

        /** @brief return true if at end of iterator
         */
        bool end() const;

        /** @brief apply the algorithm in a piecewise fashion (one application per inner structure)
         *         across all the elements from the current iterator position to the end position passed
         */
        template<typename TraitsType, typename Algo>
        void apply_algorithm(MultiIterator<TraitsType> const& end, Algo const& algo);

    private:
        Impl _impl;
        bool _continue;
};

/**
 * @brief A MultiIterator that initialises to the end of each range
 */
template<class MultiIteratorTraits>
class EndMultiIterator : public MultiIterator<detail::EndMultiIteratorTraits<MultiIteratorTraits>>
{
    public:
        // inherit constructor
        using MultiIterator<detail::EndMultiIteratorTraits<MultiIteratorTraits>>::MultiIterator;

        operator MultiIterator<MultiIteratorTraits>&() { return reinterpret_cast<MultiIterator<MultiIteratorTraits>&>(*this); }
};

/**
 * @brief convenience function to help generate a MultiIterator
 *
 * @details
 * The simplest usecase is to use the default settings
 * which will apply std::begin and std::end at each level
 * returing the type
 * @code
 * typedef std::vector<std::vector<int>> Container;
 * Container my_object= { {1,3}, {7, 9}};
 * auto multi_iterator = make_MultiIterator<Container>(my_object.begin());
 * auto end = make_MultiIterator<Container>(my_object.end());
 * int val1 = *multi_iterator;   // val1 = 1;
 * int val2 = *++multi_iterator; // val1 = 3;
 * int val3 = *++multi_iterator; // val2 = 7;
 * int val4 = *++multi_iterator; // val4 = 9;
 * assert(++multi_iterator == end);
 * @endcode
 *
 * To return level 1 objects instead of float
 * @code
 * typedef std::vector<std::vector<int>> Container;
 * Container my_object= { {1,3}, {7, 9}};
 * auto multi_iterator = make_MultiIterator<Container, DefaultIteratorDepthTraits, 1>(my_object.begin());
 * std::vector<int>& vec = *multi_iterator;
 * ...
 * @endcode
 * @param Initialisers If your IteratorConfiguration can accept an initialiser, you can pass objects of the appropriate
 *        type as a list. The order is important, it must be from the inner container type to the outer (i.e max_depth->0)
 * @code
 * auto my_callback = []() { // do something }'
 *
 * // define custom type to install a handler
 * template<typename T>
 * struct MyDepthTraits : public DefaultIteratorDepthTraits<T> {
 * };
 *
 * template<>
 * struct MyDepthTraits<std::vector<int>> : public IteratorDepthTraits<std::vector<int>, decltype(my_callback)> {
 *      MyDepthTraits2(TestHandlerType& h) : BaseT(h) {} // requires configuration in setup
 * };
 *
 * // pass an appropriate configuration object in the make_MultiIterator method
 * auto multi_iterator = make_MultiIterator<Container, DefaultIteratorDepthTraits, 1>(my_object.begin(), MyDepthTraits<std::vector<int>>(my_callback));
 *
 * @endcode
 */
template<typename T, template <typename> class IteratorConfiguration=DefaultIteratorDepthTraits, int Depth=0, typename Iterator, typename... Initialisers>
MultiIterator<MultiIteratorTraits<T, Iterator, IteratorConfiguration, Depth>> make_MultiIterator(Iterator&& begin, Iterator&& end, Initialisers&&... inits)
{
    return MultiIterator<MultiIteratorTraits<T, Iterator, IteratorConfiguration, Depth>>(std::forward<Iterator>(begin), std::forward<Iterator>(end), std::forward<Initialisers>(inits)...);
}

/**
 * @brief convenience function to generate a MultiIterator set to the end of the range at all levels (n.b Iterator begin must always be able to be derefereneced)
 * @param begin an Iterator that can be dereferenced. std::end(ContainerType&) often does nt fulfill this criteria and will give a segfault)
 */
template<typename T, template <typename> class IteratorConfiguration=DefaultIteratorDepthTraits, int Depth=0, typename Iterator, typename... Initialisers>
MultiIterator<MultiIteratorTraits<T, Iterator, IteratorConfiguration, Depth>> make_EndMultiIterator(Iterator&& begin, Iterator&& end, Initialisers&&... inits)
{
    return EndMultiIterator<MultiIteratorTraits<T, Iterator, IteratorConfiguration, Depth>>(std::forward<Iterator>(begin), std::forward<Iterator>(end), std::forward<Initialisers>(inits)...);
}

} // namespace panda
} // namespace ska
#include "panda/detail/MultiIterator.cpp"

#endif // SKA_PANDA_MULTIITERATOR_H
