/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_PASSIVEERRORPOLICY_H
#define SKA_PANDA_PASSIVEERRORPOLICY_H

#include "panda/ErrorPolicyConcept.h"


namespace ska {
namespace panda {
class Error;

/**
 * @brief
 *    Error policy will always try and reconnect, and pass any message to the logger
 * @details
 * 
 */
class PassiveErrorPolicy
{
    BOOST_CONCEPT_ASSERT((ErrorPolicyConcept<PassiveErrorPolicy>));

    public:
        PassiveErrorPolicy();
        ~PassiveErrorPolicy();

        /**
         * @return true if connection should be made immediately
         */
        bool connection_error(Error const& error);

        /**
         * @return true if connection should be made immediately
         */
        bool error(Error const& error);

        /**
         * @return true if connection should be made immediately
         */
        bool send_error(Error const& error);

        /**
         * @return true  - always try and reconnect
         */
        bool try_connect();

        /**
         * @brief inform object of successful connect
         */
        void connected();

    private:
};

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_PASSIVEERRORPOLICY_H 
