/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_POOLLIMITS_H
#define SKA_PANDA_POOLLIMITS_H
#include "panda/TupleUtilities.h"
#include <type_traits>


namespace ska {
namespace panda {

/**
 * @brief Find the limits of devices in a ResourcePool e.g. memory
 * @details
 */
class PoolLimits
{
    public:
        PoolLimits() = delete;

        /**
         * @brief returns the smallest device memory from all available ArchT devices in the pool
         * @tparam ArchT The architecture of the devices to check
         */
        template<typename ArchT
               , typename PoolType
               , typename std::enable_if<HasType<ArchT, typename PoolType::Architectures>::value, bool>::type = true>
        static
        std::size_t minimum_memory(PoolType const& pool);

        template<typename ArchT
               , typename PoolType
               , typename std::enable_if<!HasType<ArchT, typename PoolType::Architectures>::value, bool>::type = true>
        static
        constexpr std::size_t minimum_memory(PoolType const& pool);
};

} // namespace panda
} // namespace ska
#include "detail/PoolLimits.cpp"

#endif // SKA_PANDA_POOLLIMITS_H
