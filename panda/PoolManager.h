/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_POOLMANAGER_H
#define SKA_PANDA_POOLMANAGER_H

#include "panda/ProcessingEngine.h"
#include "panda/PoolManagerConfig.h"
#include "panda/ResourcePool.h"
#include "panda/detail/Reservation.h"
#include "panda/TupleUtilities.h"
#include "panda/SystemConcept.h"
#include <map>
#include <memory>
#include <tuple>
#include <vector>

namespace ska {
namespace panda {

/**
 * @brief
 *    A factory for configured resource pools.
 *    Keeps Ownership of any created pool
 *
 * @details
 *    Will generate pools according to the PoolConfigurations provided
 *    A two stage process:
 *    1) reserve the resources required for each pool
 *    2) instantiate the pool
 *
 *    this allows pools to be set up in the config, but resources will not be allocated unless the reservation is made
 *    Any non reserved resource are added to the default pool
 */
template<typename System>
class PoolManager
{
        BOOST_CONCEPT_ASSERT((SystemConcept<System>));


    public:
        typedef typename System::SupportedArchitectures Architectures;
        typedef typename PoolTypeGen<10, Architectures>::type PoolType;
        typedef typename panda::TypeTransfer<Architectures, PoolConfig>::type PoolConfigType;

    public:
        PoolManager(System&, PoolManagerConfig<System>& config);
        PoolManager(PoolManager const&) = delete;
        ~PoolManager();

        /**
         * @brief return the default pool
         * @details
         * The default pool will contain all resources that have not been reserved prior to this call
         */      
        PoolType& default_pool();

        /**
         * @brief indicate that the specified pool will be active and resources should be reserved for it
         */
        void reserve(std::string const& pool_id);

        /**
         * @brief return the total number of devices of the specified Architecture that have been reserved
         */
        template<typename Architecture>
        std::size_t reserved_count() const;

        /**
         * @brief return the list of valid pool ids
         */
        std::vector<std::string> pool_ids() const;

        /**
         * @brief return the pool with the specified id
         */
        PoolType& pool(std::string const& pool_id) const;

        /**
         * @brief block until all queues in all pools are empty and all jobs have returned
         */
        void wait() const;

    protected:
        template<typename PoolType>
        PoolType& create_pool(std::string const& pool_id);

        // Only call after all reservations have been made
        template<typename PoolType>
        void provision(PoolType& pool, std::string const& pool_id);

        // return the enginge for the specifed pool
        ProcessingEngine& engine(std::string const& pool_id);

        // return the configuration for the specified pool
        PoolConfigType& config(std::string const& pool_id);


    private:
        System& _system;
        PoolManagerConfig<System>& _config;
//        std::map<std::string, typename detail::ReservationTuple<Architectures>::type> _reservations; // architecture params mapped by pool_id
        std::vector<std::unique_ptr<ProcessingEngine>> _engines; // keep track of created engines
        std::map<std::string, ProcessingEngine*> _engine_map;   // engine lookup
        std::unique_ptr<PoolType> _default_pool;
        std::vector<std::unique_ptr<PoolType>> _pools; // keep track of created pools
        std::map<std::string, PoolType*> _pool_map;   // pool lookup
};

} // namespace panda
} // namespace ska
#include "panda/detail/PoolManager.cpp"

#endif // SKA_PANDA_POOLMANAGER_H 
