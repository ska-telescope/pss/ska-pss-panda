/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_POOLMANAGERCONFIG_H
#define SKA_PANDA_POOLMANAGERCONFIG_H


#include "panda/ConfigModule.h"
#include "panda/PoolConfig.h"
#include "panda/TupleUtilities.h"
#include "panda/detail/Reservation.h"
#include <vector>

namespace ska {
namespace panda {

/**
 * @brief
 *    Configuration for the PoolManager class
 *
 * @details
 *    Defines a collection of pools. By default wrapped equivalently to
 * @code
 *    <pools>
 *       <pool/>
 *    </pools>
 * @endcode
 */
template<typename SystemType>
class PoolManagerConfig : public ConfigModule
{
        typedef typename SystemType::SupportedArchitectures Architectures;
        typedef typename panda::TypeTransfer<Architectures, PoolConfig>::type PoolConfigType;

    public:
        typedef std::map<std::string, typename detail::ReservationTuple<Architectures>::type> ReservationsMapType; // architecture params mapped by pool_id

    public:
        /**
         * @brief create a pools configuration block for allocating resources to pools
         * @param name This is the configuration node tag for the pools definition block. It defaults to "pools"
         * @param pool_tag This is the configuration node tag for a single pool definition block. It defaults to "pool"
         *        The pool_tag will be one layer lower than the "name" tag.
         */
        PoolManagerConfig(std::string name = std::string("pools"),
                          std::string pool_tag = std::string("pool"));
        ~PoolManagerConfig();

        void add_options(OptionsDescriptionEasyInit& opts) override;

        /**
         * @brief the ids of the defined pools
         * @details retrieve the actual config using the method @ref pool_config(id);
         */
        std::vector<std::string> pool_ids() const;

        /**
         * @brief return the configuration for the requested pool
         */
        PoolConfigType& pool_config(std::string const& pool_id);
        PoolConfigType const& pool_config(std::string const& pool_id) const;

        /**
         * @brief indicate that the specified pool will be active and resources should be reserved for it
         * @return the total number of reserved devices of all architectures
         */
        std::size_t reserve(std::string const& pool_id);

        /**
         * @brief return the total number of devices of the specified Architecture that have been reserved
         */
        template<typename Architecture>
        std::size_t reserved_count() const;

        /**
         * @brief the reservations information
         */ 
        ReservationsMapType const& reservations() const;

    private:
        ReservationsMapType _reservations; // architecture params mapped by pool_id
        std::string _pool_tag;
};

} // namespace panda
} // namespace ska
#include "panda/detail/PoolManagerConfig.cpp"

#endif // SKA_PANDA_POOLMANAGERCONFIG_H 
