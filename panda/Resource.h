/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_RESOURCE_H
#define SKA_PANDA_RESOURCE_H

#include "panda/DeviceConfig.h"
#include <thread>

namespace ska {
namespace panda {
class ResourceJob;

/**
 * @brief
 *    base class for resources managed by the ResourcePool
 */

class Resource
{
    public:
        Resource();
        virtual ~Resource() = 0;
        virtual void abort() = 0;

};


template<class Type>
class PoolResource : public Resource
{
    public:
        PoolResource();
        virtual ~PoolResource() = 0;

        template<typename TaskType>
        void run(TaskType&);

        template<typename Arch>
        void configure(DeviceConfig<Arch> const& dc);
};

} // namespace panda
} // namespace ska

#include "panda/detail/Resource.cpp"

#endif // SKA_PANDA_RESOURCE_H 
