/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_RESOURCE_JOB_H
#define SKA_PANDA_RESOURCE_JOB_H

#include "Error.h"

#include <list>
#include <mutex>
#include <string>
#include <vector>
#include <exception>
#include <condition_variable>
#include <boost/shared_ptr.hpp>
#include <boost/function.hpp>

namespace ska {
namespace panda {

/**
 * @class ResourceJob
 *
 * @brief
 *    Specifies a Job to run, its input and output data
 * @details
 *
 */

class ResourceJob
{
    public:
        enum class JobStatus { None, Queued, Running, Finished, Failed, Aborted };

    public:
        ResourceJob();
        virtual ~ResourceJob();
        ResourceJob( const ResourceJob& job ) = delete;
        const ResourceJob& operator=(const ResourceJob& job) = delete;

        inline JobStatus status() const { return _status; };
        const Error& error() const { return _error; }
        void set_error( const std::string& msg ) { _error = Error(msg); }
        void set_error( const std::exception_ptr& exception ) { _error.set(exception); }
        void emit_finished();
        void add_callback( const boost::function0<void>& fn ) { _callbacks.push_back(fn); };
        const std::vector<boost::function0<void> >& callbacks() const { return _callbacks; };

        /**
         * @brief blocking wait for job to complete
         */
        void wait() const;
        virtual void reset();

        friend class ResourceManagerImpl;
        inline void set_status( const JobStatus& status ) { _status = status; };

    private:
        Error _error;
        // status variables
        bool _processing; // true if job is not in some finished state
        mutable std::mutex _mutex;
        mutable std::condition_variable* _wait_condition;
        std::vector<boost::function0<void> > _callbacks;
        JobStatus _status;
};

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_RESOURCE_JOB_H
