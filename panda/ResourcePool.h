/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_RESOURCEPOOL_H
#define SKA_PANDA_RESOURCEPOOL_H

#include <memory>
#include <deque>
#include <tuple>

namespace ska {
namespace panda {
class Resource;
class ResourceJob;
class ProcessingEngine;
template<class Type>
class PoolResource;

namespace detail {
template<unsigned, typename...Architectures> class ResourcePoolImpl;
} // namespace detail

//template<typename ResourcePoolTraits>
/**
 *  \ingroup resource
 *  @{
 *
 * @brief
 *    Match well defined Resources in a pool to Tasks submitted
 * @details
 * @tparam MaxPriority the number of priority levels that are to be supported (1 = all tasks equal priority)
 *
 * Note that copies are allowed. Each will refer to the same underlying pool of resources. This allows setting
 * of different priority offsets etc.
 *
 */
template<unsigned MaxPriority, typename... ArchitecturesT>
class ResourcePool
{
    public:
        /// The Architectures supported by this pool
        typedef std::tuple<ArchitecturesT...> Architectures;

    public:
        ResourcePool(ProcessingEngine& engine);
        ~ResourcePool();

        /**
         * @brief set the priority level offset
         * @details only affects the priority for this interface to the pool, not the underlying pool
         */
        void set_priority(unsigned);

        /// @brief add a Resource (e.g. an NVidia card, or a Processor Core) to manage
        //  @param Arch - specify the type of resource the passed object is to be interpreted as
        template<typename Arch>
        void add_resource(std::shared_ptr<Resource> r);

        /**
         * @brief return the number of idle resources of the specified type
         * available
         */
        template<typename Arch>
        unsigned free_resources_count() const;

        /**
         * @brief return the dle devices
         * @return std::deque<std::shared_ptr<PoolResource<Arch>>
         */
        template<typename Arch>
        std::deque<std::shared_ptr<PoolResource<Arch>>> const& free_resources() const;

        /**
         * @brief submit an asyncronous task, defined by TaskTraits and the data provided
         * @tparam Priority the priority level of the task. Lower numbers indicate higher priority. Must be less than MaxPriority
         * @tparam TaskTraits support the operator()(PoolResource<Arch>&, DataType&&...)  method for each supported Architecture
         *         TaskTraits should have a typdef of either Architecture, or Architectures defined - the latter being a std::tuple<Architectures>
         * @returns shared_ptr to the submitted task information object ResourceJob
         */
        template<unsigned Priority, typename TaskTraits, typename... DataTypes>
        std::shared_ptr<ResourceJob> submit(TaskTraits& traits, DataTypes&&... types) const;

        /**
         * @brief submit an asyncronous task, defined by TaskTraits and the data provided ( same as submit<0>(...) )
         */
        template<typename TaskTraits, typename... DataTypes> inline
        std::shared_ptr<ResourceJob> submit(TaskTraits& traits, DataTypes&&... types) const;

        /**
         * @brief block until the queues are empty
         */
        void wait() const;

    private:
        mutable std::shared_ptr<detail::ResourcePoolImpl<MaxPriority, ArchitecturesT...>> _pimpl;
        unsigned _priority;
};

/**
 * @brief generattor of the ResourcePool type from a std::tuple of Architectures
 */
template<unsigned MaxPriority, typename TupleType> struct PoolTypeGen;
template<unsigned MaxPriority, typename... Types>
struct PoolTypeGen<MaxPriority, std::tuple<Types...>> {
    typedef panda::ResourcePool<MaxPriority, Types...> type;
};

/**
 *  @}
 *  End Document Group
 **/

} // namespace panda
} // namespace ska
#include "panda/detail/ResourcePool.cpp"

#endif // SKA_PANDA_RESOURCEPOOL_H
