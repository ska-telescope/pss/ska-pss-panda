/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_RESOURCEUTILITIES_H
#define SKA_PANDA_RESOURCEUTILITIES_H
#include "panda/Resource.h"
#include <type_traits>

namespace ska {
namespace panda {

/**
 * @brief
 *    Specifies how to determine if a given resource is capable enough
 *
 * @details
 *    @tparam Required Capability type required
 *    @tparam ResourceType Resource type of an instance of a resource
 *
 *    The default implementations returns false.
 */
template <class Required, class ResourceType, class Enable=void>
struct ResourceSupports {
    /// @brief Check if the given resource instance is able support the
    /// supplied type
    /// @param resource A reference to a pool resource(device) instance
    /// @returns True if the instance is able to support the requirements
    ///          specified by ResourceType
    static inline bool compatible( PoolResource<ResourceType> const& );
};

template <class ResourceType, class Enable>
struct ResourceSupports<std::true_type, ResourceType, Enable> {
    /// @brief always return true
    static inline bool compatible( PoolResource<ResourceType> const& );
};

} // namespace panda
} // namespace ska
#include "panda/detail/ResourceUtilities.cpp"

#endif // SKA_PANDA_RESOURCEUTILITIES_H
