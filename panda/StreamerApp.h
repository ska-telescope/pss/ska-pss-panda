/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_STREAMERAPP_H
#define SKA_PANDA_STREAMERAPP_H

#include "panda/ServerTraits.h"
#include "panda/TickTockTimer.h"
#include <boost/asio/deadline_timer.hpp>
#include <map>
#include <vector>
#include <memory>
#include <chrono>

namespace ska {
namespace panda {
class Engine;
class AbstractConnection;

/**
 * @brief
 *    A Server App for continuous data streaming
 *
 * @details
 * \verbatim
 *     +-------------+ next()    +---------+
 *  +->| StreamerApp | --------> | Stream  |   Stream can be any class that supports the next() method
 *  :  +-------------+           +---------+
 *  :         :                       :
 *  :         + distribute Buffer provided by next() to all subscribers
 *  :         :                       :
 *  :         :        interval()     :
 *  :         :---------------------->:
 *  :         + wait for interval (interval should return std::chrono::microseconds)
 *  :         :
 *  +---------+
 *
 * Responses to Server Messages
 * ----------------------------
 *
 * +--------+  on_connect(Session)    +-------------+
 * | Server | --------------------->  | StreamerApp |  add the client to stream subscriber list
 * +--------+                         +-------------+
 *
 * +--------+  on_error(Session, Error)   +---------------+
 * | Server | ------------------------->  | StreamerApp   | remove the client from the subscriber list
 * +--------+                             +---------------+
 *
 * \endverbatim
 *
 * Fixed Clients
 * -------------
 * In addition to the dynamiclly connecting clients managed by the server, pre-defined fixed clients can also be streamed to.
 * e.g. you may wish to send UDP packets to a fixed IP/port.
 *
 * It is expected that the Connection class handles any errors in this case according to its ErrorPolicy. Errors are ignored otherwise.
 *
 * @tparam Stream
 *    A class type that implements the following methods:
 *    next()      : return the next data chunk to send
 *    interval()  : return the target wait time between sending data chunks
 *    abort()     : exit immediateley. This should cause next() to throw panda::Abort
 *
 * \ingroup connectivity
 * @{
 */

template<class Stream, class ServerTraits = ServerTraits<Udp>>
class StreamerApp
{
    public:
        StreamerApp(Stream&, Engine& engine);
        ~StreamerApp();

        // configuration methods
        void add_fixed_client(std::shared_ptr<AbstractConnection> const& fixed_client);

        // service methods
        void on_connect(typename ServerTraits::Session const & received);
        void on_error(typename ServerTraits::Session const&, Error const&);

        /**
         * @brief start sending stream data to the clients
         */
        void start();

        /**
         * @brief stop sending stream data to the clients
         */
        void stop();

    private:
        // main function to pull data from the stream and feed it to the listening clients
        // should be called from the event loop only
        void run();

    private:
        Stream& _stream;
        Engine& _engine;
        bool _running;
        bool _in_run;
        std::map<std::string, typename ServerTraits::Session> _active_clients;
        std::vector<std::shared_ptr<AbstractConnection>> _fixed_clients;

        boost::asio::deadline_timer _timer;
        TickTockTimer<std::chrono::system_clock> _elapsed_time_timer;
};

} // namespace panda
} // namespace ska

#include "panda/detail/StreamerApp.cpp"

#endif // SKA_PANDA_STREAMERAPP_H
/**
 *  @}
 *  End Document Group
 **/
