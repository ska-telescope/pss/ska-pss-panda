/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_SYSTEMCONCEPT_H
#define SKA_PANDA_SYSTEMCONCEPT_H

#include "panda/Concept.h"
#include "panda/ConceptChecker.h"
#include "panda/ArchitectureConcept.h"



/**
 * @brief The SystemConcept class check the requirements on an System type
 *
 * @details The System concept must tuple of a type satisfying the requirements of ArchitectureConcept
 *
 * \ingroup concepts
 * @{
 */

namespace ska {
namespace panda {

namespace detail {

template<typename T>
struct is_tuple;

template<typename T>
struct is_tuple<std::tuple<T>> {
    typedef ConceptChecker<ArchitectureConcept>::Check<T> ArchitectureConceptCheck;
};

} // namespace detail

template <class X>
struct SystemConcept: Concept<X> {
        typedef Concept<X> Base;

    public:
        BOOST_CONCEPT_USAGE(SystemConcept) {
            Base::template type<detail::is_tuple<typename X::SupportedArchitectures>>::exists();
        }
};

} // namespace panda
} // namespace ska

/**
 *  @}
 *  End Document Group
 **/

#endif // SKA_PANDA_SYSTEMCONCEPT_H
