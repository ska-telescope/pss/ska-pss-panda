include(subpackage)

subpackage(nvidia)
subpackage(altera)
subpackage(intel_agilex_fpga)

set(LIB_ARCH_SRC
    ${LIB_ARCH_NVIDIA}
    ${LIB_ARCH_ALTERA}
    ${LIB_ARCH_INTEL_FPGA}
    PARENT_SCOPE
)
