/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_ARCH_ALTERA_DEVICEALLOCATOR_H
#define SKA_PANDA_ARCH_ALTERA_DEVICEALLOCATOR_H

#include "panda/arch/altera/Fpga.h"
#include "panda/arch/altera/CommandQueue.h"
#include "panda/arch/altera/Architecture.h"
#include "panda/arch/altera/DevicePointer.h"
#include "panda/DeviceAllocator.h"
#include <map>

namespace ska {
namespace panda {

/**
 * @brief specialisation of device allocator for FPGA openCL devices
 * @details for use exclusively on the Cpu host code
 *          n.b. only memory is allocated, construction and destruction
 *               of objects is disabled.
 */
template<typename T>
class DeviceAllocator<T, altera::OpenCl>
{

    public:
        typedef altera::OpenCl Architecture;
        typedef T  value_type;
        typedef typename altera::DevicePointer<T>::Iterator pointer;
        typedef typename altera::DevicePointer<T>::ConstIterator const_pointer;

    public:
        DeviceAllocator(altera::Fpga& device, altera::CommandQueue& queue);

        DeviceAllocator(altera::Fpga& device);

        template<typename U>
        DeviceAllocator(DeviceAllocator<U, altera::OpenCl> const&);

        pointer allocate(std::size_t n);
        void deallocate( pointer const& p, std::size_t n);
        void deallocate( altera::DevicePointer<T> const& p);

        template<class Ptr, class... Args>
        void construct(Ptr*, Args&&...) {} // does nothing as objects are not on host

        void destroy( pointer ) {} // do nothing

        bool operator==(DeviceAllocator const&) const;

    private:
        template<typename, typename> friend class DeviceAllocator;

    private:
        std::map<const void*, altera::DevicePointer<T>> _map;
        altera::Fpga& _device;
        altera::CommandQueue& _queue;
};

} // namespace panda
} // namespace ska
#include "detail/DeviceAllocator.cpp"

#endif // SKA_PANDA_ARCH_ALTERA_DEVICEALLOCATOR_H
