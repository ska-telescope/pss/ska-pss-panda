/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_OPENCLERROR_H
#define SKA_PANDA_OPENCLERROR_H

#include "panda/arch/altera/OpenCl.h"
#include <string>

namespace ska {
namespace panda {
namespace altera {

/**
 * @brief info about OpenCl specific errors
 *
 */
class OpenClError
{
    public:
        OpenClError(cl_int opencl_error_code);

        /**
         * @brief true if an error condition is reported
         * @code
         * OpenClError status = clSomeCommand();
         * if(status) {
         *    // we have an error
         * }
         * @endcode
         */
        operator bool() const;

        /**
         * @brief return a message associated with the error code
         */
        std::string what() const;

        /**
         * @brief The error message assoicated with an opencl error return value
         */
        static const char* error_msg(cl_int error);

        /**
         * @brief return true if status matches the provided error code
         */
        bool operator==(cl_int opencl_error_code) const;

    private:
        cl_int _error_code;
};

} // namespace altera
} // namespace panda
} // namespace ska

#endif // SKA_PANDA_OPENCLERROR_H
