/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <exception>
#include <panda/Log.h>

namespace ska {
namespace panda {

template<typename T>
DeviceAllocator<T, altera::OpenCl>::DeviceAllocator(altera::Fpga& device, altera::CommandQueue& queue)
    : _device(device)
    , _queue(queue)
{
}

template<typename T>
DeviceAllocator<T, altera::OpenCl>::DeviceAllocator(altera::Fpga& device)
    : DeviceAllocator(device, device.default_queue())
{
}

template<typename T>
template<typename U>
DeviceAllocator<T, altera::OpenCl>::DeviceAllocator(DeviceAllocator<U, altera::OpenCl> const& other)
    : _device(other._device)
    , _queue(other._queue)
{
}

template<typename T>
typename DeviceAllocator<T, altera::OpenCl>::pointer DeviceAllocator<T, altera::OpenCl>::allocate(std::size_t size)
{
#ifdef SKA_PANDA_ENABLE_OPENCL
    auto ptr = altera::DevicePointer<T>(_device, size, _queue);
    auto it = _map.insert({ptr.tag(), std::move(ptr)});
    return it.first->second.begin();
#else
    throw std::bad_alloc();
#endif // SKA_PANDA_ENABLE_OPENCL
}

template<typename T>
void DeviceAllocator<T, altera::OpenCl>::deallocate(pointer const& ptr, std::size_t)
{
    deallocate(*(ptr._dev_ptr));
}

template<typename T>
void DeviceAllocator<T, altera::OpenCl>::deallocate(altera::DevicePointer<T> const& ptr)
{
    _map.erase(ptr.tag());
}

template<typename T>
bool DeviceAllocator<T, altera::OpenCl>::operator==(DeviceAllocator<T, altera::OpenCl> const& other) const
{
    return _device.device_id() == other._device.device_id();
}

} // namespace panda
} // namespace ska
