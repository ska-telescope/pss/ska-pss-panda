/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

namespace ska {
namespace panda {

template<typename DataType, typename Allocator>
template<typename... AllocatorArgs>
DeviceMemory<altera::OpenCl, DataType, Allocator>::DeviceMemory(std::size_t size, AllocatorArgs&&... args)
    : _allocator(std::forward<AllocatorArgs>(args)...)
    , _d_ptr(_allocator.allocate(size)._dev_ptr)
{
}

template<typename DataType, typename Allocator>
DeviceMemory<altera::OpenCl, DataType, Allocator>::DeviceMemory(DeviceMemory&& other)
    : _allocator(std::move(other._allocator))
    , _d_ptr(other._d_ptr)
{
}

template<typename DataType, typename Allocator>
DeviceMemory<altera::OpenCl, DataType, Allocator>::~DeviceMemory()
{
    _allocator.deallocate(*_d_ptr);
}

template<typename DataType, typename Allocator>
void DeviceMemory<altera::OpenCl, DataType, Allocator>::reset(std::size_t size)
{
    _allocator.deallocate(*_d_ptr);
    _d_ptr=_allocator.allocate(size)._dev_ptr;
}

template<typename DataType, typename Allocator>
std::size_t const& DeviceMemory<altera::OpenCl, DataType, Allocator>::size() const
{
    return _d_ptr->size();
}

template<typename DataType, typename Allocator>
typename DeviceMemory<altera::OpenCl, DataType, Allocator>::Iterator DeviceMemory<altera::OpenCl, DataType, Allocator>::begin()
{
    return _d_ptr->begin();
}

template<typename DataType, typename Allocator>
typename DeviceMemory<altera::OpenCl, DataType, Allocator>::ConstIterator DeviceMemory<altera::OpenCl, DataType, Allocator>::begin() const
{
    return _d_ptr->cbegin();
}

template<typename DataType, typename Allocator>
typename DeviceMemory<altera::OpenCl, DataType, Allocator>::ConstIterator DeviceMemory<altera::OpenCl, DataType, Allocator>::cbegin() const
{
    return _d_ptr->cbegin();
}

template<typename DataType, typename Allocator>
typename DeviceMemory<altera::OpenCl, DataType, Allocator>::Iterator DeviceMemory<altera::OpenCl, DataType, Allocator>::end()
{
    return _d_ptr->end();
}

template<typename DataType, typename Allocator>
typename DeviceMemory<altera::OpenCl, DataType, Allocator>::ConstIterator DeviceMemory<altera::OpenCl, DataType, Allocator>::end() const
{
    return _d_ptr->cend();
}

template<typename DataType, typename Allocator>
typename DeviceMemory<altera::OpenCl, DataType, Allocator>::ConstIterator DeviceMemory<altera::OpenCl, DataType, Allocator>::cend() const
{
    return _d_ptr->cend();
}

} // namespace panda
} // namespace ska
