/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/arch/altera/Kernel.h"
#include "panda/arch/altera/Aocx.h"
#include "panda/Log.h"
#include "panda/Error.h"

#include <algorithm>

namespace ska {
namespace panda {
namespace altera {

namespace{
struct OpenClArgumentHelper
{
    template<typename KernelT, typename Arg1, typename Arg2, typename... Args>
    static void inline exec(std::size_t arg_num, KernelT& kernel, Arg1&& arg, Arg2&& arg2, Args&&... args)
    {
        exec(arg_num, kernel, arg);
        exec(++arg_num, kernel, std::forward<Arg2>(arg2), std::forward<Args>(args)...);
    }

    template<typename KernelT, typename Arg1>
    static void inline exec(std::size_t arg_num, KernelT& kernel, Arg1&& arg)
    {
        cl_int error;
        error = clSetKernelArg(kernel, arg_num, sizeof(arg), (void*)&arg);
        if (error != CL_SUCCESS) {
            panda::Error e("failed to set kernel argument: ");
            e << arg;
            throw e;
        }
    }

};
} // namespace

//class Kernel
template<typename... Args>
void Kernel::operator()(CommandQueue& queue, size_t n_local, size_t n_global, Args&&... args)
{
    operator()(queue, {n_local}, {n_global}, std::forward<Args>(args)...);
}

template<typename... Args>
void Kernel::operator()(CommandQueue& queue, std::initializer_list<size_t> local_work_size, std::initializer_list<size_t> global_work_size, Args&&... args)
{
    //set the kernel arguments
    OpenClArgumentHelper::exec(0U,_kernel, std::forward<Args>(args)...);

    //Launch the kernel
    unsigned work_dim = local_work_size.size();
    if (work_dim < 1 || work_dim > 3 || work_dim != global_work_size.size()) {
        panda::Error e("invalid work dimension: ");
        e << work_dim;
        throw e;
    }

    size_t local_ws[3]; //set size of local work-items/threads in a work group
    std::copy(local_work_size.begin(), local_work_size.end(), local_ws);

    size_t global_ws[3]; //set size of global work-items/ total number of threads
    std::copy(global_work_size.begin(), global_work_size.end(), global_ws);

    cl_int status;
    status=clEnqueueNDRangeKernel(queue.queue(), _kernel, work_dim, NULL, global_ws, local_ws, 0, NULL, NULL);

    if (status != CL_SUCCESS) {
        panda::Error e("failed to launch kernel: ");
        e << _kernel_name;
        throw e;
    }
    PANDA_LOG_DEBUG << "Kernel LAUNCHED " << status;
}

} // namespace altera
} // namespace panda
} // namespace ska
