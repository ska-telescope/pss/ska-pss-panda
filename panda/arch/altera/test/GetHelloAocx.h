/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2021 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_TEST_GETHELLOAOCX_H
#define SKA_PANDA_TEST_GETHELLOAOCX_H

#include "panda/arch/altera/Fpga.h"
#include <boost/filesystem/path.hpp>

#include <panda/test/gtest.h>


/**
 * @brief search for thr aocx hello_world kernel that matches a suitable device
 * @return retunr an empty filesystem::path if there is no matching aocx file
 *          otherwisea the full path to the image file
 * @detals if not found will print to std::cout a message reporting that the image is not found
 */
inline
boost::filesystem::path get_hello_word_aocx(ska::panda::altera::Fpga const& fpga){
    std::string driver_str = fpga.driver_version();
    std::string device_name = fpga.device_name();
    boost::filesystem::path filename("data");
    filename /= "aocx"; filename /= driver_str; filename /= "hello_world_" + device_name + ".aocx";
    boost::filesystem::path p{ska::panda::test::test_file(filename.string(), false)};
    if (p.empty())
        std::cout << "Binary image " << filename << " is not available for the driver " + driver_str + "\n";
    return p;
}

#endif // SKA_PANDA_TEST_GETHELLOAOCX_H
