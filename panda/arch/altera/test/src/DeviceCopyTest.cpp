/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "DeviceCopyTest.h"
#include "panda/arch/altera/test/TestDevice.h"
#include "panda/arch/altera/Altera.h"
#include "panda/arch/altera/DeviceCopy.h"
#include "panda/System.h"
#include "panda/Copy.h"
#include <vector>
#include <list>

namespace ska {
namespace panda {
namespace altera {
namespace test {


DeviceCopyTest::DeviceCopyTest()
    : ::testing::Test()
{
}

DeviceCopyTest::~DeviceCopyTest()
{
}

void DeviceCopyTest::SetUp()
{
}

void DeviceCopyTest::TearDown()
{
}

TEST_F(DeviceCopyTest, test_copy_to_DevicePointer_random_access)
{
    std::vector<double> const input { 0, 1, 2 , 3, 4 };

    panda::System<typename ArchitectureType<altera::DevicePointer<double>>::type> system;

    if (system.system_count<typename ArchitectureType<altera::DevicePointer<double>>::type>()) {
//        for (auto & device : (system.resources<typename ArchitectureType<altera::DevicePointer<double>>::type>())) {
            altera::CommandQueue queue (*system.resources<typename ArchitectureType<altera::DevicePointer<double>>::type>()[0]);
            panda::altera::DevicePointer<double> altera_ptr(*system.resources<typename ArchitectureType<altera::DevicePointer<double>>::type>()[0], input.size(),queue);
            panda::copy(input.begin(), input.end(), altera_ptr.begin());

            std::vector<double> returned_data(input.size(), 0);
            panda::copy(++altera_ptr.begin(), altera_ptr.end(), returned_data.begin());
            for (unsigned i = 0; i < input.size() - 1; ++i) {
                ASSERT_EQ(input[i+1], returned_data[i]);
            }
            ASSERT_EQ(0, returned_data.back());
//        }
    } else {
        FAIL() << "no device found to run test";
    }
}

TEST_F(DeviceCopyTest, test_copy_to_DevicePointer_non_random_access)
{
    std::list<double> const input = { 0, 1, 2 , 3, 4 };
    TestDevice system;
    for(auto & device : system.devices()) {
        device->run([&](typename TestDevice::DeviceRefType&)
        {
            altera::CommandQueue queue (*device);
            panda::altera::DevicePointer<double> altera_ptr(*device, input.size(), queue);
            panda::copy(input.begin(), input.end(), altera_ptr.begin());
            // copy back from the device to a new vector
            std::vector<double> returned_data(input.size());
            panda::copy(altera_ptr.begin(), altera_ptr.end(), returned_data.begin());
            //altera_ptr.read(returned_data.begin());
            auto list_it = input.begin();
            auto vec_it = returned_data.begin();
            while(list_it != input.end()) {
                ASSERT_EQ(*list_it, *vec_it);
                ++list_it;
                ++vec_it;
            }
        });
    }
}

//TEST_F(DeviceCopyTest, test_device_copy_auto_alloc_random_access)
//{
//    std::vector<double> const input = { 0, 1, 2 , 3, 4 };
//    TestDevice system;
//    for(auto & device : system.devices()) {
//        device->run([&](typename TestDevice::DeviceRefType&)
//        {
//            auto altera_ptr = panda::altera::device_copy(input.begin(), input.end());
//            std::vector<double> returned_data(input.size());
//            altera_ptr.read(returned_data.begin());
//            for(unsigned i=0; i<input.size(); ++i) {
//                ASSERT_EQ(input[i], returned_data[i]);
//            }
//        });
//    }
//}

//TEST_F(DeviceCopyTest, test_device_copy_auto_alloc_non_random_access)
//{
//    std::list<double> const input = { 0, 1, 2 , 3, 4 };
//    TestDevice system;
//    for(auto & device : system.devices()) {
//        device->run([&](typename TestDevice::DeviceRefType&)
//        {
//            auto altera_ptr = panda::altera::device_copy(input.begin(), input.end());
//            std::vector<double> returned_data(input.size());
//            altera_ptr.read(returned_data.begin());
//            auto list_it = input.begin();
//            auto vec_it = returned_data.begin();
//            while(list_it != input.end()) {
//                ASSERT_EQ(*list_it, *vec_it);
//                ++list_it;
//                ++vec_it;
//            }
//        });
//    }
//}
} // namespace test
} // namespace altera

//namespace test {
//    // standard panda::copy test suite for Altera Device Pointers
//    struct AlteraCopyTraits {
//        typedef altera::OpenCl Arch;
//        template<typename T>
//        static altera::DevicePointer<T> contiguous_device_memory(std::size_t size) {
//            return altera::DevicePointer<T>(size);
//        }
//    };
//    INSTANTIATE_TYPED_TEST_SUITE_P(OpenCl, CopyTester, AlteraCopyTraits);
//
//} // namespace test
} // namespace panda
} // namespace ska
