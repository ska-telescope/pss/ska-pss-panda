/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "KernelTest.h"
#include "panda/arch/altera/test/GetHelloAocx.h"
#include "panda/arch/altera/Altera.h"
#include "panda/arch/altera/Kernel.h"
#include "panda/arch/altera/CommandQueue.h"
#include "panda/System.h"
#include "panda/test/gtest.h"

#include <boost/filesystem/path.hpp>
#include <string>


namespace ska {
namespace panda {
namespace test {


KernelTest::KernelTest()
    : BaseT()
{
}

KernelTest::~KernelTest()
{
}

void KernelTest::SetUp()
{
}

void KernelTest::TearDown()
{
}

TEST_F(KernelTest, test_kernel_hello)
{
    typedef altera::OpenCl Arch;
    panda::System<Arch> system;
    ASSERT_GT(system.count<Arch>(), std::size_t(0)) << "No devices found to test";
    for (auto const& device : system.resources<Arch>())
    {
        boost::filesystem::path p = get_hello_word_aocx(*device);
        if(p.empty()) {
            std::cout << "skipping test for device " << device->device_name() << "\n";
            continue;
        }
        ska::panda::altera::Aocx aocx(p);
        ska::panda::altera::Program program(aocx, *device);
        ska::panda::altera::CommandQueue command_queue(*device);
        std::string kernel_name = "hello_world";
        ska::panda::altera::Kernel kernel(kernel_name, program);
        cl_int thread_id_to_output = 0;
        for (int i = 0; i < 3; i++) {
            if (i % 2 == 0)
                thread_id_to_output = 0;
            else
                thread_id_to_output = 1;
            kernel(command_queue, 1, 1, thread_id_to_output); // 1,1 arguments for single work-item/thread
        }
    }
}

TEST_F(KernelTest, test_kernel_hello_move)
{
    typedef altera::OpenCl Arch;
    panda::System<Arch> system;
    ASSERT_GT(system.count<Arch>(), std::size_t(0)) << "No devices found to test";
    for (auto const& device : system.resources<Arch>())
    {
        boost::filesystem::path p = get_hello_word_aocx(*device);
        if(p.empty()) {
            std::cout << "skipping test for device " << device->device_name() << "\n";
            continue;
        }
        ska::panda::altera::Aocx aocx(p);
        ska::panda::altera::Program program(aocx,*device);
        ska::panda::altera::CommandQueue command_queue(*device);
        std::string kernel_name = "hello_world";
        std::unique_ptr<altera::Kernel> kernel_move;
        cl_int thread_id_to_output = 0;
        {
            ska::panda::altera::Kernel kernel(kernel_name, program);
            kernel_move.reset(new ska::panda::altera::Kernel(std::move(kernel)));
        }
        for (int i = 0; i < 3; i++) {
            if (i % 2 == 0)
                thread_id_to_output = 0; //print hello_world when i is even
            else
                thread_id_to_output = 1;
            (*kernel_move)(command_queue, 1, 1, thread_id_to_output); //1,1 arguments for single work-item
        }
    }
}

} // namespace test
} // namespace panda
} // namespace ska
