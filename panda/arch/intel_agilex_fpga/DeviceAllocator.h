/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *½
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_ARCH_INTEL_AGILEX_FPGA_DEVICEALLOCATOR_H
#define SKA_PANDA_ARCH_INTEL_AGILEX_FPGA_DEVICEALLOCATOR_H

#include "panda/arch/intel_agilex_fpga/Fpga.h"
#include "panda/arch/intel_agilex_fpga/Architecture.h"
#include "panda/DeviceAllocator.h"
#include "panda/arch/intel_agilex_fpga/DevicePointer.h"

namespace ska {
namespace panda {

/**
 * @brief   Specialisation of Host allocator for HugePages
 * @details For use exclusively on the Cpu host code.
 *          N.B. only memory is allocated, construction and destruction
 *               of objects is disabled.
 */
template<typename T>
class DeviceAllocator<T, intel_fpga::Rtl> : public std::allocator<T>
{
    public:
        template <typename OtherT, typename Arch>
        struct rebind
        {
            typedef DeviceAllocator<OtherT, Arch> other;
        };

        typedef intel_fpga::Rtl Architecture;
        typedef T  value_type;
        typedef T* pointer;

    public:
        DeviceAllocator() {} // TODO remove
        DeviceAllocator(Device<intel_fpga::Rtl> const& device);

        template<typename U>
        DeviceAllocator(DeviceAllocator<U, intel_fpga::Rtl> const&);

        T* allocate(std::size_t n);
        void deallocate( T* p, std::size_t n);

        template<class Ptr, class... Args>
        void construct(Ptr*, Args&&...) {} // does nothing as objects are not on host

        void destroy( pointer ) {} // do nothing

        bool operator==(DeviceAllocator const&) const { return true; }
};

} // namespace panda
} // namespace ska

#include "detail/DeviceAllocator.cpp"

#endif // SKA_PANDA_ARCH_INTEL_AGILEX_FPGA_DEVICEALLOCATOR_H
