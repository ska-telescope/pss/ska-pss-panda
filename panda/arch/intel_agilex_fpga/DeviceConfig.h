/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_ARCH_INTEL_AGILEX_FPGA_DEVICECONFIG_H
#define SKA_PANDA_ARCH_INTEL_AGILEX_FPGA_DEVICECONFIG_H

#include "panda/DeviceConfig.h"
#include "panda/arch/intel_agilex_fpga/PoolResource.h"

namespace ska {
namespace panda {

/**
 * @brief Device Configuration option descriptions for insertion into pool
 */

template<>
struct DeviceConfig<intel_fpga::Rtl> : public DeviceConfigBase<DeviceConfig<intel_fpga::Rtl>>
{
    public:
        typedef DeviceConfigBase<DeviceConfig<intel_fpga::Rtl>> BaseT;
        typedef intel_fpga::Rtl IntelFpga;

    public:
        DeviceConfig();
        DeviceConfig(DeviceConfig const&);
        DeviceConfig& operator=(DeviceConfig const&);

        /**
         * @param module_name the configuration option name (xml tag) for the module node
         */
        DeviceConfig(std::string module_name);

        virtual ~DeviceConfig();
        static std::string const& tag_name();

        /**
         * @brief sets the BDF
         * coefficients
         */
        void pci_bus_device(std::string const&);

        /**
         * @brief gets the BDF function
         */
        std::string const& pci_bus_device() const;

        /**
         * @brief Gets the value of the variable that represents the buffer
         *        size in bytes used in the configuration of the host memory
         *        space to be used in host DMA transfer to the fpga.
         */
        std::uint32_t const& buffer_transfer_size() const;

        /**
         * @brief Sets a variable with the size of the buffer in bytes that will be used
         *        to configure the space in host memory that will be required
         *        for DMA transfer into the fpga.
         */
        void buffer_transfer_size(std::uint32_t const&);

        /**
         * @brief set the filename that contains configuration of fdas filter
         * coefficients
         */
        void filters_filename(std::string const&);

        /**
         * @brief gets the configured filename with fdas filter information
         */
        std::string const& filters_filename() const;

        /**
         * @brief sets filename with the hsum configuration
         */
        void hsum_filename(std::string const&);

        /**
         * @brief gets filename with the fdas fpga hsum configuration
         */
        std::string const& hsum_filename() const;

        /**
         * @brief sets the number of filters into the fdas fpga
         */
        void number_of_filters(std::uint32_t const&);

        /**
         * @brief gets the number of filter in the fdas fpga
         */
        std::uint32_t const& number_of_filters() const;

        /**
         * @brief time duration of the de-dispersed time domain series
         * @details it is given by the multiplication of the number of samples
         *          in the time series and the sampling rate.
         */
        std::uint32_t const& duration() const;
        void duration(std::uint32_t const& value);


    protected:
        void add_options(OptionsDescriptionEasyInit& ) override;

    private:
        /// @brief pcie bus address of the fpga
        std::string _pcie_bus_device;
        /// @brief size in bytes of the input data to be DMA transferred to FPGA
        std::uint32_t _buffer_transfer_size;
        /// @brief DMA payload size in bytes and must be 64 byte aligned.
        std::uint32_t _packet_size;
        /// @brief defines the number of samples for the CLD and CONV modules to process to generate the FOP.
        std::uint64_t _fop_size;
        /// @brief overlap size is related to the length of the convolution filters
        std::uint64_t _overlap_size;
        /// @brief defines the number of times the CONV_IFFT sub-modules are actually looped through.
        std::uint32_t _loop_num;
        std::uint32_t _number_of_filters;
        std::string _filters_filename;
        std::string _hsum_filename;
        /// @brief time in seconds of observation duration
        std::uint32_t _duration;

};

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_ARCH_INTEL_AGILEX_FPGA_DEVICECONFIG_H
