/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_ARCH_INTEL_AGILEX_FPGA_DEVICECOPY_H
#define SKA_PANDA_ARCH_INTEL_AGILEX_FPGA_DEVICECOPY_H

#include "panda/arch/intel_agilex_fpga/Architecture.h"
#include "panda/arch/intel_agilex_fpga/PoolResource.h"
#include "panda/DeviceCopy.h"

namespace ska {
namespace panda {

/**
 * @brief extends panda::copy for IntelFpga devices
 */
template<typename SrcIteratorTypeTag, typename EndIteratorTypeTag, typename DstIteratorTypeTag>
struct DeviceCopy<Cpu, intel_fpga::Rtl, SrcIteratorTypeTag, EndIteratorTypeTag, DstIteratorTypeTag>
{
    public:
        /**
         * @brief perform the copy from host to Intel fpga architectures
         */
        template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
        static inline typename std::remove_reference_t<DstIteratorType>
        copy(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it);

};

template<typename SrcIteratorTypeTag, typename EndIteratorTypeTag, typename DstIteratorTypeTag>
struct DeviceCopy<intel_fpga::Rtl, Cpu, SrcIteratorTypeTag, EndIteratorTypeTag, DstIteratorTypeTag>
{
    public:
        /**
         * @brief perform the copy from Intel fpga architectures to host
         */
        template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
        static inline typename std::remove_reference_t<DstIteratorType>
        copy(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it);

};

} // namespace panda
} // namespace ska
#include "panda/arch/intel_agilex_fpga/detail/DeviceCopy.cpp"

#endif // SKA_PANDA_ARCH_INTEL_AGILEX_FPGA_DEVICECOPY_H
