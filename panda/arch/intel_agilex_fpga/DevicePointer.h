/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_ARCH_INTEL_AGILEX_FPGA_DEVICEPOINTER_H
#define SKA_PANDA_ARCH_INTEL_AGILEX_FPGA_DEVICEPOINTER_H

#include "panda/arch/intel_agilex_fpga/DeviceIterator.h"
#include "panda/arch/intel_agilex_fpga/Architecture.h"
#include "panda/arch/intel_agilex_fpga/Fpga.h"
#include "panda/arch/intel_agilex_fpga/HugePage.h"

namespace ska {
namespace panda {
namespace intel_fpga {

class Fpga;

/**
 * @brief
 *     manage a Intel FPGA pointer
 * @details
 *     The class assumes that the huge memory area is part of the device
 *     (Intel Fpga). An instance of the DevicePointer class will then be
 *     an object that provides a set of iterators for the huge page memory
 *     and a method to read and write to the memory location.
 *
 *     This assumption might not be correct because the huge page is part
 *     of the host, which is used for DMA transfer to the device. As a
 *     future enhancement, this class would rather operate on the internal
 *     RAM memory of the Intel FPGA than the huge page on the host.
 *
 */
template<typename DataType>
class DevicePointer
{
    public:
        typedef panda::DeviceIterator<Rtl, DataType> Iterator;
        typedef panda::DeviceIterator<Rtl, const DataType> ConstIterator;

    public:
        DevicePointer(std::size_t size);
        DevicePointer(DevicePointer const&) = delete;
        DevicePointer(DevicePointer&&);
        ~DevicePointer();
        DevicePointer& operator=(DevicePointer&&);

        /**
         * @brief return the size of the memory allocated (in DataType)
         */
        std::size_t const& size() const;

        /**
         * @brief return a (Intel fpga device) pointer to the first element
         */
        Iterator begin();

        /**
         * @brief return a (Intel fpga device) pointer to the last element + 1
         */
        Iterator end();

        /**
         * @brief return a (Intel fpga device) pointer to the first element
         */
        ConstIterator begin() const;

        /**
         * @brief return a (Intel fpga device) pointer to the last element + 1
         */
        ConstIterator end() const;

        /**
         * @brief return a (Intel fpga device) pointer to the first element
         */
        ConstIterator cbegin() const;

        /**
         * @brief return a (Intel fpga device) pointer to the last element + 1
         */
        ConstIterator cend() const;

        /**
         * @brief return the context of a device
         */
        Fpga const& device() const;

        /**
         * @brief write data from a buffer to the hugepage
         */
        void* hugepage_write(std::size_t const& offset, DataType const& data, std::size_t const& size) const;

        /**
         * @brief read data from a hugepage to a buffer
         */
        void hugepage_read(std::size_t const& offset, DataType& data, std::size_t const& size) const;

        /**
         * @brief get the device pointer
         */
        DataType* get_device_ptr() const;

        std::unique_ptr<HugePage> get_page() const;

    private:
        DataType* _d_ptr;
        std::size_t _size;
        std::unique_ptr<HugePage> _page;
        Fpga const* _device;
};

} // namespace intel_fpga

// template to determining arch types
template<typename... T>
struct ArchitectureType<intel_fpga::DevicePointer<T...>>
{
    typedef intel_fpga::Rtl type;
};

} // namespace panda
} // namespace ska
#include "panda/arch/intel_agilex_fpga/detail/DevicePointer.cpp"

#endif // SKA_PANDA_ARCH_INTEL_AGILEX_FPGA_DEVICEPOINTER_H
