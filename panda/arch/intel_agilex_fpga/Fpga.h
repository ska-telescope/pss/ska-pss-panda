/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_ARCH_INTEL_AGILEX_FPGA_FPGA_H
#define SKA_PANDA_ARCH_INTEL_AGILEX_FPGA_FPGA_H

#include "panda/Configuration.h"
#include "panda/Resource.h"

#ifdef SKA_PANDA_ENABLE_INTEL_FPGA

#include "FpgaAdapter.h"
#include <memory>


namespace ska {
namespace panda {
namespace intel_fpga {


/**
 * @brief
 *    An Intel FPGA card resource
 *
 */
class Fpga : public Resource
{
    public:
        Fpga(std::shared_ptr<FpgaAdapter> const&, unsigned int);
        Fpga(Fpga const&) = delete;
        Fpga(Fpga&&) = default;
        ~Fpga();

        template<typename TaskType>
        void run( TaskType& job );

        void abort() {}; //TODO

        /**
         * @brief The Intel FPGA device_id
         */
        unsigned device_id() const;

        /**
         * @brief The BDF (Bus:Device:Function) identifier that describes
         * the PCIe bus where FPGA is located.
        */
        std::string bdf_id() const;

       /**
        * @brief The FDAS version given by the product id
        */
        std::int32_t product_id() const;

       /**
        * @brief Initialise internal fpga registers to run FDAS
        *
        * @details It resets the fpga DDRs and registers used by the FDAS,
        *          then sets the filter coefficients and harmonic sum configuration
        *          registers.
        * @return zero on success or negative otherwise
        */
       std::int32_t initialise() const;

        /**
         * @brief run the fdas algorithm in the fpga
         */
        std::int32_t const& run_fdas();

        /**
        * @brief configure virtual space adress of hugepage
        */
        void hugepage(void *);

        /**
         * @brief return a lists of candidates in a vector of vector
         */
        std::vector<std::vector<std::int32_t>> const& candidates();

        /**
         * @brief loads into the fpga board a particular configuration to run Fdas.
         *
         * @details It takes all the variables that have been previously prepared and
         *          assigns them to the fpga board through the setters implemented in
         *          the fpga adapter class.
         */
        void load_fdas_configuration();

    protected:
        /// key variables required to run fdas algorithm on the fpga
        /// @brief file with the fdas filters coefficients
        mutable std::string _fdas_filter_filename;
        /// @brief file with the harmonic sum configuration of the
        mutable std::string _fdas_hsum_filename;
        /// @brief total number of fdas filters configured on the fpga board
        mutable std::uint32_t _fdas_filters_number;
        /// @brief size in bytes of the data to be DMA transferred to FPGA
        std::uint32_t _buffer_transfer_size;
        /// @brief observation time duration
        std::uint32_t _duration;

    private:
        std::shared_ptr<FpgaAdapter> _board;
        unsigned int _device_id;

};

} // namespace intel_fpga
} // namespace panda
} // namespace ska
#endif // SKA_PANDA_ENABLE_INTEL_FPGA

#endif // SKA_PANDA_ARCH_INTEL_AGILEX_FPGA_FPGA_H
