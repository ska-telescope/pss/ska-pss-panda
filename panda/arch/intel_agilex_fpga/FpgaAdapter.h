/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_ARCH_INTEL_AGILEX_FPGA_FPGAADAPTER_H
#define SKA_PANDA_ARCH_INTEL_AGILEX_FPGA_FPGAADAPTER_H

#include "panda/Configuration.h"

#ifdef SKA_PANDA_ENABLE_INTEL_FPGA

#include <memory>
#include <filesystem> // to access /dev/hugepage filesystem
#include <regex>      // get hugepage files

/// including data structs and functions from MQDMA and FDAS C code.
#include "MqdmaUtils.h"


namespace ska {
namespace panda {
namespace intel_fpga {

/**
 * @brief
 *   Interface to operate with the Intel Agilex Fpga board.
 *
 * @details
 *   Provides methods for initialising and executing the FDAS algorithm.
 *   The operation of fpga is based on the Intel MQDMA API and C functions
 *   that use the Intel API functions. The initialization allocates memory
 *   to some C structs that prepare the DMA channels to send and receive
 *   data over the PCIe bus.
 *
 *   ifc_qdma_queue: is the structure that holds the descriptors of the
 *   allocated buffers in the hugepage memory.
 *   ifc_qdma_device: holds the PCIe device board information. It also
 *   contains the QCSR (Queue Control and Status Register) and GCSR
 *   (General Control and Status Registers) located in PCIe BAR 1 and 2.
 *   ifc_qdma_channel: is the structure that stores the information about
 *   the DMA channels created. Each DMA channel is composed of one TX/RX channel.
 *   queue_ctx: global context struct containing all structures.
 */



/// Custom deleter that uses free to deallocate memory allocated with malloc
template<typename T>
struct CStructDeleter
{
    void operator()(T* ptr)
    {
        free(ptr);
    }
};

// specialised deleter for queue dma I/O buffers
template<>
struct CStructDeleter<ifc_qdma_request*>
{
    uint32_t size;
    CStructDeleter() : size(0) {}
    CStructDeleter(uint32_t size) : size(size) {}
    void operator()(ifc_qdma_request** ptr)
    {
        for (uint32_t i=0; i<size; ++i)
        {
            ifc_request_free(ptr[i]);
        }
        free(ptr); // Use free to deallocate memory allocated with malloc

    }
};

// check if the filename matches the pattern
struct hugepage_filesystem_utils
{
    static bool filename_exist(const std::filesystem::path& filename
        , const std::regex& pattern
        )
    {
        return std::regex_match(filename.filename().string(), pattern);
    }

    static void filelist(const std::string& directory
        , const std::regex& pattern
        , std::vector<std::string> & hugepage_files )
    {
        if (std::filesystem::is_directory(directory))
        {
            for(const auto& entry : std::filesystem::directory_iterator(directory) )
            {
                if(std::filesystem::is_regular_file(entry) &&
                        filename_exist(entry,pattern))
                {
                    hugepage_files.push_back(entry.path().string());
                }
            }
        }
    }
};

class FpgaAdapter
{
public:
    FpgaAdapter();
    FpgaAdapter(const char *, const char *, const char *, std::uint32_t const&, std::uint32_t const&, std::uint32_t const&);
    FpgaAdapter(FpgaAdapter const&) = delete;
    FpgaAdapter(FpgaAdapter&&) = default;
    ~FpgaAdapter();

    /**
     * @brief Initialisation of Intel libmqdma internal C-structs to operate fpga board
     *
     * @details Create huge pages in the hugetlbfs file system that will support the
     *          data transfer to the fpga and then configure the DMA descriptors in
     *          one of the huge pages that were created. The initialisation process is
     *          based on C functions from the libmqdma library. The initialisation
     *          sequence is performed in the following stages.
     *
     *          1) Create the memory space for huge pages
     *          2) Retrieves information from the PCIe devices configured on the server
     *             from /sys/class/uio/uio0 registers
     *          3) Verifies availabiltiy of UIO device linked with PCIe board.
     *          4) Create DMA descriptors into hugepages. It initialises the
     *             descriptors and data memory for both TX and RX queues
     *
     * @return zero on succes or negative otherwise
     */
    std::int32_t const& initialise();

    /**
     * @brief Start fdas algorithm on the fpga board
     *
     * @details Submit a host-to-device request, transferring the data received
     *          on the huge page area to DDR in the fpga. Then it triggers the
     *          FDAS processing, commanding the BAR2 PCIe registers to start
     *          the algorithm. It waits until the sum has been recorded in the DDR.
     *
     * @return zero on succes or negative otherwise
    */
    std::int32_t const& run_fdas_algorithm();

    /**
     * @brief Setter for the filename of fdas filter.
     *
     * @details The file contains filter convolution coefficients and
     *          a set of register addresses with coefficient values.
     */
    void fdas_filter_filename(std::string const&);

    /**
     * @brief Getter of the fdas filter name loaded on the fpga.
     *
     * @returns string with a filename
     */
    std::string const& fdas_filter_filename() const;

    /**
     * @brief setter with the filename for the hsum configuration.
     *
     * @details the file contains several configuration parameters
     *          with the register address and value pair required
     *          for harmonic summation.
     */
    void fdas_hsum_filename(std::string const&);

    /**
     * @brief Getter of the filename for the harmonic summing configuration.
     *
     * @returns string with a filename
     */
    std::string const& fdas_hsum_filename() const;

    /**
     * @brief setter for the number of filters on FDAS.
     *
     */
    void fdas_filters_number(std::uint32_t const&);

    /**
     * @brief Getter for the number of FDAS filters.
     *
     * @returns integer number greather than zero
     */
    std::uint32_t const& fdas_filters_number() const;

    /**
     * @brief size in bytes of the data that will DMA transfer
     * from hugepage area to ddr into fpga
     */
    void buffer_transfer_size(std::uint32_t const&);

    /**
     * @brief getter that returns data size in bytes configured for
     * DMA transfer from hugepage area to ddr into fpga
     */
    std::uint32_t const& buffer_transfer_size() const;

    /**
     * @brief Memory deallocation of libmqdma C-structs and release resources.
     *
     * @return zero on succes or negative otherwise
     */
    std::int32_t const& cleanup_fpga_board();

    /**
     * @brief return a lists of candidates
     *
     * @details Obtain the harmonic sum results from the PCIe BAR2 memory map
     *          and copy them into a vector hsum harmonic struct vector where
     *          the harmonic sum results will be saved. Then convert the harmonic
     *          summing from the FPGA to a combined candidate list (CCL).
    */
    std::vector<std::vector<std::int32_t>> const& candidate_lists();

    /**
     * @brief Provides a memory location to be used as part of the huge page area
     */
    void hugepage_location(void *);

    /**
     * @brief Return a memory location linked to a huge page zone
    */
    void *hugepage_location();

    /**
     * @brief Obtains the bus-device-function identifier configured in the object
     *
     * @return bus-device-function identifier as a string
     */
    std::string const& bus_device_function() const;

    /**
     * @brief Receives bus-device-function identifier as string
     *
     */
    void bus_device_function(std::string const&) const;

    /**
     * @brief Provides a unique number that identifies the board in the system.
     */
    std::uint32_t const& device_id() const;

    /**
     * @brief Setup the unique fpga board number.
     */
    void device_id(std::uint32_t const&);

    /**
     * @brief Retrieves the FDAS product identifier
     */
    std::uint32_t const& product_id() const;

    /**
     * @brief Setup of the FDAS product identifier
     */
    void product_id(std::uint32_t const&);

    /**
     * @brief port number of PCIe device linked to BDF
     *
     * @return port number from zero onwards, or negative if there is no corresponding
    */
    std::int32_t const& bdf_port_number() const;

    /**
     * @brief Total number of channels supported by Queue DMA device
     *
     * @return number of channels supported, zero otherwise
    */
    std::int32_t const& number_qdma_channels() const;

    /**
     * @brief transfer data from fpga to a buffer
     */
    std::int32_t const& copy_device_to_host(void*);

    /**
     * @brief observation duration configured in the fdas fpga
     */
    std::uint32_t const& time_duration() const;

    /**
     * @brief setter for observation duration variables required by fdas fpga
     */
    void time_duration(std::uint32_t const&);

protected:

    /**
     * @brief Reset and Calibrate the fpga DDRs
     *
     * @return zero on succes or negative otherwise
     */
    std::int32_t const& initialise_fpga_ddr() const;

    /**
     * @brief Verification of the fdas algorithm on the fpga.
     *
     * @details It performs a sequence of operations on the registers
     *          to check the fdas design, kernel version, and revision.
     *          It then sends commands to check the MCI, Convolution,
     *          MSIX, and HSUM registers.
     *
     * @return zero on succes or negative otherwise
     */
    std::int32_t const& initialise_fdas_algorithm() const;

    /**
     * @brief Verifies FDAS configuration
     */
    std::int32_t const& _fdas_verification() const;

private:
    /**
     * @brief initialisation of C data structs
     * @details Initialisation of the data structure to start configuring
     *          the FDAS algorithm with default values. But all the
     *          configuration of the board and the algorithm
     *          will be handled via xml files.
     */
    struct_flags _initialise_struct_flags(std::string const&, std::string const&) const;

    /**
     * @brief remove files linked to hugepages
     */
    std::int32_t const& _remove_hugepage_files();

    /**
     * @brief Look into the /sys filesystem for the PCIe device that matches the
     *        bus-device-function identifier.
     */
    std::int32_t const& find_fdas() const;

private:
    /// @brief holds the pcie location identified by the bus-device-function string
    mutable std::string _pcie_bus_device_function;
    /// @brief binary filename that holds coefficients of the fdas filters
    mutable std::string _fdas_filter_filename;
    /// @brief file with the configuration of the harmonic sum
    mutable std::string _fdas_hsum_filename;
    /// @brief total number of fdas filters configured on the fpga board
    mutable std::uint32_t _fdas_filters_number;
    /// @brief size of data into hugepage to be DMA transfer
    mutable std::uint32_t _buffer_transfer_size;
    /// @brief holds uio_id
    mutable std::uint32_t _fpga_device_id;

    /// C code structs
    std::unique_ptr<queue_ctx[]> _context_dma_queue;

    /// mqdma struct
    std::unique_ptr<ifc_qdma_device, CStructDeleter<ifc_qdma_device>> _qdma_device_ptr;

    /// struct that in the C code keeps the input parameters
    std::unique_ptr<struct_flags> _fdas_input_options;

    /// Queue DMA C-struct to make an I/O request on the channel
    mutable std::unique_ptr<ifc_qdma_request*, CStructDeleter<ifc_qdma_request*>> _qdma_tx_buffer;

    /**
     * @brief The ifc_qdma_channel defines the tx/rx descriptor rings.
     * @details Every descriptor ring is allocated in the huge page
     *          memory area, and the address of the descriptors is
     *          given by the 'qbuf' on the ifc_qdma_queue. But
     *          the variable ctx of ifc_qdma_queue is allocated in
     *          the heap zone.
     *
     *          These variables will eventually go into a proper
     *          intel_fpga::Config, defining them here is temporary.
     *          We normally don't define variables at this level.
     */
    std::vector<std::shared_ptr<ifc_qdma_desc_sw>> _qchnl_qctx;

    // holds the return value of the libmqdma c-functions called
    mutable int _c_function_state{0};
    // port number corresponding to PCIe BDF
    std::int32_t _bdf_port_number{-1};
    // total number of queue DMA channels supported by QDMA device
    std::int32_t _number_qdma_channels{0};
    // current queue dma channel
    int _current_qdma_channel{-1};

    mutable std::uint32_t _fpga_product_id{0};
    mutable std::uint32_t _fpga_core_version{0};
    mutable std::uint32_t _fpga_top_version{0};
    mutable std::uint32_t _fpga_core_revision{0};
    mutable std::uint32_t _fpga_top_revision{0};

    /// @brief reset of fpga ddr
    bool _force_reset_fpga_ddr;
    /// @brief enable/disable reset of fdas core
    bool _initialise_fpga_fdas{true};
    /// @brief holds the observation duration configured into the fdas fpga
    std::uint32_t _time_duration;
    std::vector<std::vector<std::int32_t>> _candidate_list;

};


/// default fdas algorithm values
static constexpr int default_request_size = 33554336;
static constexpr int default_payload = 1048576;
static constexpr int default_number_channels = 2;
static constexpr unsigned long default_fop_size = 4194176 ; // 0x3FFF80
static constexpr unsigned long default_overlap_size = 420 ; // 0x1A4
static constexpr int default_loop_num = 6;
static constexpr int default_number_pcie_bar_registers = 2;
static constexpr int hsum_results_max_number = 1024;
static constexpr unsigned int default_filters_number = 42;
static constexpr std::uint32_t _default_fpga_product_id = 0xFDA5;
static constexpr std::uint32_t _default_buffer_transfer_size{1<<23};
static const std::string default_filter_file("template.dat");
static const std::string default_hsum_file("hsum_config.txt");
static const std::string default_pcie_bus_device_function("0000:00:00.0");
// hugepage filesystem location.
static constexpr const char * hugepage_directory{"/dev/hugepages"};
// name of hugepage mapping file
static constexpr const char * hugepage_filename_pattern{"perfq_example_page_.*"};

/// this variable, defined as the macro QDEPTH (508) in the C code,
/// sets the number of I/O request buffers per DMA channel, where the buffer size
/// is given by default_payload (1048576). Note that initially the C-code defines
/// four DMA channels, but in effect only the first DMA channel is used.
static constexpr std::uint32_t _queue_depth_buffers{QDEPTH};

} // namespace intel_fpga
} // namespace panda
} // namespace ska

#endif // SKA_PANDA_ENABLE_INTEL_FPGA

#endif // SKA_PANDA_ARCH_INTEL_AGILEX_FPGA_FPGAADAPTER_H
