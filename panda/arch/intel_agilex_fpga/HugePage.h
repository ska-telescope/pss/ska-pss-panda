/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_ARCH_INTEL_AGILEX_FPGA_HUGEPAGE_H
#define SKA_PANDA_ARCH_INTEL_AGILEX_FPGA_HUGEPAGE_H

#include "panda/arch/intel_agilex_fpga/Fpga.h"
#include <string_view>
#include <sys/types.h>
#include <string>

namespace ska {
namespace panda {
namespace intel_fpga {

/**
 * @brief wrapper to handle hugepage as an object
 *
 * @details
 *   Provides the methods for creating and deleting hugepages memory map
 *
 */
class HugePage
{
    public:
        HugePage();
        HugePage(std::uint32_t const& page_number);
        HugePage(HugePage const&) = delete;
        HugePage(HugePage&&) = default;
        HugePage& operator=(HugePage const&) = delete;
        ~HugePage();

    public:
        /**
         * @brief creates a 1GB huge page in the virtual address space.
         */
        void* create_hugepage();

        /**
         * @brief unmap huge page and delete file linked to the memory map.
         */
        void remove_hugepage();

        /**
         * @brief return virtual address of huge page
         */
        void* page_address() const;

        /**
         * @brief getter of the hugepage number identifier
         */
        std::uint32_t const& page_number() const;

        /**
         * @brief setter for a hugepage number identifier
         */
        void page_number(std::uint32_t const& value);

        /**
        * @brief name of the filename mapped to hugepage memory location
        */
        std::string_view filename() const;

    private:
        /**
         * @brief number to identify hugepage
         */
        std::uint32_t _page_number;

        /**
         * @brief virtual address space of the hugepage
         */
        mutable void* _page_address;

        /**
         * @brief unique number to be used as part of the hugepage filename.
         */

        /**
         * @brief name of the filename to identify hugepage memory map
         */
        std::string _hugepage_filename;

        /**
         * @brief full path base for the file that will linked to the hugepage area.
         */
        static constexpr const char* _prefix = "/dev/hugepages/ska_panda_hugepage_";

        static constexpr size_t _page_size{1<<30}; // 1GB

};

} // namespace intel_fpga
} // namespace panda
} // namespace ska

#endif // SKA_PANDA_ARCH_INTEL_AGILEX_FPGA_HUGEPAGE_H