/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_ARCH_INTEL_AGILEX_FPGA_POOLRESOURCE_H
#define SKA_PANDA_ARCH_INTEL_AGILEX_FPGA_POOLRESOURCE_H

#include "panda/Configuration.h"
#include "panda/arch/intel_agilex_fpga/Architecture.h"
#include "panda/arch/intel_agilex_fpga/DeviceConfig.h"
#ifdef SKA_PANDA_ENABLE_INTEL_FPGA
#include "panda/arch/intel_agilex_fpga/Fpga.h"
#include "panda/Resource.h"
#include <memory>

namespace ska {
namespace panda {
namespace intel_fpga {

} // namespace intel_fpga

/**
 * @brief
 *    Intel Agilex card definition conforming to ResourcePool requirements
 */
template<>
class PoolResource<intel_fpga::Rtl> : public intel_fpga::Fpga
{
    public:
        PoolResource(std::shared_ptr<intel_fpga::FpgaAdapter> const&, unsigned int device_id);
        ~PoolResource();

        template<typename TaskType>
        void run(TaskType&& job);

        void configure(DeviceConfig<intel_fpga::Rtl> const&);
};

} // namespace panda
} // namespace ska

#include "panda/arch/intel_agilex_fpga/detail/PoolResource.cpp"
#endif // SKA_PANDA_ENABLE_INTEL_FPGA

#endif // SKA_PANDA_ARCH_INTEL_AGILEX_FPGA_POOLRESOURCE_H
