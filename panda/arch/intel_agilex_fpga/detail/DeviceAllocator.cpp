/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/Configuration.h"
#include <exception>

namespace ska {
namespace panda {

template<typename T>
DeviceAllocator<T, intel_fpga::Rtl>::DeviceAllocator(Device<intel_fpga::Rtl> const&)
{
}

template<typename T>
template<typename U>
DeviceAllocator<T, intel_fpga::Rtl>::DeviceAllocator(DeviceAllocator<U, intel_fpga::Rtl> const&)
{
}

template<typename T>
T* DeviceAllocator<T, intel_fpga::Rtl>::allocate(std::size_t size)
{
#ifdef SKA_PANDA_ENABLE_INTEL_FPGA
    ska::panda::intel_fpga::DevicePointer<T> device_pointer{size};
    return (T*)device_pointer.size();
#else
    return std::throw::bad_alloc();
#endif // SKA_PANDA_ENABLE_INTEL_FPGA
}

template<typename T>
void DeviceAllocator<T, intel_fpga::Rtl>::deallocate( T* ptr, std::size_t)
{
#ifdef SKA_PANDA_ENABLE_INTEL_FPGA
    // the hugepage will be released when the lifetime of the device_pointer ends.
    (void) ptr;
#else
    delete ptr;
    ptr = nullptr;
#endif // SKA_PANDA_ENABLE_INTEL_FPGA
}

} // namespace panda
} // namespace ska
