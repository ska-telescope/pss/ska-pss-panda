/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/arch/intel_agilex_fpga/DeviceCopy.h"
#include <vector>
#include <utility>
#include <iterator>
#include <type_traits>

namespace ska {
namespace panda {
namespace intel_fpga {

namespace detail {

struct DeviceCopyImpl {
    template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
    inline static typename std::remove_reference<DstIteratorType>::type copy_to_device(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it)
    {
        typedef std::iterator_traits<typename std::remove_reference<SrcIteratorType>::type> SrcIteratorTraits;
        static_assert(std::is_same<std::random_access_iterator_tag, typename SrcIteratorTraits::iterator_category>::value, "Read directly from non-contiguous iterator type not supported (use a preallocated buffer instead)");
    #ifdef SKA_PANDA_ENABLE_INTEL_FPGA
        //
        // this is a placeholder to implement write from hugepage location
        // to the fpga internal memory ram.
        //
    #endif
        return dst_it;
    }

    template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
    inline static typename std::remove_reference<DstIteratorType>::type copy_to_host(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it)
    {
        typedef typename std::iterator_traits<typename std::remove_reference<SrcIteratorType>::type>::value_type SrcIteratorValueT;
        static_assert(sizeof(typename std::iterator_traits<typename std::remove_reference<DstIteratorType>::type>::value_type) >= sizeof(SrcIteratorValueT), "type conversions during copy from device not yet supported - please ask if you need it");
    #ifdef SKA_PANDA_ENABLE_INTEL_FPGA
        //
        // this is a placeholder to implement write from hugepage location
        // to the fpga internal memory ram.
        //
    #endif
        return dst_it;
    }

    template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
    inline static typename std::remove_reference<DstIteratorType>::type copy_to_hugepage(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it)
    {
        typedef typename std::iterator_traits<typename std::remove_reference<SrcIteratorType>::type>::value_type SrcIteratorValueT;
        static_assert(sizeof(typename std::iterator_traits<typename std::remove_reference<DstIteratorType>::type>::value_type) >= sizeof(SrcIteratorValueT), "type conversions during copy from device not yet supported - please ask if you need it");
    #ifdef SKA_PANDA_ENABLE_INTEL_FPGA
        //
        // this is a placeholder to implement write from hugepage location
        // to the fpga internal memory ram.
        //
        std::size_t size = std::distance(std::forward<SrcIteratorType>(begin), std::forward<EndIteratorType>(end));
        auto status = dst_it.write_to_hugepage(*begin, size);
        //write_to_hugepage(DataType const& data, std::size_t size)
    #endif
        return dst_it;
    }
};

} // detail

} // namespace intel_fpga

// DeviceCopy specialisations
// Cpu -> Device
template<typename SrcIteratorTypeTag, typename EndIteratorTypeTag, typename DstIteratorTypeTag>
template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
typename std::remove_reference<DstIteratorType>::type
DeviceCopy<Cpu
          , intel_fpga::Rtl
          , SrcIteratorTypeTag
          , EndIteratorTypeTag
          , DstIteratorTypeTag>::copy(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it)
{
    return intel_fpga::detail::DeviceCopyImpl::copy_to_device(std::forward<SrcIteratorType>(begin)
                , std::forward<EndIteratorType>(end)
                , std::forward<DstIteratorType>(dst_it));
}

// Device -> Cpu
template<typename SrcIteratorTypeTag, typename EndIteratorTypeTag, typename DstIteratorTypeTag>
template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
typename std::remove_reference<DstIteratorType>::type
DeviceCopy<intel_fpga::Rtl
          , Cpu
          , SrcIteratorTypeTag
          , EndIteratorTypeTag
          , DstIteratorTypeTag>::copy(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it)
{
#ifdef SKA_PANDA_ENABLE_INTEL_FPGA
    return intel_fpga::detail::DeviceCopyImpl::copy_to_host(std::forward<SrcIteratorType>(begin)
                , std::forward<EndIteratorType>(end)
                , std::forward<DstIteratorType>(dst_it));
#else
    return dst_it;
#endif // SKA_PANDA_ENABLE_INTEL_FPGA
}

} // namespace panda
} // namespace ska
