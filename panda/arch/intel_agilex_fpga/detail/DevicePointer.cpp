/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/arch/intel_agilex_fpga/Fpga.h"
#include "panda/arch/intel_agilex_fpga/PoolResource.h"
#include "panda/Error.h"
#include "DevicePointer.h"

namespace ska {
namespace panda {
namespace intel_fpga {

template<typename DataType>
DevicePointer<DataType>::DevicePointer(std::size_t size)
    : _d_ptr{nullptr}
    , _size(size)
    , _page(std::make_unique<HugePage>(0)) //1GB
{
#ifdef SKA_PANDA_ENABLE_INTEL_FPGA
    if (size==0)
        return;
    // create a 1GB default page of continous memory
    _page.get()->create_hugepage();
#else
    (void) device;
#endif
}

template<typename DataType>
DevicePointer<DataType>::~DevicePointer()
{
}

template<typename DataType>
void* DevicePointer<DataType>::hugepage_write([[maybe_unused]]std::size_t const& offset, DataType const& data, std::size_t const& size) const
{
    // we are copying from some memory area to hugepage
    return std::memcpy( _page.get()->page_address(), (const void*)&data, sizeof(DataType)*size );
}

template<typename DataType>
void DevicePointer<DataType>::hugepage_read([[maybe_unused]]std::size_t const& offset, DataType& data, std::size_t const& size) const
{
    // getting data from hugepage to a host memory
    std::memcpy( (void*)&data, _page.get()->page_address(), sizeof(DataType)*size );
}

template<typename DataType>
std::size_t const& DevicePointer<DataType>::size() const
{
    return _size;
}

template<typename DataType>
typename DevicePointer<DataType>::Iterator DevicePointer<DataType>::begin()
{
    return Iterator(0, *this);
}

template<typename DataType>
typename DevicePointer<DataType>::Iterator DevicePointer<DataType>::end()
{
    return Iterator(_size, *this);
}

template<typename DataType>
typename DevicePointer<DataType>::ConstIterator DevicePointer<DataType>::begin() const
{
    return ConstIterator(0, *this);
}

template<typename DataType>
typename DevicePointer<DataType>::ConstIterator DevicePointer<DataType>::end() const
{
    return ConstIterator(_size, *this);
}

template<typename DataType>
typename DevicePointer<DataType>::ConstIterator DevicePointer<DataType>::cbegin() const
{
    return ConstIterator(0, *this);
}

template<typename DataType>
typename DevicePointer<DataType>::ConstIterator DevicePointer<DataType>::cend() const
{
    return ConstIterator(_size, *this);
}

template<typename DataType>
Fpga const& DevicePointer<DataType>::device() const
{
    return *_device;
}

template<typename DataType>
DevicePointer<DataType>& DevicePointer<DataType>::operator=(DevicePointer&& cp)
{
    _d_ptr = cp._d_ptr;
    _size = cp._size;
    _page = std::move(cp._page);
    _device = cp._device;

    return *this;
}

template<typename DataType>
DataType* DevicePointer<DataType>::get_device_ptr() const
{
    return _d_ptr;
}

template <typename DataType>
std::unique_ptr<HugePage> DevicePointer<DataType>::get_page() const
{
    return std::unique_ptr<HugePage>();
}


} // namespace intel_fpga
} // namespace panda
} // namespace ska
