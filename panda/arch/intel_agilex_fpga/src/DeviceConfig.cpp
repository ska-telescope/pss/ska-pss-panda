/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/arch/intel_agilex_fpga/DeviceConfig.h"

namespace ska {
namespace panda {

DeviceConfig<intel_fpga::Rtl>::DeviceConfig()
    : BaseT(tag_name())
    , _pcie_bus_device("0000:00:00.0")
    , _buffer_transfer_size(33554432) // default value 4*2^23
    , _packet_size(1048576) // default packet size for DMA transfer
    , _fop_size(4194176) // default filter output plane size
    , _overlap_size(420) // default convolution overlap
    , _loop_num(6) // default internal loop value for CONV_IFFT
    , _number_of_filters(42) // default is set to 42 filters
    , _filters_filename("")
    , _hsum_filename("")
    , _duration(536)
{
}

DeviceConfig<intel_fpga::Rtl>::DeviceConfig(DeviceConfig const& copy)
    : BaseT(copy)
    , _pcie_bus_device(copy._pcie_bus_device)
    , _buffer_transfer_size(copy._buffer_transfer_size)
    , _packet_size(copy._packet_size)
    , _fop_size(copy._fop_size)
    , _overlap_size(copy._overlap_size)
    , _loop_num(copy._loop_num)
    , _number_of_filters(copy._number_of_filters) // default is set to 42 filters
    , _filters_filename(copy._filters_filename)
    , _hsum_filename(copy._hsum_filename)
    , _duration(copy._duration)
{
}

DeviceConfig<intel_fpga::Rtl>& DeviceConfig<intel_fpga::Rtl>::operator=(DeviceConfig const& config)
{
    _pcie_bus_device = config._pcie_bus_device;
    _buffer_transfer_size = config._buffer_transfer_size;
    _packet_size = config._packet_size;
    _fop_size= config._fop_size;
    _overlap_size = config._overlap_size;
    _loop_num = config._loop_num;
    _number_of_filters = config._number_of_filters;
    _filters_filename = config._filters_filename;
    _hsum_filename = config._hsum_filename;
    _device_count = config.device_count();
    return *this;
}

DeviceConfig<intel_fpga::Rtl>::DeviceConfig(std::string module_name)
    : BaseT(std::move(module_name))
{
}

DeviceConfig<intel_fpga::Rtl>::~DeviceConfig()
{
}

void DeviceConfig<intel_fpga::Rtl>::add_options(OptionsDescriptionEasyInit& add_options)
{
    add_options
    (
     "pcie_bus_device"
     , boost::program_options::value<std::string>(&_pcie_bus_device)->default_value(_pcie_bus_device)
     , "pcie address of fpga identified by the bus-device-function string"
    )
    (
     "transfer_size"
     , boost::program_options::value<std::uint32_t>(&_buffer_transfer_size)->default_value(_buffer_transfer_size)
     , "size in bytes of the data to be transferred between Host to Device"
    )
    (
     "packet_size"
     , boost::program_options::value<std::uint32_t>(&_packet_size)->default_value(_packet_size)
     , "DMA payload size in bytes and must be 64 byte aligned"
    )
    (
     "fop_size"
     , boost::program_options::value<std::uint64_t>(&_fop_size)->default_value(_fop_size)
     , "defines the number of samples for the CLD and CONV modules to process to generate the FOP"
    )
    (
     "overlap_size"
     , boost::program_options::value<std::uint64_t>(&_overlap_size)->default_value(_overlap_size)
     , "defines the number of samples for the CLD and CONV modules to process to generate the FOP"
    )
    (
     "loop_num"
     , boost::program_options::value<std::uint32_t>(&_loop_num)->default_value(_loop_num)
     , "number of times the CONV_IFFT sub-modules are actually looped through."
    )
    (
     "filters_filename"
     , boost::program_options::value<std::string>(&_filters_filename)->default_value(_filters_filename)
     , "file with the FDAS algorithm filter convolution coefficients"
    )
    (
     "number_of_filters"
     , boost::program_options::value<unsigned int>(&_number_of_filters)->default_value(_number_of_filters)
     , "number of the FDAS filters programmed on the FPGA card"
    )
    (
     "hsum_config_filename"
     , boost::program_options::value<std::string>(&_hsum_filename)->default_value(_hsum_filename)
     , "file including address of the registers on the FPGA card to perform harmonic summation"
    )
    ("observation_duration", boost::program_options::value<std::uint32_t>(&_duration)
        ->default_value(_duration)
     , "time in seconds of the total DM trial length; this is the observation duration configured as the PSBC's dump time.");
    ;

    DeviceConfigBase<DeviceConfig<intel_fpga::Rtl>>::add_options(add_options);
}

std::string const& DeviceConfig<intel_fpga::Rtl>::pci_bus_device() const
{
    return _pcie_bus_device;
}

void DeviceConfig<intel_fpga::Rtl>::pci_bus_device(std::string const& name)
{
    _pcie_bus_device = name;
}

std::uint32_t const& DeviceConfig<intel_fpga::Rtl>::buffer_transfer_size() const
{
    return _buffer_transfer_size;
}

void DeviceConfig<intel_fpga::Rtl>::buffer_transfer_size(std::uint32_t const& value)
{
    _buffer_transfer_size = value;
}

std::string const& DeviceConfig<intel_fpga::Rtl>::filters_filename() const
{
    return _filters_filename;
}

void DeviceConfig<intel_fpga::Rtl>::filters_filename(std::string const& name)
{
    _filters_filename = name;
}

std::string const& DeviceConfig<intel_fpga::Rtl>::hsum_filename() const
{
    return _hsum_filename;
}

void DeviceConfig<intel_fpga::Rtl>::hsum_filename(std::string const& name)
{
    _hsum_filename = name;
}

std::uint32_t const& DeviceConfig<intel_fpga::Rtl>::number_of_filters() const
{
    return _number_of_filters;
}

void DeviceConfig<intel_fpga::Rtl>::number_of_filters(std::uint32_t const& value)
{
    _number_of_filters = value;
}

std::uint32_t const& DeviceConfig<intel_fpga::Rtl>::duration() const
{
    return _duration;
}

void DeviceConfig<intel_fpga::Rtl>::duration(std::uint32_t const& value)
{
    _duration = value;
}

std::string const& DeviceConfig<intel_fpga::Rtl>::tag_name()
{
    static std::string tag("intel_fpga::Rtl");
    return tag;
}

} // namespace panda
} // namespace ska
