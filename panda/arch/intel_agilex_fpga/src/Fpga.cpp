/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/arch/intel_agilex_fpga/Fpga.h"

namespace ska {
namespace panda {
namespace intel_fpga {


Fpga::Fpga(std::shared_ptr<FpgaAdapter> const& board, unsigned int device_id)
    : _board(board)
    , _device_id(device_id)
{
}

Fpga::~Fpga()
{
}

unsigned Fpga::device_id() const
{
    return _device_id;
}

std::string Fpga::bdf_id() const
{
    return _board->bus_device_function();
}

std::int32_t Fpga::product_id() const
{
    return _board->product_id();
}

// Initialisation of the FDAS algorithm, note that the
// board must have been previously instantiated.
std::int32_t Fpga::initialise() const
{
    return _board->initialise();
}

std::int32_t const& Fpga::run_fdas()
{
    return _board->run_fdas_algorithm();
}

void Fpga::hugepage(void *addr)
{
    _board->hugepage_location(addr);
}

std::vector<std::vector<std::int32_t>> const& Fpga::candidates()
{
    return _board->candidate_lists();
}

void Fpga::load_fdas_configuration()
{
    _board->fdas_filter_filename(_fdas_filter_filename);
    _board->fdas_hsum_filename(_fdas_hsum_filename);
    _board->fdas_filters_number(_fdas_filters_number);
    _board->buffer_transfer_size(_buffer_transfer_size);
    _board->time_duration(_duration);
}

} // namespace intel_fpga
} // namespace panda
} // namespace ska
