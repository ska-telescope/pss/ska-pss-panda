/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/arch/intel_agilex_fpga/FpgaAdapter.h"
#include "panda/Error.h"
#include "panda/Log.h"


namespace ska {
namespace panda {
namespace intel_fpga {

FpgaAdapter::FpgaAdapter()
    : _pcie_bus_device_function(default_pcie_bus_device_function)
    , _fdas_filter_filename(default_filter_file)
    , _fdas_hsum_filename(default_hsum_file)
    , _fdas_filters_number(default_filters_number)
    , _buffer_transfer_size(_default_buffer_transfer_size)
    , _fpga_device_id(0)
    , _context_dma_queue(std::make_unique<queue_ctx[]>(4))
    , _fdas_input_options(
        std::make_unique<struct_flags>(
        _initialise_struct_flags(_pcie_bus_device_function,_fdas_filter_filename)
        )
        )
    , _fpga_product_id(_default_fpga_product_id)
    , _force_reset_fpga_ddr(false)
    , _time_duration(536) // default value configured on the fpga
{
#ifdef SKA_PANDA_ENABLE_INTEL_FPGA
    // Host resources configuration.
    // finding FGPA PCIe slot (BDF).
    find_fdas();
#else
    PANDA_LOG_WARN << "Not compiled with Intel libmqdma support";
#endif // SKA_PANDA_ENABLE_INTEL_FPGA

}

FpgaAdapter::FpgaAdapter(const char * bdf
, const char* config_file
, const char* hsum_config
, std::uint32_t const& filters_number
, std::uint32_t const& device_id
, std::uint32_t const& product_id)
    : _pcie_bus_device_function(bdf)
    , _fdas_filter_filename(config_file)
    , _fdas_hsum_filename(hsum_config)
    , _fdas_filters_number(filters_number)
    , _buffer_transfer_size(_default_buffer_transfer_size)
    , _fpga_device_id(device_id)
    , _context_dma_queue(std::make_unique<queue_ctx[]>(4))
    , _fdas_input_options(
        std::make_unique<struct_flags>(
            _initialise_struct_flags(bdf,config_file)
            )
            )
    , _fpga_product_id(product_id)
    , _force_reset_fpga_ddr(false)
    , _time_duration(536) // default value configured on the fpga
{
#ifdef SKA_PANDA_ENABLE_INTEL_FPGA
    // find_fdas requires product id to find fpga
    find_fdas();
#else
    PANDA_LOG_WARN << "Not compiled with Intel libmqdma support";
#endif // SKA_PANDA_ENABLE_INTEL_FPGA
}

FpgaAdapter::~FpgaAdapter()
{
    cleanup_fpga_board();
}

std::int32_t const& FpgaAdapter::initialise()
{
    _c_function_state = 0;

    // an indirect approach to ensure that fpga resources have been initialised
    // is to make sure that the _qdma_device_ptr points to a valid memory address
    if (_qdma_device_ptr != nullptr)
        return _c_function_state;

    // if the Linux capability cap_sys_admin is enabled
    // the files located into /proc/<pid> are owned by
    // root. This function will change the ownership of the
    // files, in particular /proc/<pid>/pagemap which is required
    // to obtain physical memory address of the hugepages.
    _c_function_state = prctl_set_dumpable();
    if (_c_function_state < 0)
    {
        PANDA_LOG_ERROR << "Failed to set dumpable attribute of the process";
        return _c_function_state;
    }

    // Start the engine.
    // This function maps and enable device memory to user space and makes memory allocation
    // from huge page file system
    ifc_app_start(_fdas_input_options->bdf, _fdas_input_options->packet_size);

    // Obtains the port number to corresponding BDF
    _bdf_port_number = ifc_mcdma_port_by_name(_fdas_input_options->bdf);
    if(_bdf_port_number < 0 )
    {
        PANDA_LOG_ERROR << "INTEL AGILEX FPGA: unable to get dma port for BDF: "  <<
        _fdas_input_options->bdf;
        return _bdf_port_number;
    }

    // getting a raw pointer to use in C-code, no heap allocation yet
    ifc_qdma_device* qdev_raw_ptr;

    // getting DMA device, it verifies if there is a UIO device linked with
    // PCIe board. The uio_id should be the same as the port number.
    // There is allocation on heap memory for the ifc_qdma_device struct
    _c_function_state = ifc_qdma_device_get(_bdf_port_number, &qdev_raw_ptr, 128, IFC_CONFIG_QDMA_COMPL_PROC);
    if (_c_function_state < 0)
    {
        PANDA_LOG_ERROR << "Failed to get context device " << _bdf_port_number;
        return _c_function_state;
    }

    // taking the ownership of C struct pointer ifc_qdma_device created by ifc_qdma_device_get
    std::unique_ptr<ifc_qdma_device, CStructDeleter<ifc_qdma_device>> qdma_ptr(qdev_raw_ptr, CStructDeleter<ifc_qdma_device>());
    _qdma_device_ptr = std::move(qdma_ptr);

    // returns the total number of channels supported by QDMA device
    _number_qdma_channels = ifc_num_channels_get(qdev_raw_ptr);
    if (_number_qdma_channels <= 0)
    {
        PANDA_LOG_ERROR <<"Failed to find channel";
        return _number_qdma_channels;
    }

    // The original initialisation C function creates four queue contexts,
    // and each queue context defines two TX/RX DMA channels. However,
    // the context queue used in the entire program, is only the first
    // element in the context queue array. It seems the for-loop is
    // just to prepare MSIX interrupts.
    for (int chno=0; chno < _fdas_input_options->chnls; ++chno)
    {

        // four channels indexes
        const int idx = 2*chno;

        // Get the next available channel.
        // Initialize the descriptors and data memory for both TX and RX queues and enable the channel.
        // input parameters:  QDMA device, Pointer to update channel context, specific channel number.
        _current_qdma_channel = ifc_qdma_channel_get(qdev_raw_ptr, &_context_dma_queue[idx].qchnl, chno, IFC_QDMA_DIRECTION_BOTH);
        if (_current_qdma_channel < 0)
        {
            PANDA_LOG_ERROR << "Failed channel get" << chno ;
            return _current_qdma_channel;
        }

        _context_dma_queue[idx].qdev = _qdma_device_ptr.get();
        _context_dma_queue[idx].channel_id = _current_qdma_channel;
        _context_dma_queue[idx].direction = IFC_QDMA_DIRECTION_RX;
        _context_dma_queue[idx+1].qdev = _qdma_device_ptr.get();
        _context_dma_queue[idx+1].channel_id = _current_qdma_channel;
        _context_dma_queue[idx+1].direction = IFC_QDMA_DIRECTION_TX;

        // same ifc_qdma_channel in both channel contexts
        _context_dma_queue[idx+1].qchnl = _context_dma_queue[idx].qchnl ;

        // This is done just to get the ownership and avoid memory leaks
        std::shared_ptr<ifc_qdma_desc_sw> rx_ctx_ptr(_context_dma_queue[idx].qchnl->rx.ctx);
        _qchnl_qctx.emplace_back(std::move(rx_ctx_ptr));
        std::shared_ptr<ifc_qdma_desc_sw> tx_ctx_ptr(_context_dma_queue[idx].qchnl->tx.ctx);
        _qchnl_qctx.emplace_back(std::move(tx_ctx_ptr));

        // MSI-X interrupts configuration
        queue_ctx* ctx_ptr = _context_dma_queue.get() + idx;
        if(msix_init(ctx_ptr)<0)
        {
            PANDA_LOG_ERROR << "MSIX Init Failed";
        }
        ctx_ptr = _context_dma_queue.get() + idx + 1;
        if(msix_init(ctx_ptr)<0)
        {
            PANDA_LOG_ERROR << "MSIX Init Failed";
        }

    }

    // sharing the smart pointer of libmqdma C-structs with the fdas C-code.
    // This is done because the fdas functions uses the data from these C-structs
    // e.g. the function that loads config file requires _fdas_input_options
    copy_struct_pointers(_fdas_input_options.get(),
                       _qdma_device_ptr.get(),
                       _context_dma_queue.get());

    return _c_function_state;
}
//operator(Config)
std::int32_t const& FpgaAdapter::run_fdas_algorithm()
{
    _c_function_state = 0;

    // will return shortly if it has already been initialised
    if ((_c_function_state = initialise()) < 0)
        return _c_function_state;

    // this block is only made once before executing FDAS
    if(_initialise_fpga_fdas)
    {
        PANDA_LOG << " Initialising " << (_force_reset_fpga_ddr?" DDRs ":" ")
                  << "modules CONV, CLD, HSUM, CTRL, MSIX";

        // reset and calibrate fpga's DDRs
        if (_force_reset_fpga_ddr)
        {
            if((_c_function_state = initialise_fpga_ddr()) < 0)
                return _c_function_state;
        }
        // reset fdas core algorithm
        if ((_c_function_state = initialise_fdas_algorithm()) < 0)
            return _c_function_state;

        _initialise_fpga_fdas = false;
    }

    // Note: before start fdas operation a DM-f should have been
    // allocated on the hugepage memory area
    if (fdas_operate())
    {
        _c_function_state = -1;
    }

    return _c_function_state;
}

void FpgaAdapter::fdas_filter_filename(std::string const& filter_filename)
{
    _fdas_filter_filename = filter_filename;
}

std::string const& FpgaAdapter::fdas_filter_filename() const
{
    return _fdas_filter_filename;
}

void FpgaAdapter::fdas_hsum_filename(std::string const& hsum_filename)
{
    _fdas_hsum_filename = hsum_filename;
}

std::string const& FpgaAdapter::fdas_hsum_filename() const
{
    return _fdas_hsum_filename;
}

void FpgaAdapter::fdas_filters_number(std::uint32_t const& filters_number)
{
    _fdas_filters_number = filters_number;
}

std::uint32_t const& FpgaAdapter::fdas_filters_number() const
{
    return _fdas_filters_number;
}

std::int32_t const& FpgaAdapter::cleanup_fpga_board()
{
    _c_function_state = 0;

    // The ifc_app_stop execute the following actions:
    // i.  unmap uio pcie registers (bar0, bar2)
    // ii. unmap and delete hugepages created by Intel API
    ifc_app_stop();

    // remove any remaining file on /dev/hugepages
    _remove_hugepage_files();

    return _c_function_state;
}

/// return a vector of vector with integers of the hsum
/// the return result will be eventually converted to a template type or
/// a particular struct.
//
// Harmonic summing results
// Check p.104 FDAS implementation document
//
std::vector<std::vector<std::int32_t>> const& FpgaAdapter::candidate_lists()
{

    // preallocate
    if (_candidate_list.capacity() == 0)
    {
        // Allocate space for hsum_results_max_number items, but cand_list is still empty.
        _candidate_list.reserve(hsum_results_max_number);
    }

    //  removes all elements from the vector, making its size zero
    _candidate_list.clear();

    std::int32_t _rvalue;

    for (int i=0 ; i< hsum_results_max_number; ++i)
    {
        _rvalue = ifc_qdma_read64(_qdma_device_ptr.get(), HSUMBASE+RESULTS_BASE_ADDR + i*32, _fdas_input_options->rbar );

        std::int32_t _harmonic_number, _fop_column, _foprow;

        // The special value 0x60 is a user configurable value M[31:0] instead of a FOP Row
        _foprow = ifc_qdma_read64(_qdma_device_ptr.get(), HSUMBASE+RESULTS_BASE_ADDR + i*32 + 16, _fdas_input_options->rbar);

        if ((_rvalue & 0x100) && ((_foprow & 0x7F) != 0x60))
        {
            _harmonic_number = (int)(_rvalue & 0xF);

            _fop_column = ifc_qdma_read64(_qdma_device_ptr.get(), HSUMBASE+RESULTS_BASE_ADDR + i*32 + 8, _fdas_input_options->rbar);

            _rvalue = ifc_qdma_read64(_qdma_device_ptr.get(), HSUMBASE+RESULTS_BASE_ADDR + i*32 + 24, _fdas_input_options->rbar);

            // Free Running count of the DMs that have been processed by HSUM.
            std::int32_t _dm_cnt = ifc_qdma_read64(_qdma_device_ptr.get(), HSUMBASE + DM_CNT_ADDR, _fdas_input_options->rbar);

            // DM_ID, Harmonic_Number, FOP_Column, FOP_Row, Power
            std::vector<std::int32_t> hsum_vec ={(int)_dm_cnt, _harmonic_number, (int)(_fop_column & 0x3FFFFF), (int)(_foprow & 0x7F), _rvalue};

            // checks rows are not zero
            if (std::all_of(hsum_vec.begin()+1, hsum_vec.end(), [](std::int32_t x){ return x == 0; }))
            {
                continue; // Found a zero row!
            }

            _candidate_list.emplace_back(hsum_vec);

        }
    }

    return _candidate_list;
}




std::int32_t const& FpgaAdapter::initialise_fpga_ddr() const
{
    _c_function_state = 0;

    // DDR identifier on the fpga that would be reset in the lambda function
    int _ddr_number{0};

    // register addresses of the fpga DDRs
    std::vector<std::uint64_t> _ddr_registers
    {
        RESET_ON_DDR0
    ,   RESET_ON_DDR1
    ,   RESET_ON_DDR2
    ,   RESET_ON_DDR3
    };

    // this lambda function will just be called one time on the initialisation
    auto _ddr_reset = [&](std::uint64_t const& _ddr_addr)
    {
        // PCIe BAR2 identifier
        int const& _rbar = _fdas_input_options->rbar;
        // get raw c-pointer to pcie struct on the c code
        ifc_qdma_device* const _raw_ptr = _qdma_device_ptr.get();

        // convert DDR address to DDR{0,1,2,3} status byte (0x100,0x200,0x400,0x800)
        int _ddr_byte_status = _ddr_addr >> 1;

        // perform reset and calibration of DDR
        // RESET_ADDR (0x00000080) is the address on the BAR2 register
        // _ddr_addr is a index address given to DDRs (0x200, 0x400, 0x800, 0x1000)
        ifc_qdma_write64(_raw_ptr, RESET_ADDR, RESET_OFF, _rbar);
        ifc_qdma_write64(_raw_ptr, RESET_ADDR, _ddr_addr, _rbar);
        ifc_qdma_write64(_raw_ptr, RESET_ADDR, RESET_OFF, _rbar);

        int retries = 0;
        while ((ifc_qdma_read64(_raw_ptr, DDR_STATUS_ADDR, _rbar) & _ddr_byte_status) == 0)
        {
            if (++retries < DDR_RESET_TIMEOUT)
            {
                std::this_thread::sleep_for(std::chrono::milliseconds{1});
            }
            else
                break;
        }

        // get a shift number given the DDR number
        int response_status{_ddr_byte_status/0x100};
        int _shift_counter{0};
        while (response_status>>=1)
            ++_shift_counter;

        // preparing a status byte word for each DDR
        int _status_failed = response_status << _shift_counter;
        int _status_passed = _status_failed << 1;

        // read DDR status DDR0:{fail=1,pass=2} DDR1:{x4,x8} DDR2:{x10,16}
        int const& res = ifc_qdma_read64(_raw_ptr, DDR_STATUS_ADDR, _rbar);
        if ( res & _ddr_byte_status )
        {
            PANDA_LOG << "complete reset DDR" << _ddr_number;
            if (res & _status_passed)
            {
                PANDA_LOG << " Calibration Passed DDR" << _ddr_number;
            }
            if (res & _status_failed)
            {
                PANDA_LOG << " Calibration Failed DDR" << _ddr_number;
            }
        }
        else
        {
            PANDA_LOG_ERROR << "DDR" << _ddr_number << " reset not complete";
        }

        ++_ddr_number;
    };


    // the pointer would be null if initialisation has not been done
    if (_qdma_device_ptr)
    {
        // reset DDRs
        std::for_each(_ddr_registers.cbegin(), _ddr_registers.cend(), _ddr_reset);
    }
    else
    {
        _c_function_state = -1;
    }

    return _c_function_state;
}

std::int32_t const& FpgaAdapter::initialise_fdas_algorithm() const
{
    _c_function_state = 0;

    // pointer to the QDMA device structure should have been
    // initialised before perform reset.
    if (_qdma_device_ptr == nullptr)
    {
        return (_c_function_state = -1);
    }

    // PCIe BAR2 identifier
    int const& _rbar = _fdas_input_options->rbar;
    // size of the Filter Output Plane (FOP)
    unsigned long const& fop_size = _fdas_input_options->fop_size;
    // convolution overlap size
    unsigned long const& overlap_size = _fdas_input_options->overlap_size;
    // The number of times the CONV module loops through its Inverse Fourier Transforms
    unsigned long const& loop_num = static_cast<unsigned long>(_fdas_input_options->loop_num);

    // get raw c-pointer to pcie struct on the c code
    ifc_qdma_device* const _raw_ptr = _qdma_device_ptr.get();

    // reset of modules CONV, CLD, HSUM, CTRL, MSIX (see 7.2.2 p.78 FDAS manual)
    ifc_qdma_write64(_raw_ptr, RESET_ADDR, RESET_OFF    , _rbar);
    ifc_qdma_write64(_raw_ptr, RESET_ADDR, RESET_ON_NODDRIF, _rbar);
    ifc_qdma_write64(_raw_ptr, RESET_ADDR, RESET_OFF    , _rbar);

    // 100 micro seconds delay taken from the C fdas code
    std::this_thread::sleep_for(std::chrono::microseconds{100});

    // fdas configuration
    ifc_qdma_write64(_raw_ptr, CTRLBASE + PAGE_ADDR      , 0x0000UL, _rbar);
    ifc_qdma_write64(_raw_ptr, CTRLBASE + OVERLAP_ADDR   , overlap_size, _rbar);
    ifc_qdma_write64(_raw_ptr, CTRLBASE + FOP_NUM_ADDR   , fop_size-1, _rbar);
    ifc_qdma_write64(_raw_ptr, CTRLBASE + IFFT_LOOP_ADDR , loop_num, _rbar);
    ifc_qdma_write64(_raw_ptr, CONVBASE + ROW0_DELAY_ADDR, 0xD2UL, _rbar);

    PANDA_LOG_DEBUG << "Reset applied to FDAS core";

    if (ifc_qdma_read64(_raw_ptr, RESET_ADDR, _rbar) != 0)
    {
        PANDA_LOG_ERROR << "Failed to reset FDAS core";
        return (_c_function_state = -1);
    }

    // Setup Queue: Get all the descriptors

    // allocate I/O memory buffer for qdma request
    ifc_qdma_request **rawPtr = (ifc_qdma_request**)calloc(_queue_depth_buffers, sizeof(ifc_qdma_request *));

    // pre-filling, memory allocation to store I/O buffer requests
    for (std::uint32_t i = 0; i < _queue_depth_buffers ; ++i)
    {
        rawPtr[i] = ifc_request_malloc(_fdas_input_options->packet_size);
        if (rawPtr[i] == nullptr)
            return (_c_function_state = -1);
        rawPtr[i]->len = _fdas_input_options->packet_size;
    }

    // take the ownership of allocated memory
    std::unique_ptr<ifc_qdma_request*, CStructDeleter<ifc_qdma_request*>> smPtr
        (
            rawPtr
            , CStructDeleter<ifc_qdma_request*>(_queue_depth_buffers)
        );
    _qdma_tx_buffer = std::move(smPtr);

    queue_ctx* qctx = _context_dma_queue.get();
    qctx->req_buf = _qdma_tx_buffer.get();
    qctx->isempty = true;

    // This function in the source code performs a complete read and write
    // check of the registers in modules CTRL, CONV, MSIX, and HSUM, ending
    // with a general reset.
    // TODO: migrate this function to specialised method
    chk_fdas(_raw_ptr);

    // load fdas filter coefficients on the fpga
    load_coef_file(_fdas_filters_number, _fdas_filter_filename.c_str(),  _raw_ptr);

    // load hsum configuration
    load_config_txt_file(_fdas_hsum_filename.c_str(), _raw_ptr);

    return _c_function_state;
}

void FpgaAdapter::hugepage_location(void *hpaddr)
{
    set_hugepage_memory_location(hpaddr);
}

void* FpgaAdapter::hugepage_location()
{
    uint64_t *addr = (uint64_t *)get_hugepage_memory_location();
    return (void *)addr;
}

std::int32_t const& FpgaAdapter::find_fdas() const
{
    std::unique_ptr<device_info> dev_info_ptr (new device_info());
    device_info * dev_info = dev_info_ptr.get();

    /* Find FPGA device and populate dev_info, look for specific product ID if set */
    dev_info->product_id = _fpga_product_id;
    _c_function_state = fdas_find(dev_info);
    if ( _c_function_state == 0 )
    {
        bus_device_function(dev_info->slot_id);
        _fpga_device_id = dev_info->device_id;
        _fpga_product_id = dev_info->product_id;
        _fpga_core_version = dev_info->core_version;
        _fpga_top_version = dev_info->top_version;
        _fpga_core_revision = dev_info->core_revision;
        _fpga_top_revision = dev_info->top_revision;
    }
    return _c_function_state;
}

std::string const& FpgaAdapter::bus_device_function() const
{
    return _pcie_bus_device_function;
}

void FpgaAdapter::bus_device_function(std::string const& bdf) const
{
    // copy strings
    _pcie_bus_device_function = bdf ;
    // TODO: decide to remove '_pcie_bus_device_function' or keep it
    std::memcpy(_fdas_input_options->bdf, bdf.c_str(), bdf.length() + 1);
}

std::uint32_t const& FpgaAdapter::device_id() const
{
    return _fpga_device_id;
}

void FpgaAdapter::device_id(std::uint32_t const& dev_id)
{
    _fpga_device_id = dev_id;
}

void FpgaAdapter::product_id(std::uint32_t const& prod_id)
{
    _fpga_product_id = prod_id;
}

std::uint32_t const& FpgaAdapter::product_id() const
{
    return _fpga_product_id;
}

std::int32_t const& FpgaAdapter::bdf_port_number() const
{
    return _bdf_port_number;
}

std::int32_t const& FpgaAdapter::number_qdma_channels() const
{
    return _number_qdma_channels;
}

/// Configuring the input parameters with default values.
/// But all the configuration of the board and the algorithm
/// will be handled via xml files.
struct_flags FpgaAdapter::_initialise_struct_flags(std::string const& bdf_str="",std::string const& config_str="") const
{
    struct_flags input_flags ;
    std::memcpy(input_flags.bdf, bdf_str.c_str(), bdf_str.length() + 1);
    input_flags.request_size = default_request_size;
    input_flags.packet_size = default_payload;
    input_flags.packets = default_request_size/default_payload;
    input_flags.chnls = default_number_channels;
    input_flags.fop_size = default_fop_size;
    input_flags.overlap_size = default_overlap_size;
    input_flags.loop_num = default_loop_num;
    input_flags.wbar = default_number_pcie_bar_registers;
    input_flags.rbar = default_number_pcie_bar_registers;
    std::memcpy(input_flags.config_file, config_str.c_str(), config_str.length() + 1);

    return input_flags;
}

std::int32_t const& FpgaAdapter::_remove_hugepage_files()
{
    _c_function_state = 0;
    // nothing to remove if not initialised
    if (_qdma_device_ptr != nullptr)
    {
        std::vector<std::string> hugepage_files;
        const std::filesystem::path directory(hugepage_directory);
        const std::regex pattern(hugepage_filename_pattern);
        hugepage_filesystem_utils::filelist(directory, pattern, hugepage_files);

        for (const auto& file : hugepage_files)
        {
            try
            {
                std::filesystem::permissions(
                    file,
                    std::filesystem::perms::owner_read | std::filesystem::perms::owner_write,
                    std::filesystem::perm_options::add
                );

                //delete file
                std::filesystem::remove(file);
            }
            catch (const std::filesystem::filesystem_error& e )
            {
                std::cout << "cannot change permissions :" << file << ": " <<  e.what() << std::endl;
                _c_function_state = -1;
            }
        }
    }

    return _c_function_state;
}

std::int32_t const& FpgaAdapter::_fdas_verification() const
{
    return _c_function_state;
}

std::int32_t const& FpgaAdapter::copy_device_to_host(void* buffer)
{
    _c_function_state = 0;
    // nothing to transfer if not initialised
    if (_qdma_device_ptr != nullptr)
    {
        _c_function_state = get_data_from_device(buffer);
    }

    return _c_function_state;
}

void FpgaAdapter::buffer_transfer_size(std::uint32_t const& buffer_size)
{
    _buffer_transfer_size = buffer_size;
}

std::uint32_t const& FpgaAdapter::buffer_transfer_size() const
{
    return _buffer_transfer_size;
}

std::uint32_t const& FpgaAdapter::time_duration() const
{
    return _time_duration;
}
void FpgaAdapter::time_duration(std::uint32_t const& value)
{
    if (_time_duration != value)
    {
        _time_duration = value;
    }
}

} // namespace intel_fpga
} // namespace panda
} // namespace ska
