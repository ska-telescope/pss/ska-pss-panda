/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/arch/intel_agilex_fpga/HugePage.h"
#include "panda/Error.h"
#include "panda/Log.h"
#include <sstream>
#include <filesystem>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/file.h>
#include <cstring>


namespace ska {
namespace panda {
namespace intel_fpga {

HugePage::HugePage()
    : _page_number{0}
    , _page_address{nullptr}
    , _hugepage_filename(std::string(_prefix) + std::to_string(getpid()) + "_" + std::to_string(_page_number))
{
}


HugePage::HugePage(std::uint32_t const& page)
    : _page_number{page}
    , _page_address{nullptr}
    , _hugepage_filename(std::string(_prefix) + std::to_string(getpid()) + "_" + std::to_string(_page_number))
{
}

void* HugePage::create_hugepage()
{

    // Open a file descriptor for huge page allocation
    int fd = open(_hugepage_filename.c_str(), O_RDWR | O_CREAT| O_EXCL, 0666);
    if(fd < 0)
    {
        PANDA_LOG_ERROR << "Failed to open filename descriptor";
        return nullptr;
    }

    // Allocate a huge page
    _page_address = mmap(0
                        , _page_size
                        , PROT_READ | PROT_WRITE
                        , MAP_SHARED | MAP_POPULATE
                        , fd
                        , 0
                        );
    if(_page_address == MAP_FAILED)
    {
        PANDA_LOG_ERROR <<"Failed to allocate huge page";
        close(fd);
        return nullptr;
    }

    // Place a shared advisory lock, more than one process may hold a shared lock.
    // The inclusion of LOCK_NB makes a nonblocking request, in case incompatible
    // lock is held by another process.
    if(flock(fd, LOCK_SH | LOCK_NB) == -1)
    {
        PANDA_LOG_ERROR <<"Failed to lock huge page" << std::strerror(errno);
        close(fd);
        //returning a nullptr because returning the _page_address for an unlocked
        //page might lead to unexpected behaviour
        return nullptr;
    }

    close(fd);

    return _page_address;

}

/// remove file located in '/proc/hugepages' directory
void HugePage::remove_hugepage()
{
    if(_page_address == nullptr)
    {
        return;
    }

    /// unmap hugepage
    if(munmap(_page_address, _page_size) == -1)
    {
        PANDA_LOG_ERROR <<"Failed to unmap huge page"
                        << std::strerror(errno);
        return;
    }

    /// verify _filename exist
    std::filesystem::path _file(_hugepage_filename);
    if(!std::filesystem::is_regular_file(_hugepage_filename))
    {
        std::stringstream str;
        str << "The " + _hugepage_filename  + " not available" ;
        PANDA_LOG_ERROR << str.str();
        return;
    }

    /// open to put a lock on the file
    int fd = open(_hugepage_filename.c_str(), O_RDONLY);
    if(fd < 0)
    {
        PANDA_LOG_ERROR << "Failed to open filename descriptor "
                        << std::strerror(errno);
    }

    /// advisory lock to the file before to delete it.
    const bool success = flock(fd, LOCK_EX | LOCK_NB) == 0;
    if(success)
    {
        std::filesystem::remove(_hugepage_filename);
    }
    else
    {
        PANDA_LOG_ERROR << "Failed to lock the file ";
        close(fd);
        return;
    }
    close(fd);
}

void* HugePage::page_address() const
{
    return _page_address;
}

std::uint32_t const& HugePage::page_number() const
{
    return _page_number;
}

std::string_view HugePage::filename() const
{
        return _hugepage_filename;
}

void HugePage::page_number(std::uint32_t const& value)
{
    _page_number = value;
}

HugePage::~HugePage()
{
    if(_page_address != nullptr)
        remove_hugepage();
}

} //intel_fpga
} //panda
} //ska