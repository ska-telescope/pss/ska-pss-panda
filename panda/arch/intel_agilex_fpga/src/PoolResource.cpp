/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/arch/intel_agilex_fpga/PoolResource.h"

namespace ska {
namespace panda {

PoolResource<intel_fpga::Rtl>::PoolResource(std::shared_ptr<intel_fpga::FpgaAdapter> const& fpga, unsigned int device_id)
    : Fpga(fpga, device_id)
{
}

PoolResource<intel_fpga::Rtl>::~PoolResource()
{
}

void PoolResource<intel_fpga::Rtl>::configure(DeviceConfig<intel_fpga::Rtl> const& intel_fpga_config)
{
    _buffer_transfer_size = intel_fpga_config.buffer_transfer_size();
    _fdas_hsum_filename = intel_fpga_config.hsum_filename();
    _fdas_filter_filename = intel_fpga_config.filters_filename();
    _fdas_filters_number = intel_fpga_config.number_of_filters();
    _duration = intel_fpga_config.duration();
    // load configuration variables into the fpga adapater
    load_fdas_configuration();
}

} // namespace panda
} // namespace ska
