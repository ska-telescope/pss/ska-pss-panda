/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/arch/intel_agilex_fpga/test/DevicePointerTest.h"
#include "panda/arch/intel_agilex_fpga/IntelAgilexFpga.h"
#include "panda/arch/intel_agilex_fpga/DevicePointer.h"
#include "panda/arch/intel_agilex_fpga/DevicePointer.h"
#include "panda/System.h"

namespace ska {
namespace panda {
namespace test {


DevicePointerTest::DevicePointerTest()
    : ::testing::Test()
{
}

DevicePointerTest::~DevicePointerTest()
{
}

void DevicePointerTest::SetUp()
{
}

void DevicePointerTest::TearDown()
{
}

TEST_F(DevicePointerTest, test_write_read)
{
    std::vector<int> data { 0, 1, 2, 3, 4};
    typedef intel_fpga::DevicePointer<int> Type;
    typedef typename ArchitectureType<Type>::type Arch;
    panda::System<Arch> system;
    ASSERT_GT(system.count<Arch>(), std::size_t(0)) << "No devices found to test";
    for([[maybe_unused]]auto const& device : system.resources<Arch>())
    {
        Type host_ptr(data.size());
        host_ptr.hugepage_write(0, *data.begin(), data.size());
        std::vector<int> out(data.size(), 0 );
        host_ptr.hugepage_read(0, *out.data(), out.size());
        ASSERT_EQ(data, out);
    }
}


} // namespace test
} // namespace panda
} // namespace ska
