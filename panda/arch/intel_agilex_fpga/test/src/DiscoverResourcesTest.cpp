/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/arch/intel_agilex_fpga/test/DiscoverResourcesTest.h"
#include "panda/arch/intel_agilex_fpga/DiscoverResources.h"


namespace ska {
namespace panda {
namespace intel_fpga {
namespace test {


DiscoverResourcesTest::DiscoverResourcesTest()
    : ::testing::Test()
{
}

DiscoverResourcesTest::~DiscoverResourcesTest()
{
}

void DiscoverResourcesTest::SetUp()
{
}

void DiscoverResourcesTest::TearDown()
{
}

TEST_F(DiscoverResourcesTest, test_fpga_board_found)
{
    /// RTL: Register Transactions Layer architecture
    typedef intel_fpga::Rtl Arch;

    panda::DiscoverResources<Arch> discover;
    std::vector<std::shared_ptr<PoolResource<Arch>>> resources(discover());
    unsigned int compare_val=0;
    ASSERT_GT(resources.size(), compare_val);
}

} // namespace test
} // namespace intel_fpga
} // namespace panda
} // namespace ska
