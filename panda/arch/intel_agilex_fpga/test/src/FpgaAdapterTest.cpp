/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/arch/intel_agilex_fpga/test/FpgaAdapterTest.h"
#include "panda/arch/intel_agilex_fpga/IntelAgilexFpga.h"
#include "panda/arch/intel_agilex_fpga/FpgaAdapter.h"
#include "panda/System.h"
#include "panda/test/gtest.h"
#include "panda/Error.h"
#include "panda/Log.h"
#include <fstream>
#include <filesystem>
#include <memory_resource>
#include <numeric>

namespace ska {
namespace panda {
namespace intel_fpga {
namespace test {

// helper struct for copying data from a file into a memory area
struct HugePageData
{
    inline static void save(void * _addr)
    {
        /// open file with DM data
        std::string dat_file {panda::test::test_file("fdas.dat")};
        std::ifstream ifs(dat_file, std::ios::in | std::ios::binary );
        if (!ifs)
        {
            FAIL() << "Not DM file found";
        }

        /// get length of file:
        ifs.seekg (0, ifs.end);
        int length = ifs.tellg();
        ifs.seekg (0, ifs.beg);

        /// copy data from file in a hugepage area
        ifs.read((char *)_addr, length);
        ifs.close();
    }
};


FpgaAdapterTest::FpgaAdapterTest()
{
}

FpgaAdapterTest::~FpgaAdapterTest()
{
}

void FpgaAdapterTest::SetUp()
{
    const char * bdf = "0000:b1:00.0";
    std::string hsum_config { panda::test::test_file("config.txt") };
    std::string filter_config { panda::test::test_file("filter.dat") };
    unsigned int number_filters = 42;
    unsigned int device_id = 0;
    unsigned int product_id = 0;

    _test_fpga = std::make_unique<ska::panda::intel_fpga::FpgaAdapter>(bdf
                                                            , filter_config.c_str()
                                                            , hsum_config.c_str()
                                                            , number_filters
                                                            , device_id
                                                            , product_id
                                                            );

    // create another hugepage area to get data back from device and compare it
    _hugepage = std::make_shared<ska::panda::intel_fpga::HugePage>(1);
    _hugepage->create_hugepage();
    _second_page = (uint64_t *)_hugepage->page_address();
    // copy DM data into continuous memory
    HugePageData::save(_second_page);
}

void FpgaAdapterTest::TearDown()
{
}



TEST_F(FpgaAdapterTest, test_intel_lib_qdma_strncpy)
{
    const char* src = "test";
    char dest[] = "random";
    int src_len = strlen(src);
    int dest_len = strlen(dest);

    ifc_qdma_strncpy(dest, dest_len, src, src_len);
    ASSERT_STREQ(dest, src);
}

TEST_F(FpgaAdapterTest, test_get_set_fdas_input_parameters_default_constructor)
{

    std::unique_ptr<ska::panda::intel_fpga::FpgaAdapter> intel_fpga_adapter(
        std::make_unique<ska::panda::intel_fpga::FpgaAdapter>()
        );

    const char * test_bdf = "0000:01:00.0";
    intel_fpga_adapter->bus_device_function(test_bdf);
    ASSERT_STREQ(intel_fpga_adapter->bus_device_function().c_str(), test_bdf);

    // filename with fdas filter coefficients
    const char *filters_file= "fdas_filter_coefficients.dat";
    // default name of the file with filter coefficients
    ASSERT_STREQ(intel_fpga_adapter->fdas_filter_filename().c_str(), "template.dat");
    intel_fpga_adapter->fdas_filter_filename(filters_file);
    ASSERT_STREQ(intel_fpga_adapter->fdas_filter_filename().c_str(),filters_file);

    // hsum configuration file
    ASSERT_STREQ(intel_fpga_adapter->fdas_hsum_filename().c_str(),"hsum_config.txt");
    const char *hsum_file = "hsum_config_file.txt";
    intel_fpga_adapter->fdas_hsum_filename(hsum_file);
    ASSERT_STREQ(intel_fpga_adapter->fdas_hsum_filename().c_str(),hsum_file);

    // number of filters should be 42
    ASSERT_EQ(intel_fpga_adapter->fdas_filters_number(), 42);
    std::uint32_t filters_number{64};
    intel_fpga_adapter->fdas_filters_number(filters_number);
    ASSERT_EQ(intel_fpga_adapter->fdas_filters_number(),filters_number);

}

TEST_F(FpgaAdapterTest, test_get_set_fdas_input_parameters_specialised_constructor)
{
    // bus-device-function
    const char * some_bdf = "0000:02:00.0";
    _test_fpga->bus_device_function(some_bdf);
    ASSERT_STREQ(_test_fpga->bus_device_function().c_str(), some_bdf);

    // fdas filter coefficients filename
    const char *filters_file= "fdas_filter_coefficients.dat";
    _test_fpga->fdas_filter_filename(filters_file);
    ASSERT_STREQ(_test_fpga->fdas_filter_filename().c_str(),filters_file);

    // hsum configuration file
    const char *hsum_file = "hsum_config_file.txt";
    _test_fpga->fdas_hsum_filename(hsum_file);
    ASSERT_STREQ(_test_fpga->fdas_hsum_filename().c_str(),hsum_file);

    // number of filters should be 42
    unsigned int number_filters = 42;
    ASSERT_EQ(_test_fpga->fdas_filters_number(), number_filters);
    std::uint32_t filters_number{64};
    _test_fpga->fdas_filters_number(filters_number);
    ASSERT_EQ(_test_fpga->fdas_filters_number(),filters_number);
}

TEST_F(FpgaAdapterTest, initialise)
{
    // the bdf port number should be negative before initialisation
    ASSERT_LT(_test_fpga->bdf_port_number(),0);

    // number of queue dma should be zero
    ASSERT_EQ(_test_fpga->number_qdma_channels(), 0);

    int val = _test_fpga->initialise();
    ASSERT_EQ(val,0);

    // the bdf port number should be greater or equal zero
    ASSERT_GE(_test_fpga->bdf_port_number(),0);

    // in this particular fpga board number of channels must be 512
    ASSERT_EQ(_test_fpga->number_qdma_channels(), 512);

}

TEST_F(FpgaAdapterTest, test_hugepages_creation)
{

    // verify the returning address of hugepage location
    std::uint64_t * addr = (std::uint64_t *)(_test_fpga->hugepage_location());
    ASSERT_TRUE(addr != NULL);

    // verify hugepages were created

}

TEST_F(FpgaAdapterTest, fpga_find)
{
    std::string path("/sys/class/uio/");
    std::string token("PCI_SLOT_NAME");
    std::string str;
    std::string bdf;
    std::string uio_drv;
    int val;

    // Find BDF from filesystem i.e. "/sys/class/uio/uio%d/device/uevent",uio_id

    // Find uio driver in filesystem
    std::filesystem::path directorypath = "/sys/class/uio/";
    for (const auto& entry :
             std::filesystem::directory_iterator(directorypath))
    {
        uio_drv = entry.path().filename();
        if (uio_drv.substr(0,3)== "uio")
        break;
    }

    ASSERT_STREQ(uio_drv.substr(0,3).c_str(), "uio");

    // open uevent file, which contains uio driver info
    path.append(uio_drv);
    path.append("/device/uevent");

    std::ifstream infile(path, std::ios::in | std::ios::binary);
    if (!infile)
    {
    FAIL() << "Couldn't open " << path << std::endl;
    }
    else
    {

        // find BDF in uevent file
        while (std::getline(infile, str, '='))
        {
            if (str == "PCI_SLOT_NAME")
                break;
            std::getline(infile, str);
        }
        std::getline(infile, bdf);

        // check FpgaAdapter information is correct
        val = _test_fpga->product_id();
        ASSERT_EQ(val, 0xFDA5);

        val = _test_fpga->device_id();
        ASSERT_EQ(val, 0);

        // check against BDF from filesystem
        ASSERT_STREQ(_test_fpga->bus_device_function().c_str(), bdf.c_str());

        infile.close();
    }
}

TEST_F(FpgaAdapterTest, fpga_filter_filename)
{
    const char *input_filename{"fdas_filter_filename.txt"};
    _test_fpga->fdas_filter_filename(input_filename);

    std::string const& outuput_filename = _test_fpga->fdas_filter_filename();

    ASSERT_STREQ(input_filename, outuput_filename.c_str());
}

TEST_F(FpgaAdapterTest, fpga_run_fdas_algorithm)
{

    // initialise FDAS and DDRs
    _test_fpga->initialise();

    uint64_t *addr = (uint64_t *)_test_fpga->hugepage_location();
    // copy DM data into continuous memory
    HugePageData::save(addr);

    uint32_t res = _test_fpga->run_fdas_algorithm();
    ASSERT_EQ(res, 0);

    std::vector<std::vector<std::int32_t>> const& hsum_list = _test_fpga->candidate_lists();

    // we know values of candidate on row 21
    std::vector<int> v1{1, 4, 2684, 0, 1142022264};
    bool val = (hsum_list[21] == v1);
    EXPECT_TRUE(val);

    // The following part could be done in an additional test, but
    // It has to be done here, because if it is done in another test,
    // the FPGA needs to be initialised, which will also reinitialise
    // the DDR's FPGA and data on fpga ram would be erased.

    if (!val)
    {
        // This comparison is done only if there was a failure getting
        // the candidate list.
        // If there was a discrepance with the candidate list
        // we want then to compare data in the FPGA device with data
        // from the file to find some discrepancy in case of failure.

        const size_t length = static_cast<size_t>(
            std::filesystem::file_size(panda::test::test_file("fdas.dat"))
            );

        std::cout << "Comparing host-device data ..." << std::endl;
        // reset to zero hugepage
        memset(addr,0,length);
        // read data back from device
        _test_fpga->copy_device_to_host(addr);

        char const *_dev_ptr  = (char *)addr;
        char const *_host_ptr = (char *)_second_page;

        std::cout << "Starting comparing host-device data ..." << std::endl;

        // comparing the values allocated in both memory vectors
        for(std::size_t i=0; i < ((std::size_t)length-104); ++i)
        {
            ASSERT_EQ(*(_dev_ptr+i), *(_host_ptr+i))
            << i
            << " Expected value: " << *(_dev_ptr+i)
            << ", Actual value: " << *(_host_ptr+i);
        }

        std::cout << "Finishing comparing host-device data ..." << std::endl;
    }

}


TEST_F(FpgaAdapterTest, fpga_cleanup_board)
{
    int val = _test_fpga->cleanup_fpga_board();
    ASSERT_EQ(val, 0);
}

} // namespace test
} // namespace intel_fpga
} // namespace panda
} // namespace ska
