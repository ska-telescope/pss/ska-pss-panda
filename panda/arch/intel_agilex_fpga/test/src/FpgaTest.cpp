/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/arch/intel_agilex_fpga/test/FpgaTest.h"
#include "panda/arch/intel_agilex_fpga/IntelAgilexFpga.h"
#include "panda/arch/intel_agilex_fpga/FpgaAdapter.h"
#include "panda/System.h"
#include "panda/test/gtest.h"
#include "panda/Error.h"
#include "panda/Log.h"


namespace ska {
namespace panda {
namespace intel_fpga {
namespace test {


FpgaTest::FpgaTest()
{
}

FpgaTest::~FpgaTest()
{
}

void FpgaTest::SetUp()
{
}

void FpgaTest::TearDown()
{
}

TEST_F(FpgaTest, test_fpga)
{
    /// RTL: Register Transactions Layer
    typedef intel_fpga::Rtl Arch;
    panda::System<Arch> system;
    ASSERT_GT(system.count<Arch>(), std::size_t(0)) << "No devices found to test";
    auto fpga_device = system.resources<Arch>().at(0);

    std::string str = fpga_device->bdf_id();
    PANDA_LOG << "BDF Id: " << str ;

    unsigned int device_id = fpga_device->device_id();
    PANDA_LOG << "Device Id: " << device_id ;

}

} // namespace test
} // namespace intel_fpga
} // namespace panda
} // namespace ska
