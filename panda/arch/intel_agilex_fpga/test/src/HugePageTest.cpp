/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/arch/intel_agilex_fpga/test/HugePageTest.h"
#include "panda/arch/intel_agilex_fpga/HugePage.h"
#include "panda/Copy.h"
#include <memory_resource>
#include <numeric>

namespace ska {
namespace panda {
namespace test {


HugePageTest::HugePageTest()
    : ::testing::Test()
{
}

HugePageTest::~HugePageTest()
{
}

void HugePageTest::SetUp()
{
}

void HugePageTest::TearDown()
{
}

TEST_F(HugePageTest, test_hugepage_default_constructor)
{
    std::shared_ptr<intel_fpga::HugePage> _hugepage = std::make_shared<intel_fpga::HugePage>();

    // testing default values, notice still the hugepage has not been allocated yet.
    ASSERT_EQ(_hugepage->page_number(), 0);
    ASSERT_EQ(_hugepage->page_address(), nullptr);
}

TEST_F(HugePageTest, test_hugepage_specialised_constructor)
{
    std::shared_ptr<intel_fpga::HugePage> _hugepage = std::make_shared<intel_fpga::HugePage>(0);

    // testing default values configured with specialised constructor
    ASSERT_EQ(_hugepage->page_number(), 0);
    ASSERT_EQ(_hugepage->page_address(), nullptr);
}

TEST_F(HugePageTest, test_hugepage_allocating_a_page)
{
    constexpr std::size_t memblock{1<<25}; // 32 MB
    std::size_t _capacity_size{100};
    std::shared_ptr<intel_fpga::HugePage> _hugepage = std::make_shared<intel_fpga::HugePage>(0);

    // here we the hugepage is created
    _hugepage->create_hugepage();

    std::uint64_t* hpaddr = (std::uint64_t*)_hugepage->page_address();

    // use hugepage as memory pool
    std::pmr::monotonic_buffer_resource pool{hpaddr, memblock};

    // vector that uses hugepage memory pool with a predefined capacity
    std::vector<int, std::pmr::polymorphic_allocator<int>> buff{_capacity_size, &pool};

    //preallocate on the hugepage
    buff.reserve(_capacity_size);
    std::iota(buff.begin(), buff.end(), 0);

    // vector to copy data from hugepage
    std::vector<int> out(buff.size());

    panda::copy(buff.begin(), buff.end(), out.begin());

    int const *_buff_ptr = buff.data();
    int const *_out_ptr = out.data();

    // comparing the values allocated in both memory vectors
    for(std::size_t i=0; i < buff.size(); ++i)
    {
        ASSERT_EQ(*(_buff_ptr+i), *(_out_ptr+i));
    }

}

} // namespace test
} // namespace panda
} // namespace ska
