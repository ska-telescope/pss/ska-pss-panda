/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2024 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/arch/intel_agilex_fpga/test/PoolResourceTest.h"
#include "panda/arch/intel_agilex_fpga/IntelAgilexFpga.h"
#include "panda/arch/intel_agilex_fpga/PoolResource.h"
#include "panda/System.h"

namespace ska {
namespace panda {
namespace intel_fpga {
namespace test {


PoolResourceTest::PoolResourceTest()
    : ::testing::Test()
{
}

PoolResourceTest::~PoolResourceTest()
{
}

void PoolResourceTest::SetUp()
{
}

void PoolResourceTest::TearDown()
{
}

TEST_F(PoolResourceTest, test_run)
{
    /// RTL: Register Transactions Layer architecture
    typedef intel_fpga::Rtl Arch;
    panda::System<Arch> system;

    unsigned called=0;
    if (system.system_count<Arch>())
    {

        /// there is only one device in the system
        auto device = system.resources<Arch>().at(0);

        auto test_function = [&](panda::PoolResource<Arch>& d)
                             {
                                 ASSERT_EQ(&*device, &d);
                                 ++called;
                             };
        device->run(test_function);

        ASSERT_EQ(system.system_count<Arch>(), called);
    }

}

} // namespace test
} // namespace intel_fpga
} // namespace panda
} // namespace ska
