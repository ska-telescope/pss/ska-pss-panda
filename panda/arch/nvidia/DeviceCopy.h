/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_ARCH_NVIDIA_DEVICECOPY_H
#define SKA_PANDA_ARCH_NVIDIA_DEVICECOPY_H

#include "panda/arch/nvidia/CudaDevicePointer.h"
#include "panda/arch/nvidia/PoolResource.h"
#include "panda/DeviceCopy.h"
#ifdef SKA_PANDA_ENABLE_CUDA
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wpragmas"
#include <cuda_runtime.h>
#include <thrust/device_vector.h>
#include <thrust/device_ptr.h>
#include <thrust/device_reference.h>
#include <thrust/copy.h>
#pragma GCC diagnostic pop
#endif // SKA_PANDA_ENABLE_CUDA
#include <iterator>

namespace ska {
namespace panda {

/**
 * @brief copy a range to the current cuda device reusing pre-allocated device memory
 * @details the device memory must be sufficiently large to contain the data. No checks are made.
 */
template<typename IteratorType>
void copy(IteratorType&& begin, IteratorType&& end, nvidia::CudaDevicePointer<typename IteratorType::value_type>&);

/**
 * @brief extends panda::copy for Cuda devices
 */
template<typename SrcIteratorTypeTag, typename EndIteratorTypeTag, typename DstIteratorTypeTag>
struct DeviceCopy<Cpu, nvidia::Cuda, SrcIteratorTypeTag, EndIteratorTypeTag, DstIteratorTypeTag>
{
    public:
        /**
         * @brief perform the copy from host to cuda architectures
         */
        template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
        static typename std::remove_reference<DstIteratorType>::type copy(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it);

#ifdef SKA_PANDA_ENABLE_CUDA
        /**
         * @brief version that can handle thrust::device_ptr
         */
        template<typename SrcIteratorType, typename EndIteratorType, typename... DstType>
        thrust::device_ptr<DstType...>& copy(SrcIteratorType&& begin, EndIteratorType&& end, thrust::device_ptr<DstType...>& dst_it);
#endif // SKA_PANDA_ENABLE_CUDA
};

template<typename SrcIteratorTypeTag, typename EndIteratorTypeTag, typename DstIteratorTypeTag>
struct DeviceCopy<nvidia::Cuda, Cpu, SrcIteratorTypeTag, EndIteratorTypeTag, DstIteratorTypeTag>
{
    public:
        /**
         * @brief perform the copy from cuda architectures to host
         */
        template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
        static typename std::remove_reference<DstIteratorType>::type copy(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it);

#ifdef SKA_PANDA_ENABLE_CUDA
        /**
         * @brief version that can handle thrust::device_ptr
         */
        template<typename DstIteratorType, typename... SrcType>
        typename std::remove_reference<DstIteratorType>::type copy(thrust::device_ptr<SrcType...>& begin, thrust::device_ptr<SrcType...> const& end, DstIteratorType&& dst_it);
#endif // SKA_PANDA_ENABLE_CUDA
};

//Copy within the device
template<typename SrcIteratorTypeTag, typename EndIteratorTypeTag, typename DstIteratorTypeTag>
struct DeviceCopy<nvidia::Cuda, nvidia::Cuda, SrcIteratorTypeTag, EndIteratorTypeTag, DstIteratorTypeTag>
{
    public:

        /**
         * @brief perform the copy within cuda architectures
         */
        template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
        static typename std::remove_reference<DstIteratorType>::type copy(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it);

#ifdef SKA_PANDA_ENABLE_CUDA
        /**
         * @brief version that can handle thrust::device_ptr
         */
        template<typename... DstType, typename... SrcType>
        thrust::device_ptr<DstType...>& copy(thrust::device_ptr<SrcType...>& begin, thrust::device_ptr<SrcType...> const& end, thrust::device_ptr<DstType...>& dst_it);
#endif // SKA_PANDA_ENABLE_CUDA
};

// --------- DeviceCopyPostProcessor --------
namespace nvidia {

/**
 * @brief copy a range to the current cuda device allocating device memmory in the process
 */
template<typename IteratorType>
CudaDevicePointer<typename IteratorType::value_type> device_copy(IteratorType&& begin, IteratorType&& end);


/**
 * @brief
 *    Helper class for transfering data to a CUDA device - use the device_copy and copy functions rather than these directly
 *
 * @details
 *
 */
template<typename IteratorType, typename IteratorCategory=typename IteratorType::iterator_category>
class DeviceCopy
{
    public:
        /**
         * @brief copy to a pre-existing device pointer
         */
        static void copy_to_device(IteratorType&& begin, IteratorType&& end, CudaDevicePointer<typename IteratorType::value_type>& cuda_ptr);

        /**
         * @brief creae a suitable device ptr and copy the memory to it
         */
        static CudaDevicePointer<typename IteratorType::value_type> copy_to_device(IteratorType&& begin, IteratorType&& end);
};

// optimisation for contiguous CPU memory
template<typename IteratorType>
class DeviceCopy<IteratorType, std::random_access_iterator_tag>
{
    public:
        /**
         * @brief copy to a pre-existing device pointer
         */
        static void copy_to_device(IteratorType&& begin, IteratorType&& end, CudaDevicePointer<typename IteratorType::value_type>& cuda_ptr);

        /**
         * @brief creae a suitable device ptr and copy the memory to it
         */
        static CudaDevicePointer<typename IteratorType::value_type> copy_to_device(IteratorType&& begin, IteratorType&& end);
};

} // namespace nvidia
} // namespace panda
} // namespace ska
#include "panda/arch/nvidia/detail/DeviceCopy.cpp"

#endif // SKA_PANDA_ARCH_NVIDIA_DEVICECOPY_H
