/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2021 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_NVIDIA_DEVICEITERATOR_H
#define SKA_PANDA_NVIDIA_DEVICEITERATOR_H


#include "panda/arch/nvidia/Architecture.h"
#include "panda/DeviceIterator.h"
#include <cuda_runtime.h>
#include <thrust/device_ptr.h>
#include <boost/iterator/iterator_adaptor.hpp>
#include <type_traits>

namespace ska {
namespace panda {

/**
 * @brief
 *   Specialisation for Cuda
 */
template<typename T>
class DeviceIterator<nvidia::Cuda, T*> : public boost::iterator_adaptor<DeviceIterator<nvidia::Cuda, T*>, T*>
{
        typedef boost::iterator_adaptor<DeviceIterator<nvidia::Cuda, T*>, T*> BaseT;

    public:
        typedef typename BaseT::value_type value_type;
        typedef typename BaseT::pointer pointer;
        typedef typename BaseT::reference reference;

    public:
        explicit DeviceIterator(T* ptr);

        template <class OtherType>
        DeviceIterator( DeviceIterator<nvidia::Cuda, OtherType> const& other
                      , typename std::enable_if<
                              std::is_convertible<OtherType*,T*>::value
                            , bool
                        >::type = true
        );

        // conversion operator
        operator thrust::device_ptr<T>();

    private:
       friend class boost::iterator_core_access;
};

/**
 * @brief convert DevicePointer<Cuda, T*> to the underlying T* type
 * @details to be compatible with the thrusr::raw_pointer_cast
 */
template<typename T>
T* raw_pointer_cast(DeviceIterator<nvidia::Cuda, T*> const& it)
{
    return &*it;
}

} // namespace panda
} // namespace ska
#include "detail/DeviceIterator.cpp"

#endif // SKA_PANDA_NVIDIA_DEVICEITERATOR_H
