/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wpragmas"
#include <cuda_runtime.h>
#include <thrust/device_vector.h>
#include <thrust/device_ptr.h>
#include <thrust/device_reference.h>
#include <thrust/fill.h>
#pragma GCC diagnostic pop

#include "ThrustUtils.h"

namespace ska {
namespace panda {
namespace nvidia {

struct DeviceFillThrustImplBase
{
    template<typename SrcIteratorType, typename EndIteratorType, typename ValueT>
    static inline
    void fill(SrcIteratorType&& begin, EndIteratorType&& end, ValueT const& value)
    {
        thrust::fill(std::forward<SrcIteratorType>(begin)
                   , std::forward<EndIteratorType>(end)
                   , value);
    }
};

template<typename IteratorT, typename Enable = void>
struct DeviceFillImpl
{
    template<typename SrcIteratorType, typename EndIteratorType, typename ValueT>
    static inline
    void fill(SrcIteratorType&& begin, EndIteratorType&& end, ValueT const& value)
    {
        DeviceFillImpl<thrust::device_ptr<ValueT>>::fill( static_cast<thrust::device_ptr<ValueT>>(begin)
                                                        , static_cast<thrust::device_ptr<ValueT>>(end)
                                                        , value);
    }
};

template<typename IteratorT>
struct DeviceFillImpl<IteratorT, EnableIfIsThrustIterator<IteratorT>>
{
    template<typename SrcIteratorType, typename EndIteratorType, typename ValueT>
    static
    void fill(SrcIteratorType&& begin, EndIteratorType&& end, ValueT const& value);
};

#ifdef __CUDACC__
template<typename IteratorT>
template<typename SrcIteratorType, typename EndIteratorType, typename ValueT>
void DeviceFillImpl<IteratorT, EnableIfIsThrustIterator<IteratorT>>::fill(SrcIteratorType&& begin, EndIteratorType&& end, ValueT const& value)
{
    DeviceFillThrustImplBase::fill(std::forward<SrcIteratorType>(begin)
                           , std::forward<EndIteratorType>(end)
                           , value);
}
#endif // __CUDACC__

} // namespace nvidia

template<typename SrcIteratorType, typename EndIteratorType, typename ValueT>
void DeviceFill<nvidia::Cuda>::fill(SrcIteratorType&& begin, EndIteratorType&& end, ValueT const& value)
{
    nvidia::DeviceFillImpl<SrcIteratorType>::fill(std::forward<SrcIteratorType>(begin)
                                                , std::forward<EndIteratorType>(end)
                                                , value);
}

} // namespace panda
} // namespace ska
