/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wpragmas"
#include <cuda_runtime.h>
#include <thrust/device_vector.h>
#include <thrust/device_ptr.h>
#include <thrust/device_reference.h>
#include <thrust/generate.h>
#pragma GCC diagnostic pop

#include "ThrustUtils.h"
#include <iterator>
#include <type_traits>

namespace ska {
namespace panda {
namespace nvidia {

struct DeviceGenerateThrustImplBase
{
    template<typename SrcIteratorT, typename EndIteratorT, typename GeneratorT>
    static inline
    void generate(SrcIteratorT&& begin, EndIteratorT&& end, GeneratorT&& generator)
    {
        thrust::generate(std::forward<SrcIteratorT>(begin)
                   , std::forward<EndIteratorT>(end)
                   , std::forward<GeneratorT>(generator));
    }
};

template<typename IteratorT, typename Enable = void>
struct DeviceGenerateImpl
{
    template<typename SrcIteratorT, typename EndIteratorT, typename GeneratorT>
    static inline
    void generate(SrcIteratorT&& begin, EndIteratorT&& end, GeneratorT&& generator)
    {
        typedef typename std::remove_pointer<typename std::iterator_traits<typename std::remove_reference<SrcIteratorT>::type>::value_type>::type ValT;
        DeviceGenerateImpl<thrust::device_ptr<ValT>>::generate( static_cast<thrust::device_ptr<ValT>>(begin)
                                                              , static_cast<thrust::device_ptr<ValT>>(end)
                                                              , std::forward<GeneratorT>(generator));
    }
};

template<typename IteratorT>
struct DeviceGenerateImpl<IteratorT, EnableIfIsThrustIterator<IteratorT>>
{
    template<typename SrcIteratorT, typename EndIteratorT, typename GeneratorT>
    static
    void generate(SrcIteratorT&& begin, EndIteratorT&& end, GeneratorT&& generator);
};

#ifdef __CUDACC__
template<typename IteratorT>
template<typename SrcIteratorT, typename EndIteratorT, typename GeneratorT>
void DeviceGenerateImpl<IteratorT, EnableIfIsThrustIterator<IteratorT>>::generate(SrcIteratorT&& begin, EndIteratorT&& end, GeneratorT&& generator)
{
    DeviceGenerateThrustImplBase::generate(std::forward<SrcIteratorT>(begin)
                           , std::forward<EndIteratorT>(end)
                           , std::forward<GeneratorT>(generator));
}
#endif // __CUDACC__

} // namespace nvidia

template<typename GeneratorT>
template<typename SrcIteratorT, typename EndIteratorT, typename GeneratorArgT>
void DeviceGenerate<nvidia::Cuda, GeneratorT, EnableIfIsArchitectureType<GeneratorT, nvidia::Cuda>>::generate(SrcIteratorT&& begin, EndIteratorT&& end, GeneratorArgT&& generator)
{
    nvidia::DeviceGenerateImpl<SrcIteratorT>::generate(std::forward<SrcIteratorT>(begin)
                                                , std::forward<EndIteratorT>(end)
                                                , std::forward<GeneratorArgT>(generator));
}

} // namespace panda
} // namespace ska
