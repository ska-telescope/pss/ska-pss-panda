/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_ARCH_NVIDIA_THRUSTUTILS_H
#define SKA_PANDA_ARCH_NVIDIA_THRUSTUTILS_H

#include "panda/Architecture.h"
#include "panda/Version.h"
#include "panda/arch/nvidia/DeviceIterator.h"
#include <type_traits>
#include <iterator>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wpragmas"
#include <cuda_runtime.h>
#include <thrust/device_ptr.h>
#include <thrust/device_reference.h>
#pragma GCC diagnostic pop

namespace ska {
namespace panda {
namespace nvidia {
namespace detail {

template<typename Iterator, typename RefType = typename std::iterator_traits<typename std::remove_reference<Iterator>::type>::reference>
struct is_thrust_iterator : public std::false_type
{
};

template<typename Iterator, typename... T>
struct is_thrust_iterator<Iterator, thrust::device_reference<T...>> : public std::true_type
{
};

} // namespace detail

template<typename IteratorT>
using EnableIfIsThrustIterator = typename std::enable_if<detail::is_thrust_iterator<IteratorT>::value>::type;

// cuda thrust device_ptr code in older versions of thrust cannot handle calls to functions for non-trivial types
// without using nvcc
template<typename IteratorT>
using EnableIfIsNotThrustCppCompat = typename std::enable_if<!std::is_trivially_copy_constructible<typename std::iterator_traits<typename std::remove_reference<IteratorT>::type>::value_type>::value>::type;

//  we determine if the version of thrust supports the current tool chain
#ifdef __CUDACC__
#define __THRUST_DEVICE_PTR_ON_CPP_WORKING__
#else // __CUDACC__
#if PANDA_CUDA_MAJOR_VERSION > 10
#define __THRUST_DEVICE_PTR_ON_CPP_WORKING__
#else
#undef __THRUST_DEVICE_PTR_ON_CPP_WORKING__
#endif // PANDA_CUDA_MAJOR_VERSION
#endif // __CUDACC__

} // namespace nvidia


#ifdef SKA_PANDA_ENABLE_CUDA
// Define thrust typeas as Cuda arch types
template<typename... T, typename Enable>
struct ArchitectureType<thrust::device_ptr<T...>, Enable>
{
    typedef nvidia::Cuda type;
};

template<typename T>
struct ArchitectureType<T, typename std::enable_if<nvidia::detail::is_thrust_iterator<T>::value
                                                 && !is_device_iterator<T>::value
                                                   >::type>
{
    typedef nvidia::Cuda type;
};

#endif // SKA_PANDA_ENABLE_CUDA

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_ARCH_NVIDIA_THRUSTUTILS_H
