/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/arch/nvidia/ArchitectureUtilities.h"

/**
 * The checks here are not runtime unit tests but compile-time checks of the
 * IsCompatible functionality. The use of static_assert means that the checks
 * do not necessarily need to be in a test fixture.
 */

namespace ska {
namespace panda {
namespace nvidia {
namespace test {

    static_assert( IsCompatible<nvidia::Cuda, nvidia::Cuda>::value,
                   "Equal Architectures should be compatible" );

    // Any device capability is compatible with general Cuda
    static_assert( IsCompatible<nvidia::Cuda, nvidia::DeviceCapability<2, 0, 3000>>::value,
                   "Specific capability should be compatible with general Cuda" );

    // but not the other way around
    static_assert( !IsCompatible<nvidia::DeviceCapability<2, 0, 3000>, nvidia::Cuda>::value,
                   "Cuda should *not* be compatible with a given capability" );

    // Version matching
    static_assert( IsCompatible<nvidia::DeviceCapability<2, 0, 3000>,
                                nvidia::DeviceCapability<2, 0, 3000>>::value,
                   "Equal versions & memory memory should be compatible" );

    static_assert( IsCompatible<nvidia::DeviceCapability<2, 0, 3000>,
                                nvidia::DeviceCapability<3, 0, 3000>>::value,
                   "Lower required major version and equal memory should be compatible" );

    static_assert( IsCompatible<nvidia::DeviceCapability<2, 0, 2999>,
                                nvidia::DeviceCapability<3, 0, 3000>>::value,
                   "Lower required major version and less memory should be compatible" );

    static_assert( !IsCompatible<nvidia::DeviceCapability<2, 0, 3001>,
                                 nvidia::DeviceCapability<3, 0, 3000>>::value,
                   "Lower required major version but more memory required should not be compatible" );

}
}
}
}
