/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "DeviceGenerateTest.h"
#include "panda/arch/nvidia/DeviceGenerate.h"
#include "panda/test/DeviceGenerateTester.h"


namespace ska {
namespace panda {
namespace nvidia {
namespace test {


DeviceGenerateTest::DeviceGenerateTest()
    : BaseT()
{
}

DeviceGenerateTest::~DeviceGenerateTest()
{
}

} // namespace test
} // namespace nvidia

namespace test {

typedef ::testing::Types<panda::test::DeviceGenerateTesterTraits<nvidia::Cuda, uint8_t>> CudaDeviceGenerateTraitsTypes;
INSTANTIATE_TYPED_TEST_SUITE_P(BasicCudaDeviceGenerate, DeviceGenerateTester, CudaDeviceGenerateTraitsTypes);

} // namespace test
} // namespace panda
} // namespace ska
