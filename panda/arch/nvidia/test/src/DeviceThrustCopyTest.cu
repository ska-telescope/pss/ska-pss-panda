#include "panda/test/CopyTester.h"
#include "panda/arch/nvidia/DeviceCopy.h"
#include "panda/arch/nvidia/test/TestDevice.h"
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wpragmas"
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wpedantic"
#include <cuda_runtime.h>
#include <thrust/device_vector.h>
#pragma GCC diagnostic pop

namespace ska {
namespace panda {
namespace test {
    struct ThrustCopyTraits {
        typedef nvidia::Cuda Arch;
        template<typename T>
        static thrust::device_vector<T> contiguous_device_memory(std::size_t size) {
            return thrust::device_vector<T>(size);
        }
    };

    INSTANTIATE_TYPED_TEST_SUITE_P(CudaThrust, CopyTester, ThrustCopyTraits);

} // namespace test
} // namespace panda
} // namespace ska
