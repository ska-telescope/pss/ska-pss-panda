/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "panda/AggregationBuffer.h"
#include "panda/Log.h"
#include "panda/Copy.h"
#include <cstring>
#include <algorithm>
#include <utility>
#include <iterator>

namespace ska {
namespace panda {

template<typename DataType, typename BufferType>
template<typename Arg, typename... Args
       , typename std::enable_if<!std::is_same<typename std::decay<Arg>::type, AggregationBuffer<DataType, BufferType>>::value, bool>::type>
AggregationBuffer<DataType, BufferType>::AggregationBuffer(Arg&& arg, Args&& ... args)
    : _buffer(new BufferType(std::forward<Arg>(arg), std::forward<Args>(args)...))
{
    reset();
    _end_ptr = _buffer->end();
}

template<typename DataType, typename BufferType>
AggregationBuffer<DataType, BufferType>::AggregationBuffer(AggregationBuffer<DataType,BufferType>&& b)
    : _buffer(std::move(b._buffer))
    , _objects(std::move(b._objects))
    , _current_ptr(std::move(b._current_ptr))
    , _end_ptr(std::move(b._end_ptr))
    , _previous_ptr(std::move(b._previous_ptr))
    , _data_boundary(std::move(b._data_boundary))
    , _first_chunk_offset(std::move(b._first_chunk_offset))
{
}

template<typename DataType, typename BufferType>
void AggregationBuffer<DataType, BufferType>::swap(AggregationBuffer& b)
{
    std::swap(_buffer, b._buffer);
    std::swap(b._objects, _objects);
    std::swap(b._current_ptr, _current_ptr);
    std::swap(b._end_ptr, _end_ptr);
    std::swap(b._previous_ptr, _previous_ptr);
    std::swap(b._data_boundary, _data_boundary);
    std::swap(b._first_chunk_offset, _first_chunk_offset);
}

template<typename DataType, typename BufferType>
BufferType& AggregationBuffer<DataType, BufferType>::buffer()
{
    return *_buffer;
}

template<typename DataType, typename BufferType>
BufferType const& AggregationBuffer<DataType, BufferType>::buffer() const
{
    return *_buffer;
}

template<typename DataType, typename BufferType>
AggregationBuffer<DataType, BufferType>::~AggregationBuffer()
{
}

template<typename DataType, typename BufferType>
void AggregationBuffer<DataType, BufferType>::reset()
{
    _current_ptr = _buffer->begin();
    _previous_ptr = _current_ptr;
    _objects.clear();
    _data_boundary.clear();
    _first_chunk_offset = 0U;
}

template<typename DataType, typename BufferType>
typename AggregationBuffer<DataType, BufferType>::iterator AggregationBuffer<DataType, BufferType>::data(unsigned offset)
{
    return _buffer->begin() + offset;
}

template<typename DataType, typename BufferType>
typename AggregationBuffer<DataType, BufferType>::const_iterator AggregationBuffer<DataType, BufferType>::data(unsigned offset) const
{
    return _buffer->cbegin() + offset;
}

template<typename DataType, typename BufferType>
typename AggregationBuffer<DataType, BufferType>::iterator AggregationBuffer<DataType, BufferType>::begin()
{
    return _buffer->begin();
}

template<typename DataType, typename BufferType>
typename AggregationBuffer<DataType, BufferType>::const_iterator AggregationBuffer<DataType, BufferType>::begin() const
{
    return _buffer->cbegin();
}

template<typename DataType, typename BufferType>
typename AggregationBuffer<DataType, BufferType>::const_iterator AggregationBuffer<DataType, BufferType>::cbegin() const
{
    return _buffer->cbegin();
}

template<typename DataType, typename BufferType>
typename AggregationBuffer<DataType, BufferType>::iterator AggregationBuffer<DataType, BufferType>::end()
{
    return _current_ptr;
}

template<typename DataType, typename BufferType>
typename AggregationBuffer<DataType, BufferType>::const_iterator AggregationBuffer<DataType, BufferType>::end() const
{
    return _current_ptr;
}

template<typename DataType, typename BufferType>
typename AggregationBuffer<DataType, BufferType>::const_iterator AggregationBuffer<DataType, BufferType>::cend() const
{
    return _current_ptr;
}

template<typename DataType, typename BufferType>
typename AggregationBuffer<DataType, BufferType>::Composition const& AggregationBuffer<DataType, BufferType>::composition() const
{
    return _objects;
}

template<typename DataType, typename BufferType>
std::size_t AggregationBuffer<DataType, BufferType>::data_size() const
{
    return std::distance(_buffer->cbegin(), cend());
}

template<typename DataType, typename BufferType>
std::size_t AggregationBuffer<DataType, BufferType>::capacity() const
{
    return std::distance(_buffer->cbegin(), _buffer->cend());
}

template<typename DataType, typename BufferType>
template<typename IteratorType>
void AggregationBuffer<DataType, BufferType>::insert( IteratorType& begin_iter, IteratorType const & end_iter, std::shared_ptr<const DataType> const& object ) {
    auto tmp = _current_ptr;
    while( _current_ptr < _end_ptr) {
        *_current_ptr = *begin_iter;
        ++_current_ptr;
        if( ++begin_iter == end_iter ) break;
    }
    if( tmp != _current_ptr ) {
        _previous_ptr = tmp;
        if(_objects.empty() || _objects.back().get() != object.get())
        {
            _objects.push_back(object);
            _data_boundary.push_back(tmp); // allows us to calculate how many bytes associated with this insert
        }
    }
}

template<typename DataType, typename BufferType>
void AggregationBuffer<DataType, BufferType>::transfer(std::size_t size, AggregationBuffer<DataType, BufferType>& dest) const
{
    std::size_t max = std::min(data_size(), std::min(size, dest.remaining_capacity())) ;

    if(max) {
        if(max < size) {
            PANDA_LOG_WARN << "insufficient space in buffer to copy " << size << " elements";
        }

        // copy the end of the current buffer to the beginning of the destination
        dest._previous_ptr = dest._current_ptr;
        //std::memcpy(dest._current_ptr, _end_ptr - max
        //            , max*sizeof(AggregationBuffer<DataType, BufferType>::value_type));

        panda::copy(_end_ptr - max, _end_ptr, dest._current_ptr);
        dest._current_ptr+=max;

        std::size_t nelements = _current_ptr - _previous_ptr;

        unsigned obj_count=1;

        // calculate the number of objects from the end of the compostion list
        // we need to transfer to the destination
        auto it = _data_boundary.rbegin();
        while(++it != _data_boundary.rend() && nelements < max)
        {
            ++obj_count;
            nelements = _current_ptr - *it;
        }

        unsigned const start_index = _objects.size() - obj_count;
        dest._first_chunk_offset = _current_ptr - max - _data_boundary[start_index];

        for(unsigned i=start_index; i < _objects.size(); ++i) {
            dest._objects.push_back(_objects[i]);
        }
        dest._data_boundary.push_back(dest._previous_ptr);
        if(start_index +1 != _objects.size()) {
            dest._data_boundary.push_back(dest._previous_ptr + (_data_boundary[start_index+1] - _data_boundary[start_index] - dest._first_chunk_offset));
        }
        auto offset_ptr = dest._data_boundary.back();
        for(unsigned i=start_index + 2; i < _objects.size(); ++i) {
            dest._data_boundary.push_back(offset_ptr + (_data_boundary[i] - _data_boundary[start_index +1]));
        }

        for(int i=start_index; i !=0; --i) {
            // deal with mutliple inserts from same block
            if( _objects[i] != _objects[i-1]) break;
            dest._first_chunk_offset += _data_boundary[i] - _data_boundary[i-1];
        }
    }
}

template<typename DataType, typename BufferType>
std::size_t AggregationBuffer<DataType, BufferType>::offset_first_block() const
{
    return _first_chunk_offset;
}

template<typename DataType, typename BufferType>
void AggregationBuffer<DataType, BufferType>::offset_first_block(std::size_t offset)
{
    _first_chunk_offset = offset;
}

template<typename DataType, typename BufferType>
std::size_t AggregationBuffer<DataType, BufferType>::remaining_capacity() const
{
    return std::distance(_current_ptr, _end_ptr);
}

} // namespace panda
} // namespace ska
