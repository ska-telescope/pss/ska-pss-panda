/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 The SKA organisation
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/CommaSeparatedOptions.h"
#include <boost/tokenizer.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/throw_exception.hpp>
#include <boost/program_options/errors.hpp>
#include <boost/program_options/value_semantic.hpp>

namespace ska {
namespace panda {

template<typename T, typename ContainerType>
std::istream& operator>>(std::istream& in, CommaSeparatedOptions<T, ContainerType>& value)
{
    return value.read(in);
}

template<typename T, typename ContainerType>
CommaSeparatedOptions<T, ContainerType>::CommaSeparatedOptions()
{
}

template<typename T, typename ContainerType>
CommaSeparatedOptions<T, ContainerType>::~CommaSeparatedOptions()
{
}

template<typename T, typename ContainerType>
CommaSeparatedOptions<T, ContainerType>::CommaSeparatedOptions(ContainerType const& vals)
    : _values(vals)
{
}

template<typename T, typename ContainerType>
std::istream& CommaSeparatedOptions<T, ContainerType>::read(std::istream& in)
{
    std::string tokens;
    in >> tokens;
    for(auto const& token : boost::tokenizer<boost::char_separator<char>>(tokens, boost::char_separator<char>(","))) {
        _values.emplace_back(boost::lexical_cast<T>(token));
    }
    return in;
}

template<typename T, typename ContainerType>
void CommaSeparatedOptions<T, ContainerType>::emplace_back(T&& t) 
{
    _values.emplace_back(std::move(t));
}

} // namespace panda
} // namespace ska

namespace boost { 
namespace program_options {

template<typename T, typename ContainerType>
void validate(boost::any& v,
              const std::vector<std::string>& values,
              ska::panda::CommaSeparatedOptions<T, ContainerType>*, int)
{
    if (v.empty()) {
        v = boost::any(ska::panda::CommaSeparatedOptions<T, ContainerType>());
    }

    //std::vector<T>* tv = boost::any_cast< std::vector<T> >(&v);
    ska::panda::CommaSeparatedOptions<T, ContainerType>* tv = boost::any_cast< ska::panda::CommaSeparatedOptions<T, ContainerType> >(&v);
    assert(NULL != tv);
    for (unsigned i = 0; i < values.size(); ++i)
    {
        for(auto const& token : boost::tokenizer<boost::char_separator<char>>(values[i], boost::char_separator<char>(","))) {
            try {
                /* We call validate so that if user provided
                 * a validator for class T, we use it even
                 * when parsing vector<T>.  */
                boost::any a;
                std::vector<std::basic_string<char> > cv;
                cv.push_back(token);
                validate(a, cv, (T*)0, 0);
                tv->emplace_back(boost::any_cast<T>(a));
            }
            catch(const boost::bad_lexical_cast& /*e*/) {
                boost::throw_exception(boost::program_options::invalid_option_value(token));
            }
        }
    }
}

} // namespace boost
} // namespace program_options
