/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/Config.h"
#include "panda/Version.h"
#include "panda/ConfigFile.h"
#include "panda/PropertyTreeUtils.h"
#include <boost/filesystem.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/version.hpp>
#include <iostream>

namespace ska {
namespace panda {


template<typename... Architectures>
typename Config<Architectures...>::SystemType Config<Architectures...>::_system;

template<typename... Architectures>
Config<Architectures...>::Config(std::string app_name, std::string short_description)
    : BasicAppConfig(app_name, short_description)
    , _pool_manager(_system, _pool_manager_config)
{
    // default sections
    add(_pool_manager_config);
}

template<typename... Architectures>
void Config<Architectures...>::add_options(OptionsDescriptionEasyInit&)
{
}

template<typename... Architectures>
Config<Architectures...>::~Config()
{
}

template<typename... Architectures>
typename Config<Architectures...>::PoolManagerType const& Config<Architectures...>::pool_manager() const
{
    return _pool_manager;
}

template<typename... Architectures>
typename Config<Architectures...>::PoolManagerType& Config<Architectures...>::pool_manager()
{
    return _pool_manager;
}

template<typename... Architectures>
typename Config<Architectures...>::SystemType& Config<Architectures...>::system()
{
    return _system;
}

} // namespace panda
} // namespace ska
