/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_CONFIGURABLETASKBASE_H
#define SKA_PANDA_CONFIGURABLETASKBASE_H

#include "panda/TupleUtilities.h"

namespace ska {
namespace panda {

namespace detail {
    // -------------------------------
    // helpers to sort the data types so that they are grouped together by type
    // This is required to help the implementation
    // Ordering is: panda::Method, panda::SubmitMethod, and then OtherDataTypes
    // -------------------------------
    template<typename... DataTypes>
    struct SortDataTypesHelper
    {
        typedef std::tuple<> MethodTypes;
        typedef std::tuple<> SubmitMethodTypes;
        typedef std::tuple<> SyncMethodTypes;
        typedef std::tuple<> SignatureTypes;
        typedef std::tuple<DataTypes...> OtherTypes;
    };

    template<typename T, typename... DataTypes>
    struct SortDataTypesHelper<T, DataTypes...> : public SortDataTypesHelper<DataTypes...>
    {
        typedef SortDataTypesHelper<DataTypes...> BaseT;
        typedef typename join_tuples<std::tuple<T>, typename BaseT::OtherTypes>::type OtherTypes;
    };

    template<typename... SignatureParamsT, typename... DataTypes>
    struct SortDataTypesHelper<Signature<SignatureParamsT...>, DataTypes...> : protected SortDataTypesHelper<DataTypes...>
    {
        typedef SortDataTypesHelper<DataTypes...> BaseT;
        typedef typename join_tuples<std::tuple<Signature<SignatureParamsT...>>, typename BaseT::SignatureTypes>::type SignatureTypes;
    };

    template<typename... DataTypes>
    struct SortSignatureTypes : protected SortDataTypesHelper<DataTypes...>
    {
        typedef SortDataTypesHelper<DataTypes...> BaseT;
        typedef typename join_tuples<typename BaseT::SignatureTypes, typename BaseT::OtherTypes>::type type;
    };

    template<typename Functor, typename... MethodParamsT, typename... DataTypes>
    struct SortDataTypesHelper<Method<Functor, MethodParamsT...>, DataTypes...> : public SortDataTypesHelper<DataTypes...>
    {
            typedef SortDataTypesHelper<DataTypes...> BaseT;

        private:
            template<typename TupleType>
            struct TupleExpander;

            template<typename... Types>
            struct TupleExpander<std::tuple<Types...>> {
                typedef Method<Functor, Types...> type;
            };

        public:
            typedef typename join_tuples<std::tuple<typename TupleExpander<typename SortSignatureTypes<MethodParamsT...>::type>::type>, typename BaseT::MethodTypes>::type MethodTypes;
    };

    template<typename... SubmitMethodParamsT, typename... DataTypes>
    struct SortDataTypesHelper<SubmitMethod<SubmitMethodParamsT...>, DataTypes...> : public SortDataTypesHelper<DataTypes...>
    {
        typedef SortDataTypesHelper<DataTypes...> BaseT;
        typedef typename join_tuples<std::tuple<SubmitMethod<SubmitMethodParamsT...>>, typename BaseT::SubmitMethodTypes>::type SubmitMethodTypes;
    };

    template<typename... DataTypes>
    struct SortDataTypes : public SortDataTypesHelper<DataTypes...>
    {
        private:
            typedef SortDataTypesHelper<DataTypes...> BaseT;
            typedef typename std::conditional<std::tuple_size<typename BaseT::OtherTypes>::value == 0
                                            , std::tuple<>
                                            , std::tuple<typename TypeTransfer<typename BaseT::OtherTypes, SubmitMethod>::type>
                                            >::type OtherTypeAsSubmitMethod;
        public:
            typedef typename join_tuples<typename BaseT::SignatureTypes
                                       , typename BaseT::MethodTypes
                                       , typename BaseT::SubmitMethodTypes
                                       , OtherTypeAsSubmitMethod
                                       >::type type;
    };

} // namespace detail

template<class ImplType, class PoolManager, typename AsyncHandler, typename... DataTypes>
class ConfigurableTaskBase
{
      //  template<class Arch, typename... Ts>
      //  using ResultType = decltype(std::declval<ImplType&>().exec_algo(std::declval<panda::PoolResource<Arch>>(), std::declval<Ts>()...));

    public:
        ConfigurableTaskBase(PoolManager, AsyncHandler handler);
        virtual ~ConfigurableTaskBase();

        /**
         * @brief submit the configured task to the pool
         * @param DataTypeArgs should match the DataTypes type parameters, although they may be r or l-values
         */
        template<typename... DataTypeArgs>
        inline
        std::shared_ptr<ResourceJob> submit(DataTypeArgs&&...);

    protected:
        template<class ImplSpecialisation, typename Algorithms, typename... Functors>
        void do_set_algorithms(Algorithms&&, Functors&&...);

    protected:
        AsyncHandler _handler;
        PoolManager _pool;
        ImplType _null_default;
        ImplType* _impl;
};

template<class ImplType, class PoolManager, typename AsyncHandler, typename DataType, typename... DataTypes>
class ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler, DataType, DataTypes...>
    : public ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler>
{
        typedef ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler> BaseT;

    public:
        ConfigurableTaskBase(PoolManager, AsyncHandler handler);

    protected:
        template<typename AlgoTuple>
        void set_algorithms(AlgoTuple&&);
};

// --------------------------------------------------------------------------------
// -- SubmitMethod specialisations
// --------------------------------------------------------------------------------
template<class ImplType, class PoolManager, typename AsyncHandler, typename... SubmitSignature, typename... DataTypes>
class ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler, SubmitMethod<SubmitSignature...>, DataTypes...>
    : public ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler, DataTypes...>
{
        typedef ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler, DataTypes...> BaseT;

    protected:
        template<typename AlgoTuple>
        void set_algorithms(AlgoTuple&&);

    public:
        using ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler, DataTypes...>::ConfigurableTaskBase;

        /**
         * @brief launch the algorithm on a specific device (i.e not submittng to a pool)
         * @details the handler will be called in the usual way
         *          Only devices supported by the PoolManager can be used
         */
        template<class Arch>
        inline
        void operator()(std::shared_ptr<panda::PoolResource<Arch>> const& resource
                      , detail::DataTypeFwd<SubmitSignature>...) const;

        /**
         * @brief launch the algorithm on a specific device (i.e not submittng to a pool)
         * @details the handler will be called in the usual way
         *          Only devices supported by the PoolManager can be used
         */
        template<class Arch>
        inline
        void operator()(panda::PoolResource<Arch>& resource
                      , detail::DataTypeFwd<SubmitSignature>...) const;

};

template<class ImplType, class PoolManager, typename AsyncHandler, typename... SubmitSignature, typename... SubmitSig2, typename... DataTypes>
class ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler, SubmitMethod<SubmitSignature...>, SubmitMethod<SubmitSig2...>, DataTypes...>
    : public ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler, SubmitMethod<SubmitSig2...>, DataTypes...>
{
        typedef ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler, SubmitMethod<SubmitSig2...>, DataTypes...> BaseT;

    protected:
        template<typename AlgoTuple>
        void set_algorithms(AlgoTuple&&);

    public:
        using ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler, SubmitMethod<SubmitSig2...>, DataTypes...>::ConfigurableTaskBase;
        using BaseT::operator();

        /**
         * @brief launch the algorithm on a specific device (i.e not submittng to a pool)
         * @details the handler will be called in the usual way
         *          Only devices supported by the PoolManager can be used
         */
        template<class Arch>
        inline
        void operator()(std::shared_ptr<panda::PoolResource<Arch>> const& resource
                      , detail::DataTypeFwd<SubmitSignature>...) const;

        /**
         * @brief launch the algorithm on a specific device (i.e not submittng to a pool)
         * @details the handler will be called in the usual way
         *          Only devices supported by the PoolManager can be used
         */
        template<class Arch>
        inline
        void operator()(panda::PoolResource<Arch>& resource
                      , detail::DataTypeFwd<SubmitSignature>...) const;

};

template<class ImplType, class PoolManager, typename AsyncHandler, typename Functor, typename... SigArgs, typename... DataTypes>
class ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler, Method<Functor, Signature<SigArgs...>>, DataTypes...>
    : public ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler, DataTypes...>
{
        typedef ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler, DataTypes...> BaseT;

    public:
        template<typename... Args>
        ConfigurableTaskBase(PoolManager, AsyncHandler handler, Functor const& functor, Args&&...);

        // Call the Functor
        void operator()(SigArgs...);
        void operator()(SigArgs...) const;

    protected:
        template<typename AlgoTuple>
        void set_algorithms(AlgoTuple&&);

        template<typename ImplSpecialisation, typename Algorithms, typename... Functors>
        inline
        void do_set_algorithms(Algorithms&&, Functors&&...);

    private:
        Functor _functor;
};

// ----------------------------------------------------
// ----- Specialisation to support Method declarations with multiple Signatures on a single functor ---------------
// ----------------------------------------------------
template<class ImplType, class PoolManager, typename AsyncHandler, typename Functor, typename... SigArgs, typename... OtherMethodArgs, typename... DataTypes>
class ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler, Method<Functor, Signature<SigArgs...>, OtherMethodArgs...>, DataTypes...>
    : public ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler, Method<Functor, OtherMethodArgs...>, DataTypes...>
{
        typedef ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler, Method<Functor, OtherMethodArgs...>, DataTypes...> BaseT;

    public:
        template<typename... Args>
        ConfigurableTaskBase(PoolManager, AsyncHandler handler, Functor const& functor, Args&&...);

        // Call the Functor
        void operator()(SigArgs...);
        void operator()(SigArgs...) const;

        // Expose the operator() of the base class
        using BaseT::operator();

    protected:
        template<typename AlgoTuple>
        void set_algorithms(AlgoTuple&&);
};

template<class ImplType, class PoolManager, typename AsyncHandler, typename Functor, typename... FunctorArgs, typename... DataTypes>
class ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler, Method<Functor, FunctorArgs...>, DataTypes...>
    : public ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler, Method<Functor, Signature<FunctorArgs...>>, DataTypes...>
{
        typedef ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler, Method<Functor, Signature<FunctorArgs...>>, DataTypes...> BaseT;

    public:
        template<typename... Args>
        ConfigurableTaskBase(PoolManager, AsyncHandler handler, Functor const& functor, Args&&...);

    protected:
        template<typename AlgoTuple>
        void set_algorithms(AlgoTuple&&);
};

template<class ImplType, class PoolManager, typename AsyncHandler, typename Functor, typename... FunctorArgs, typename Functor2, typename... Functor2Args, typename... DataTypes>
class ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler, Method<Functor, FunctorArgs...>, Method<Functor2, Functor2Args...>, DataTypes...>
    : public ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler, Method<Functor2, Functor2Args...>, DataTypes...>
{
        typedef ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler, Method<Functor2, Functor2Args...>, DataTypes...> BaseT;

    public:
        template<typename... Args>
        ConfigurableTaskBase(PoolManager, AsyncHandler handler, Functor const& functor, Args&&...);

        // Call the Functor
        void operator()(FunctorArgs...);
        void operator()(FunctorArgs...) const;

        // Expose the operator() of the base class
        using BaseT::operator();

    protected:
        template<typename AlgoTuple>
        void set_algorithms(AlgoTuple&&);

        template<typename ImplSpecialisation, typename Algorithms, typename... Functors>
        inline
        void do_set_algorithms(Algorithms&&, Functors&&...);

    private:
        Functor _functor;
};

// ----------------------------------------------------
// ----- Specialisation to support top level Signature declarations
// ----------------------------------------------------
template<class ImplType, class PoolManager, typename AsyncHandler, typename... SigArgs>
class ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler, Signature<SigArgs...>>
    : public ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler>
{
        typedef ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler> BaseT;

    public:
        using ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler>::ConfigurableTaskBase;

        // Call function with the signature
        template<typename Arch>
        void operator()(panda::PoolResource<Arch>&, SigArgs...);

    protected:
        template<typename AlgoTuple>
        void set_algorithms(AlgoTuple&&);

};

template<class ImplType, class PoolManager, typename AsyncHandler, typename... SigArgs, typename... DataTypes>
class ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler, Signature<SigArgs...>, DataTypes...>
    : public ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler, DataTypes...>
{
        typedef ConfigurableTaskBase<ImplType, PoolManager, AsyncHandler, DataTypes...> BaseT;

    public:
        template<typename... Args>
        ConfigurableTaskBase(PoolManager, AsyncHandler handler, Args&&...);

        // Call function with the signature
        template<typename Arch>
        void operator()(panda::PoolResource<Arch>&, SigArgs...);

        // Expose the operator() of the base class
        using BaseT::operator();

    protected:
        template<typename AlgoTuple>
        void set_algorithms(AlgoTuple&&);

};


// ------------------------------------
namespace detail {

    template<typename PoolManager, class AsyncHandler, typename DataTypeTuple>
    struct ConfigurableTaskBaseSortedTuple;

    template<typename PoolManager, class AsyncHandler, typename... DataTypes>
    struct ConfigurableTaskBaseSortedTuple<PoolManager, AsyncHandler, std::tuple<DataTypes...>>
    {
        typedef panda::ConfigurableTaskBase<detail::ConfigurableTaskImplBase<PoolManager, DataTypes...>, PoolManager, AsyncHandler, DataTypes...> type;
    };

} // namespace detail


// ------------------------------------
// generator for the BaseType from the parameters passed down to the ConfigurableTask class by the user
// ------------------------------------
template<typename PoolManager, class AsyncHandler, typename... DataTypes>
using ConfigurableTaskBaseType = typename detail::ConfigurableTaskBaseSortedTuple<PoolManager
                                                        , AsyncHandler
                                                        , typename detail::SortDataTypes<DataTypes...>::type
                                                        >::type;

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_CONFIGURABLETASKBASE_H
