/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/Connection.h"
#include "panda/Error.h"
#include "panda/Buffer.h"
#include "panda/IpAddress.h"
#include "panda/detail/ConnectionImpl.h"

#include <boost/asio.hpp>
#include <memory>


namespace ska {
namespace panda {

// -------------------------------------------------------------------
// --- class implementation details                                ---
// -------------------------------------------------------------------

template<typename DerivedType, typename ConnectionTraits>
template<typename... Args>
SocketConnection<DerivedType, ConnectionTraits>::SocketConnection(Args&&... args)
    : _pimpl(std::make_shared<PimplT>(std::forward<Args>(args)...))
{
}


template<typename DerivedType, typename ConnectionTraits>
SocketConnection<DerivedType, ConnectionTraits>::~SocketConnection()
{
}

template<typename DerivedType, typename ConnectionTraits>
void SocketConnection<DerivedType, ConnectionTraits>::set_remote_end_point(IpAddress const& endpoint)
{
    set_remote_end_point(endpoint.end_point<EndPointType>());
}

template<typename DerivedType, typename ConnectionTraits>
void SocketConnection<DerivedType, ConnectionTraits>::set_remote_end_point(typename SocketConnection<DerivedType, ConnectionTraits>::EndPointType const& endpoint)
{
    _pimpl->set_remote_end_point(endpoint);
}

template<typename DerivedType, typename ConnectionTraits>
typename SocketConnection<DerivedType, ConnectionTraits>::EndPointType const& SocketConnection<DerivedType, ConnectionTraits>::remote_end_point() const
{
    return _pimpl->remote_end_point();
}

template<typename DerivedType, typename ConnectionTraits>
typename SocketConnection<DerivedType, ConnectionTraits>::ErrorPolicyType& SocketConnection<DerivedType, ConnectionTraits>::policy()
{
    return _pimpl->policy();
}

template<typename DerivedType, typename ConnectionTraits>
typename SocketConnection<DerivedType, ConnectionTraits>::SocketType& SocketConnection<DerivedType, ConnectionTraits>::socket()
{
    return _pimpl->socket();
}

template<typename DerivedType, typename ConnectionTraits>
typename SocketConnection<DerivedType, ConnectionTraits>::SocketType const& SocketConnection<DerivedType, ConnectionTraits>::socket() const
{
    return _pimpl->socket();
}

template<typename DerivedType, typename ConnectionTraits>
typename SocketConnection<DerivedType, ConnectionTraits>::EngineType& SocketConnection<DerivedType, ConnectionTraits>::engine()
{
    return _pimpl->engine();
}

template<typename DerivedType, typename ConnectionTraits>
void SocketConnection<DerivedType, ConnectionTraits>::send(SocketConnection<DerivedType, ConnectionTraits>::BufferType const buffer, SendHandler const& handler)
{
    _pimpl->send(std::move(buffer), handler);
}

template<typename DerivedType, typename ConnectionTraits>
void SocketConnection<DerivedType, ConnectionTraits>::receive(BufferType buffer, ReceiveHandler handler)
{
    _pimpl->receive(std::move(buffer), std::move(handler));
}


template<typename DerivedType, typename ConnectionTraits>
void SocketConnection<DerivedType, ConnectionTraits>::close()
{
    _pimpl->close();
}

// treat all constructor args as being for the base class
template<typename ConnectionTraits>
template<typename... Args>
Connection<ConnectionTraits>::Connection(Args&&... args)
    : BaseT(std::forward<Args>(args)...)
{
}

// treat all constructor args as being for the base class
template<typename ConnectionTraits, typename ClockType>
template<typename... Args>
TimedConnection<ConnectionTraits, ClockType>::TimedConnection(Args&&... args)
    : BaseT(std::forward<Args>(args)...)
    , _rate_monitor(new DataRateMonitor<ClockType>())
{
}

template<typename ConnectionTraits, typename ClockType>
void TimedConnection<ConnectionTraits, ClockType>::send(typename BaseT::BufferType const buffer, typename BaseT::SendHandler const& handler)
{
    auto tracker = (*_rate_monitor)(buffer.size());
    BaseT::send(std::move(buffer), [tracker, handler](panda::Error const& e) mutable { 
        if(!e) tracker.tock();
        handler(e);
    });
}

template<typename ConnectionTraits, typename ClockType>
DataRateMonitor<ClockType> const& TimedConnection<ConnectionTraits, ClockType>::monitor() const
{
    return *_rate_monitor;
}

} // namespace panda
} // namespace ska
