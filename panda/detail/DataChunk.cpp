/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/DataChunk.h"


namespace ska {
namespace panda {

template<typename DerivedType>
DataChunk<DerivedType>::DataChunk()
{
}

template<typename DerivedType>
DataChunk<DerivedType>::~DataChunk()
{
}

template<typename DerivedType>
template<typename... ConstructorArgs>
std::shared_ptr<DerivedType> DataChunk<DerivedType>::make_shared(ConstructorArgs&&... args)
{
    return std::static_pointer_cast<DerivedType>(std::make_shared<DerivedTypeWrapper>(std::forward<ConstructorArgs>(args)...));
}

template<typename DerivedType>
template<typename DeleterType, typename... ConstructorArgs>
std::shared_ptr<DerivedType> DataChunk<DerivedType>::make_shared_with_deleter(DeleterType deleter, ConstructorArgs&&... args)
{
    return std::shared_ptr<DerivedType>(new DerivedTypeWrapper(std::forward<ConstructorArgs>(args)...), std::move(deleter));
}

} // namespace panda
} // namespace ska
