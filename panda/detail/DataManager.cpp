/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/DataManager.h"
#include "panda/TupleUtilities.h"
#include "panda/Abort.h"
#include "panda/Log.h"
#include <tuple>
#include <numeric>

namespace ska {
namespace panda {

// --------------------------------------------------------------------
// Helper functions to contruct a suitable tuple when new data is added
// Iterates over the tuple and will insert a new value if the types match
// otherwise makes a copy of the reference tuple.
// --------------------------------------------------------------------
template<typename T, typename ReplaceType>
inline T const& copy_or_replace(T const& copy, ReplaceType const &)
{
    return copy;
}

template<typename T>
inline T const& copy_or_replace(T const&, T const& replacement)
{
    return replacement;
}

template<std::size_t I, typename Container, typename ReplaceType>
struct _copy_replace_type
{
    _copy_replace_type(Container const& container, Container& copy, ReplaceType const& replacement);
};

template<std::size_t I, typename Container, typename ReplaceType>
_copy_replace_type<I, Container, ReplaceType>::_copy_replace_type(Container const& container, Container& copy, ReplaceType const& replacement)
{
    std::get<I>(copy)=copy_or_replace(std::get<I>(container), replacement);
    _copy_replace_type<I-1, Container, ReplaceType>(container, copy, replacement);
}

template<typename Container, typename ReplaceType>
struct _copy_replace_type<0, Container, ReplaceType>
{
    _copy_replace_type(Container const& container, Container& copy, ReplaceType const& replacement)
    {
        std::get<0>(copy)=copy_or_replace(std::get<0>(container), replacement);
    }
};

// --------------------------------------------------------------------

template<typename Producer>
DataManager<Producer>::DataManager(Producer& chunker)
    : _abort(false)
    , _stop(false)
    , _awaiting_count(0)
    , _chunker(chunker)
    , _complete_set(InserterType::size, 0)
{
    _chunker.set_chunk_manager(this);
}

template<typename Producer>
DataManager<Producer>::~DataManager()
{
    stop();
    while(_awaiting_count) {
        std::this_thread::yield();
    }
}

template<typename Producer>
void DataManager<Producer>::stop()
{
    _chunker.stop();
    _abort = true;
    _stop = true;
}

namespace {
// implementation helper classes for push_service_chunk
template<typename T>
struct SetZeroIfNotServiceData {
   inline void operator()(std::size_t& i, unsigned& b) const {
        b = 0;
        ++i;
    }
};

template<typename T>
struct SetZeroIfNotServiceData<ServiceData<T>> {
    inline void operator()(std::size_t& i, unsigned&) const {
        ++i;
    }
};

} //namespace


template<typename Producer>
template<typename T>
void DataManager<Producer>::push_service_chunk(T const& chunk)
{
    // new service data - reset the data set complete count for stream data
    /*
    for(std::size_t i=0; i < InserterType::size; ++i) {
        if(0==_complete_set_template[i]) {
            _complete_set[i] = 0;
        }
    }
    */
    std::size_t i=0;
    panda::ForEach<typename Producer::DataSetType, SetZeroIfNotServiceData>::exec(i, _complete_set[i]);
    push_chunk<T>(chunk);
}

namespace {
    // helpers for push_chunk
    // These classes reset the _complete_set to match that represented by service data
    template<typename DataT>
    struct ResetValue {
        static constexpr unsigned value = 0;
    };

    template<typename T>
    struct ResetValue<ServiceData<T>> {
        static constexpr unsigned value = 1;
    };

    template<typename Producer, int I>
    struct ResetContainer {
        template<typename Container>
        static void reset(Container& c) {
            c[I] = ResetValue<typename std::tuple_element<I, typename Producer::DataSetType>::type>::value;
            ResetContainer<Producer, I-1>::reset(c);
        }
    };

    template<typename Producer>
    struct ResetContainer<Producer, 0> {
        template<typename Container>
        static void reset(Container& c) {
            c[0] = ResetValue<typename std::tuple_element<0, typename Producer::DataSetType>::type>::value;
        }
    };
}// namespace

template<typename Producer>
template<typename T>
void DataManager<Producer>::push_chunk(T const& chunk)
{
    // compile time assert index is within _complete_set_template bounds
    static_assert(Index<T, DataSetType>::value < InserterType::size, "Index out of range would occur");
    _complete_set[Index<T, DataSetType>::value] = 1;

    DataSetType copy;
    _copy_replace_type<InserterType::size-1, DataSetType, T>(_last, copy, chunk);
    _last = copy;
    if(std::accumulate(_complete_set.begin(), _complete_set.end(), 0) == InserterType::size)  {
        ResetContainer<Producer, InserterType::size -1>::reset(_complete_set);
        std::lock_guard<std::mutex> lock(_queue_mutex);
        _queue.emplace_back(std::move(copy));
    }
}

template<typename Producer>
typename DataManager<Producer>::DataSetType DataManager<Producer>::next()
{
    // process any events
    if(!_abort) {
        do {
            _abort = _chunker.process();
        } while(_queue.empty() && !_abort && !_stop);
    }

    if( (_abort || _stop)  && _queue.empty() ) {
       throw Abort();
    }

    std::lock_guard<std::mutex> lock(_queue_mutex);
    auto result = _queue.front();
    _queue.pop_front();
    return result;
}

template<typename Producer>
template<typename T, class PushFunction, typename... Args>
std::shared_ptr<T> DataManager<Producer>::get_writable_impl(Args&&... args)
{
    ++_awaiting_count; // before _abort test to avoid race condition
    try {
        return std::shared_ptr<T>(new T(std::forward<Args>(args)...),
                              [this](T* ptr)
                              {
                                  if(!_abort){
                                     // create a new shared_ptr that does not have this destructor
                                     // so we can dispose of the data when it goes out of scope
                                     PANDA_LOG_DEBUG << "pushing data " << (void*)ptr;
                                     try {
                                         PushFunction::push(*this, std::shared_ptr<T>(ptr));
                                     }
                                     catch(...) {
                                         delete ptr;
                                     }
                                  }
                                  else {
                                     delete ptr;
                                  }
                                  --_awaiting_count; // ensure DataManager object stays around until end of this lambda
                              }
                             );
    }
    catch(...) {
        // something went wrong - ensure we don't wait around for nothing
        --_awaiting_count;
        throw;
    }
}

template<typename Producer>
template<typename T>
struct DataManager<Producer>::PushChunk {
    template<typename DataManagerType>
    static inline void push(DataManagerType& dm, std::shared_ptr<T> const& chunk) {
        dm.push_chunk(chunk);
    }
};

template<typename Producer>
template<typename T>
struct DataManager<Producer>::PushChunk<panda::ServiceData<T>> {
    template<typename DataManagerType>
    static inline void push(DataManagerType& dm, std::shared_ptr<T> const& chunk) {
        dm.push_service_chunk(chunk);
    }
};

template<typename Producer>
template<typename T, typename... Args>
std::shared_ptr<T> DataManager<Producer>::get_writable(Args&&... args)
{
    typedef typename DataType<T>::type RedT;
    static_assert(panda::HasType<std::shared_ptr<RedT>, DataSetType>::value, "get_writable request for an unsupported data type");

    typedef typename std::conditional<panda::HasType<ServiceData<RedT>, typename Producer::DataSetType>::value, ServiceData<RedT>, RedT>::type WrappedType;
    return this->get_writable_impl<typename DataType<T>::type, PushChunk<WrappedType>>(std::forward<Args>(args)...);
}

template<typename Producer>
unsigned DataManager<Producer>::size() const
{
    return _queue.size();
}

} // namespace panda
} // namespace ska
