/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/DeviceIterator.h"


namespace ska {
namespace panda {

// specialisation to break recursive DeviceIterator

template<class Arch, typename IteratorType>
class DeviceIterator<Arch, DeviceIterator<Arch, IteratorType>>
{
    public:
        typedef DeviceIterator<Arch, IteratorType> SelfType;

    private:
        explicit DeviceIterator(IteratorType&&) {
            static_assert(is_device_iterator<DeviceIterator<Arch, IteratorType>>::value, "Something is wrong - attempt to build a nested DeviceIterator class");
        }
};

template<class Arch, typename IteratorType>
DeviceIterator<Arch, IteratorType>::DeviceIterator(IteratorType const& it)
    : IteratorType(it)
{
}

template<class Arch, typename IteratorType>
DeviceIterator<Arch, IteratorType>::DeviceIterator(IteratorType&& it)
    : IteratorType(std::move(it))
{
}

// --- make_device_iterator
template<class Arch
        ,typename T
        ,typename std::enable_if<!is_device_iterator<typename std::remove_reference<T>::type>::value, bool>::type>
DeviceIterator<Arch, typename std::remove_reference<T>::type> make_device_iterator(T&& t)
{
    return DeviceIterator<Arch, typename std::remove_reference<T>::type>(std::forward<T>(t));
}

template<class Arch
        ,typename T
        ,typename std::enable_if<is_device_iterator<typename std::remove_reference<T>::type>::value, bool>::type = true>
T& make_device_iterator(T& t)
{
    return t;
}

// pointer specialisation

template<class Arch, typename T>
DeviceIterator<Arch, T*>::DeviceIterator(T* ptr)
    : DeviceIterator<Arch, T*>::iterator_adaptor_(ptr)
{
}

template<class Arch, typename T>
template <class OtherType>
DeviceIterator<Arch, T*>::DeviceIterator(DeviceIterator<Arch, OtherType> const& other
                      , typename std::enable_if<
                              std::is_convertible<OtherType*,T*>::value
                            , Enabler>::type)
    : DeviceIterator<Arch, T*>::iterator_adaptor_(other.base())
{
}

} // namespace panda
} // namespace ska
