/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/detail/HandleWrapper.h"
#include <memory>

namespace ska {
namespace panda {
namespace detail {

template<typename... Args>
class HandleWrapperHelperBase {
    public:
        HandleWrapperHelperBase() {};
        virtual ~HandleWrapperHelperBase() {};

        virtual void operator()(Args&&...) = 0;
};

template<typename HandleType, typename... Args>
class HandleWrapperHelper : public HandleWrapperHelperBase<Args...> {
    public:
        HandleWrapperHelper(HandleType handler)
            : _handler(std::move(handler))
        {
        }

        ~HandleWrapperHelper() {}

        void operator()(Args&&... args) override {
            _handler(std::forward<Args>(args)...);
        }

    private:
        HandleType _handler;
};

template<typename HandleType, typename... Args>
class HandleWrapperHelper<std::unique_ptr<HandleType>, Args...> : public HandleWrapperHelperBase<Args...> {
    public:
        HandleWrapperHelper(std::unique_ptr<HandleType> handler)
            : _handler(std::move(handler))
        {
        }

        ~HandleWrapperHelper() {}

        void operator()(Args&&... args) override {
            *_handler(std::forward<Args>(args)...);
        }

    private:
        std::unique_ptr<HandleType> _handler;
};

template<typename... Args>
template<typename HandleType>
HandleWrapper<Args...>::HandleWrapper(HandleType handler)
    : _pimpl(std::static_pointer_cast<HandleWrapperHelperBase<Args...>>(std::make_shared<HandleWrapperHelper<HandleType, Args...>>(std::move(handler))))
{
}

template<typename... Args>
HandleWrapper<Args...>::~HandleWrapper()
{
}

template<typename... Args>
void HandleWrapper<Args...>::operator()(Args&&... args)
{
    _pimpl->operator()(std::forward<Args>(args)...);
}

} // namespace detail
} // namespace panda
} // namespace ska
