/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_HANDLERWRAPPER_H
#define SKA_PANDA_HANDLERWRAPPER_H

#include <memory>

namespace ska {
namespace panda {
namespace detail {

template<typename... Args>
class HandleWrapperHelperBase;

/**
 * @brief
 *    Wraps a generic async handler, making a copy with a common base class for all handler types
 * @details
 *    Provides a virtual interface for executing handlers and eliminateas any copy chop  of the underlying type.
 *    i.e. handlers with state can be stored safely.
 */
template<typename... Args>
class HandleWrapper
{
    public:
        template<typename HandleType>
        HandleWrapper(HandleType);
        HandleWrapper(HandleWrapper const& rhs) : _pimpl(rhs._pimpl) {}
        HandleWrapper(HandleWrapper&& rhs) : _pimpl(std::move(rhs._pimpl)) {}
        ~HandleWrapper();

        void operator()(Args&&... args);


    private:
        std::shared_ptr<HandleWrapperHelperBase<Args...>> _pimpl;
};

} // namespace detail
} // namespace panda
} // namespace ska
#include "panda/detail/HandleWrapper.cpp"

#endif // SKA_PANDA_HANDLERWRAPPER_H 
