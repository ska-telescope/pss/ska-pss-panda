/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/ModuloNumber.h"


namespace ska {
namespace panda {

template<typename T, typename StorageType>
ModuloNumber<T, StorageType>::ModuloNumber(T const& modulo, T const& val)
    : _val(val%modulo)
    , _modulo(modulo)
{
}

template<typename T, typename StorageType>
ModuloNumber<T, StorageType>::operator StorageType& ()
{
    return _val;
}

template<typename T, typename StorageType>
ModuloNumber<T, StorageType>::operator StorageType const& () const
{
    return _val;
}

template<typename T, typename StorageType>
ModuloNumber<T, StorageType>& ModuloNumber<T, StorageType>::operator=(T const& v)
{
    _val = v%_modulo;
    return *this;
}

template<typename T, typename StorageType>
template<typename S>
ModuloNumber<T, StorageType>& ModuloNumber<T, StorageType>::operator=(ModuloNumber<T, S> const& v)
{
    _val = static_cast<S>(v)%_modulo;
    return *this;
}

template<typename T, typename StorageType>
ModuloNumber<T, StorageType>& ModuloNumber<T, StorageType>::operator+=(T const& v)
{
    _val = (_val + v)%_modulo;
    return *this;
}

template<typename T, typename StorageType>
ModuloNumber<T, StorageType>& ModuloNumber<T, StorageType>::operator-=(T const& v)
{
    T v2 = v%_modulo;
    if(_val >= v2 ) {
        _val -= v2;
    }
    else {
        _val = _modulo - (v2 - _val);
    }
    return *this;
}

template<typename T, typename StorageType>
template<typename S>
ModuloNumber<T, StorageType>& ModuloNumber<T, StorageType>::operator+=(ModuloNumber<T, S> const& v)
{
    _val = (_val + static_cast<S>(v))%_modulo;
    return *this;
}

template<typename T, typename StorageType>
template<typename S>
ModuloNumber<T, StorageType>& ModuloNumber<T, StorageType>::operator-=(ModuloNumber<T, S> const& v)
{
    return (*this) -= static_cast<S>(v);
}

template<typename T, typename StorageType>
bool ModuloNumber<T, StorageType>::operator==(T const& val) const
{
    return _val == val;
}

template<typename T, typename StorageType>
bool ModuloNumber<T, StorageType>::operator!=(T const& val) const
{
    return _val != val;
}

template<typename T, typename StorageType>
template<typename S>
inline bool ModuloNumber<T, StorageType>::operator==(ModuloNumber<T, S> const& val) const
{
    return _val == static_cast<S const&>(val);
}

template<typename T, typename StorageType>
template<typename S>
inline bool ModuloNumber<T, StorageType>::operator!=(ModuloNumber<T, S> const& val) const
{
    return _val != static_cast<S const&>(val);
}

template<typename T, typename StorageType>
ModuloNumber<T, StorageType>& ModuloNumber<T, StorageType>::operator++()
{
    ++_val;
    if(_val >= _modulo) _val = 0;
    return *this;
}

template<typename T, typename StorageType>
ModuloNumber<T, StorageType> ModuloNumber<T, StorageType>::operator++(int)
{
    ModuloNumber<T> cpy(_modulo, _val);
    ++(*this);
    return cpy;
}

template<typename T, typename StorageType>
ModuloNumber<T, StorageType>& ModuloNumber<T, StorageType>::operator--()
{
    if(_val == 0 ) _val = _modulo;
    --_val;
    return *this;
}

template<typename T, typename StorageType>
ModuloNumber<T, StorageType> ModuloNumber<T, StorageType>::operator--(int)
{
    ModuloNumber<T> cpy(_modulo, _val);
    --(*this);
    return cpy;
}

template<typename T, typename StorageType>
ModuloNumber<T, StorageType> ModuloNumber<T, StorageType>::operator+(T const& v) const
{
    return ModuloNumber<T>(_modulo, (_val + v)%_modulo);
}

template<typename T, typename StorageType>
ModuloNumber<T, StorageType> ModuloNumber<T, StorageType>::operator-(T const& v) const
{
    T v2 = v%_modulo;
    return ModuloNumber<T>(_modulo, (_val >= v2)?(_val - v2):(_modulo - (v2 - _val)));
}

template<typename T, typename StorageType>
template<typename S>
ModuloNumber<T, StorageType> ModuloNumber<T, StorageType>::operator+(ModuloNumber<T, S> const& v) const
{
    return *this + static_cast<S>(v);
}

template<typename T, typename StorageType>
template<typename S>
ModuloNumber<T, StorageType> ModuloNumber<T, StorageType>::operator-(ModuloNumber<T, S> const& v) const
{
    return *this - static_cast<S>(v);
}

template<typename T, typename StorageType>
bool ModuloNumber<T, StorageType>::operator<(T const& v) const
{
   if(_val == v ) return false;
   if(_val < v) {
       return v - _val < (_modulo - v) + _val;
   }
   return _val - v > (_modulo - _val) + v;
}

template<typename T, typename StorageType>
template<typename S>
bool ModuloNumber<T, StorageType>::operator<(ModuloNumber<T, S> const& v) const
{
    return operator<(static_cast<S const&>(v));
}

template<typename T, std::memory_order MemoryOrdering>
AtomicModuloNumber<T, MemoryOrdering>::AtomicModuloNumber(T const& modulo, T const& val)
    : BaseT(modulo, val)
{
}

template<typename T, std::memory_order MemoryOrdering>
inline AtomicModuloNumber<T, MemoryOrdering>::operator T () const
{
    return this->load(static_cast<std::memory_order>(MemoryOrdering));
}

template<typename T, std::memory_order MemoryOrdering>
T AtomicModuloNumber<T, MemoryOrdering>::load( std::memory_order order) const
{
    return static_cast<Atomic<T, MemoryOrdering> const&>(static_cast<BaseT const&>(*this)).load(order);
}

} // namespace panda
} // namespace ska
