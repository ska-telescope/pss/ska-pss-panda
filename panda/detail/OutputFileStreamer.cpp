/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 The SKA organisation
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/OutputFileStreamer.h"
#include <boost/algorithm/string/trim.hpp>
#include <iomanip>

namespace ska {
namespace panda {

/*
template<typename O_Stream, typename DataType>
O_Stream& operator<<(O_Stream& os, PassThroughAdapter<DataType> const& d)
{
    os << *d._data;
    return os;
}
*/

template<class DataType, class FileFormatTraits>
OutputFileStreamer<DataType, FileFormatTraits>::OutputFileStreamer(boost::filesystem::path const& dir)
    : BaseT(dir)
{
}

template<class DataType, class FileFormatTraits>
OutputFileStreamer<DataType, FileFormatTraits>::~OutputFileStreamer()
{
}

template<class DataType, class FileFormatTraits>
OutputFileStreamer<DataType, FileFormatTraits>& OutputFileStreamer<DataType, FileFormatTraits>::operator<<(DataType const& data)
{
    if(this->_file_stream.is_open()) {
        if(!_format_traits.consistent_with_existing_file(data)) {
            _format_traits.footers(this->_file_stream);
            this->_file_stream.flush();
            this->_file_stream.close();
        }
    }
    if(!this->_file_stream.is_open()) {
        open_unique_filename(next_filename(data));
        _format_traits.headers(this->_file_stream, data);
    }

    _format_traits.write(this->_file_stream, data);

    return *this;
}

template<class DataType, class FileFormatTraits>
boost::filesystem::path OutputFileStreamer<DataType, FileFormatTraits>::next_filename(DataType const&)
{
    std::time_t t = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    char stem[20];
    std::strftime(stem, sizeof(stem), "%Y_%m_%d_%H:%M:%S", std::gmtime(&t));
    boost::filesystem::path file = boost::filesystem::path(_prefix + stem + _extension);
    return file;
}

} // namespace panda
} // namespace ska
