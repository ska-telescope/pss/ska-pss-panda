/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/detail/Recycler.h"
#include "panda/Log.h"


namespace ska {
namespace panda {


template<typename RecycledType, typename RecycleHandler>
Recycler<RecycledType, RecycleHandler>::Recycler(RecycleHandler handler)
    : _destructor(false)
    , _recycle_fn(std::move(handler))
{
}

template<typename RecycledType, typename RecycleHandler>
Recycler<RecycledType, RecycleHandler>::~Recycler()
{
    clear();
}

template<typename RecycledType, typename RecycleHandler>
void Recycler<RecycledType, RecycleHandler>::clear()
{
    _mutex.lock();
    _destructor = true;
    for(RecycledType* ptr : _recycled)
    {
        delete ptr;
    }
    _mutex.unlock();
    _recycled.clear();
}

template<typename RecycledType, typename RecycleHandler>
void Recycler<RecycledType, RecycleHandler>::reset()
{
    _mutex.lock();
    _destructor = false;
    _mutex.unlock();
}

template<typename RecycledType, typename RecycleHandler>
template<typename... Args>
std::shared_ptr<RecycledType> Recycler<RecycledType, RecycleHandler>::create(Args&&... args)
{
    std::shared_ptr<SelfType> keep_alive(this->shared_from_this());
    return std::shared_ptr<RecycledType>(new RecycledType(std::forward<Args>(args)...),
                [this, keep_alive](RecycledType* c) mutable
                {
                    bool ok;

                    try {
                        ok=_recycle_fn(c); 
                    }
                    catch(...) {
                        delete c;
                        throw;
                    }

                    if (ok) {
                        keep_alive->push(c);
                    }
                    else {
                        delete c;
                    }
                });
}

template<typename RecycledType, typename RecycleHandler>
void Recycler<RecycledType, RecycleHandler>::push(RecycledType* obj) {
    std::lock_guard<std::mutex> lk(_mutex);
    if(!_destructor) {
        try {
            _recycled.push_front(obj);
        }
        catch(...) {
            PANDA_LOG_DEBUG << "deleting object - unable to store";
            delete obj;
            throw;
        }
    }
    else {
        PANDA_LOG_DEBUG << "deleting object (cacheing disabled)";
        delete obj;
    }
}

template<typename RecycledType, typename RecycleHandler>
std::shared_ptr<RecycledType> Recycler<RecycledType, RecycleHandler>::pop() {
    std::lock_guard<std::mutex> lk(_mutex);
    if(_recycled.empty()) return std::shared_ptr<RecycledType>(nullptr);
        PANDA_LOG_DEBUG << "recycling object:" <<_recycled.front();

    std::shared_ptr<SelfType> keep_alive(this->shared_from_this());
    std::shared_ptr<RecycledType> rv(_recycled.front(), 
        [this, keep_alive](RecycledType* c) mutable
        {
            bool ok;
                    
            try {
                ok=_recycle_fn(c);
            }
            catch(...) {
                delete c;
                throw;
            }
                    
            if (ok) {
                keep_alive->push(c);
            }   
            else {  
                delete c;
            }
        });

    _recycled.pop_front();
    
    return rv;
}

} // namespace panda
} // namespace ska
