/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "ResourcePoolImpl.h"
#include "panda/ProcessingEngine.h"
#include "panda/Resource.h"
#include "panda/ResourceJob.h"
#include "panda/ResourceUtilities.h"
#include "panda/detail/Task.h"
#include "panda/TupleUtilities.h"
#include "panda/Log.h"
#include "panda/Error.h"

#include <stdexcept>
#include <thread>

namespace ska {
namespace panda {
namespace detail {

template<unsigned MaxPriority, typename... Architectures>
ResourcePoolImpl<MaxPriority, Architectures...>::ResourcePoolImpl(panda::ProcessingEngine& engine)
    : _destructor(false)
    , _engine(engine)
{
    _typed_resource_count.fill(0U);
}

template<unsigned MaxPriority, typename... Architectures>
ResourcePoolImpl<MaxPriority, Architectures...>::~ResourcePoolImpl()
{
    abort();
    PANDA_LOG_CONTROL << "End of pipeline";
}

template<unsigned MaxPriority, typename... Architectures>
void ResourcePoolImpl<MaxPriority, Architectures...>::abort()
{
    std::unique_lock<std::mutex> lock(_resource_mutex);
    _destructor = true;

    // abort all queued jobs
    panda::ForEach<ArchitectureTypes, AbortJobs>::exec(*this);

    // ask all managed resources to abort any jobs
    for(auto& res : _resources) {
        try {
            res->abort();
        }
        catch(...) {}
    }

    // tell anybody waiting to exit
    lock.unlock();
    _wait.notify_all();
}

namespace {

template<typename Arch>
struct LogDeviceAdd
{
    static void exec() {
        PANDA_LOG << "adding resource to pool " << to_string<Arch>();
    }
};

template<>
struct LogDeviceAdd<panda::Cpu>
{
    static void exec()
    {
        // disable message about threads
    }
};

template<typename Arch, typename... Architectures>
struct LogArchitectures
{
    inline static
    std::string exec(std::string const& sep=",") {
        return to_string<Arch>() + sep + LogArchitectures<Architectures...>::exec(sep);
    }
};
template<typename Arch>
struct LogArchitectures<Arch>
{
    inline static
    std::string exec(std::string const&) {
        return to_string<Arch>();
    }
};

template<typename... Architectures>
struct LogArchitectures<std::tuple<Architectures...>>
{
    inline static
    std::string exec(std::string const& sep=",") {
        return LogArchitectures<Architectures...>::exec(sep);
    }
};

} // namespace

template<unsigned MaxPriority, typename... Architectures>
template<typename Arch>
void ResourcePoolImpl<MaxPriority, Architectures...>::add_resource( std::shared_ptr<panda::Resource> r ) {
    std::unique_lock<std::mutex> lock(_resource_mutex);
    _resources.push_back(r);
    ++_resource_count<Arch>();
    LogDeviceAdd<Arch>::exec();
    _free_resource<Arch>().emplace_back(std::move(r));
    _match_resources<Arch>();
}

template<unsigned MaxPriority, typename... Architectures>
template<typename Arch>
typename ResourcePoolImpl<MaxPriority, Architectures...>::template QueueType<Arch>& ResourcePoolImpl<MaxPriority, Architectures...>::_queues() {
    return std::get<panda::Index<QueueType<Arch>, decltype(_queue)>::value>(_queue);
}

template<unsigned MaxPriority, typename... Architectures>
template<typename Arch>
typename ResourcePoolImpl<MaxPriority, Architectures...>::template QueueType<Arch> const& ResourcePoolImpl<MaxPriority, Architectures...>::_queues() const {
    return std::get<panda::Index<QueueType<Arch>, decltype(_queue)>::value>(_queue);
}

namespace {
template<typename Arch>
struct JobLauncher {
    template<typename Job>
    static inline void exec(panda::ProcessingEngine& engine, PoolResource<Arch>&, Job&& job)
    {
        engine.post(std::forward<Job>(job));
    }
};

template<>
struct JobLauncher<Cpu> {
    template<typename Job>
    static inline void exec(panda::ProcessingEngine&, PoolResource<Cpu>& resource, Job&& job)
    {
        resource.exec(std::forward<Job>(job));
    }
};
} // namespace

template<unsigned MaxPriority, typename... Architectures>
template<typename Arch>
bool ResourcePoolImpl<MaxPriority, Architectures...>::_match_resources() {
    // ensure _resource_mutex is locked before calling this
    // function
    for( auto& queue : _queues<Arch>() ) {
        if( _free_resource<Arch>().size() > 0 ) {
            auto q_it = queue.begin();
            while( q_it != queue.end() ) {
                TaskHolder<Arch>& job = *q_it;
                if(!job.try_lock()) {
                  // this job is already being processed
                  // (perhaps by a different architecture)
                  // so move on to the next
                  ++q_it;
                  continue;
                }
                // Find a compatible resource
                auto r_it = _free_resource<Arch>().begin();
                while( r_it != _free_resource<Arch>().end() ) {
                    auto const & resource = *r_it;
                    if( job.is_compatible(static_cast<PoolResource<Arch>const&>(*resource)) ) {
                        JobLauncher<Arch>::exec(_engine, static_cast<PoolResource<Arch>&>(*resource),
                                                std::bind(&ResourcePoolImpl<MaxPriority, Architectures...>::_run_job<Arch>, this,
                                                          std::move(resource), std::move(*q_it)) );
                        _free_resource<Arch>().erase(r_it);
                        queue.erase(q_it);
                        return true;
                    }
                    ++r_it;
                }
                // if we get here then no compatible resource was found so try the next job
                ++q_it;
            }
        }
    }
    return false;
}

template<unsigned MaxPriority, typename... Architectures>
template<typename Arch>
typename std::enable_if<HasType<Arch, typename ResourcePoolImpl<MaxPriority, Architectures...>::ArchitectureTypes>::value,
std::deque<std::shared_ptr<PoolResource<Arch>>>>::type const& ResourcePoolImpl<MaxPriority, Architectures...>::free_resources() const {
    return reinterpret_cast<std::deque<std::shared_ptr<PoolResource<Arch>>> const&>(_typed_free_resource[panda::Index<Arch, ArchitectureTypes>::value]);
}

template<unsigned MaxPriority, typename... Architectures>
template<typename Arch>
typename std::enable_if<!HasType<Arch, typename ResourcePoolImpl<MaxPriority, Architectures...>::ArchitectureTypes>::value,
std::deque<std::shared_ptr<PoolResource<Arch>>>>::type const& ResourcePoolImpl<MaxPriority, Architectures...>::free_resources() const {
    static std::deque<std::shared_ptr<PoolResource<Arch>>> res;
    return res;
}

template<unsigned MaxPriority, typename... Architectures>
template<typename Arch>
unsigned ResourcePoolImpl<MaxPriority, Architectures...>::_resource_count() const
{
    return _typed_resource_count[panda::Index<Arch, ArchitectureTypes>::value];
}

template<unsigned MaxPriority, typename... Architectures>
template<typename Arch>
unsigned& ResourcePoolImpl<MaxPriority, Architectures...>::_resource_count()
{
    return _typed_resource_count[panda::Index<Arch, ArchitectureTypes>::value];
}

template<unsigned MaxPriority, typename... Architectures>
template<typename Arch>
std::deque<std::shared_ptr<panda::Resource>>& ResourcePoolImpl<MaxPriority, Architectures...>::_free_resource() {
    return _typed_free_resource[panda::Index<Arch, ArchitectureTypes>::value];
}

template<unsigned MaxPriority, typename... Architectures>
template<typename Arch>
std::deque<std::shared_ptr<panda::Resource>> const& ResourcePoolImpl<MaxPriority, Architectures...>::_free_resource() const {
    return _typed_free_resource[panda::Index<Arch, ArchitectureTypes>::value];
}

template<unsigned MaxPriority, typename... Architectures>
template<typename Arch>
void ResourcePoolImpl<MaxPriority, Architectures...>::_run_job( std::shared_ptr<panda::Resource> r, TaskHolder<Arch> task )
{
    auto& job = task.resource_job();
    job.set_status( panda::ResourceJob::JobStatus::Running );
    try {
        static_cast<PoolResource<Arch>&>(*r).run(task);
        job.set_status( panda::ResourceJob::JobStatus::Finished );
    }
    catch( panda::Error const& e ) {
        PANDA_LOG_ERROR << "uncaught exception:" << e;
        job.set_error( std::current_exception() );
        job.set_status( panda::ResourceJob::JobStatus::Failed );
    }
    catch( std::exception const& e ) {
        PANDA_LOG_ERROR << "uncaught exception:" << e;
        job.set_error( std::current_exception() );
        job.set_status( panda::ResourceJob::JobStatus::Failed );
    }
    catch( ... ) {
        PANDA_LOG_ERROR << "unknown uncaught exception";
        job.set_error( std::current_exception() );
        job.set_status( panda::ResourceJob::JobStatus::Failed );
    }
    job.emit_finished();
    _resource_free<Arch>( std::move(r) );

    // execute any job callbacks
    for( auto& fn : job.callbacks() ) {
        fn();
    }
}

template<unsigned MaxPriority, typename... Architectures>
void ResourcePoolImpl<MaxPriority, Architectures...>::wait() const {
    std::unique_lock<std::mutex> lk(_resource_mutex);
    _wait.wait(lk, [this]()
              {
                  //if(_destructor) return true;
                  bool empty;
                  panda::ForEach<ArchitectureTypes, EmptyQueue>::exec(*this, empty);
                  if(! empty) return false;
                  // the queues are all empty so now wait until all the operating jobs have terminated
                  // check the number of free resources match the resources in the pool
                  std::size_t tot = 0;
                  for(auto const& typed_res : this->_typed_free_resource) {
                      tot += typed_res.size();
                  }
                  return tot == this->_resources.size();
              });
}

template<unsigned MaxPriority, typename... Architectures>
template<typename Arch>
typename std::enable_if<HasType<Arch, typename ResourcePoolImpl<MaxPriority, Architectures...>::ArchitectureTypes>::value,
int>::type ResourcePoolImpl<MaxPriority, Architectures...>::free_resources_count() const {
    std::unique_lock<std::mutex> lock(_resource_mutex);
    return _free_resource<Arch>().size();
}

template<unsigned MaxPriority, typename... Architectures>
template<typename Arch>
typename std::enable_if<!HasType<Arch, typename ResourcePoolImpl<MaxPriority, Architectures...>::ArchitectureTypes>::value,
int>::type ResourcePoolImpl<MaxPriority, Architectures...>::free_resources_count() {
    return 0;
}

namespace {
template<typename TaskType>
struct ArchExtractorGnuBugWorkAround
{
    // GNU compiler ignores namespace when the name matches expanded types
    // so this struct allows to hide the name Architectures from confused expansion
    typedef typename TaskType::Architectures type;
};
} // namespace

template<unsigned MaxPriority, typename... Architectures>
template<typename TaskType>
void ResourcePoolImpl<MaxPriority, Architectures...>::submit( unsigned priority, std::shared_ptr<TaskType> job ) {
    assert(priority < MaxPriority);
    job->set_status( panda::ResourceJob::JobStatus::Queued );
    std::unique_lock<std::mutex> lock(_resource_mutex);
    if(_destructor) {
        job->set_status(panda::ResourceJob::JobStatus::Aborted);
        return;
    }
    // add the task to the appropriate queue
    unsigned count = 0;
    PANDA_LOG_DEBUG << "submit()";
    panda::ForEach<ArchitectureTypes, QueueLoader>::exec(priority, *this, job, count);
    if(count == 0) {
        // job can never run
        panda::Error e("job submitted to a pool with no suitable devices (");
        e << LogArchitectures<typename ArchExtractorGnuBugWorkAround<TaskType>::type>::exec(", ") << ")";
        throw e;
    }
    panda::ForEachIf<ArchitectureTypes, Matcher>::exec(*this, job);
}

template<unsigned MaxPriority, typename... Architectures>
template<typename Arch>
void ResourcePoolImpl<MaxPriority, Architectures...>::_resource_free( std::shared_ptr<panda::Resource> res ) {
    std::unique_lock<std::mutex> lock(_resource_mutex);
    if( ! _destructor ) {
        _free_resource<Arch>().emplace_back(res);
        PANDA_LOG_DEBUG << "Resources returned to pool `" << to_string<Arch>() << "`";
        _match_resources<Arch>();
        lock.unlock();
        _wait.notify_all();
    }
}

} // namespace detail
} // namespace panda
} // namespace ska
