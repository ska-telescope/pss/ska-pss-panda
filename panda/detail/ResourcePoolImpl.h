/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_RESOURCEMANAGERIMPL_H
#define SKA_PANDA_RESOURCEMANAGERIMPL_H

#include "panda/detail/Task.h"
#include "panda/ArchitectureUtilities.h"
#include "panda/CompilerInfo.h"
#include "panda/Architecture.h"
#include "panda/Log.h"
#include <deque>
#include <vector>
#include <array>
#include <mutex>
#include <condition_variable>
#include <memory>

namespace ska {
namespace panda {
namespace detail {

class Resource;
class ResourceJob;
class ProcessingEngine;

/**
 * @class ResourcePoolImpl
 *
 * @brief
 *    A Processor Resource Manager and Job Queue
 *
 * @details
 *    Allows you to submit jobs to a queue of Compute resources
 *    As the resource becomes available, the next item from the queue
 *    is taken and executed.
 *
 *    Use the @code add_resource() method to add Resources to be managed. Note that
 *    all the resources should be compatible with the types of jobs to be
 *    submitted - there is no checking of job suitability.
 *
 *    call @code submit() to add a job to be processed. All job status
 *    information/callbacks etc can be found through the ProcessorJob interface.
 *
 *    @tparam the number of priority queues to assign, 1 = a single queue with eqaul priority to all tasks
 */

template<typename Arch>
struct TaskHolder {
    private:
        typedef detail::TaskBase<std::tuple<Arch>> TaskType;

    public:
        template<typename ExtendedTaskType>
        TaskHolder(std::shared_ptr<ExtendedTaskType> const& task)
          :   _compat_check(&task->template compatability_checker<Arch>())
            , _launcher(&task->template launcher<Arch>())
            , _task(std::static_pointer_cast<LockingResourceJob, ExtendedTaskType>(task))
        {
        }

        bool try_lock() {
            return _task->try_lock();
        }

        bool is_compatible(PoolResource<Arch> const& r) const {
            return (*_compat_check)(r);
        }

        void operator()(PoolResource<Arch>& r) const {
            (*_launcher)(r);
        }

        panda::ResourceJob& resource_job() { return *_task; }

    private:
        // retain the essential before loosing type info
        typename TaskType::template CompatabilityFunction<Arch> const * _compat_check;
        typename TaskType::template LauncherFunction<Arch> const * _launcher;
        std::shared_ptr<LockingResourceJob> _task; // keep the passed task alive
};


template<typename JobType>
struct ArchitectureExtractor : public ArchitectureInfo<JobType>
{
    private:
        typedef ArchitectureInfo<JobType> BaseT;
    public:
        typedef typename BaseT::Architectures type;
};

template<typename Arch>
struct QueueLoader {
    // task Arch matches an Arch in a pool
    template<typename ResourcePoolType, typename JobType>
    inline typename std::enable_if<panda::IsCompatible<typename ArchitectureExtractor<JobType>::type, Arch>::value, void>::type
    operator()(unsigned priority, ResourcePoolType& pool, std::shared_ptr<JobType> job, unsigned& count) {
        if(pool.template _resource_count<Arch>()) {
            ++count;
            PANDA_LOG_DEBUG << "adding job to queue " << panda::to_string<Arch>();
            pool.template _queues<Arch>()[priority].emplace_back(TaskHolder<Arch>(std::move(job)));
        }
    }

    // task Arch does not match an Arch in a pool
    template<typename ResourcePoolType, typename JobType>
    inline typename std::enable_if<!panda::IsCompatible<typename ArchitectureExtractor<JobType>::type, Arch>::value>::type
    operator()(unsigned, ResourcePoolType&, std::shared_ptr<JobType>, unsigned&) {
    }
};

template<typename Arch>
struct Matcher {
    // task Arch matches an Arch in a pool
    template<typename ResourcePoolType, typename JobType>
    inline typename std::enable_if<panda::IsCompatible<typename ArchitectureExtractor<JobType>::type, Arch>::value, bool>::type
    operator()(ResourcePoolType& pool, std::shared_ptr<JobType> const&) {
        return pool.template _match_resources<Arch>();
    }

    // JobType Arch does not match an Arch in a pool
    template<typename ResourcePoolType, typename JobType>
    constexpr inline typename std::enable_if<!panda::IsCompatible<typename ArchitectureExtractor<JobType>::type, Arch>::value, bool>::type
    operator()(ResourcePoolType&, std::shared_ptr<JobType> const&) const {
        return false;
    }
};

template<unsigned MaxPriority, typename... Architectures>
class ResourcePoolImpl : public std::enable_shared_from_this<ResourcePoolImpl<MaxPriority, Architectures...>>
{
    private:
        typedef std::tuple<Architectures...> ArchitectureTypes;

    template<typename Arch>
    struct AbortJobs {
        template <typename Api>
        inline void operator()(Api& rp) {
            std::size_t aborted=0;
            for(auto& queue : rp.template _queues<Arch>() ) {
                for( auto& job : queue ) {
                    if(job.try_lock()) {
                        job.resource_job().set_status( panda::ResourceJob::JobStatus::Aborted );
                        ++aborted;
                    }
                }
                queue.clear();
            }
            if(aborted > 0)
            {
                PANDA_LOG_WARN << "Aborted " << panda::to_string<Arch>() << " jobs (" << aborted << ")";
            }
        }
    };

    template<typename Arch>
    struct EmptyQueue {
        template <typename Api>
        inline void operator()(Api& rp, bool& result) {
            for(auto& queue : rp.template _queues<Arch>() ) {
                if(queue.size()) {
                    result = false;
                    return;
                }
            }
            result = true;
        }
    };

    public:
        ResourcePoolImpl(panda::ProcessingEngine& engine);
        ~ResourcePoolImpl();

        /// add a Resource (e.g. an NVidia card, or a Processor Core) to manage
        template<typename Arch>
        void add_resource(std::shared_ptr<panda::Resource> r);

        /// return the number of idle resources
        ///  available
        template<typename Arch>
        typename std::enable_if<HasType<Arch, ArchitectureTypes>::value,
        int>::type free_resources_count() const;

        template<typename Arch>
        typename std::enable_if<!HasType<Arch, ArchitectureTypes>::value,
        int>::type free_resources_count();

        /// submit a job to be processed by the resource
        // @param priority is not bounds checked - must be < MaxPriority
        template<typename TaskType>
        void submit( unsigned priority, std::shared_ptr<TaskType> job );

        /**
         * @brief wait for all ques to empty and all jobs to terminate
         */
        void wait() const;

        /**
         * @brief abort all jobs and queues
         */
        void abort();

        /**
         * @brief return the idle devices
         */
        template<typename Arch>
        typename std::enable_if<HasType<Arch, ArchitectureTypes>::value,
        std::deque<std::shared_ptr<PoolResource<Arch>>>>::type const& free_resources() const;

        template<typename Arch>
        typename std::enable_if<!HasType<Arch, ArchitectureTypes>::value,
        std::deque<std::shared_ptr<PoolResource<Arch>>>>::type const& free_resources() const;

    private:
        template<typename Arch>
        using QueueType = std::array<std::deque<TaskHolder<Arch>>, MaxPriority>;      // prioritised queues (0 = highest priority)

    private:
        // methods
        // return the array of queues that match the specified architecture
        template<typename Arch>
        QueueType<Arch>& _queues();

        template<typename Arch>
        QueueType<Arch> const& _queues() const;

        template<typename Arch>
        bool _match_resources();

        template<typename Arch>
        std::deque<std::shared_ptr<panda::Resource>>& _free_resource();

        template<typename Arch>
        std::deque<std::shared_ptr<panda::Resource>> const& _free_resource() const;

        // total number of resources assigned to a specified Arch
        template<typename Arch>
        inline unsigned _resource_count() const;

        template<typename Arch>
        inline unsigned& _resource_count();

        template<typename Arch>
        void _run_job( std::shared_ptr<panda::Resource>, TaskHolder<Arch> );

        template<typename Arch>
        void _resource_free( std::shared_ptr<panda::Resource> );

    private:
        template<typename Arch> friend struct QueueLoader;
        template<typename Arch> friend struct Matcher;

    private:
        mutable std::mutex _resource_mutex;
        mutable std::condition_variable _wait; // condition variable for waiting for queues to empty
        mutable std::array<std::mutex, sizeof...(Architectures)> _queue_mutexs;
        bool _destructor;
        panda::ProcessingEngine& _engine;          // for launching accelerator jobs

        std::tuple<QueueType<Architectures>...> _queue;                                // queues for each Architecture

        std::vector<std::shared_ptr<panda::Resource>> _resources;
        std::array<unsigned, sizeof...(Architectures)> _typed_resource_count; // Architecture types mapped to the index of the array
        std::array<std::deque<std::shared_ptr<panda::Resource>>, sizeof...(Architectures)> _typed_free_resource; // Architecture types mapped to the index of the array
};

} // namespace detail
} // namespace panda
} // namespace ska
#include "panda/detail/ResourcePoolImpl.cpp"

#endif // SKA_PANDA_RESOURCEMANAGERIMPL_H
