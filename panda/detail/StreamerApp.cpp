/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/StreamerApp.h"
#include "panda/ServerClient.h"
#include "panda/Engine.h"
#include "panda/Abort.h"
#include "panda/Log.h"
#include "panda/Connection.h"

#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <algorithm> // for std::max
#include <thread>

namespace ska {
namespace panda {

template<class Stream, class ServerTraits>
StreamerApp<Stream, ServerTraits>::StreamerApp(Stream& s, Engine& engine)
    : _stream(s)
    , _engine(engine)
    , _running(false)
    , _in_run(false)
    , _timer(engine)
{
}

template<class Stream, class ServerTraits>
StreamerApp<Stream, ServerTraits>::~StreamerApp()
{
    stop();
    _stream.abort();
    // ensure we wait for run to post any timer events
    while(_in_run) {
        _engine.poll_one();
    }
    _timer.cancel();
}

template<class Stream, class ServerTraits>
void StreamerApp<Stream, ServerTraits>::add_fixed_client(std::shared_ptr<AbstractConnection> const& connection)
{
    if(connection.get() != nullptr)
    {
        _fixed_clients.push_back(connection);
    }
    start();
}

template<class Stream, class ServerTraits>
void StreamerApp<Stream, ServerTraits>::on_connect(typename ServerTraits::Session const& request)
{
    // add to client list
    _active_clients.insert( std::make_pair(request->id(), request) );
    start();
}

template<typename NextReturnType, typename ConnectionType>
struct SendAdapter {
    template<typename HandlerType>
    static inline
    void send(ConnectionType& connection, NextReturnType const& data, HandlerType&& h)
    {
        connection.send(data, std::forward<HandlerType>(h)); // errors handled by Connection policy
    }
};

// specialisatio to convert strings
template<typename ConnectionType>
struct SendAdapter<std::string, ConnectionType> {
    template<typename HandlerType>
    static inline
    void send(ConnectionType& connection, std::string const& str, HandlerType&& h)
    {
        typename ConnectionType::BufferType buffer(str.size());
        std::copy(str.begin(), str.end(), buffer.data()); // TODO remove copy

        connection.send(buffer, std::forward<HandlerType>(h)); // errors handled by Connection policy
    }
};

template<class Stream, class ServerTraits>
void StreamerApp<Stream, ServerTraits>::run()
{
    if(_running) {
        _in_run = true;
        try {
            // iterate over all our clients
            auto data = _stream.next();
            for( auto const& client : _fixed_clients ) {
                SendAdapter<decltype(data), AbstractConnection>::send(*client, data, [](const Error&){});
            }
            for( auto const& client_pair : _active_clients ) {
                auto s=client_pair.second;
                SendAdapter<decltype(data), typename std::decay<decltype(*s)>::type>::send(*s, data, [this, s](Error const& e){ on_error(s, e); });
                //s->send(data, [this, s](Error const& e){ on_error(s, e); });
            }

            // schedule the next run
            std::chrono::microseconds interval=std::chrono::microseconds(_stream.interval());
            const std::chrono::microseconds elapsed(std::chrono::duration_cast<std::chrono::microseconds>(_elapsed_time_timer()));
            if(elapsed < interval) {
                interval -= elapsed;
                _timer.expires_from_now(boost::posix_time::microseconds(interval.count()));
                _timer.async_wait( [this](boost::system::error_code const&) { run(); } ); //TODO log error
            }
            else {
                _engine.post([this](){run();});
            }
        }
        catch(panda::Abort&) {
            _running = false;
        }
        catch(std::exception const& e)
        {
            PANDA_LOG_ERROR << "iexception in StreamerApp::run : " << e.what();
        }
        _in_run = false;
    }
}

template<class Stream, class ServerTraits>
void StreamerApp<Stream, ServerTraits>::start()
{
    if( ! _running ) {
        _running = true;
        _engine.post([this](){run();});
    }
}

template<class Stream, class ServerTraits>
void StreamerApp<Stream, ServerTraits>::stop()
{
    _running = false;
}

template<class Stream, class ServerTraits>
void StreamerApp<Stream, ServerTraits>::on_error(typename ServerTraits::Session const & response, Error const& ec)
{
    // drop the client from the list
    if(ec) {
        return;
    }
    auto it = _active_clients.find(response->id());
    if(it != _active_clients.end() ) {
        _active_clients.erase(it);
    }
}

} // namespace panda
} // namespace ska
