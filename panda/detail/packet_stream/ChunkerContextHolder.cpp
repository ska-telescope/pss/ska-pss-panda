/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/detail/ChunkerContextHolder.h"
#include <cmath>
#include <iostream>

namespace ska {
namespace panda {

template<typename DerivedType, typename DataTraits, bool is_empty>
typename ChunkerContextHolderBase<DerivedType, DataTraits, is_empty>::LimitType ChunkerContextHolderBase<DerivedType, DataTraits, is_empty>::sequence_number(typename DataTraits::PacketInspector const& packet)
{
    return _data.sequence_number(packet);
}

template<typename DerivedType, typename DataTraits, bool is_empty>
std::size_t ChunkerContextHolderBase<DerivedType, DataTraits, is_empty>::packet_alignment_offset(typename DataTraits::PacketInspector const& packet)
{
    return _data.packet_alignment_offset(packet);
}

template<typename DerivedType, typename DataTraits>
typename ChunkerContextHolderBase<DerivedType, DataTraits, true>::LimitType ChunkerContextHolderBase<DerivedType, DataTraits, true>::sequence_number(typename DataTraits::PacketInspector const& packet)
{
    return DataTraits::sequence_number(packet);
}

template<typename DerivedType, typename DataTraits, bool is_empty>
typename ChunkerContextHolderBase<DerivedType, DataTraits, is_empty>::LimitType ChunkerContextHolderBase<DerivedType, DataTraits, is_empty>::max_sequence_number()
{
    return _data.max_sequence_number();
}

template<typename DerivedType, typename DataTraits>
typename ChunkerContextHolderBase<DerivedType, DataTraits, true>::LimitType ChunkerContextHolderBase<DerivedType, DataTraits, true>::max_sequence_number()
{
    return DataTraits::max_sequence_number();
}

template<typename DataTraits>
ChunkerContextHolder<DataTraits>::ChunkerContextHolder(ChunkerContextHolder const& cpy)
    : BaseT(static_cast<BaseT const&>(cpy))
    , _recycler(cpy._recycler)
    , _ok_packets(0)
{
}

template<typename DataTraits>
template<typename... DataConfigArgs>
ChunkerContextHolder<DataTraits>::ChunkerContextHolder(std::shared_ptr<RecyclerType> recycler, DataConfigArgs&&... args)
    : BaseT(std::forward<DataConfigArgs>(args)...)
    , _recycler(std::move(recycler))
    , _ok_packets(0) // non-zero for the first item in the chain to ensure it is not marked complete before it is called
    , _complete_packets(1) // non-zero for the first item in the chain to ensure it is not marked complete before it is called
{
}

template<typename DataTraits>
std::shared_ptr<ChunkerContextHolder<DataTraits>> ChunkerContextHolder<DataTraits>::create() const
{
/*
    auto rv = _recycler->pop();
    if(rv)
    {
        return rv;
    }

    // create a new object
    return _recycler->create(*this);
*/
    return std::shared_ptr<ChunkerContextHolder<DataTraits>>(new SelfType(*this));
}

template<typename DataTraits>
ChunkerContextHolder<DataTraits>::~ChunkerContextHolder()
{
}

template<typename DataTraits>
template<typename... DataConfigs>
std::shared_ptr<ChunkerContextHolder<DataTraits>> ChunkerContextHolder<DataTraits>::create_initial(DataConfigs&&... args)
{
    std::shared_ptr<RecyclerType> recycler = std::make_shared<RecyclerType>([](SelfType* c){ return c->recycle();} ); // common object to be shared between all instances in this create chain

    //return recycler->create(recycler, std::forward<DataConfigs>(args)...);
    return std::shared_ptr<ChunkerContextHolder<DataTraits>>(new SelfType(recycler, std::forward<DataConfigs>(args)...));
}

template<typename DataTraits>
std::shared_ptr<typename ChunkerContextHolder<DataTraits>::DataType>& ChunkerContextHolder<DataTraits>::chunk()
{
    return _chunk;
}

template<typename DataTraits>
void ChunkerContextHolder<DataTraits>::mark_ok_packets()
{
    ++_ok_packets;
}

template<typename DataTraits>
bool ChunkerContextHolder<DataTraits>::complete() const
{
    return _ok_packets >= _complete_packets;
}

template<typename DataTraits>
typename ChunkerContextHolder<DataTraits>::LimitType ChunkerContextHolder<DataTraits>::offset() const
{
    return _first_offset;
}

template<typename DataTraits>
bool ChunkerContextHolder<DataTraits>::recycle()
{
    // reset variables
    _chunk.reset();
    _ok_packets = 0;
    return true;
}

template<typename DataTraits>
void ChunkerContextHolder<DataTraits>::reset(std::shared_ptr<DataType> chunk)
{
    _chunk = std::move(chunk);
    _ok_packets = 0;
    _packet_size = this->BaseT::packet_size();
    _chunk_size = this->BaseT::chunk_size(*_chunk);
}

template<typename DataTraits>
typename ChunkerContextHolder<DataTraits>::ChunkSizeType ChunkerContextHolder<DataTraits>::chunk_size() const
{
    return _chunk_size;
}

template<typename DataTraits>
void ChunkerContextHolder<DataTraits>::set_required_packets(LimitType p)
{
    _complete_packets = p;
}

template<typename DataTraits>
typename ChunkerContextHolder<DataTraits>::LimitType ChunkerContextHolder<DataTraits>::ok_packets() const
{
    return _ok_packets;
}

} // namespace panda
} // namespace ska
