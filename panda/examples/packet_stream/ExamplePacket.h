/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 The SKA organisation
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_EXAMPLEPACKET_H
#define SKA_PANDA_EXAMPLEPACKET_H

#include <array>
#include <cstdint>

namespace ska {
namespace panda {
namespace examples {

/**
 * @brief
 *    This class provides a bit replication of the expected packet structure
 *    and an interface to interpret access the structures data
 *
 *    This is a very simple packet with a single uint32_t to indicate the packet number
 *    with the rest being the data payload of 100 uint32_t data samples.
 */
class ExamplePacket
{
    public:
        ExamplePacket(uint32_t packet_number);
        ~ExamplePacket();

        // This gives the number of elements that the packet contains
        // This unit of this size is somewhat arbirtary, but must be used consistently
        // thoroughout.
        static constexpr std::size_t payload_size();

        /// return the packet header sequence number
        uint32_t packet_number() const;

    private:
        uint32_t  _packet_number;
        std::array<uint32_t, 100> _d; // packet contains 100 unsigned 32 bit ints
};

constexpr std::size_t ExamplePacket::payload_size()
{
    return sizeof(_d);
}

} // namespace examples
} // namespace panda
} // namespace ska

#endif // SKA_PANDA_EXAMPLEPACKET_H 
