/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 The SKA organisation
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_EXAMPLEPACKETINSPECTOR_H
#define SKA_PANDA_EXAMPLEPACKETINSPECTOR_H

#include "ExamplePacket.h"

namespace ska {
namespace panda {
namespace examples {

/**
 * @brief
 *    Interpret the ExamplePacket
 */
class ExamplePacketInspector
{
    public: // PacketStream required typedef
        typedef ExamplePacket Packet;

    public:
        // constructor must take const ref to the packet to inspect
        ExamplePacketInspector(ExamplePacket const&);
        ~ExamplePacketInspector();

        /**
         * @brief the position of the packet in the stream.
         * In this case it is just the packet number from the header.
         */
        // not a PacketStream requirement, but used by ExampleDataTraits
        uint32_t sequence_number() const;

        /**
         * @brief return true if no further processing is required on this packet
         *        In this example we want to process every packet and so we can use
         *        a static inline method to help the compiler optimise out any unecessary
         *        testing code.
         */
        // A PacketStream requirement
        inline static bool ignore() { return false; }

    private:
        ExamplePacket const& _packet;
};

} // namespace examples 
} // namespace panda
} // namespace ska

#endif // SKA_PANDA_EXAMPLEPACKETINSPECTOR_H 
