/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 The SKA organisation
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/ChannelInfo.h"

namespace ska {
namespace panda {

namespace {

class SinkId : public panda::ConfigModule {
    public:
        SinkId() : panda::ConfigModule("sink") {}
        void add_options(OptionsDescriptionEasyInit& add_options) override
        {
            add_options
                ("id", boost::program_options::value<std::string>(&_sink_id), "the unique identifier tag for the sink");
        }

        std::string _type;
        std::string _sink_id;
};

} // namespace

ChannelInfo::ChannelInfo(panda::ChannelId id)
    : ConfigModule("channel_" + std::move(id.to_string()))
    , _id(id)
    , _active(false)
    , _sinks_init(false)
{
    add_factory("sink", [](){ return new SinkId(); } );
}

ChannelInfo::~ChannelInfo()
{
}

void ChannelInfo::add_options(OptionsDescriptionEasyInit& add_options)
{
    add_options
        ("active", boost::program_options::value<bool>()->default_value(_active)->notifier(std::bind(&ChannelInfo::activate, this, std::placeholders::_1)), "activate the channel");
}

panda::ChannelId const& ChannelInfo::id() const
{
    return _id;
}

void ChannelInfo::activate(bool active)
{
    _active = active;
}

bool ChannelInfo::active() const
{
    return _active;
}

void ChannelInfo::sink(std::string const& id)
{
    _sinks.emplace_back(id);
}

ChannelInfo::SinkInfoType const& ChannelInfo::sinks() const
{
    if(!_sinks_init) {
        _sinks_init = true;
        auto it=subsection("sink");
        while(it != subsection_end()) {
            SinkId const& sid = static_cast<SinkId const&>(*it);
            if(sid._sink_id != "") {
                _sinks.emplace_back(sid._sink_id);
            }
            ++it;
        }
    }
    return _sinks;
}

} // namespace panda
} // namespace ska
