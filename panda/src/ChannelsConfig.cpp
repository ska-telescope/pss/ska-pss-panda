/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/ChannelsConfig.h"


namespace ska {
namespace panda {


ChannelsConfig::ChannelsConfig(std::string const& tag)
    : ConfigModule(tag)
{
    add_default_factory( [](std::string const& name) { return new ChannelInfo(panda::ChannelId(name)); } );
}

ChannelsConfig::~ChannelsConfig()
{
}

void ChannelsConfig::add_options(OptionsDescriptionEasyInit&)
{
}

std::vector<ChannelInfo const*> const& ChannelsConfig::channels() const
{
    if(_channels.size() < subsections().size()) {
        for(auto const& section_name : subsections() ) {
            auto it=subsection(section_name);
            while(it!=subsection_end()) {
                _channels.push_back(static_cast<ChannelInfo const*>(&*it));
                ++it;
            }
        }
    }
    return _channels;
}

void ChannelsConfig::channel(ChannelInfo const& channel)
{
    _channels.push_back(&channel);
}

} // namespace panda
} // namespace ska
