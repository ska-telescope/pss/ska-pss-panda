/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/CumulativeDelayErrorPolicy.h"
#include "panda/Error.h"
#include <iostream>
#include <cassert>

namespace ska {
namespace panda {

CumulativeDelayErrorPolicy::CumulativeDelayErrorPolicy(IntervalType intervals)
    : _error_count(0)
    , _intervals(intervals)
{
    assert(intervals.size() > 1);
}

CumulativeDelayErrorPolicy::~CumulativeDelayErrorPolicy()
{
}

bool CumulativeDelayErrorPolicy::connection_error(Error const&)
{
    _last_error = std::chrono::system_clock::now();
    if( _error_count < _intervals.size() )
        ++_error_count;
    return _intervals[_error_count - 1] == std::chrono::seconds(0);
}

bool CumulativeDelayErrorPolicy::error(Error const& error)
{
    std::cerr << "socket error: " << error.what() << std::endl;
    return connection_error(error);
}


bool CumulativeDelayErrorPolicy::send_error(Error const& error)
{
    std::cerr << "socket send error: " << error.what() << std::endl;
    return connection_error(error);
}

bool CumulativeDelayErrorPolicy::try_connect()
{
    if(_error_count) {
        if( std::chrono::system_clock::now() - _last_error < _intervals[_error_count - 1] )
            return false;
    }
    return true;
}

void CumulativeDelayErrorPolicy::connected()
{
    _error_count = 0;
}

} // namespace panda
} // namespace ska
