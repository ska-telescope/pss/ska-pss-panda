/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/FileSearchPath.h"

namespace ska {
namespace panda {

FileSearchPath::FileSearchPath()
{
}

FileSearchPath::FileSearchPath(boost::filesystem::path const& path)
{
    _paths.push_back(path);
}

FileSearchPath& FileSearchPath::push_back(boost::filesystem::path const& dir)
{
    _paths.push_back(dir);
    return *this;
}

FileSearchPath& FileSearchPath::push_front(boost::filesystem::path const& dir)
{
    _paths.push_front(dir);
    return *this;
}

std::string FileSearchPath::find(boost::filesystem::path const& filename) const
{
    auto pair = find_path(filename);
    if(pair.second) {
        return boost::filesystem::canonical(pair.first).string();
    }
    return "";
}

std::pair<boost::filesystem::path, bool> FileSearchPath::find_path(boost::filesystem::path const& filename) const
{
    for(auto const& dir : _paths) {
        boost::filesystem::directory_iterator end_it;
        boost::filesystem::path path = dir / filename;
        if(boost::filesystem::exists( path )) {
            return std::make_pair(path, true);
        }
    }
    return std::make_pair(boost::filesystem::path(), false);
}

std::list<boost::filesystem::path> const& FileSearchPath::paths() const
{
    return _paths;
}

} // namespace panda
} // namespace ska
