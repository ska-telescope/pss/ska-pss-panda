/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/Log.h"

namespace ska {
namespace panda {

Logger log("log", true);
Logger log_warn("warn", true);
Logger log_error("error", true);
Logger log_debug("debug", false);
Logger log_control("control", true);

std::mutex Logger::_mutex;
std::atomic<unsigned char> Logger::_pos(0);
unsigned char Logger::_send_pos(0);
unsigned char Logger::_written(0);
std::vector<std::ostream*> Logger::_outputstreams({ &std::cout }); // TODO make optional, add filestream etc
typename Logger::ArrayType Logger::_msg_queue;

#ifdef PANDA_LOGGING_THREAD 
LoggingThread Logger::_write_thread;
#endif

Logger::Logger(std::string&& msg, bool enabled)
    : _prefix(std::move(msg))
    , _enabled(enabled)
{
}

Logger::~Logger()
{
    process(); // flush all the messages
}

bool Logger::enabled() const {
    return _enabled;
}

void Logger::enable(bool active) 
{
    _enabled = active;
}

void Logger::send(LogLineHolder& lineholder) {
#ifndef PANDA_LOGGING_THREAD 
    LogLine const& line = lineholder.logline();
    std::lock_guard<std::mutex> lock(_mutex); // shouldn't really need this but seems clang is buggy
    for(auto os_ptr : _outputstreams) {
        line.write(*os_ptr);
        os_ptr->flush();
    }
#else
    auto pos=++_pos;
    lineholder.swap(_msg_queue[pos]);
    //if(_send_pos +1 <= pos) _send_pos = pos;
#endif
}

std::string const& Logger::prefix() const
{
    return _prefix;
}

void Logger::process() 
{
    while(_written != _pos) 
    {
        ++_written;
        LogLineHolder const& lineholder = _msg_queue[_written];
        LogLine const& line = lineholder.logline();
        for(auto os_ptr : _outputstreams) {
            line.write(*os_ptr);
            os_ptr->flush();
        }
    }
}

LogLineHolder& operator<<(Logger& logger, LogLineHolder& logline) 
{
    logline.reset(logger);
    return logline;
}

LogLineHolder& operator<<(Logger& logger, LogLineSentry& sentry) 
{
    return logger << sentry.logline();
}

} // namespace panda
} // namespace ska
