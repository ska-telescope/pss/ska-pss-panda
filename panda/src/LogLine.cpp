/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/detail/LogLine.h"
#include "panda/Log.h"


namespace ska {
namespace panda {

LogLineHolder::LogLineHolder()
    : _pimpl(new LogLine)
{
}

LogLineHolder::LogLineHolder(const char* line, const char* file)
    : _pimpl(new LogLine)
    , _logger(nullptr)
    , _location(std::string("[") + file + std::string(":") + std::string(line) + "]")
{
}

LogLineHolder::~LogLineHolder()
{
    delete _pimpl;
}

void LogLineHolder::lock()
{
    _mutex.lock();
}

void LogLineHolder::unlock()
{
    _pimpl->rewind();
    _mutex.unlock();
}

std::string const& LogLineHolder::location() const
{
    return _location;
}

void LogLineHolder::swap(LogLineHolder& l)
{
    std::swap(l._pimpl, _pimpl);
}

LogLine const& LogLineHolder::logline() const {
    return *_pimpl;
}

LogLine& LogLineHolder::logline() {
    return *_pimpl;
}

void LogLineHolder::reset(Logger& l)
{
    _pimpl->reset(l);
    _logger=&l;
    _pimpl->add(_location);
}

void LogLineHolder::send()
{
    if(_logger && _logger->enabled()) {
        _pimpl->add("\n");
        _logger->send(*this);
    }
}

LogLine::LogLine()
    : _active(false)
    , _truncated(false)
{
}

LogLine::~LogLine()
{
}

void LogLine::reset(Logger& logger)
{
    _active = logger.enabled();
    if(_active) {
        _truncated = false;
        rewind();
        _ss << "[" << logger.prefix() << "][tid=" << std::this_thread::get_id() << "]";
    }
}

void LogLine::rewind()
{
    _ss.str(""); // TODO verify no instantiation of new buffer
}

} // namespace panda
} // namespace ska
