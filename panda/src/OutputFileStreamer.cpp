/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 The SKA organisation
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/OutputFileStreamer.h"
#include "panda/Error.h"
#include "panda/Log.h"
#include <boost/algorithm/string/trim.hpp>

#include <iostream>
namespace ska {
namespace panda {

std::mutex OutputFileStreamerBase::_fs_mutex;

OutputFileStreamerBase::OutputFileStreamerBase(boost::filesystem::path const& dir)
{
    set_dir(dir);
}

OutputFileStreamerBase::~OutputFileStreamerBase()
{
}

void OutputFileStreamerBase::set_dir(boost::filesystem::path const& path)
{
    _dir=path;
    if(!boost::filesystem::is_directory(path)) {
        panda::Error e("OutputFileStreamer: directory does not exist: '");
        e << _dir.string() << "'";
        throw e;
    }
}

void OutputFileStreamerBase::extension(std::string const& extension)
{
    _extension = extension;
}

void OutputFileStreamerBase::prefix(std::string const& prefix)
{
    _prefix = prefix;
}

void OutputFileStreamerBase::open_unique_filename(boost::filesystem::path const& file)
{
    std::lock_guard<std::mutex> lock(_fs_mutex);
    boost::filesystem::path full_file=file;
    if(full_file.is_relative()) {
        full_file = _dir / full_file;
    }
    full_file=unique_filename(full_file);
    PANDA_LOG_CONTROL << "Opening file " << full_file.c_str() << " for writing";
    _file_stream.open(full_file.c_str());
}

boost::filesystem::path OutputFileStreamerBase::unique_filename(boost::filesystem::path const& file)
{
    // if the file exists append a number count on the end
    if(boost::filesystem::exists(file)) {
        std::string file_pat = (file.parent_path() / file.stem()).string() + "_";
        unsigned count = 0;
        std::string file_trial;
        do 
        {
            file_trial=file_pat + std::to_string(++count) + _extension;
        } while( boost::filesystem::exists(file_trial));
        return file_trial;
    }
    return file;
}

} // namespace panda
} // namespace ska
