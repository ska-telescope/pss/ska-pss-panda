/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/ProcessingEngine.h"
#include "panda/ProcessingEngineConfig.h"
#include "panda/Log.h"
#include <boost/assert.hpp>

namespace ska {
namespace panda {

ProcessingEngine::ProcessingEngine(ProcessingEngineConfig const& config)
    : _event_loop(config.number_of_threads())
    , _work(_event_loop)
{
    _exit=false;
    // construct the threads

    for(unsigned i = 0; i < config.number_of_threads(); ++i )
    {
        _threads.emplace_back(new Thread(config.affinities(),
            [this]() {
                try {
                    if(!_exit) { // avoid race condition if the destructor gets called before thread startup
                        boost::system::error_code ec;
                        _event_loop.run(ec);
                        if(ec) {
                            PANDA_LOG_ERROR << "ProcessingEngine: error " << ec;
                        }
                        // can legitimately come here if, for example stop() is called on the underlying io_service
                        //BOOST_ASSERT_MSG(_exit, "ProcessingEngine: unexpected thread exit");
                    }
                    } catch(std::exception const& e) {
                        PANDA_LOG_ERROR << "exception thrown:" << e.what();
                        // get the thread back to work
                        throw e;
                    } catch(...) {
                        PANDA_LOG_ERROR << "unknown exception thrown";
                        // get the thread back to work
                        throw;
                    }
            }
        ));
    }
}

ProcessingEngine::~ProcessingEngine()
{
    join();
}

ProcessingEngine::operator panda::Engine&()
{
    return _event_loop;
}

std::size_t ProcessingEngine::poll_one()
{
    return _event_loop.poll_one();
}

std::size_t ProcessingEngine::number_of_threads() const
{
    return _threads.size();
}

void ProcessingEngine::join()
{
    if(!_exit) {
        // halt our event loop
        _exit = true;
        _event_loop.stop();

        // wait for the threads to finish
        for( auto & thread : _threads ) {
            thread->join();
        }
    }
}

} // namespace panda
} // namespace ska
