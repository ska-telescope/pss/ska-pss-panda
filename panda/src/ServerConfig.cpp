/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/ServerConfig.h"


namespace ska {
namespace panda {


ServerConfig::ServerConfig(std::string const& name_tag)
    : ConfigModule(name_tag)
{
    number_of_threads(1);
}

ServerConfig::~ServerConfig()
{
}

void ServerConfig::number_of_threads(unsigned thread_num)
{
    ProcessingEngineConfig pe_config(thread_num);
    _engine.reset(new ProcessingEngine(pe_config));
}

void ServerConfig::add_options(OptionsDescriptionEasyInit& add_options) 
{
    add_options
    ("threads", boost::program_options::value<unsigned>()->default_value(1)->notifier(
        [this](unsigned thread_num) {
            this->number_of_threads(thread_num);
        }
    ), "the number of threads used in the server")
    ("port", boost::program_options::value<unsigned>()->default_value(88998)->notifier(
         [this](unsigned port) { 
             _listen_address.port(port);
         })
         , "specify the port to listen to")
    ;
}

IpAddress const& ServerConfig::listen_address() const
{
    return _listen_address;
}

Engine& ServerConfig::engine() const
{
    return *_engine;
}

} // namespace panda
} // namespace ska
