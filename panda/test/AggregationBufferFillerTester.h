/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_TEST_AGGREGATIONBUFFERFILLERTESTER_H
#define SKA_PANDA_TEST_AGGREGATIONBUFFERFILLERTESTER_H

#include <gtest/gtest.h>
#include <numeric>

namespace ska {
namespace panda {
namespace test {

/**
 * @brief Inherit from this class to define a Traits type suitable for passing to the AggregationBufferFillerTest
 * @tparam BufferFillerT The explicit AggregationBufferFiller class to test
 * @tparam ChunkType     The type of chunks to generate and insert into the AggregationBuffer. Defaults to the underlying DataType of the AggBuffer
 */
template<class BufferFillerT, class ChunkType = typename BufferFillerT::DataType>
struct AggregationBufferFillerTestTraits {
    protected:
        class ChunkIndex : public std::enable_shared_from_this<ChunkIndex>
        {
                typedef ChunkType BaseT;

            public:
                template<typename... Args>
                ChunkIndex(std::size_t index, Args&&... args);

                /**
                * @brief iterators for beginning to end of the chunk
                */
                auto begin() -> decltype(std::declval<ChunkType>().begin());
                auto begin() const -> decltype(std::declval<ChunkType const&>().begin());
                auto end() -> decltype(std::declval<ChunkType>().end());
                auto end() const -> decltype(std::declval<ChunkType const&>().end());

                /**
                * @brief index of the chunk
                */
                std::size_t index() const;

                /**
                * @brief size of chunk in number of elements
                */
                std::size_t size() const;

            private:
                ChunkType _chunk;
                std::size_t _index;
        };

    public:
        typedef typename BufferFillerT::template ReplaceChunkType<ChunkIndex>::type BufferFillerType;
        typedef typename BufferFillerType::value_type value_type;
        typedef typename BufferFillerType::FullBufferHandlerT FullBufferHandlerT;
        typedef typename BufferFillerType::SizeType SizeType;

        /**
         * @brief Override this method to generate a suitably sized AggregationBufferFiller.
         */
        virtual BufferFillerType* get_buffer_filler(FullBufferHandlerT handler, SizeType numer_of_elements) = 0;

        /**
         * @brief generate a suitably sized chunk if the default is not suitable
         * @details If the ChunkType has a constructor that does not take SizeType as its only
         *          argument add a customisation of the ChunkConstructArgs<ChunkType> template struct.
         *          @code
         *          template<>
         *          struct ChunkConstructArgs<Chunktype> {
         *              // return the arguments to be passed to the ChunkType constructor
         *              // as a std::tuple
         *              static inline
         *              std::tuple<Arg1, Arg2, Arg3> construction_args(SizeType size);
         *          };
         *          @endcode
         */
        std::shared_ptr<ChunkIndex> get_chunk(SizeType size, std::size_t chunk_count = 0);

};

/**
 * @brief
 *    Unit tests for the AggregationBufferFiller
 */

template<typename TestTraits>
class AggregationBufferFillerTester : public ::testing::Test
{
    protected:
        void SetUp() override;
        void TearDown() override;

    public:
        AggregationBufferFillerTester();
        ~AggregationBufferFillerTester();

};

TYPED_TEST_SUITE_P(AggregationBufferFillerTester);

} // namespace test
} // namespace panda
} // namespace ska

#include "detail/AggregationBufferFillerTester.cpp"
#endif // SKA_PANDA_TEST_AGGREGATIONBUFFERFILLERTESTER_H
