/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_TEST_COPYTESTER_H
#define SKA_PANDA_TEST_COPYTESTER_H

#include <gtest/gtest.h>

namespace ska {
namespace panda {
namespace test {

 /**
  * \ingroup testutil
  * @{
  */

/**
 * @brief
 *    Tests implementations of the DeviceCopy<SrcArch, DstArch>
 *
 * @details
 *    Uses the standard google parameterized test case infrastructure. Requires you to construct a traits class
 *    with a typedef Arch to indicate the Architeture tag, and a method that returns an object representing device mamange memory
 *
 * @code
 * struct TestArchCopyTraits {
 *    typedef MyTestArch Arch;
 *    typedef TestArchMemoryHolder DevicePtr; // some type that manages the device memory resource (clean up on destruction)
 *
 *   template<typename T>
 *   static DevicePtr contiguous_device_memory(std::size_t size) {
 *       return DevicePtr(size);
 *   }
 * }
 *
 * INSTANTIATE_TYPED_TEST_SUITE_P(MyTestCaseName, CopyTester, TestArchCopyTraits);
 * @endcode
 */
template<typename T>
class CopyTester : public ::testing::Test
{
    protected:
        void SetUp() override;
        void TearDown() override;

    public:
        CopyTester();
        ~CopyTester();

    private:
};

TYPED_TEST_SUITE_P(CopyTester);

/**
 *  @}
 *  End Document Group
 **/

} // namespace test
} // namespace panda
} // namespace ska
#include "panda/test/detail/CopyTester.cpp"

#endif // SKA_PANDA_TEST_COPYTESTER_H
