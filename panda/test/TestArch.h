/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_TESTARCH_H
#define SKA_PANDA_TESTARCH_H

#include "panda/Architecture.h"
#include "panda/DeviceAllocator.h"
#include "panda/DiscoverResources.h"
#include "panda/DeviceMemory.h"
#include "panda/DeviceCopy.h"
#include "panda/Log.h"
#include <iterator>

namespace ska {
namespace panda {
namespace test {

/**
 * @brief A fully implemented TestArch class, providing a device with memory allocation for testing
 *
 * @details
 *    memory allocation is Implemented on the host
 *    specialisations provided for:
 *      DiscoverResources
 *      DeviceIterator
 *      DeviceMemory
 *      DeviceCopy
 *      to_string
 */
static const std::string test_arch_string("panda::test::TestArch");
class TestArch : public panda::Architecture
{
    TestArch()
        : panda::Architecture(test_arch_string)
    {
    }
};

} // namespace test

template<>
constexpr std::string const& to_string<test::TestArch>() { return test::test_arch_string; }

template<>
class PoolResource<test::TestArch>
{
    public:
        PoolResource(std::size_t num)
            : _num(num)
        {
        }

        template<typename TaskType>
        void run(TaskType job) { job(*this); }

        void abort() {
            PANDA_LOG_DEBUG << "ResourcePool<" << test::test_arch_string << ">::abort() called";
        }

        std::size_t id() const {
            return _num;
        }

    private:
        std::size_t _num;
};

template<>
class DiscoverResources<test::TestArch>
{
    public:
        std::vector<std::shared_ptr<PoolResource<test::TestArch>>> operator()() {
            std::vector<std::shared_ptr<PoolResource<test::TestArch>>> res;

            unsigned num_cores = std::thread::hardware_concurrency();
            if(num_cores ==0) num_cores = 1;
            for(unsigned int i = 0; i < num_cores; ++i) {
                res.emplace_back(std::make_shared<PoolResource<test::TestArch>>(i));
            }
            return res;
        }
};

template<typename T>
class DeviceAllocator<T, test::TestArch>
{
    public:
        typedef test::TestArch Architecture;
        template<typename OtherT>
        struct rebind
        {
            typedef DeviceAllocator<OtherT, Architecture> other;
        };
        typedef T value_type;
        typedef T* pointer;

    public:
        DeviceAllocator(Device<Architecture> const&)
            : _alloc()
        {
        }

        template<typename U>
        DeviceAllocator(DeviceAllocator<U, Architecture> const& alloc)
            : _alloc(alloc._alloc)
        {
        }

        T* allocate(std::size_t n ) { return _alloc.allocate(n); }
        void deallocate( T* p, std::size_t n ){ _alloc.deallocate(p, n); }

        template<class Ptr, class... Args>
        void construct(Ptr* ptr, Args&&... args) { _alloc.construct(ptr, std::forward<Args>(args)...); }

        void destroy( pointer ptr ) { _alloc.destroy(ptr); }

        bool operator==(DeviceAllocator const&) const { return true; }

    protected:
        friend class DeviceMemory<Architecture, T, DeviceAllocator<T, Architecture>>;
        DeviceAllocator<T, panda::Cpu> const& cpu_allocator() const { return _alloc; }

    protected:
        template<typename, typename>
        friend class DeviceAllocator;
        DeviceAllocator<T, panda::Cpu> _alloc;
};

template<typename SrcIteratorTypeTag, typename EndIteratorTypeTag, typename DstIteratorTypeTag>
struct DeviceCopy<Cpu, test::TestArch, SrcIteratorTypeTag, EndIteratorTypeTag, DstIteratorTypeTag>
{
    public:
        /**
         * @brief perform the copy from host to TestArch architectures
         */
        template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
        static inline typename std::remove_reference<DstIteratorType>::type copy(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it)
        {
            return std::copy(std::forward<SrcIteratorType>(begin), std::forward<EndIteratorType>(end), std::forward<DstIteratorType>(dst_it));
        }
};

template<typename SrcIteratorTypeTag, typename EndIteratorTypeTag, typename DstIteratorTypeTag>
struct DeviceCopy<test::TestArch, Cpu, SrcIteratorTypeTag, EndIteratorTypeTag, DstIteratorTypeTag>
{
    public:
        /**
         * @brief perform the copy from host to TestArch architectures
         */
        template<typename SrcIteratorType, typename EndIteratorType, typename DstIteratorType>
        static inline typename std::remove_reference<DstIteratorType>::type copy(SrcIteratorType&& begin, EndIteratorType&& end, DstIteratorType&& dst_it)
        {
            return std::copy(std::forward<SrcIteratorType>(begin), std::forward<EndIteratorType>(end), std::forward<DstIteratorType>(dst_it));
        }
};

template<typename T>
class DeviceMemory<test::TestArch, T, DeviceAllocator<T, test::TestArch>>
{
        typedef DeviceMemory<panda::Cpu, T> AggType;

    protected:
        typedef typename std::iterator_traits<typename AggType::Iterator>::pointer Pointer;
        typedef typename std::iterator_traits<typename AggType::ConstIterator>::pointer ConstPointer;

    public:
        typedef test::TestArch Architecture;
        typedef panda::DeviceIterator<Architecture, ConstPointer> ConstIterator;
        typedef panda::DeviceIterator<Architecture, Pointer> Iterator;

    public:
        template<typename...AllocatorArgs>
        DeviceMemory(std::size_t size, AllocatorArgs&&... allocator_constructor_args)
            : _mem(size, DeviceAllocator<T, Architecture>(std::forward<AllocatorArgs>(allocator_constructor_args)...).cpu_allocator())
        {
        }

        std::size_t const& size() const { return _mem.size(); }

        /**
         * @brief forced reallocaton of memory to the specfied size
         */
        void reset(std::size_t size) { _mem.reset(size); }

        /**
         * @brief return a pointer to the first element on the device
         * @details do not attempt to write to access this memory directly from the host
         */
        Iterator begin() { return Iterator(&*_mem.begin()); }

        /**
         * @brief return a pointer to the last element + 1 (on the device)
         * @details do not attempt to access this memory on the host
         */
        Iterator end() { return Iterator(&*_mem.end()); }

        /**
         * @brief return a pointer to the first element on the device
         * @details do not attempt to write to access this memory directly from the host
         */
        ConstIterator begin() const { return ConstIterator(&*_mem.begin()); }

        /**
         * @brief return a pointer to the last element + 1 (on the device)
         * @details do not attempt to access this memory on the host
         */
        ConstIterator end() const { return ConstIterator(&*_mem.end()); }

        /**
         * @brief return a const pointer to the first element on the device
         * @details do not attempt to write to access this memory directly from the host
         */
        ConstIterator cbegin() const { return ConstIterator(&*_mem.cbegin()); }

        /**
         * @brief return a const pointer to the last element + 1 on the device
         */
        ConstIterator cend() const { return ConstIterator(&*_mem.cend()); }

    protected:
        AggType _mem;
};

} // namespace panda
} // namespace ska

#endif // SKA_PANDA_TESTARCH_H
