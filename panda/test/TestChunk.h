/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_TESTCHUNK_H
#define SKA_PANDA_TESTCHUNK_H

#include <panda/DataChunk.h>
#include <string>
#include <memory>
#include <ostream>
#include <vector>
#include <initializer_list>

namespace ska {
namespace panda {
namespace test {

/**
 * \ingroup testutil
 * @{
 */

/**
 * @brief
 *    A simple Data chunk that holds its data in a std::string
 * @details
 *    TestChunk<char>, TestChunk_A and TestChunk_B are all unique DataChunk standards confirming types for testing
 */
template <typename T>
class TestChunkBase
{
    public:
        typedef T Container;
        typedef typename Container::const_iterator const_iterator;
        typedef typename Container::iterator iterator;

    public:
        TestChunkBase(Container const & data, unsigned index = 0);
        TestChunkBase(unsigned index, std::initializer_list<T>);
        virtual ~TestChunkBase() = 0; // don't instantiate this class directly
        Container const & data() const;
        void data(Container const& data);

        std::size_t size() const;

        const_iterator begin() const;
        const_iterator end() const;
        iterator begin();
        iterator end();

        unsigned index() const;

        bool missing_called() const;
        void set_missing_called();

    private:
        Container _data;
        unsigned _index;
        bool _missing_called;
};

template<typename T=char>
class TestChunk : public TestChunkBase<std::vector<T>>, public panda::DataChunk<TestChunk<T>>
{
    typedef TestChunkBase<std::vector<T>> BaseT;

    public:
        TestChunk(std::vector<T> const & data, unsigned index = 0);
        TestChunk(std::size_t size, unsigned index = 0);
        ~TestChunk();
};

template<>
class TestChunk<char> : public TestChunkBase<std::string>, public panda::DataChunk<TestChunk<char>>
{
    typedef TestChunkBase<std::string> BaseT;

    public:
        TestChunk(std::string const & data, unsigned index = 0);
        TestChunk(std::size_t size, unsigned index = 0);
        ~TestChunk();
};

struct TestChunk_A : public TestChunkBase<std::string>, public panda::DataChunk<TestChunk_A>
{
    private:
        typedef TestChunkBase<std::string> BaseT;

    public:
        TestChunk_A(std::string const& s = std::string("A")) : BaseT(s) {}
};

struct TestChunk_B : public TestChunkBase<std::string>, public panda::DataChunk<TestChunk_B>
{
    private:
        typedef TestChunkBase<std::string> BaseT;

    public:
        TestChunk_B(std::string const& s = std::string("B")) : BaseT(s) {}
};

struct TestChunk_C : public TestChunkBase<std::string>, public panda::DataChunk<TestChunk_C>
{
    private:
        typedef TestChunkBase<std::string> BaseT;

    public:
        TestChunk_C(std::string const& s = std::string("C")) : BaseT(s) {}
};

template <typename T>
bool operator==(TestChunkBase<T> const& chunk_A, TestChunkBase<T> const& chunk_B);

template <typename T>
bool operator!=(TestChunkBase<T> const& chunk_A, TestChunkBase<T> const& chunk_B);

template <typename T>
std::ostream& operator<<(std::ostream& os, TestChunkBase<T> const&);

/**
 *  @}
 *  End Document Group
 **/

} // namespace test
} // namespace panda
} // namespace ska

#include "detail/TestChunk.cpp"
#endif // SKA_PANDA_TESTCHUNK_H
