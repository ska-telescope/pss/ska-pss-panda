/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_TEST_TESTDEVICE_H
#define SKA_PANDA_TEST_TESTDEVICE_H

#include "panda/System.h"
#include <memory>

namespace ska {
namespace panda {
namespace test {

/**
 * \ingroup testutil
 * @{
 */

/**
 * @brief
 *    A convenience class to find devices of a specifc type only
 * @details
 * 
 */
template<typename ArchTag>
class TestDevice
{
    protected:
        typedef panda::PoolResource<ArchTag> DeviceRefType;
        typedef std::shared_ptr<DeviceRefType> DeviceType;

    public:
        TestDevice();
        ~TestDevice();

        /**
         * @brief return list of all know devices matching the ArchTag
         */
        std::vector<DeviceType> devices();

        /** TODO
         * @brief return  devices that match the specified capabilities
        template<typename... CapabilityTags>
        std::vector<DeviceType> matching_devices();
         */

    private:
        panda::System<ArchTag> _system;
};

/**
 *  @}
 *  End Document Group
 **/

} // namespace test
} // namespace panda
} // namespace ska
#include "panda/test/detail/TestDevice.cpp"

#endif // SKA_PANDA_TEST_TESTDEVICE_H
