/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_TESTFILE_H
#define SKA_PANDA_TESTFILE_H

#include <boost/filesystem.hpp>
//#include <boost/iostreams/stream.hpp>
//#include <boost/iostreams/device/file_descriptor.hpp>
#include <string>
#include <fstream>

namespace ska {
namespace panda {
namespace test {

/**
 * \ingroup testutil
 * @{
 */

/**
 * @brief
 *     Generates a unique filename, removing file on destruction
 * @details
 * 
 */
class TestFile
{
    public:

        TestFile();
        /**
         * @brief ensure filename conforms to the specified template
         *        see boost::filesystem::unique_path for details
         */
        TestFile(boost::filesystem::path name_template);

        ~TestFile();

        /**
         * @brief full file path as a string
         */
        std::string const& filename() const;

        /**
         * @brief the name of the file (devoid of path information)
         */
        std::string name() const;

        /**
         * @brief full file path as a path object
         */
        boost::filesystem::path const& filepath() const;

        /**
         * @brief write data to the file, opening it if necessary
         */
        template<typename T>
        TestFile& write(T const& data);

        /**
         * @brief flush any write operations to disk
         */
        void flush();

    protected:
        /**
         * @brief open the file for writing
         */
        void open();

    private:
        boost::filesystem::path _path;
        std::ofstream _file_stream;
        //boost::iostreams::stream<boost::iostreams::file_descriptor_sink> _file_stream;
};

/**
 * @brief write data to the file, opening it if necessary
 */
template<typename T>
TestFile& operator<<(TestFile& file, T const& data);

/**
 *  @}
 *  End Document Group
 **/

} // namespace test
} // namespace panda
} // namespace ska
#include "panda/test/detail/TestFile.cpp"

#endif // SKA_PANDA_TESTFILE_H
