/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_TESTPACKET_H
#define SKA_PANDA_TESTPACKET_H
#include <array>
#include <cstdint>


namespace ska {
namespace panda {
namespace test {

/**
 * \ingroup testutil
 * @{
 */

/**
 * @brief
 * 
 * @details
 * 
 */
template<std::size_t PayloadSize>
class TestPacket
{
        static std::size_t size() { return sizeof(TestPacket); }

    public:
        typedef std::size_t LimitType;

    public:
        TestPacket(int count=0);
        ~TestPacket();

        LimitType packet_count() const;

        uint32_t const* begin() const;
        uint32_t const* end() const;

        static std::size_t payload_size() { return PayloadSize; }

    private:
        std::size_t _count;
        std::array<uint32_t, PayloadSize> _d; // payload
};

template<std::size_t PayloadSize=10>
class TestPacketInspector {
    public:
        typedef TestPacket<PayloadSize> Packet;
        typedef typename Packet::LimitType LimitType;

    public:
        TestPacketInspector(Packet const& packet)
            : _packet(packet)
        {
        }

        TestPacketInspector(TestPacketInspector const&) = delete;

        LimitType packet_count() const {
            return _packet.packet_count();
        }

        bool ignore()
        {
            return false;
        }

        // conversion operator
        operator Packet const& () { return _packet; }

        Packet const& packet() const { return _packet; }

    private:
        Packet const& _packet;
};

/**
 *  @}
 *  End Document Group
 **/

} // namespace test
} // namespace panda
} // namespace ska
#include "detail/TestPacket.cpp"

#endif // SKA_PANDA_TESTPACKET_H
