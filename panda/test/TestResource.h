/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_TEST_TESTRESOURCE_H
#define SKA_PANDA_TEST_TESTRESOURCE_H


#include "panda/Resource.h"
#include "panda/DeviceConfig.h"
#include <mutex>
#include <condition_variable>
#include <functional>

namespace ska {
namespace panda {
namespace test {

/**
 * \ingroup testutil
 * @{
 */

/**
 * @class TestResource
 *  
 * @brief
 *    A dummy Resource used for unit testing purposes
 * @details
 * 
 */

class TestResource : public panda::Resource
{
    public:
        TestResource(std::size_t memory=0);
        ~TestResource();

        /// return the currently processing job
        void* current_job() const;

        /// terminate the currently "processing" job
        void complete_job();

        /// termiate the current processing job by throwing
        template<typename T> void throw_job( const T object ) {
            _do_throw = true;
            _object = std::bind( &TestResource::do_throw<T>, this, object );
            complete_job();
        }

        /// abort all jobs on the resource
        void abort();

        template<typename TaskType>
        void run(TaskType& job);

        template<typename Arch>
        void configure(DeviceConfig<Arch> const&) {}

        std::size_t device_memory() const { return _memory; }

    protected: 
        template<typename T> void do_throw( const T object ) {
            throw object;
        }

    private:
        void* _current;
        std::mutex _mutex;
        std::condition_variable _wait_condition;
        std::function<void()> _object;
        bool _do_throw;
        std::size_t _memory;
};

template<typename TaskType>
void TestResource::run( TaskType& job ) {
   std::unique_lock<std::mutex> loc(_mutex);
   _current = reinterpret_cast<void*>(&job);
   _wait_condition.wait(loc);
   if( _do_throw ) {
       _object();
   }
}

/**
 *  @}
 *  End Document Group
 **/

} // namespace test
} // namespace panda
} // namespace ska
#endif // SKA_PANDA_TEST_TESTRESOURCE_H
