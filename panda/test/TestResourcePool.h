/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_PANDA_TESTRESOURCEPOOL_H
#define SKA_PANDA_TESTRESOURCEPOOL_H

#include "panda/ResourcePool.h"
#include <memory>

namespace ska {
namespace panda {
namespace test {

/**
 * \ingroup testutil
 * @{
 */

/**
 * @brief
 *    A ResourcePool object with a simple engine
 * @details
 *
 */
template<typename... ArchitecturesT>
class TestResourcePool
{
    public:
        typedef std::tuple<ArchitecturesT...> Architectures;

    public:
        TestResourcePool();
        TestResourcePool(panda::ProcessingEngine&); // dummy, passed engine is ignored
        TestResourcePool(TestResourcePool const&) = default;
        TestResourcePool(TestResourcePool&&);
        ~TestResourcePool();

        /**
         * @brief submit an asyncronous task, defined by TaskTraits and the data provided
         * @tparam Priority the priority level of the task. Lower numbers indicate higher priority. Must be less than MaxPriority
         * @tparam TaskTraits support the operator()(PoolResource<Arch>&, DataType&&...)  method for each supported Architecture
         * @returns shared_ptr to the submitted task information object ResourceJob
         */
        template<unsigned Priority, typename TaskTraits, typename... DataTypes>
        std::shared_ptr<ResourceJob> submit(TaskTraits&& traits, DataTypes&&... data);

        /**
         * @brief submit an asyncronous task, defined by TaskTraits and the data provided ( same as submit<0>(...) )
         */
        template<typename TaskTraits, typename... DataTypes> inline
        std::shared_ptr<ResourceJob> submit(TaskTraits& traits, DataTypes&&... types) const;

        /// @brief add a Resource (e.g. an NVidia card, or a Processor Core) to manage
        //  @param Arch - the type of resource the resource should be interpreted as
        template<typename Arch>
        void add_resource(std::shared_ptr<panda::Resource> r);

        /**
         * @brief return the number of idle resources of the specified type
         * available
         */
        template<typename Arch>
        unsigned free_resources_count() const;

        /**
         * @brief return the dle devices
         */
        template<typename Arch>
        std::deque<std::shared_ptr<panda::PoolResource<Arch>>> const& free_resources() const;

        /**
         * @brief block until the queues are empty
         */
        void wait() const;

    private:
        panda::ProcessingEngineConfig _config;
        std::shared_ptr<panda::ProcessingEngine> _engine;
        panda::ResourcePool<10, ArchitecturesT...> _pool;
};

/**
 *  @}
 *  End Document Group
 **/

} // namespace test
} // namespace panda
} // namespace ska
#include "panda/test/detail/TestResourcePool.cpp"

#endif // SKA_PANDA_TESTRESOURCEPOOL_H
