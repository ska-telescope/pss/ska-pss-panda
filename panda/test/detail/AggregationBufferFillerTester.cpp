/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "panda/AggregationBufferFiller.h"
#include <string>

namespace ska {
namespace panda {
namespace test {

template<typename T>
AggregationBufferFillerTester<T>::AggregationBufferFillerTester()
    : ::testing::Test()
{
}

template<typename T>
AggregationBufferFillerTester<T>::~AggregationBufferFillerTester()
{
}

template<typename T>
void AggregationBufferFillerTester<T>::SetUp()
{
}

template<typename T>
void AggregationBufferFillerTester<T>::TearDown()
{
}

template<class BufferFillerT, class ChunkType>
template<typename... Args>
AggregationBufferFillerTestTraits<BufferFillerT, ChunkType>::ChunkIndex::ChunkIndex(std::size_t index, Args&&... args)
    : _chunk(std::forward<Args>(args)...)
    , _index(index)
{
}

template<class BufferFillerT, class ChunkType>
auto AggregationBufferFillerTestTraits<BufferFillerT, ChunkType>::ChunkIndex
        ::begin() -> decltype(std::declval<AggregationBufferFillerTestTraits<BufferFillerT, ChunkType>::ChunkIndex::BaseT>().begin())
{
    return _chunk.begin();
}

template<class BufferFillerT, class ChunkType>
auto AggregationBufferFillerTestTraits<BufferFillerT, ChunkType>::ChunkIndex
        ::begin() const -> decltype(std::declval<AggregationBufferFillerTestTraits<BufferFillerT, ChunkType>::ChunkIndex::BaseT const&>().begin())
{
    return _chunk.begin();
}

template<class BufferFillerT, class ChunkType>
auto AggregationBufferFillerTestTraits<BufferFillerT, ChunkType>::ChunkIndex
        ::end() -> decltype(std::declval<AggregationBufferFillerTestTraits<BufferFillerT, ChunkType>::ChunkIndex::BaseT>().end())
{
    return _chunk.end();
}

template<class BufferFillerT, class ChunkType>
auto AggregationBufferFillerTestTraits<BufferFillerT, ChunkType>::ChunkIndex
        ::end() const -> decltype(std::declval<AggregationBufferFillerTestTraits<BufferFillerT, ChunkType>::ChunkIndex::BaseT const&>().end())
{
    return _chunk.end();
}

template<class BufferFillerT, class ChunkType>
std::size_t AggregationBufferFillerTestTraits<BufferFillerT, ChunkType>::ChunkIndex
        ::index() const
{
    return _index;
}

template<class BufferFillerT, class ChunkType>
std::size_t AggregationBufferFillerTestTraits<BufferFillerT, ChunkType>::ChunkIndex::size() const
{
    return std::distance(_chunk.begin(),_chunk.end());
}

namespace {

template<typename ChunkIndexT, typename TupleT, std::size_t I = std::tuple_size<TupleT>::value>
struct GetChunkFactoryHelper
{
    template<typename... Ts>
    static std::shared_ptr<ChunkIndexT> do_create(std::size_t chunk_count, TupleT const& tpl, Ts&&... args)
    {
        return GetChunkFactoryHelper<ChunkIndexT, TupleT, I-1>::do_create(chunk_count, tpl, std::get<I-1>(tpl), std::forward<Ts>(args)...);
    }
};

template<typename ChunkIndexT, typename TupleT>
struct GetChunkFactoryHelper<ChunkIndexT, TupleT, 0>
{
    template<typename... Ts>
    static std::shared_ptr<ChunkIndexT> do_create(std::size_t chunk_count, TupleT const&, Ts&&... args)
    {
        return std::make_shared<ChunkIndexT>(chunk_count, std::forward<Ts>(args)...);
    }
};


template<typename ChunkIndexT>
struct GetChunkFactory
{
    template<typename TupleType>
    static inline
    std::shared_ptr<ChunkIndexT> create(std::size_t chunk_count, TupleType const& tpl)
    {
        return GetChunkFactoryHelper<ChunkIndexT, TupleType>::do_create(chunk_count, tpl);
    }
};
} // namespace

template<typename T>
struct ChunkConstructArgs
{
    template<typename SizeType>
    static inline
    std::tuple<SizeType> construction_args(SizeType size) {
        return std::tuple<SizeType>(size);
    }
};

template<class BufferFillerT, class ChunkType>
std::shared_ptr<typename AggregationBufferFillerTestTraits<BufferFillerT, ChunkType>::ChunkIndex>
AggregationBufferFillerTestTraits<BufferFillerT, ChunkType>::get_chunk(SizeType size, std::size_t chunk_count)
{
    std::shared_ptr<ChunkIndex> chunk = GetChunkFactory<ChunkIndex>::create(chunk_count, ChunkConstructArgs<ChunkType>::construction_args(size));
    std::iota(chunk->begin(), chunk->end(), chunk_count * size);
    return chunk;
}

TYPED_TEST_P(AggregationBufferFillerTester, test_multi_chunk_to_fill)
{

    typedef typename TypeParam::BufferFillerType BufferFillerType;
    typedef typename BufferFillerType::SizeType SizeType;

    TypeParam t;

    SizeType chunk_size = SizeType(5);
    int num_chunks = 3;
    SizeType max_size = chunk_size * num_chunks;
    unsigned run=0;

    std::unique_ptr<BufferFillerType> buffer_filler(t.get_buffer_filler(
                    [&run, &buffer_filler](typename BufferFillerType::AggregationBufferType buffer)
                    {
                       switch(++run) {
                           case 1:
                               ASSERT_EQ(buffer.data_size(), buffer_filler->size());
                               break;
                           case 2:
                               ASSERT_EQ(std::size_t(0), buffer.data_size());
                               break;
                           default:
                               FAIL();
                       }
                    }, max_size ));


    *buffer_filler << *t.get_chunk(chunk_size);
    ASSERT_EQ(unsigned(0), run);
    *buffer_filler << *t.get_chunk(chunk_size);
    ASSERT_EQ(unsigned(0), run);
    *buffer_filler << *t.get_chunk(chunk_size);
    ASSERT_EQ(unsigned(1), run);

}

TYPED_TEST_P(AggregationBufferFillerTester, test_add_chunk_bigger_than_a_single_buffer)
{
    typedef typename TypeParam::BufferFillerType BufferFillerType;
    typedef typename BufferFillerType::SizeType SizeType;
    unsigned run=0;

    {
        TypeParam t;
        SizeType max_size = SizeType(10);
        auto chunk(t.get_chunk(max_size + 1));

        BufferFillerType* buffer_filler_ptr;
        std::unique_ptr<BufferFillerType> buffer_filler(t.get_buffer_filler(
                        [&run, &chunk, &buffer_filler, &buffer_filler_ptr]
                        (typename BufferFillerType::AggregationBufferType buffer)
                        {
                           switch(++run) {
                               case 1:
                                    {
                                        buffer_filler_ptr = buffer_filler.get(); // save for destructor
                                        ASSERT_EQ(buffer.data_size(), buffer_filler->size());
                                        auto chunk_it = chunk->begin();
                                        auto buffer_it = buffer.data();
                                        for(unsigned i=0; i<buffer_filler->size(); ++i)
                                        {
                                            ASSERT_EQ(*(chunk_it+i), *(buffer_it+i));
                                        }
                                    }
                                    break;
                               case 2: // destructor should pass down an unfilled buffer
                                    {
                                        // n.b. it is no longer safe to use the unique_ptr here as some implementations (e.g. clang)
                                        // can reset the pointer seen by the unique_ptr interface to nullptr before calling delete
                                        ASSERT_EQ(buffer_filler_ptr->size()-buffer_filler_ptr->remaining_capacity(), buffer.data_size());
                                        auto chunk_it = chunk->begin() + buffer_filler_ptr->size();
                                        auto buffer_it = buffer.data();
                                        for(unsigned i=0; i<1; ++i)
                                        {
                                            ASSERT_EQ(*(chunk_it+i), *(buffer_it+i));
                                        }
                                    }
                                    break;
                           }

                           // check composition data correct
                           auto objects=buffer.composition();
                           ASSERT_EQ(unsigned(1), objects.size());
                        }, max_size)
                       );

        *buffer_filler << *chunk;

        // Ensure data that can be consumed has been consumed
        ASSERT_EQ(unsigned(1), run);
    }
    // check fn called on destruct
    ASSERT_EQ(unsigned(2), run);

}

TYPED_TEST_P(AggregationBufferFillerTester, test_add_chunk_bigger_than_a_single_buffer_with_overlap)
{
    typedef typename TypeParam::BufferFillerType BufferFillerType;
    typedef typename BufferFillerType::SizeType SizeType;
    unsigned run=0;

    {
        TypeParam t;
        SizeType max_size = SizeType(10);
        SizeType delta = SizeType(1);
        SizeType chunk_size = max_size + delta;
        auto chunk=t.get_chunk(chunk_size);
        auto chunk2=t.get_chunk(chunk_size, 1);
        auto chunk3=t.get_chunk(chunk_size, 2);

        SizeType overlap_size= SizeType(1);
        SizeType last_chunk_offset= SizeType(0);

        BufferFillerType* buffer_filler_ptr;
        std::unique_ptr<BufferFillerType> buffer_filler(t.get_buffer_filler(
                        [ &run
                        , &last_chunk_offset
                        , &chunk, &chunk2, &chunk3
                        , &buffer_filler
                        , &buffer_filler_ptr](typename  BufferFillerType::AggregationBufferType buffer)
                        {
                           auto objects=buffer.composition();
                           run++;
                           switch(run) {
                               case 1:
                                    {
                                        buffer_filler_ptr = buffer_filler.get(); // save for when we can no longer use buffer_filler in the destructor
                                        ASSERT_EQ(buffer.data_size(), buffer_filler->size());
                                        auto chunk_it = chunk->begin();
                                        auto buffer_it = buffer.data();
                                        for(unsigned i=0; i<buffer_filler->size(); ++i)
                                        {
                                            ASSERT_EQ(*(chunk_it+i), *(buffer_it+i));
                                        }
                                        ASSERT_EQ(1U, objects.size());
                                        ASSERT_EQ(objects[0].get(), chunk.get());
                                        ASSERT_EQ(0U, buffer.offset_first_block());
                                    }
                                    break;
                               case 2:
                                    {
                                        std::size_t delta_size = chunk->size() - buffer_filler->size();
                                        SCOPED_TRACE("delta_size= " + std::to_string(delta_size));
                                        SCOPED_TRACE("overlap_size= " + std::to_string(buffer_filler->overlap()));
                                        SCOPED_TRACE("chunk_size= " + std::to_string(chunk->size()));
                                        ASSERT_EQ(buffer.data_size(), buffer_filler->size());
                                        ASSERT_EQ(2U, objects.size());
                                        ASSERT_EQ(objects[0].get(), chunk.get());
                                        ASSERT_EQ(objects[1].get(), chunk2.get());
                                        auto chunk_it = chunk->begin();
                                        auto buffer_it = buffer.data();
                                        chunk_it += (chunk->size() - buffer_filler->overlap() - delta_size);
                                        for(unsigned i=0; i<delta_size + buffer_filler->overlap(); ++i)
                                        {
                                            ASSERT_EQ(*(chunk_it+i), *(buffer_it+i));
                                        }

                                        chunk_it =  chunk2->begin();
                                        buffer_it = (buffer.data() + buffer_filler->overlap() + delta_size);
                                        for(unsigned i=0; i<buffer_filler->size() - buffer_filler->overlap() - delta_size; ++i)
                                        {
                                            ASSERT_EQ(*(chunk_it+i), *(buffer_it+i));
                                        }
                                    }
                                    break;
                               case 3: // destructor should pass down an unfilled buffer
                                       // |         |         |    3    |
                                       // |    c1    |
                                       //           |1 |   c2 |  |
                                       //                     |2|  | c3      |
                                    {
                                        std::size_t delta_size = chunk->size() - buffer.data_size();
                                        SCOPED_TRACE("delta_size= " + std::to_string(delta_size));
                                        SCOPED_TRACE("chunk_size= " + std::to_string(chunk->size()));
                                        ASSERT_EQ(2U, objects.size());
                                        ASSERT_EQ(objects[0].get(), chunk2.get());
                                        ASSERT_EQ(objects[1].get(), chunk3.get());
                                        ASSERT_EQ((buffer_filler->size() - buffer_filler->overlap() - delta_size) - buffer_filler->overlap(), buffer.offset_first_block());
                                        ASSERT_EQ(buffer_filler->size(), buffer.data_size());
                                        auto chunk_it = chunk2->begin() + buffer.offset_first_block();
                                        auto buffer_it = buffer.data();
                                        for(unsigned i=0; i<chunk->size() - buffer.offset_first_block(); ++i)
                                        {
                                            ASSERT_EQ(*(chunk_it+i), *(buffer_it+i));
                                        }

                                        last_chunk_offset = buffer_filler->size() - (chunk->size() - buffer.offset_first_block());
                                        chunk_it = chunk3->begin();
                                        buffer_it = buffer.data() + (chunk->size() - buffer.offset_first_block());
                                        for(unsigned i=0; i<(buffer_filler->size() - (chunk->size() - buffer.offset_first_block())); ++i)
                                        {
                                            ASSERT_EQ(*(chunk_it+i), *(buffer_it+i));
                                        }
                                    }
                                    break;
                               case 4: // destructor should pass down an unfilled buffer
                                    {
                                        // n.b. it is no longer safe to use the unique_ptr here as some implementations (e.g. clang)
                                        // can reset the pointer seen by the unique_ptr interface to nullptr before calling delete
                                        std::size_t delta_size = chunk->size() - buffer_filler_ptr->size();
                                        ASSERT_EQ(1U, objects.size());
                                        ASSERT_EQ(objects[0].get(), chunk3.get());
                                        ASSERT_EQ(std::size_t(3 * (delta_size + buffer_filler_ptr->overlap())), buffer.data_size());
                                        ASSERT_EQ(last_chunk_offset - buffer_filler_ptr->overlap(), buffer.offset_first_block());
                                        auto chunk_it = chunk3->begin() + last_chunk_offset - buffer_filler_ptr->overlap();
                                        auto buffer_it = buffer.data();
                                        for(unsigned i=0; i<buffer.data_size(); ++i)
                                        {
                                            ASSERT_EQ(*(chunk_it+i), *(buffer_it+i));
                                        }
                                    }
                                    break;
                           }

                        }, max_size)
                       );
        buffer_filler->set_overlap(overlap_size);

        *buffer_filler << *chunk;
        *buffer_filler << *chunk2;
        *buffer_filler << *chunk3;

        // Ensure data that can be consumed has been consumed
        ASSERT_EQ(unsigned(3), run);
    }
    // check fn called on destruct
    ASSERT_EQ(unsigned(4), run);
}


TYPED_TEST_P(AggregationBufferFillerTester, test_resize)
{
    typedef typename TypeParam::BufferFillerType BufferFillerT;
    typedef typename BufferFillerT::SizeType SizeType;

    TypeParam t;
    SizeType bsize {1000};

    std::unique_ptr<BufferFillerT> buffer(std::move(t.get_buffer_filler([](typename BufferFillerT::AggregationBufferType){}, bsize)));
    unsigned ratio = buffer->size()/bsize;
    buffer->resize(SizeType(2*bsize));
    ASSERT_EQ(2*bsize*ratio, buffer->size());
}

TYPED_TEST_P(AggregationBufferFillerTester, test_overlap_first_chunk_big_buffers)
{
    typedef typename TypeParam::BufferFillerType BufferFillerType;
    typedef typename BufferFillerType::SizeType SizeType;

    TypeParam t;
    SizeType chunk_size = SizeType(1<<20);
    SizeType max_size = SizeType(1<<22);
    unsigned ratio_floor = unsigned(max_size)/unsigned(chunk_size);
    SizeType overlap_size = SizeType(26560);

    unsigned run=0;
    std::vector<typename BufferFillerType::AggregationBufferType> buffers_rcvd;

    std::unique_ptr<BufferFillerType> buffer_filler(t.get_buffer_filler(
                    [&run, &buffers_rcvd](typename BufferFillerType::AggregationBufferType buffer)
                    {
                        buffers_rcvd.emplace_back(std::move(buffer));
                        ++run; // should be last thing
                    }
                    , max_size
    ));
    buffer_filler->set_overlap(overlap_size);

    auto chunk1 = t.get_chunk(chunk_size);

    unsigned chunk_count=0;
    for(unsigned i=0; i < ratio_floor; ++i)
    {
        ++chunk_count;
        auto chunk = t.get_chunk(chunk_size, chunk_count);
        *buffer_filler << *chunk;
    }
    ASSERT_EQ(1U, run) << chunk_count;
    ASSERT_EQ(0U, buffers_rcvd[0].offset_first_block());
    ASSERT_EQ(1U, buffers_rcvd[run-1].composition()[0]->index());

    std::size_t expected_buffer_space = buffer_filler->size() - buffer_filler->overlap();
    ASSERT_EQ(expected_buffer_space, buffer_filler->remaining_capacity());

    chunk_count=0;
    while(run < 2U) {
        ++chunk_count;
        auto chunk = t.get_chunk(chunk_size, chunk_count);
        *buffer_filler << *chunk;
    }
    ASSERT_EQ(chunk_count, buffers_rcvd[run-1].composition()[0]->index());

    std::size_t expected_buffer_overflow = (chunk_count * chunk1->size() + buffer_filler->overlap()) - expected_buffer_space;

    expected_buffer_space = buffer_filler->size() - expected_buffer_overflow;
    ASSERT_EQ(expected_buffer_space, buffer_filler->remaining_capacity());

    std::size_t first_offset = chunk1->size() - buffer_filler->overlap();
    ASSERT_EQ(first_offset, buffers_rcvd[1].offset_first_block());

    chunk_count=0;
    while(run < 3) {
        ++chunk_count;
        auto chunk = t.get_chunk(chunk_size, chunk_count);
        *buffer_filler << *chunk;
    }
    expected_buffer_overflow = (chunk_count * chunk1->size() + buffer_filler->overlap()) - expected_buffer_space;
    expected_buffer_space = buffer_filler->size() - expected_buffer_overflow;
    std::size_t second_offset = chunk1->size() - expected_buffer_overflow + buffer_filler->overlap();
    ASSERT_EQ(expected_buffer_space, buffer_filler->remaining_capacity());

    ASSERT_EQ(chunk_count, buffers_rcvd[run-1].composition()[0]->index());

    ASSERT_EQ(3U, buffers_rcvd.size());
    ASSERT_EQ(second_offset, buffers_rcvd[2].offset_first_block());
}

REGISTER_TYPED_TEST_SUITE_P(AggregationBufferFillerTester, test_overlap_first_chunk_big_buffers, test_resize, test_add_chunk_bigger_than_a_single_buffer_with_overlap, test_add_chunk_bigger_than_a_single_buffer, test_multi_chunk_to_fill);

} // namespace test
} // namespace panda
} // namespace ska
