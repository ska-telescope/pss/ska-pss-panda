/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/test/CopyTester.h"
#include "panda/DeviceIterator.h"
#include "panda/Copy.h"
#include <array>
#include <list>

namespace ska {
namespace panda {
namespace test {

template<typename T>
CopyTester<T>::CopyTester()
    : ::testing::Test()
{
}

template<typename T>
CopyTester<T>::~CopyTester()
{
}

template<typename T>
void CopyTester<T>::SetUp()
{
}

template<typename T>
void CopyTester<T>::TearDown()
{
}

TYPED_TEST_P(CopyTester, copy_to_from_device_contiguous_memory_input)
{
    // we test we can copy from a contiguous memory -> contiguous memory ion the device and back again
    typedef typename TypeParam::Arch Arch;
    auto device_mem = TypeParam::template contiguous_device_memory<int>(5);
    auto device_ptr = device_mem.begin();

    std::array<int,5> input = {{1, 2, 3, 4, 5}};
    typedef typename DeviceIterator<Arch, decltype(device_ptr)>::SelfType DeviceIteratorType;
    // copy to device
    panda::copy(input.begin(), input.end(), DeviceIteratorType(device_ptr));

    // copy to host
    std::array<int,6> out = {{0, 0, 0, 0 , 0, 100}};
    panda::copy(DeviceIteratorType(device_mem.begin()), DeviceIteratorType(decltype(device_ptr)(device_mem.begin() + 5)), out.begin());

    for(unsigned i=0; i < input.size(); ++i) {
        ASSERT_EQ(input[i], out[i]) << "i=" << i;
    }
    ASSERT_EQ(100, out[out.size() - 1]); // check for overflow
}

TYPED_TEST_P(CopyTester, copy_to_from_device_contiguous_memory_input_with_type_conversion)
{
    // we test we can copy from a contiguous memory -> contiguous memory on the device and back again, but with a type conversion (int -> float)
    typedef typename TypeParam::Arch Arch;
    auto device_mem = TypeParam::template contiguous_device_memory<float>(5);
    auto device_ptr = device_mem.begin();

    std::array<int,5> input = {{1, 2, 3, 4, 5}};
    typedef typename DeviceIterator<Arch, decltype(device_ptr)>::SelfType DeviceIteratorType;
    // copy to device
    panda::copy(input.begin(), input.end(), DeviceIteratorType(device_ptr));

    // copy to host
    std::array<float,6> out = {{0, 0, 0, 0 , 0, 100}};
    panda::copy(DeviceIteratorType(device_mem.begin()), DeviceIteratorType(decltype(device_ptr)(device_mem.begin() + 5)), out.begin());

    for(unsigned i=0; i < input.size(); ++i) {
        ASSERT_EQ((float)input[i], out[i]) << "i=" << i;
    }
    ASSERT_EQ(100, out[out.size() - 1]); // check for overflow
}

TYPED_TEST_P(CopyTester, copy_to_from_device_non_contiguous_memory_input)
{
    typedef typename TypeParam::Arch Arch;
    auto device_mem = TypeParam::template contiguous_device_memory<int>(5);
    auto device_ptr = device_mem.begin();

    std::list<int> input = {{1, 2, 3, 4, 5}};
    typedef typename DeviceIterator<Arch, decltype(device_ptr)>::SelfType DeviceIteratorType;
    // copy to device
    panda::copy(input.begin(), input.end(), DeviceIteratorType(device_ptr));

    // copy to host
    std::array<int,6> out = {{0, 0, 0, 0 , 0, 100}};
    panda::copy(DeviceIteratorType(device_mem.begin()), DeviceIteratorType(decltype(device_ptr)(device_mem.begin() + 5)), out.begin());

    auto list_it = input.begin();
    for(unsigned i=0; i < out.size() - 1; ++i) {
        ASSERT_EQ(*list_it++, out[i]) << "i=" << i;
    }
    ASSERT_EQ(100, out[out.size() - 1]); // check for overflow
}

TYPED_TEST_P(CopyTester, copy_to_from_device_non_contiguous_memory_input_with_type_conversion)
{
    typedef typename TypeParam::Arch Arch;
    auto device_mem = TypeParam::template contiguous_device_memory<float>(5);
    auto device_ptr = device_mem.begin();

    std::list<int> input = {{1, 2, 3, 4, 5}};
    typedef typename DeviceIterator<Arch, decltype(device_ptr)>::SelfType DeviceIteratorType;
    // copy to device
    panda::copy(input.begin(), input.end(), DeviceIteratorType(device_ptr));

    // copy to host
    std::array<float,6> out = {{0, 0, 0, 0 , 0, 100}};
    panda::copy(DeviceIteratorType(device_mem.begin()), DeviceIteratorType(decltype(device_ptr)(device_mem.begin() + 5)), out.begin());

    auto list_it = input.begin();
    for(unsigned i=0; i < out.size() - 1; ++i) {
        ASSERT_EQ(*list_it++, out[i]) << "i=" << i;
    }
    ASSERT_EQ(100, out[out.size() - 1]); // check for overflow
}

TYPED_TEST_P(CopyTester, copy_to_from_device_non_contiguous_memory_out)
{
    // test if we can copy from device to non-contiguous memory
    typedef typename TypeParam::Arch Arch;
    auto device_mem = TypeParam::template contiguous_device_memory<int>(5);
    auto device_ptr = device_mem.begin();

    std::array<int,5> input = {{1, 2, 3, 4, 5}};
    typedef typename DeviceIterator<Arch, decltype(device_ptr)>::SelfType DeviceIteratorType;
    // copy to device
    panda::copy(input.begin(), input.end(), DeviceIteratorType(device_ptr));

    // copy to host
    std::list<int> out = {{0, 0, 0, 0 , 0, 100}}; // last element should not be overwritten
    panda::copy(DeviceIteratorType(device_mem.begin()), DeviceIteratorType(decltype(device_ptr)(device_mem.begin() + 5)), out.begin());

    auto list_it = out.begin();
    for(unsigned i=0; i < input.size(); ++i) {
        ASSERT_EQ(input[i], *list_it++) << "i=" << i;
    }
    ASSERT_EQ(100, *list_it);
}

TYPED_TEST_P(CopyTester, copy_to_from_device_non_contiguous_memory_out_with_type_conversion)
{
    // test if we can copy from device to non-contiguous memory of a different type requiring a type conversion
    typedef typename TypeParam::Arch Arch;
    auto device_mem = TypeParam::template contiguous_device_memory<int>(5);
    auto device_ptr = device_mem.begin();

    std::array<int,5> input = {{1, 2, 3, 4, 5}};
    typedef typename DeviceIterator<Arch, decltype(device_ptr)>::SelfType DeviceIteratorType;
    // copy to device
    panda::copy(input.begin(), input.end(), DeviceIteratorType(device_ptr));

    // copy to host
    std::list<float> out = {{0, 0, 0, 0 , 0, 100}}; // last element should not be overwritten
    panda::copy(DeviceIteratorType(device_mem.begin()), DeviceIteratorType(decltype(device_ptr)(device_mem.begin() + 5)), out.begin());

    auto list_it = out.begin();
    for(unsigned i=0; i < input.size(); ++i) {
        ASSERT_EQ((float)input[i], *list_it++) << "i=" << i;
    }
    ASSERT_EQ(100.0, *list_it);
}

TYPED_TEST_P(CopyTester, copy_within_device_contiguous_memory_input)
{
    // here we test if we can do device -> device copies where the destination is of the type as the src (int)
    typedef typename TypeParam::Arch Arch;
    auto device_mem_input = TypeParam::template contiguous_device_memory<int>(5);
    auto device_input_ptr = device_mem_input.begin();

    auto device_mem_output = TypeParam::template contiguous_device_memory<int>(5);
    auto device_output_ptr = device_mem_output.begin();

    std::array<int,5> input = {{1, 2, 3, 4, 5}};
    typedef typename DeviceIterator<Arch, decltype(device_input_ptr)>::SelfType DeviceIteratorType;
    // copy to device
    panda::copy(input.begin(), input.end(), DeviceIteratorType(device_input_ptr));

    // copy device to device
    panda::copy(device_mem_input.begin(), device_mem_input.end(), DeviceIteratorType(device_output_ptr));

    // copy to host
    std::array<int,6> out = {{0, 0, 0, 0 , 0, 100}};
    panda::copy(DeviceIteratorType(device_mem_output.begin()), DeviceIteratorType(decltype(device_output_ptr)(device_mem_output.begin() + 5)), out.begin());

    for(unsigned i=0; i < input.size(); ++i) {
        ASSERT_EQ(input[i], out[i]) << "i=" << i;
    }
    ASSERT_EQ(100, out[out.size() - 1]); // check for overflow
}

TYPED_TEST_P(CopyTester, copy_within_device_contiguous_memory_input_with_type_conversion)
{
    // here we test if we can do device -> device copies where the destination is of a different value_type (float) to the src (int)
    typedef typename TypeParam::Arch Arch;
    auto device_mem_input = TypeParam::template contiguous_device_memory<int>(5);
    auto device_input_ptr = device_mem_input.begin();

    auto device_mem_output = TypeParam::template contiguous_device_memory<float>(5);
    auto device_output_ptr = device_mem_output.begin();

    std::array<int,5> input = {{1, 2, 3, 4, 5}};
    typedef typename DeviceIterator<Arch, decltype(device_input_ptr)>::SelfType DeviceIteratorType;
    typedef typename DeviceIterator<Arch, decltype(device_output_ptr)>::SelfType DeviceOutputIteratorType;
    // copy to device
    panda::copy(input.begin(), input.end(), DeviceIteratorType(device_input_ptr));

    // copy device to device
    panda::copy(device_mem_input.begin(), device_mem_input.end(), DeviceOutputIteratorType(device_output_ptr));

    // copy to host
    std::array<float,6> out = {{0, 0, 0, 0 , 0, 100}};
    panda::copy(DeviceOutputIteratorType(device_mem_output.begin()), DeviceOutputIteratorType(decltype(device_output_ptr)(device_mem_output.begin() + 5)), out.begin());

    for(unsigned i=0; i < input.size(); ++i) {
        ASSERT_EQ(input[i], out[i]) << "i=" << i;
    }
    ASSERT_EQ(100, out[out.size() - 1]); // check for overflow
}

REGISTER_TYPED_TEST_SUITE_P(CopyTester, copy_to_from_device_contiguous_memory_input, copy_to_from_device_contiguous_memory_input_with_type_conversion, copy_to_from_device_non_contiguous_memory_input, copy_to_from_device_non_contiguous_memory_input_with_type_conversion, copy_to_from_device_non_contiguous_memory_out, copy_to_from_device_non_contiguous_memory_out_with_type_conversion, copy_within_device_contiguous_memory_input, copy_within_device_contiguous_memory_input_with_type_conversion);

} // namespace test
} // namespace panda
} // namespace ska
