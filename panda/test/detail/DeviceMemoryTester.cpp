/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "panda/test/TestDevice.h"
#include <vector>
#include <algorithm>

namespace ska {
namespace panda {
namespace test {


template<typename TestTraits>
DeviceMemoryTester<TestTraits>::DeviceMemoryTester()
{
}

TYPED_TEST_P(DeviceMemoryTester, zero_length_construct)
{
    typedef TypeParam Traits;
    typedef typename Traits::DeviceMemoryType MemoryType;
    typedef typename MemoryType::Architecture Arch;
    typedef panda::test::TestDevice<Arch> TestDevices;

    TestDevices system;
    for(auto & device : system.devices()) {
        MemoryType memory(0, *device);
        ASSERT_EQ(memory.size(), std::size_t(0));
    }
}

TYPED_TEST_P(DeviceMemoryTester, reset)
{
    typedef TypeParam Traits;
    typedef typename Traits::DeviceMemoryType MemoryType;
    typedef typename MemoryType::Architecture Arch;
    typedef panda::test::TestDevice<Arch> TestDevices;

    TestDevices system;
    std::size_t n = 10;
    for(auto & device : system.devices()) {
        MemoryType memory(n, *device);
        ASSERT_EQ(memory.size(), n);
        // greater
        memory.reset(n+1);
        ASSERT_EQ(memory.size(), n+1);
        // smaller
        memory.reset(n);
        ASSERT_EQ(memory.size(), n);
    }
}

/*
 * Generic test for Device Memory to check the begin and end methods
 * of vartious flavours are callable and usable
 * Note in general you cannot use std::algorithms with the iterators from the host
 */
TYPED_TEST_P(DeviceMemoryTester, begin_end)
{
    typedef TypeParam Traits;
    typedef typename Traits::DeviceMemoryType MemoryType;
    typedef typename MemoryType::Architecture Arch;
    typedef panda::test::TestDevice<Arch> TestDevices;

    TestDevices system;
    std::size_t n = 10;
    for(auto & device : system.devices()) {
        MemoryType memory(n, *device);
        ASSERT_EQ(memory.size(), n);

        // begin end on non-const object
        std::size_t count=0;
        {
            auto it = memory.begin();
            ASSERT_EQ(n, std::size_t(std::distance(it, memory.end())));
            while(it != memory.end()) {
                ++count;
                ++it;
            }
            ASSERT_EQ(count, n);
        }

        // begin end on const object
        const MemoryType& const_memory = memory;
        {
            count=0;
            auto it = const_memory.begin();
            ASSERT_EQ(n, std::size_t(std::distance(it, const_memory.end())));
            while(it != const_memory.end()) {
                ++count;
                ++it;
            }
            ASSERT_EQ(count, n);
        }

        // cbegin cend on const object
        {
            count=0;
            auto it = const_memory.cbegin();
            ASSERT_EQ(n, std::size_t(std::distance(it, memory.cend())));
            while(it != const_memory.cend()) {
                ++count;
                ++it;
            }
            ASSERT_EQ(count, n);
        }
    }
}

REGISTER_TYPED_TEST_SUITE_P(DeviceMemoryTester, zero_length_construct, reset, begin_end);

} // namespace test
} // namespace panda
} // namespace ska
