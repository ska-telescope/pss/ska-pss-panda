/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/test/TestPacketStreamDataTraits.h"
#include <limits>


namespace ska {
namespace panda {
namespace test {


template<typename NumericalRep, typename PacketSizeType>
TestPacketStreamDataTraits<NumericalRep, PacketSizeType>::TestPacketStreamDataTraits(double packets_per_chunk)
    : _chunk_size(packets_per_chunk * packet_size())
    , _deserialise_chunks(new std::deque<DataType*>())
    , _missing_slice_chunks(new std::deque<DataType*>())
    , _resize_passed_params(new std::vector<std::pair<DataType*, PacketSizeType>>())
    , _shared(std::make_shared<SharedMembers>())
{
}

template<typename NumericalRep, typename PacketSizeType>
TestPacketStreamDataTraits<NumericalRep, PacketSizeType>::TestPacketStreamDataTraits(TestPacketStreamDataTraits const& dt)
    : _chunk_size(dt._chunk_size)
    , _deserialise_chunks(new std::deque<DataType*>())
    , _missing_slice_chunks(new std::deque<DataType*>())
    , _resize_passed_params(new std::vector<std::pair<DataType*, PacketSizeType>>())
    , _shared(dt._shared)
{
}

template<typename NumericalRep, typename PacketSizeType>
TestPacketStreamDataTraits<NumericalRep, PacketSizeType>::~TestPacketStreamDataTraits()
{
}

template<typename NumericalRep, typename PacketSizeType>
void TestPacketStreamDataTraits<NumericalRep, PacketSizeType>::chunk_size(std::size_t size)
{
    _chunk_size = size;
}

template<typename NumericalRep, typename PacketSizeType>
void TestPacketStreamDataTraits<NumericalRep, PacketSizeType>::block_in_deserialise_packet(bool val)
{
    {
        std::lock_guard<std::mutex> lk(_shared->_block_ds_mutex);
        _shared->_block_ds = val;
    }
    _shared->_block_ds_cv.notify_all();
}

template<typename NumericalRep, typename PacketSizeType>
void TestPacketStreamDataTraits<NumericalRep, PacketSizeType>::block_in_missing_packet(bool val)
{
    {
        std::lock_guard<std::mutex> lk(_shared->_block_mp_mutex);
        _shared->_block_mp = val;
    }
    _shared->_block_mp_cv.notify_all();
}

template<typename NumericalRep, typename PacketSizeType>
bool TestPacketStreamDataTraits<NumericalRep, PacketSizeType>::deserialise_called() const
{
    return _deserialise_chunks->size() > 0;
}

template<typename NumericalRep, typename PacketSizeType>
bool TestPacketStreamDataTraits<NumericalRep, PacketSizeType>::process_missing_slice_called() const
{
    return _missing_slice_chunks->size() > 0;
}

template<typename NumericalRep, typename PacketSizeType>
typename TestPacketStreamDataTraits<NumericalRep, PacketSizeType>::DataType* TestPacketStreamDataTraits<NumericalRep, PacketSizeType>::get_missing_chunk()
{
    auto front = _missing_slice_chunks->front();
    _missing_slice_chunks->pop_front();
    return front;
}

// ---------------- concepts interfaces -------------------
template<typename NumericalRep, typename PacketSizeType>
NumericalRep TestPacketStreamDataTraits<NumericalRep, PacketSizeType>::max_sequence_number()
{
    return std::numeric_limits<NumericalRep>::max();
}

template<typename NumericalRep, typename PacketSizeType>
std::size_t TestPacketStreamDataTraits<NumericalRep, PacketSizeType>::chunk_size(DataType const& data)
{
    return data.size();
}

template<typename NumericalRep, typename PacketSizeType>
NumericalRep TestPacketStreamDataTraits<NumericalRep, PacketSizeType>::sequence_number(PacketInspector const& p)
{
    return p.packet_count();
}

template<typename NumericalRep, typename PacketSizeType>
std::size_t TestPacketStreamDataTraits<NumericalRep, PacketSizeType>::packet_alignment_offset(PacketInspector const&)
{
    return 0;
}

template<typename NumericalRep, typename PacketSizeType>
template<typename ContextType>
void TestPacketStreamDataTraits<NumericalRep, PacketSizeType>::deserialise_packet(ContextType& ctx, PacketInspector const& p)
{
    PANDA_LOG_DEBUG << "Deserialising packet " << p.packet_count() << " data=" << *p.packet().begin() << " (" << ctx << ")";
    static_assert(!std::is_const<ContextType>::value, "expecting a non-const context");
    {
        std::lock_guard<std::mutex> lk(_shared->_mutex);
        _deserialise_chunks->push_back(&ctx.chunk());
        // copy data from packet into chunk
        std::copy(p.packet().begin(), p.packet().begin() + ctx.size(), ctx.chunk().begin() + ctx.offset());
    }

    // block if requested
    std::unique_lock<std::mutex> lk(_shared->_block_ds_mutex);
    _shared->_block_ds_cv.wait(lk, [this]() { return !_shared->_block_ds; });
}

template<typename NumericalRep, typename PacketSizeType>
template<typename ContextType>
void TestPacketStreamDataTraits<NumericalRep, PacketSizeType>::process_missing_slice(ContextType& ctx) {
    PANDA_LOG_DEBUG << "missing slice processing (" << ctx << ")";
    {
        std::lock_guard<std::mutex> lk(_shared->_mutex);
        _missing_slice_chunks->push_back(&ctx.chunk());
        ctx.chunk().set_missing_called();
    }
    // must come before any blocking of threa
    _shared->_cv.notify_all();

    std::unique_lock<std::mutex> lk(_shared->_block_mp_mutex);
    _shared->_block_mp_cv.wait(lk, [this]() { return !_shared->_block_mp; });
}

template<typename NumericalRep, typename PacketSizeType>
template<typename ContextType>
void TestPacketStreamDataTraits<NumericalRep, PacketSizeType>::resize_chunk(ContextType& ctx)
{
    PANDA_LOG_DEBUG << "chunk_resize processing size=" << ctx.size() << ctx;
    {
        std::unique_lock<std::mutex> lk(_shared->_resize_mutex);
        _resize_passed_params->emplace_back(&ctx.chunk(), ctx.size());
    }
    _shared->_resize_cv.notify_all();
}

template<typename NumericalRep, typename PacketSizeType>
PacketSizeType TestPacketStreamDataTraits<NumericalRep, PacketSizeType>::packet_size()
{
    return 2;
}

template<typename NumericalRep, typename PacketSizeType>
void TestPacketStreamDataTraits<NumericalRep, PacketSizeType>::packet_stats(uint64_t packets_received, uint64_t packets_expected)
{
    PANDA_LOG << "packet_stats (received:expected): "
        << packets_received << ":"
        << packets_expected;
}

template<typename NumericalRep, typename PacketSizeType>
std::vector<std::pair<typename TestPacketStreamDataTraits<NumericalRep, PacketSizeType>::DataType*, PacketSizeType>> const& TestPacketStreamDataTraits<NumericalRep, PacketSizeType>::resize_params() const
{
    return *_resize_passed_params;
}


template<typename NumericalRep, typename PacketSizeType>
void TestPacketStreamDataTraits<NumericalRep, PacketSizeType>::wait_process_missing_slice_called() const
{
    std::unique_lock<std::mutex> lk(_shared->_mutex);
    _shared->_cv.wait(lk, [&](){ return _missing_slice_chunks->size() != 0; });
}

template<typename NumericalRep, typename PacketSizeType>
void TestPacketStreamDataTraits<NumericalRep, PacketSizeType>::wait_resize_called() const
{
    std::unique_lock<std::mutex> lk(_shared->_resize_mutex);
    _shared->_resize_cv.wait(lk, [&](){ return _resize_passed_params->size() != 0; });
}

} // namespace test
} // namespace panda
} // namespace ska
