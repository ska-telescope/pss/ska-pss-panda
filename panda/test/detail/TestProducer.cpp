/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/test/TestProducer.h"
#include "panda/TupleUtilities.h"


namespace ska {
namespace panda {
namespace test {

template<typename... DataTypes>
TestProducer<DataTypes...>::TestProducer()
    : _rv(false)
{
}

template<typename... DataTypes>
template<typename IgnoredConfigType>
TestProducer<DataTypes...>::TestProducer(IgnoredConfigType const&)
    : _rv(false)
{
}

template<typename... DataTypes>
TestProducer<DataTypes...>::~TestProducer()
{
}

template<typename... DataTypes>
void TestProducer<DataTypes...>::stop()
{
}

template<typename... DataTypes>
void TestProducer<DataTypes...>::set_process_return(bool rv)
{
    _rv = rv;
}

template<typename... DataTypes>
template<typename ExceptionType>
void TestProducer<DataTypes...>::set_throw(ExceptionType exception)
{
    _exception_ptr = std::make_exception_ptr<ExceptionType>(exception);
}

template<typename ProducerType>
struct ProcessType {
    ProcessType(ProducerType& p) : _p(p) {};

    template<typename T>
    void operator()(T&& t) { 
        _p.process_type(std::forward<T&>(t));
    }

    ProducerType& _p;
};

template<typename... DataTypes>
bool TestProducer<DataTypes...>::process()
{
    if(_exception_ptr) std::rethrow_exception(_exception_ptr);
    panda::for_each(_data, ProcessType<TestProducer<DataTypes...>>(*this) );
    return _rv;
}

template<typename... DataTypes>
template<typename T>
void TestProducer<DataTypes...>::process_type(std::deque<T>& queue)
{
    std::lock_guard<std::mutex> lock(_mutex);
    for(auto data : queue) {
        auto write = this->template get_chunk<decltype(data)>(std::move(data)); // allow to drop out of scope
    }
    queue.clear();
}

template<typename... DataTypes>
template<typename DataType>
void TestProducer<DataTypes...>::data(DataType data)
{
    std::lock_guard<std::mutex> lock(_mutex);
    // put the data into the type specific queue
    std::get<panda::Index<std::deque<DataType>, decltype(_data)>::value>(_data).emplace_back(data);
}

template<typename... Types, typename ChunkType>
TestProducer<Types...>& operator<<(TestProducer<Types...>& p, ChunkType const & type)
{
    p.data(type);
    return p;
}

} // namespace test
} // namespace panda
} // namespace ska
