/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "panda/AggregationBufferFiller.h"
#include "panda/test/src/AggregationBufferFillerTest.h"
#include "panda/test/AggregationBufferFillerTester.h"
#include "panda/test/TestChunk.h"
#include <string>
#include <memory>
#include <typeinfo>
namespace ska {
namespace panda {
namespace test {

AggregationBufferFillerTest::AggregationBufferFillerTest()
    : ::testing::Test()
{
}

AggregationBufferFillerTest::~AggregationBufferFillerTest()
{
}

void AggregationBufferFillerTest::SetUp()
{
}

void AggregationBufferFillerTest::TearDown()
{
}

namespace {
    class A {
        public:
            template<typename T>
            int* begin(TestChunk<T>&);
            template<typename T>
            int* end(TestChunk<T>&);
    };

    class B
    {
        public:
            template<typename T>
            float* begin(TestChunk<T>&);
            template<typename T>
            float* end(TestChunk<T>&);
            std::size_t size() const;
            void resize(std::size_t);
            float create_buffer();
            std::size_t number_of_elements(std::size_t);
    };
    class C
    {
        public:
            std::size_t size();
            void resize(std::size_t);
            float create_buffer();
            std::size_t number_of_elements(std::size_t);
    };
    class D
    {
        public:
            std::size_t size() const;
            float create_buffer();
    };
    class E
    {
        public:
            std::size_t size() const;
            void resize(std::size_t);
            std::size_t number_of_elements(std::size_t);
    };
    class F
    {
    };

    class InheritBeginEndFactory : public BeginEndFactory<TestChunk<int>>
    {
            typedef BeginEndFactory<TestChunk<int>> BaseT;
        public:
            using BaseT::begin;
            using BaseT::end;
            int* begin(TestChunk<double>&);
            int* end(TestChunk<double>&);
    };

} // namespace

// use default traits for the PandaBuffer AggregationBufferFiller
struct PandaBufferAggBufferTraits {
};

template<typename NumericalRep, typename FactoryTraits>
class PandaBufferAggregationBufferFillerTestTraits : public AggregationBufferFillerTestTraits<panda::AggregationBufferFiller<TestChunk<NumericalRep>, FactoryTraits>>
{
        typedef AggregationBufferFillerTestTraits<AggregationBufferFiller<TestChunk<NumericalRep>, FactoryTraits>> BaseT;

    public:
        typedef typename BaseT::BufferFillerType BufferFillerType;
        typedef typename BaseT::FullBufferHandlerT FullBufferHandlerT;

    public:
        BufferFillerType* get_buffer_filler(FullBufferHandlerT handler, typename BaseT::SizeType size) override
        {
            return new BufferFillerType(handler, size);
        }

};

TEST_F(AggregationBufferFillerTest, test_has_method)
{
    static_assert(panda::detail::has_begin<A, TestChunk<char>>::value, "error");
    static_assert(panda::detail::has_end<A, TestChunk<char>>::value, "error");
    static_assert(!panda::detail::has_size<A>::value, "error");
    static_assert(!panda::detail::has_resize<A>::value, "error");
    static_assert(!panda::detail::has_create_buffer<A>::value, "error");
    static_assert(!panda::detail::has_number_of_elements<A>::value, "error");
    typedef typename panda::detail::AggregationBufferFillerTraitsAdaptor<TestChunk<char>, F> traits_A;
    static_assert(std::is_same<std::remove_pointer<decltype(std::declval<traits_A>().create_buffer())>::type, typename panda::Buffer<char>>(), "error");


    static_assert(panda::detail::has_begin<B, TestChunk<char>>::value, "error");
    static_assert(panda::detail::has_end<B, TestChunk<char>>::value, "error");
    static_assert(panda::detail::has_size<B>::value, "error");
    static_assert(panda::detail::has_resize<B>::value, "error");
    static_assert(panda::detail::has_create_buffer<B>::value, "error");
    static_assert(panda::detail::has_number_of_elements<B>::value, "error");
    typedef typename panda::detail::AggregationBufferFillerTraitsAdaptor<TestChunk<char>, B> traits_B;
    static_assert(!std::is_same<std::remove_pointer<decltype(std::declval<traits_B>().create_buffer())>::type, typename panda::Buffer<char>>(), "error");
    static_assert(std::is_same<std::remove_pointer<decltype(std::declval<traits_B>().create_buffer())>::type, float>::value, "error");

    static_assert(!panda::detail::has_begin<C, TestChunk<char>>::value, "error");
    static_assert(!panda::detail::has_end<C, TestChunk<char>>::value, "error");
    static_assert(!panda::detail::has_size<C>::value, "error");
    static_assert(!panda::detail::has_resize<C>::value, "error");
    static_assert(panda::detail::has_create_buffer<C>::value, "error");
    static_assert(!panda::detail::has_number_of_elements<C>::value, "error");
    typedef typename panda::detail::AggregationBufferFillerTraitsAdaptor<TestChunk<char>, C> TraitsC;
    static_assert(std::is_same<decltype(std::declval<TraitsC>().create_buffer()), decltype(std::declval<C>().create_buffer())>::value, "error");

    static_assert(!panda::detail::has_begin<D, TestChunk<char>>::value, "error");
    static_assert(!panda::detail::has_end<D, TestChunk<char>>::value, "error");
    static_assert(panda::detail::has_size<D>::value, "error");
    static_assert(!panda::detail::has_resize<D>::value, "error");
    static_assert(panda::detail::has_create_buffer<D>::value, "error");
    static_assert(!panda::detail::has_number_of_elements<D>::value, "error");
    typedef typename panda::detail::AggregationBufferFillerTraitsAdaptor<TestChunk<char>, D> traits_D;
    static_assert(std::is_same<std::remove_pointer<decltype(std::declval<traits_D>().create_buffer())>::type, decltype(std::declval<D>().create_buffer())>::value, "error");

    static_assert(!panda::detail::has_begin<E, TestChunk<char>>::value, "error");
    static_assert(!panda::detail::has_end<E, TestChunk<char>>::value, "error");
    static_assert(panda::detail::has_size<E>::value, "error");
    static_assert(panda::detail::has_resize<E>::value, "error");
    static_assert(!panda::detail::has_create_buffer<E>::value, "error");
    static_assert(panda::detail::has_number_of_elements<E>::value, "error");
    typedef typename panda::detail::AggregationBufferFillerTraitsAdaptor<TestChunk<char>, E> traits_E;
    static_assert(std::is_same<std::remove_pointer<decltype(std::declval<traits_E>().create_buffer())>::type, typename panda::Buffer<char>>(), "error");

    // test use of BeginEndFactory wrt begin/end
    static_assert(panda::detail::has_begin<BeginEndFactory<TestChunk<char>>, TestChunk<char>>::value, "error");
    static_assert(!panda::detail::has_begin<InheritBeginEndFactory, TestChunk<char>>::value, "error");
    static_assert(panda::detail::has_begin<InheritBeginEndFactory, TestChunk<double>>::value, "error");
    static_assert(panda::detail::has_begin<InheritBeginEndFactory, TestChunk<int>>::value, "error");

    static_assert(panda::detail::has_end<BeginEndFactory<TestChunk<char>>, TestChunk<char>>::value, "error");
    static_assert(!panda::detail::has_end<InheritBeginEndFactory, TestChunk<char>>::value, "error");
    static_assert(panda::detail::has_end<InheritBeginEndFactory, TestChunk<double>>::value, "error");
    static_assert(panda::detail::has_end<InheritBeginEndFactory, TestChunk<int>>::value, "error");
}

// use the AggregateBufferFillerTester to test interfaces
typedef ::testing::Types<PandaBufferAggregationBufferFillerTestTraits<char,PandaBufferAggBufferTraits>
                        , PandaBufferAggregationBufferFillerTestTraits<uint8_t,PandaBufferAggBufferTraits>
                        , PandaBufferAggregationBufferFillerTestTraits<uint16_t,PandaBufferAggBufferTraits>
                        , PandaBufferAggregationBufferFillerTestTraits<float,PandaBufferAggBufferTraits>
                        , PandaBufferAggregationBufferFillerTestTraits<uint32_t,PandaBufferAggBufferTraits>
                        , PandaBufferAggregationBufferFillerTestTraits<int,PandaBufferAggBufferTraits>
                        > BufferFactoryTypes;

INSTANTIATE_TYPED_TEST_SUITE_P(PandaBufferAggregationBufferFillerTests, AggregationBufferFillerTester, BufferFactoryTypes);

} // namespace test
} // namespace panda
} // namespace ska
