/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "AlgorithmInfoTest.h"
#include "panda/AlgorithmInfo.h"


namespace ska {
namespace panda {
namespace test {

struct ArchA {};
struct ArchB {};
struct SubArchB : ArchB {};

struct Algo1 {
    typedef ArchA Architecture;
};

struct Algo2 {
    typedef ArchB Architecture;
};

struct Algo3 {
    typedef ArchB Architecture;
    typedef SubArchB ArchitectureCapability;
};


AlgorithmInfoTest::AlgorithmInfoTest()
    : BaseT()
{
}

AlgorithmInfoTest::~AlgorithmInfoTest()
{
}

void AlgorithmInfoTest::SetUp()
{
}

void AlgorithmInfoTest::TearDown()
{
}

TEST_F(AlgorithmInfoTest, test_mixin_type_generator_multi_arch_algo_tuple)
{
    typedef std::tuple<Algo1, Algo2> Type;
    static_assert(std::is_same<std::tuple<ArchA,ArchB>, typename MixedAlgoTraits<Type>::Architectures >::value, "Mixin Failed to extract correct Arch list");
    static_assert(std::is_same<std::tuple<ArchA,ArchB>, typename MixedAlgoTraits<Type>::Capabilities >::value, "Mixin Failed to extract correct Capability list");
}

TEST_F(AlgorithmInfoTest, test_mixin_type_generator_single_arch_with_algo_tuple_same_type)
{
    typedef std::tuple<Algo1> Type2;
    static_assert(std::is_same<std::tuple<ArchA>, typename MixedAlgoTraits<Type2>::Architectures >::value, "Mixin Failed to extract correct Arch list");
    static_assert(std::is_same<std::tuple<ArchA>, typename MixedAlgoTraits<Type2>::Capabilities >::value, "Mixin Failed to extract correct Capability list");
}

TEST_F(AlgorithmInfoTest, test_mixin_type_generator_tuple_with_only_duplicate_algos)
{
    typedef std::tuple<Algo1, Algo1> Type3;
    static_assert(std::is_same<std::tuple<ArchA>, typename MixedAlgoTraits<Type3>::Architectures >::value, "Mixin Failed to extract correct Arch list");
    static_assert(std::is_same<std::tuple<ArchA>, typename MixedAlgoTraits<Type3>::Capabilities >::value, "Mixin Failed to extract correct Capability list");
}

TEST_F(AlgorithmInfoTest, test_mixin_type_generator_tuple_with_multiple_algos_some_duplicates)
{
    typedef std::tuple<Algo1, Algo2, Algo1> Type4;
    static_assert(std::is_same<std::tuple<ArchB,ArchA>, typename MixedAlgoTraits<Type4>::Architectures >::value, "Mixin Failed to extract correct Arch list");
    static_assert(std::is_same<std::tuple<ArchB, ArchA>, typename MixedAlgoTraits<Type4>::Capabilities >::value, "Mixin Failed to extract correct Capability list");
}

TEST_F(AlgorithmInfoTest, test_mixin_type_generator_single_algo_with_capabilities_specifications)
{
    typedef std::tuple<Algo3> Type5;
    static_assert(std::is_same<std::tuple<ArchB>, typename MixedAlgoTraits<Type5>::Architectures >::value, "Mixin Failed to extract correct Arch list");
    static_assert(std::is_same<std::tuple<SubArchB>, typename MixedAlgoTraits<Type5>::Capabilities >::value, "Mixin Failed to extract correct Capability list");
}

TEST_F(AlgorithmInfoTest, test_mixin_type_generator_algo_with_capabilities_specifications_with_algo_without_capabilities)
{
    typedef std::tuple<Algo2, Algo3> Type6;
    static_assert(std::is_same<std::tuple<ArchB, ArchB>, typename MixedAlgoTraits<Type6>::Architectures >::value, "Mixin Failed to extract correct Arch list");
    static_assert(std::is_same<std::tuple<ArchB, SubArchB>, typename MixedAlgoTraits<Type6>::Capabilities >::value, "Mixin Failed to extract correct Capability list");

    typedef std::tuple<Algo3, Algo2> Type7;
    static_assert(std::is_same<std::tuple<ArchB, ArchB>, typename MixedAlgoTraits<Type7>::Architectures >::value, "Mixin Failed to extract correct Arch list");
    static_assert(std::is_same<std::tuple<SubArchB, ArchB>, typename MixedAlgoTraits<Type7>::Capabilities >::value, "Mixin Failed to extract correct Capability list");
}

TEST_F(AlgorithmInfoTest, test_mixin_type_generator_duplicate_algo_with_capabilities_specifications_with_algo_without_capabilities)
{
    typedef std::tuple<Algo3, Algo2, Algo3> Type8;
    static_assert(std::is_same<std::tuple<ArchB, ArchB>, typename MixedAlgoTraits<Type8>::Architectures >::value, "Mixin Failed to extract correct Arch list");
    static_assert(std::is_same<std::tuple<ArchB, SubArchB>, typename MixedAlgoTraits<Type8>::Capabilities >::value, "Mixin Failed to extract correct Capability list");
}

} // namespace test
} // namespace panda
} // namespace ska
