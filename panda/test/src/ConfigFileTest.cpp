/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "ConfigFileTest.h"
#include "panda/ConfigFile.h"
#include "panda/Error.h"
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wall"
#include <boost/range/adaptor/filtered.hpp>
#pragma GCC diagnostic pop
#include <string>

namespace ska {
namespace panda {
namespace test {

ConfigFileTest::ConfigFileTest()
    : ::testing::Test()
{
}

ConfigFileTest::~ConfigFileTest()
{
}

void ConfigFileTest::SetUp()
{
}

void ConfigFileTest::TearDown()
{
}

TEST_F(ConfigFileTest, test_file_not_exist)
{
    panda::ConfigFile xml("non_existing_config.xml");
    ASSERT_THROW(xml.parse(), panda::Error);
}

TEST_F(ConfigFileTest, test_type_detection)
{
    panda::ConfigFile xml("config.xml");
    ASSERT_EQ(panda::ConfigFile::FileType::Xml, xml.type());
    panda::ConfigFile Xml("config.XML");
    ASSERT_EQ(panda::ConfigFile::FileType::Xml, Xml.type());

    panda::ConfigFile json("config.json");
    ASSERT_EQ(panda::ConfigFile::FileType::Json, json.type());
    panda::ConfigFile Json("config.Json");
    ASSERT_EQ(panda::ConfigFile::FileType::Json, Json.type());

    panda::ConfigFile ini("config.ini");
    ASSERT_EQ(panda::ConfigFile::FileType::Ini, ini.type());
    panda::ConfigFile no_ext("config");
    ASSERT_EQ(panda::ConfigFile::FileType::Xml, no_ext.type());
    panda::ConfigFile unknown_ext("config.unknown");
    ASSERT_EQ(panda::ConfigFile::FileType::None, unknown_ext.type());
}

} // namespace test
} // namespace panda
} // namespace ska
