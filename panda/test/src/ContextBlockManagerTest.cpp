/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "ContextBlockManagerTest.h"
#include "panda/ProcessingEngine.h"
#include "panda/detail/packet_stream/ContextBlockManager.h"
#include "panda/detail/packet_stream/ChunkerContext.h"
#include "panda/test/TestPacket.h"
#include "panda/test/TestChunk.h"
#include "panda/test/TestPacketStreamDataTraits.h"

namespace ska {
namespace panda {
namespace test {

struct ChunkFactory {
    ChunkFactory(std::size_t data_size)
        : _data(data_size, 'A')
        , _creation_ordered_list(std::make_shared<std::vector<TestChunk_A*>>())
    {
    }

    ~ChunkFactory()
    {
    }

    template<typename... Types>
    std::shared_ptr<TestChunk_A> operator()(Types&&...)
    {
        auto chunk = std::make_shared<TestChunk_A>(_data);
        _creation_ordered_list->push_back(chunk.get());
        return chunk;
    }

    std::vector<TestChunk_A*> const& created_object_list() const
    {
        return *_creation_ordered_list;
    }

    private:
        std::string _data;
        std::shared_ptr<std::vector<TestChunk_A*>> _creation_ordered_list; // does not own
};


ContextBlockManagerTest::ContextBlockManagerTest()
    : ::testing::Test()
{
}

ContextBlockManagerTest::~ContextBlockManagerTest()
{
}

void ContextBlockManagerTest::SetUp()
{
}

void ContextBlockManagerTest::TearDown()
{
}

template<typename T>
ContextBlockManagerTypedTest<T>::ContextBlockManagerTypedTest()
    : ::testing::Test()
{
}

template<typename T>
ContextBlockManagerTypedTest<T>::~ContextBlockManagerTypedTest()
{
}

template<typename LimitTypeT, typename PacketSizeTypeT>
struct TestDataTraits : public TestPacketStreamDataTraits<LimitTypeT, PacketSizeTypeT>
{
        typedef TestPacketStreamDataTraits<LimitTypeT, PacketSizeTypeT> BaseT;

    public:
        typedef LimitTypeT LimitType;
        typedef PacketSizeTypeT PacketSizeType;

    public:
        TestDataTraits(double packets_per_chunk)
            : BaseT(packets_per_chunk)
        {
        }
        typedef TestChunk_A DataType;
        typedef TestPacket<2> PacketInspector;

};

typedef ::testing::Types< TestDataTraits<uint64_t, std::size_t>
                        , TestDataTraits<std::size_t, std::size_t>
                        , TestDataTraits<std::size_t, std::size_t>
                        , TestDataTraits<unsigned int, std::size_t>
                        , TestDataTraits<int, std::size_t>
                        , TestDataTraits<uint16_t, std::size_t>
                        , TestDataTraits<uint8_t, std::size_t>
                        > ContextManagerTestTypes;
TYPED_TEST_CASE(ContextBlockManagerTypedTest, ContextManagerTestTypes);


static panda::ProcessingEngine engine(1);

template<typename ChunkFactory, typename DataTraitsType=TestDataTraits<uint32_t, std::size_t>>
class TestContextBlockManager : public panda::packet_stream::ContextBlockManager<panda::packet_stream::ChunkerContext<DataTraitsType, ChunkFactory>>
{
        typedef panda::packet_stream::ContextBlockManager<panda::packet_stream::ChunkerContext<DataTraitsType, ChunkFactory>> BaseT;

    public:
        template<typename ... Args>
        TestContextBlockManager(ChunkFactory const& factory, std::size_t number_of_blocks, std::size_t purge_cadence, Args&&... args)
            : BaseT(engine, factory, number_of_blocks, purge_cadence, std::forward<Args>(args)...)
        {
        }
};

TEST_F(ContextBlockManagerTest, test_purge_cadence_non_complete)
{
    unsigned cadence=4; // this is 5 packets behind (counts from 0)
    unsigned wrap_around=2;
    unsigned packets_per_chunk=2*cadence + 1;
    unsigned data_size=packets_per_chunk*2; // packet_size * packets_per_chunk

    ChunkFactory chunk_factory(data_size);
    TestContextBlockManager<decltype(chunk_factory)> manager(chunk_factory, wrap_around, cadence, packets_per_chunk);
    typedef typename TestContextBlockManager<decltype(chunk_factory)>::ContextType ContextType;

    // activate all the packets to the purge cadence
    std::vector<ContextType*> contexts;
    for(unsigned i=0; i < cadence * 2; ++i) {
        TestPacket<2> packet(i);
        TestPacketInspector<2> pi(packet);
        ContextType* ctx=manager.get_context(pi);
        ASSERT_FALSE(ctx==nullptr);
        contexts.push_back(ctx);
        ASSERT_FALSE(contexts[i]->complete()) << i;
    }

    // ensure all packets in first block are marked complete
    for(unsigned i=0; i < cadence -1; ++i) {
        TestPacket<2> packet(i);
        TestPacketInspector<2> pi(packet);
        //ContextType* ctx=manager.get_context(pi);
        //ASSERT_TRUE(ctx==nullptr) << i << *ctx;
        ASSERT_TRUE(contexts[i]->complete());
    }

    // ensure all packets in second block are not marked complete
    for(unsigned i=cadence - 1; i < 2*cadence; ++i) {
        TestPacket<2> packet(i);
        TestPacketInspector<2> pi(packet);
        ContextType* ctx=manager.get_context(pi);
        ASSERT_FALSE(ctx==nullptr) << i;
        ASSERT_TRUE(ctx->is_ready());
        ASSERT_FALSE(ctx->complete());
    }
}

TYPED_TEST(ContextBlockManagerTypedTest, test_equal_size_normal_ordering)
{
    typedef TypeParam TestDataTraits;
    typedef typename TestDataTraits::LimitType LimitType;
    typedef typename TestDataTraits::PacketSizeType PacketSizeType;

    unsigned packets_per_chunk=1;

    auto data_size = 1 * TestDataTraits::packet_size();
    ChunkFactory factory(data_size);
    typedef TestContextBlockManager<ChunkFactory, TestDataTraits> ContextMgrType;
    ContextMgrType context_mgr(factory, 100, 10, packets_per_chunk);

    auto context = context_mgr.get_context(0);
    ASSERT_EQ(context->packet_number(), LimitType(0));
    ASSERT_EQ(context->offset(), PacketSizeType(0));
    ASSERT_EQ(context->packet_offset(), PacketSizeType(0));

    auto next=context->get_context(1);
    ASSERT_NE(next, context);
    ASSERT_EQ(next->packet_number(), LimitType(1));
    ASSERT_EQ(next->packet_offset(), PacketSizeType(0));
    ASSERT_EQ(next->offset(), PacketSizeType(0));

    // Use Case: get_context called called on initial constructed context after a previous call
    auto nextnext=context->get_context(2);
    ASSERT_NE(nextnext->chunk(), context->chunk());
    ASSERT_EQ(nextnext->packet_number(), LimitType(2));
    ASSERT_EQ(nextnext->packet_offset(), PacketSizeType(0));
    ASSERT_EQ(nextnext->offset(), PacketSizeType(0));

    // Use Case: get_context called called on initial constructed context after a previous call
    // verify that the context returned is the same no matter which context it is called from
    auto nextnext1=next->get_context(2);
    ASSERT_EQ(nextnext->chunk(), nextnext1->chunk());

}

TYPED_TEST(ContextBlockManagerTypedTest, test_equal_size_out_of_order)
{
    typedef TypeParam TestDataTraits;
    typedef typename TestDataTraits::LimitType LimitType;
    typedef typename TestDataTraits::PacketSizeType PacketSizeType;
    unsigned packets_per_chunk=1;

    auto data_size = 1 * TestDataTraits::packet_size();
    ChunkFactory factory(data_size);
    typedef TestContextBlockManager<ChunkFactory, TestDataTraits> ContextMgrType;
    ContextMgrType context_mgr(factory, 100, 10, packets_per_chunk);

    // should realign to ctx 10
    auto context = context_mgr.get_context(10);
    ASSERT_NE(context, nullptr);
    ASSERT_EQ(context->packet_number(), LimitType(10));
    ASSERT_EQ(context->offset(), PacketSizeType(0));

    // contexts requested after a realign should return nullptr
    for(int i=0; i < 10; ++i) {
        auto prev=context->get_context(i);
        ASSERT_EQ(prev, nullptr);
    }

    // 12 called before 11
    auto ctx12=context->get_context(12);
    ASSERT_NE(ctx12, nullptr);
    ASSERT_EQ(ctx12->packet_number(), LimitType(12));
    ASSERT_EQ(ctx12->offset(), PacketSizeType(0));

    // get missing context from preceding context
    auto ctx11=context->get_context(11);
    ASSERT_NE(ctx11, nullptr);
    ASSERT_EQ(ctx11->packet_number(), LimitType(11));
    ASSERT_EQ(ctx11->offset(), PacketSizeType(0));

    // skip packet 13
    auto ctx14=ctx11->get_context(14);
    ASSERT_NE(ctx14, nullptr);
    ASSERT_NE(ctx14, ctx12);
    ASSERT_NE(ctx14, ctx11);
    ASSERT_EQ(ctx14->packet_number(), LimitType(14));
    ASSERT_EQ(ctx14->offset(), PacketSizeType(0));

    // request missing context from trailing context
    auto ctx13=ctx14->get_context(13);
    ASSERT_NE(ctx13, nullptr);
    ASSERT_EQ(ctx13->packet_number(), LimitType(13));
    ASSERT_EQ(ctx13->offset(), PacketSizeType(0));
}

TYPED_TEST(ContextBlockManagerTypedTest, test_chunk_slightly_bigger_than_packet)
{
    // Use Case
    // Packets  | 1| 2| 3| 4| 5| 6|
    // Chunks   |  1|  2|  3|  4|
    // Context  |  || | ||  |  || |  new context for each packet and chunk boundary
    //          0  12 3 45  6  78
    // offsets should realign after 3 chunks (2 packets)

    typedef TypeParam TestDataTraits;
    typedef typename TestDataTraits::LimitType LimitType;
    typedef typename TestDataTraits::PacketSizeType PacketSizeType;
    double data_size = 1 + TestDataTraits::packet_size();

    double packets_per_chunk=data_size/TestDataTraits::packet_size();
    std::cout << "data_size=" << data_size << " packets_per_chunk=" << packets_per_chunk << "\n";

    ChunkFactory factory(data_size);
    typedef TestContextBlockManager<ChunkFactory, TestDataTraits> ContextMgrType;
    ContextMgrType context_mgr(factory, 100, 10, packets_per_chunk);

    auto ctx1=context_mgr.get_context(1);
    ASSERT_NE(ctx1, nullptr);
    ASSERT_EQ(ctx1->packet_number(), LimitType(1));
    ASSERT_EQ(ctx1->offset(), PacketSizeType(0));
    ASSERT_EQ(ctx1->packet_offset(), PacketSizeType(0));

    auto ctx2=context_mgr.get_context(2);
    ASSERT_NE(ctx2, nullptr);
    ASSERT_NE(ctx2, ctx1);
    ASSERT_EQ(ctx2->packet_number(), LimitType(2));
    ASSERT_EQ(ctx2->offset(), TestDataTraits::packet_size()) << *ctx2;
    ASSERT_EQ(ctx2->packet_offset(), PacketSizeType(0));
    ASSERT_EQ(&*ctx1->chunk(), &*ctx2->chunk());

    auto ctx3=context_mgr.get_context(3);
    ASSERT_NE(ctx3, nullptr);
    ASSERT_NE(ctx2, ctx3);
    ASSERT_EQ(ctx3->packet_number(), LimitType(3));
    ASSERT_EQ(ctx3->offset(), PacketSizeType(1));
    ASSERT_EQ(ctx3->packet_offset(), PacketSizeType(0));
    ASSERT_NE(&*ctx3->chunk(), &*ctx2->chunk());

    auto ctx4=context_mgr.get_context(4);
    ASSERT_NE(ctx4, nullptr);
    ASSERT_NE(ctx2, ctx4);
    ASSERT_EQ(ctx4->packet_number(), LimitType(4));
    ASSERT_EQ(ctx4->offset(), PacketSizeType(0));
    ASSERT_EQ(ctx4->packet_offset(), PacketSizeType(0));
    ASSERT_NE(&*ctx4->chunk(), &*ctx2->chunk());
    ASSERT_NE(&*ctx4->chunk(), &*ctx3->chunk());

}

TYPED_TEST(ContextBlockManagerTypedTest, test_next_chunk_bigger_than_packet_missing_chunks_across_chunk_bonndary)
{
    // Use Case
    // Packets  | 0| 1| 2| 3| 4| 5| 6| 7|
    // Chunks   |  1|  2|  3|  4|  5|  6|
    // Context  |  || | ||  |  || |  new context for each packet and chunk boundary
    //          0  12 3 45  6  78

    typedef TypeParam TestDataTraits;
    typedef typename TestDataTraits::LimitType LimitType;
    typedef typename TestDataTraits::PacketSizeType PacketSizeType;
    double data_size = 1 + TestDataTraits::packet_size();

    double packets_per_chunk=data_size/TestDataTraits::packet_size();
    //std::cout << "data_size=" << data_size << " packets_per_chunk=" << packets_per_chunk << "\n";

    ChunkFactory factory(data_size);
    typedef TestContextBlockManager<ChunkFactory, TestDataTraits> ContextMgrType;
    ContextMgrType context_mgr(factory, 100, 10, packets_per_chunk);

    auto ctx0=context_mgr.get_context(0);
    ASSERT_NE(ctx0, nullptr);
    ASSERT_EQ(ctx0->packet_number(), LimitType(0));
    ASSERT_EQ(ctx0->offset(), PacketSizeType(0));
    ASSERT_EQ(ctx0->packet_offset(), PacketSizeType(0));

    // skip packet 1 (extends over chunk boundary)
    auto ctx2=context_mgr.get_context(2);
    ASSERT_NE(ctx2, nullptr);
    ASSERT_EQ(ctx2->packet_number(), LimitType(2));
    ASSERT_EQ(ctx2->offset(), PacketSizeType(1));
    ASSERT_EQ(ctx2->packet_offset(), PacketSizeType(0));
    ASSERT_NE(&*ctx0->chunk(), &*ctx2->chunk());

}

TYPED_TEST(ContextBlockManagerTypedTest, test_chunk_smaller_than_packet)
{
    // Use Case
    // Packets | 1| 2| 3| 4|
    // Chunks  |1|2|3|4|5|6|
    // Context | ||| | ||| |  new context for each packet and chunk boundary
    //         0 123 4 567
    // offsets should realign after 3 chunks (2 packets)

    typedef TypeParam TestDataTraits;
    typedef typename TestDataTraits::LimitType LimitType;
    typedef typename TestDataTraits::PacketSizeType PacketSizeType;
    double data_size = TestDataTraits::packet_size() - 1;

    double packets_per_chunk=data_size/TestDataTraits::packet_size();

    ChunkFactory factory(data_size);
    typedef TestContextBlockManager<ChunkFactory, TestDataTraits> ContextMgrType;
    ContextMgrType context_mgr(factory, 100, 10, packets_per_chunk);

    auto ctx1=context_mgr.get_context(1);
    ASSERT_NE(ctx1, nullptr);
    ASSERT_EQ(ctx1->packet_number(), LimitType(1));
    ASSERT_EQ(ctx1->offset(), PacketSizeType(0));
    ASSERT_EQ(ctx1->packet_offset(), PacketSizeType(0));

    auto ctx2=context_mgr.get_context(2);
    ASSERT_NE(ctx2, nullptr);
    ASSERT_EQ(ctx2->packet_number(), LimitType(2));
    ASSERT_EQ(ctx2->offset(), PacketSizeType(0));
    ASSERT_EQ(ctx2->packet_offset(), PacketSizeType(0));
    ASSERT_NE(&*ctx1->chunk(), &*ctx2->chunk());

    auto ctx3=context_mgr.get_context(3);
    ASSERT_NE(ctx3, nullptr);
    ASSERT_EQ(ctx3->packet_number(), LimitType(3));
    ASSERT_EQ(ctx3->offset(), PacketSizeType(0));
    ASSERT_EQ(ctx3->packet_offset(), PacketSizeType(0));
    ASSERT_NE(&*ctx1->chunk(), &*ctx3->chunk());
    ASSERT_NE(&*ctx2->chunk(), &*ctx3->chunk());

}

TYPED_TEST(ContextBlockManagerTypedTest, realign_chunk)
{
    // Use Case: we cannot expect our stream to start with a count of 0
    //           but it could be anywhere in the stream. Check that the first packet received
    //           aligns with the beginning of the context
    typedef TypeParam TestDataTraits;
    typedef typename TestDataTraits::LimitType LimitType;
    typedef typename TestDataTraits::PacketSizeType PacketSizeType;
    double data_size = TestDataTraits::packet_size() * 3;

    double packets_per_chunk=data_size/TestDataTraits::packet_size();

    ChunkFactory factory(data_size);
    typedef TestContextBlockManager<ChunkFactory, TestDataTraits> ContextMgrType;
    ContextMgrType context_mgr(factory, 100, 10, packets_per_chunk);

    auto ctx1=context_mgr.get_context(1);
    ASSERT_NE(ctx1, nullptr);
    ASSERT_EQ(ctx1->packet_number(), LimitType(1));
    ASSERT_EQ(ctx1->offset(), PacketSizeType(0));
    ASSERT_EQ(ctx1->packet_offset(), PacketSizeType(0));
    auto const* chunk1=&*ctx1->chunk();
    auto const& data1 = ctx1->data();

    auto ctx99=context_mgr.get_context(99);
    ASSERT_NE(ctx99, nullptr);
    ASSERT_EQ(ctx99->packet_number(), LimitType(99));
    ASSERT_EQ(ctx99->offset(), PacketSizeType(0));
    ASSERT_EQ(ctx99->packet_offset(), PacketSizeType(0));
    ASSERT_NE(chunk1, &*ctx99->chunk());

    // verify we get resize callback with the correct values
    data1.wait_resize_called();
    ASSERT_EQ(data1.resize_params()[0].first, chunk1);
    ASSERT_EQ(data1.resize_params()[0].second, PacketSizeType(0)); // ctx1 will not be deserialised so should not be counted

    // should not go back after a realign
    auto ctx98=context_mgr.get_context(98);
    ASSERT_EQ(ctx98, nullptr);

    auto ctx100=context_mgr.get_context(100);
    ASSERT_NE(ctx100, nullptr);
    ASSERT_EQ(ctx100->packet_number(), LimitType(100));
    ASSERT_EQ(ctx100->offset(), TestDataTraits::packet_size());
    ASSERT_EQ(ctx100->packet_offset(), PacketSizeType(0));
    ASSERT_NE(chunk1, &*ctx100->chunk());
    auto const& data100 = ctx100->data();
    auto const* chunk100=&*ctx100->chunk();

    auto ctx101=context_mgr.get_context(101);
    ASSERT_NE(ctx101, nullptr);
    ASSERT_EQ(ctx101->packet_number(), LimitType(101));
    ASSERT_EQ(ctx101->offset(), 2 * TestDataTraits::packet_size());
    ASSERT_EQ(ctx101->packet_offset(), PacketSizeType(0));
    ASSERT_NE(chunk1, &*ctx101->chunk());
    ASSERT_EQ(&*ctx100->chunk(), &*ctx101->chunk());

    // force another realign, this time with an incomplete chunk with more than one packet in DataReady status
    auto ctx200=context_mgr.get_context(200);
    ASSERT_NE(ctx200, nullptr);
    ASSERT_EQ(ctx200->packet_number(), LimitType(200));
    ASSERT_EQ(ctx200->offset(), PacketSizeType(0));
    ASSERT_EQ(ctx200->packet_offset(), PacketSizeType(0));
    ASSERT_NE(&*ctx100->chunk(), &*ctx200->chunk());

    PANDA_LOG_DEBUG << "waiting for resize to be called";
    data100.wait_resize_called();
    PANDA_LOG_DEBUG << " done waiting for resize to be called";
    ASSERT_EQ(data100.resize_params()[0].first, chunk100);
    ASSERT_EQ(data100.resize_params()[0].second, TestDataTraits::packet_size() * 2); // ctx1 will not be deserialised so should not be counted
}

TYPED_TEST(ContextBlockManagerTypedTest, resize_callback_deserialised_chunks)
{
    // Use Case: stream is realigned in the middle of filling a chunk where deserialise_packet has completed
    typedef TypeParam TestDataTraits;
    typedef typename TestDataTraits::LimitType LimitType;
    typedef typename TestDataTraits::PacketSizeType PacketSizeType;
    double data_size = TestDataTraits::packet_size() * 3;

    double packets_per_chunk=data_size/TestDataTraits::packet_size();

    ChunkFactory factory(data_size);
    typedef TestContextBlockManager<ChunkFactory, TestDataTraits> ContextMgrType;
    ContextMgrType context_mgr(factory, 100, 10, packets_per_chunk);

    auto ctx300=context_mgr.get_context(300);
    ASSERT_NE(ctx300, nullptr);
    ASSERT_EQ(ctx300->packet_number(), LimitType(300));
    ASSERT_EQ(ctx300->offset(), PacketSizeType(0));
    ASSERT_EQ(ctx300->packet_offset(), PacketSizeType(0));
    TestPacket<2> packet(300);
    ctx300->deserialise_packet(packet);

    auto ctx301=context_mgr.get_context(301);
    ASSERT_NE(ctx301, nullptr);
    ASSERT_EQ(ctx301->packet_number(), LimitType(301));
    ASSERT_EQ(ctx301->offset(), TestDataTraits::packet_size());
    ASSERT_EQ(ctx301->packet_offset(), PacketSizeType(0));
    TestPacket<2> packet301(301);
    ctx301->deserialise_packet(packet301);
    auto const& data301 = ctx301->data();
    auto const* chunk301=&*ctx301->chunk();

    auto ctx400=context_mgr.get_context(400);
    ASSERT_NE(ctx400, nullptr);
    ASSERT_EQ(ctx400->packet_number(), LimitType(400));
    ASSERT_EQ(ctx400->offset(), PacketSizeType(0));
    ASSERT_EQ(ctx400->packet_offset(), PacketSizeType(0));
    ASSERT_NE(&*ctx301->chunk(), &*ctx400->chunk());

    data301.wait_resize_called();
    ASSERT_EQ(data301.resize_params()[0].first, chunk301);
    ASSERT_EQ(data301.resize_params()[0].second, TestDataTraits::packet_size() * 2);

}

TYPED_TEST(ContextBlockManagerTypedTest, resize_callback_last_chunk_dataready)
{
    // Use Case: stream is realigned in the middle of filling a chunk where deserialise_packet has completed
    typedef TypeParam TestDataTraits;
    typedef typename TestDataTraits::LimitType LimitType;
    typedef typename TestDataTraits::PacketSizeType PacketSizeType;
    double data_size = TestDataTraits::packet_size() * 3;

    double packets_per_chunk=data_size/TestDataTraits::packet_size();

    ChunkFactory factory(data_size);
    typedef TestContextBlockManager<ChunkFactory, TestDataTraits> ContextMgrType;
    ContextMgrType context_mgr(factory, 100, 10, packets_per_chunk);

    auto ctx300=context_mgr.get_context(300);
    ASSERT_NE(ctx300, nullptr);
    ASSERT_EQ(ctx300->packet_number(), LimitType(300));
    ASSERT_EQ(ctx300->offset(), PacketSizeType(0));
    ASSERT_EQ(ctx300->packet_offset(), PacketSizeType(0));
    TestPacket<2> packet(300);
    ctx300->deserialise_packet(packet);

    auto ctx301=context_mgr.get_context(301);
    ASSERT_NE(ctx301, nullptr);
    ASSERT_EQ(ctx301->packet_number(), LimitType(301));
    ASSERT_EQ(ctx301->offset(), TestDataTraits::packet_size());
    ASSERT_EQ(ctx301->packet_offset(), PacketSizeType(0));
    auto const& data301 = ctx301->data();
    auto const* chunk301=&*ctx301->chunk();

    auto ctx400=context_mgr.get_context(400);
    ASSERT_NE(ctx400, nullptr);
    ASSERT_EQ(ctx400->packet_number(), LimitType(400));
    ASSERT_EQ(ctx400->offset(), PacketSizeType(0));
    ASSERT_EQ(ctx400->packet_offset(), PacketSizeType(0));
    ASSERT_NE(&*ctx301->chunk(), &*ctx400->chunk());

    data301.wait_resize_called();
    ASSERT_EQ(data301.resize_params()[0].first, chunk301);
    ASSERT_EQ(data301.resize_params()[0].second, TestDataTraits::packet_size());

}

TYPED_TEST(ContextBlockManagerTypedTest, counter_wraparound)
{
    typedef TypeParam TestDataTraits;
    typedef typename TestDataTraits::LimitType LimitType;
    double data_size = TestDataTraits::packet_size() * 3;

    double packets_per_chunk=data_size/TestDataTraits::packet_size();

    ChunkFactory factory(data_size);
    typedef TestContextBlockManager<ChunkFactory, TestDataTraits> ContextMgrType;
    ContextMgrType context_mgr(factory, 100, 10, packets_per_chunk);

    auto ctx_max=context_mgr.get_context(TestDataTraits::max_sequence_number());
    ASSERT_EQ(ctx_max->packet_number(), TestDataTraits::max_sequence_number());

    // should wrap around when we call zero
    auto ctx_zero=context_mgr.get_context(LimitType(0));
    ASSERT_NE(nullptr, ctx_zero);
    ASSERT_EQ(ctx_max, ctx_zero->prev());
}

} // namespace test
} // namespace panda
} // namespace ska
