/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "CopyTest.h"
#include "panda/Copy.h"
#include "panda/Cpu.h"
#include "panda/test/CopyTester.h"
#include <array>
#include <vector>

namespace ska {
namespace panda {
namespace test {


CopyTest::CopyTest()
    : ::testing::Test()
{
}

CopyTest::~CopyTest()
{
}

void CopyTest::SetUp()
{
}

void CopyTest::TearDown()
{
}

TEST_F(CopyTest, test_untagged_host_to_host)
{
    std::array<int,5> input = {{1, 2, 3, 4, 5}};
    std::array<int,5> out = {{0, 0, 0, 0 , 0}};
    panda::copy(input.begin(), input.end(), out.begin());
    for(unsigned i=0; i < input.size(); ++i) {
        ASSERT_EQ(input[i], out[i]);
    }
}

// use the CopyTester to test the Cpu type
struct CpuCopyTraits {
    typedef panda::Cpu Arch;

    template<typename T>
    static std::vector<T> contiguous_device_memory(std::size_t size) {
        return std::vector<T>(size);
    }
};

INSTANTIATE_TYPED_TEST_SUITE_P(Cpu, CopyTester, CpuCopyTraits);

} // namespace test
} // namespace panda
} // namespace ska
