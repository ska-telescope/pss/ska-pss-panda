/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "DataChannelTest.h"
#include "panda/DataChannel.h"
#include "panda/test/TestChunk.h"
#include "panda/test/TestHandler.h"
#include "panda/test/TestChunkHandler.h"

namespace ska {
namespace panda {
namespace test {

DataChannelTest::DataChannelTest()
    : ::testing::Test()
    , _channel_a("a")
{
}

DataChannelTest::~DataChannelTest()
{
}

void DataChannelTest::SetUp()
{
}

void DataChannelTest::TearDown()
{
}

TEST_F(DataChannelTest, test_add)
{
    panda::DataSwitchConfig config;
    panda::DataChannel<TestChunk<char>> data_switch(config);

    data_switch.add(_channel_a, [](TestChunk<char> const&){});

}

TEST_F(DataChannelTest, test_send)
{
    panda::DataSwitchConfig config;
    panda::DataChannel<TestChunk<char>> data_switch(config);

    std::string payload("A");
    TestChunkHandler<TestChunk<char>> handler_a(true);
    data_switch.add(_channel_a, handler_a);
    data_switch.activate(_channel_a);

    /**
     * @test ensure we can send data as many times as we want sequentially
     */
    std::shared_ptr<TestChunk<char>> chunk = std::make_shared<TestChunk<char>>(payload);
    data_switch.send(_channel_a, chunk);
    handler_a.wait();
    ASSERT_EQ(handler_a.chunk().data(), payload); // verify we were passed the correct data
    handler_a.reset();

    // should be no problems sending the same chunk again
    data_switch.send(_channel_a, chunk);
    handler_a.wait();
    ASSERT_EQ(handler_a.chunk().data(), payload); // verify we were passed the correct data

}

TEST_F(DataChannelTest, test_send_lifetime)
{
    panda::DataSwitchConfig config;
    panda::DataChannel<TestChunk<char>> data_switch(config);

    std::string payload("A");
    TestChunkHandler<TestChunk<char>> handler_a(true);
    data_switch.add(_channel_a, handler_a);
    data_switch.activate(_channel_a);

    {
        /**
         * @test ensure when we send data, the data is still available even when
         *       the data goes out of scope in any other context.
         */
        std::shared_ptr<TestChunk<char>> chunk = std::make_shared<TestChunk<char>>(payload);
        data_switch.send(_channel_a, chunk); // will block using the data as the caller goes out of scope
        chunk.reset(); // make sure local copy is gone before we release the block
    }
    handler_a.wait();
    ASSERT_EQ(handler_a.chunk().data(), payload); // verify we were passed the correct data


    handler_a.reset();
    {
        /**
         * @test ensure when can send data as a reference rather than a shared_ptr
         *       the data goes out of scope in any other context.
         */
        std::shared_ptr<TestChunk<char>> chunk = std::make_shared<TestChunk<char>>("B");
        data_switch.send(_channel_a, *chunk); // will block using the data as the caller goes out of scope
        chunk.reset(); // make sure local copy is gone before we release the block
    }
    handler_a.wait();
    ASSERT_EQ(handler_a.chunk().data(), "B"); // verify we were passed the correct data
}

TEST_F(DataChannelTest, test_send_no_shared_ptr)
{
    panda::DataSwitchConfig config;
    panda::DataChannel<TestChunk<char>> data_switch(config);

    std::string payload("A");
    TestChunkHandler<TestChunk<char>> handler_a(true);
    data_switch.add(_channel_a, handler_a);
    data_switch.activate(_channel_a);

    /**
     * @test we don't want to allow potentially unsafe data getting through (at this stage)
     */
    TestChunk<char> chunk(payload);
    ASSERT_THROW(data_switch.send(_channel_a, chunk), std::bad_weak_ptr);
}

TEST_F(DataChannelTest, test_subscribe_multi_channel_handler)
{
    /**
     * @test call add with a multiple handlers to the same stream
     *       expect handler to be added to the list fo handlers that are called
     */
    std::string payload("A");
    std::string channel("a");

    panda::DataSwitchConfig config;
    panda::ProcessingEngineConfig engine_config;
    engine_config.number_of_threads(0); // sync mode
    config.set_engine_config(engine_config);

    panda::DataChannel<TestChunk<char>> data_switch(config);

    TestChunkHandler<TestChunk<char>> handler_a(false);
    TestChunkHandler<TestChunk<char>> handler_b(false);
    ASSERT_FALSE(handler_a.executed());
    ASSERT_FALSE(handler_b.executed());

    data_switch.add(_channel_a, handler_a);
    data_switch.add(_channel_a, handler_b);
    data_switch.activate(_channel_a);

    std::shared_ptr<TestChunk<char>> chunk = std::make_shared<TestChunk<char>>(payload);
    data_switch.send(_channel_a, *chunk);

    ASSERT_TRUE(handler_a.executed());
    ASSERT_TRUE(handler_b.executed());
}

TEST_F(DataChannelTest, test_deactivate_channel)
{
    /**
     * @test deactivate an entire channel
     *       no handlers on that channel should be called
     */
    panda::ChannelId channel_b("b");
    panda::DataSwitchConfig config;
    panda::ProcessingEngineConfig engine_config;
    engine_config.number_of_threads(0); // sync mode
    config.set_engine_config(engine_config);

    panda::DataChannel<TestChunk<char>> data_switch(config);

    TestChunkHandler<TestChunk<char>> handler_a(false);
    TestChunkHandler<TestChunk<char>> handler_b(false);

    std::string payload("A");

    data_switch.add(_channel_a, handler_a);
    data_switch.add(channel_b, handler_b);

    data_switch.deactivate(_channel_a);
    data_switch.activate(channel_b);
    ASSERT_FALSE(data_switch.is_active(_channel_a));
    ASSERT_TRUE(data_switch.is_active(channel_b));

    std::shared_ptr<TestChunk<char>> chunk = std::make_shared<TestChunk<char>>(payload);
    data_switch.send(_channel_a, *chunk);
    data_switch.send(channel_b, *chunk);

    ASSERT_FALSE(handler_a.executed());
    ASSERT_TRUE(handler_b.executed());

    // reactivate the channel
    data_switch.activate(_channel_a);
    ASSERT_TRUE(data_switch.is_active(_channel_a));
    ASSERT_TRUE(data_switch.is_active(channel_b));

    data_switch.send(_channel_a, *chunk);

    ASSERT_TRUE(handler_a.executed());
}

TEST_F(DataChannelTest, test_engine_propagation)
{
    // ensure the engine specified by the config is used
    panda::DataSwitchConfig config;
    panda::ProcessingEngineConfig p_config;
    p_config.number_of_threads(0);
    config.set_engine_config(p_config);

    panda::DataChannel<TestChunk_A> data_switch(config);
    data_switch.add(_channel_a, [](TestChunk_A const&){} );
    ASSERT_EQ(&config.engine(_channel_a), &data_switch.engine(_channel_a));
}

} // namespace test
} // namespace panda
} // namespace ska
