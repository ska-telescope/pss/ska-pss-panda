/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "DataSwitchTest.h"
#include "panda/DataSwitch.h"
#include "panda/test/TestChunk.h"
#include "panda/test/TestHandler.h"
#include "panda/test/TestChunkHandler.h"

namespace ska {
namespace panda {
namespace test {

DataSwitchTest::DataSwitchTest()
    : ::testing::Test()
    , _chunk_a(std::make_shared<TestChunk_A>("A"))
    , _chunk_b(std::make_shared<TestChunk_B>())
{
}

DataSwitchTest::~DataSwitchTest()
{
}

void DataSwitchTest::SetUp()
{
}

void DataSwitchTest::TearDown()
{
}

TEST_F(DataSwitchTest, test_send_no_handlers_multi_channel)
{
    panda::DataSwitchConfig config;
    panda::DataSwitch<TestChunk_A, TestChunk_B> data_switch(config);

    std::string payload("A");
    ChannelId channel_name("channel_a");
    config.activate_channel(channel_name);

    // send as a shared_ptr
    data_switch.send(channel_name, _chunk_a);
    data_switch.send(channel_name, _chunk_b);

    // send as a ref
    data_switch.send(channel_name, *_chunk_a);
    data_switch.send(channel_name, *_chunk_b);

    // undefined channel_name should cause no errors
    data_switch.send(ChannelId("undefined_channel_name"), *_chunk_a);
}

TEST_F(DataSwitchTest, test_send_const_types)
{
    ChannelId channel_a("channel_a");
    panda::DataSwitchConfig config;
    config.activate_channel(channel_a);
    panda::DataSwitch<const TestChunk_A, const TestChunk_B> data_switch(config);
    TestChunkHandler<const TestChunk_A> handler_a(false);
    TestChunkHandler<const TestChunk_B> handler_b(false);
    data_switch.add<const TestChunk_A>(channel_a, handler_a);
    data_switch.add<TestChunk_B>(channel_a, handler_b); // without const qualifier

    std::string payload("A");
    ChannelId channel_name("channel_a");

    // send as a shared_ptr
    std::shared_ptr<const TestChunk_A> chunk_a = std::make_shared<TestChunk_A>();
    data_switch.send(channel_name, chunk_a);

    // send as a ref
    data_switch.send(channel_name, *chunk_a);

    // undefined channel_name should cause no errors
    data_switch.send(ChannelId("undefined_channel_name"), *chunk_a);
}

TEST_F(DataSwitchTest, test_send_with_handlers_multi_channel)
{
    TestChunkHandler<TestChunk_A> handler_a(true);
    TestChunkHandler<TestChunk_B> handler_b(true);
    ChannelId channel_a("channel_a");

    // add both handlers to channel_a
    panda::DataSwitchConfig config;
    panda::DataSwitch<TestChunk_A, TestChunk_B> data_switch(config);
    data_switch.add<TestChunk_A>(channel_a, handler_a);
    data_switch.add<TestChunk_B>(channel_a, handler_b);

    data_switch.send(channel_a, _chunk_a);
    data_switch.send(channel_a, _chunk_b);
    handler_a.wait();
    handler_b.wait();
    ASSERT_EQ(handler_a.chunk().data(), _chunk_a->data()); // verify we were passed the correct data
    ASSERT_EQ(handler_b.chunk().data(), _chunk_b->data()); // verify we were passed the correct data
}

TEST_F(DataSwitchTest, test_deactivate_channel)
{
    TestChunkHandler<TestChunk_A> handler_a(false);
    TestChunkHandler<TestChunk_B> handler_b(false);

    ChannelId channel_a("channel_a");

    // add both handlers to channel_a
    panda::DataSwitchConfig config;
    panda::ProcessingEngineConfig engine_config;
    engine_config.number_of_threads(0); // sync mode
    config.set_engine_config(engine_config);

    panda::DataSwitch<TestChunk_A, TestChunk_B> data_switch(config);
    data_switch.add<TestChunk_A>(channel_a, handler_a);
    data_switch.add<TestChunk_B>(channel_a, handler_b);

    // deactivate a single type
    data_switch.deactivate<TestChunk_A>(channel_a);
    data_switch.send(channel_a, _chunk_a);
    data_switch.send(channel_a, _chunk_b);
    ASSERT_FALSE(handler_a.executed());
    ASSERT_TRUE(handler_b.executed());

    handler_a.reset();
    handler_b.reset();

    // @test reactivate type
    data_switch.activate<TestChunk_A>(channel_a);
    data_switch.send(channel_a, _chunk_a);
    data_switch.send(channel_a, _chunk_b);
    ASSERT_TRUE(handler_a.executed());
    ASSERT_TRUE(handler_b.executed());

    handler_a.reset();
    handler_b.reset();

    // @test deactivate_all
    data_switch.deactivate_all(channel_a);
    data_switch.send(channel_a, _chunk_a);
    data_switch.send(channel_a, _chunk_b);
    ASSERT_FALSE(handler_a.executed());
    ASSERT_FALSE(handler_b.executed());

    handler_a.reset();
    handler_b.reset();

    // @test reactivate_all
    data_switch.activate_all(channel_a);
    data_switch.send(channel_a, _chunk_a);
    data_switch.send(channel_a, _chunk_b);
    ASSERT_TRUE(handler_a.executed());
    ASSERT_TRUE(handler_b.executed());
}

} // namespace test
} // namespace panda
} // namespace ska
