/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 The SKA organisation
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "DeviceLocalTest.h"
#include "panda/DeviceLocal.h"
#include <vector>

namespace ska {
namespace panda {
namespace test {


DeviceLocalTest::DeviceLocalTest()
    : ::testing::Test()
{
}

DeviceLocalTest::~DeviceLocalTest()
{
}

void DeviceLocalTest::SetUp()
{
}

void DeviceLocalTest::TearDown()
{
}

struct DeviceType {};

struct TestFactory {
    std::vector<int>* operator()(DeviceType const&) {
        return new std::vector<int>();
    }
};

TEST_F(DeviceLocalTest, test_object_generation)
{
    TestFactory factory;
    DeviceLocal<DeviceType, TestFactory&> device_local(factory);

    // run with the first device
    DeviceType device_1;
    std::vector<int>& object_1 = device_local(device_1);
    std::vector<int>& object_2 = device_local(device_1);
    ASSERT_EQ(&object_1, &object_2);

    // a new device
    DeviceType device_2;
    std::vector<int>& object_3 = device_local(device_2);
    ASSERT_NE(&object_1, &object_3);

    // ensure the device_1 object is still valid
    std::vector<int>& object_4 = device_local(device_1);
    ASSERT_EQ(&object_1, &object_4);
}

} // namespace test
} // namespace panda
} // namespace ska
