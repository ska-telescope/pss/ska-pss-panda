/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "DeviceMemoryTest.h"
#include "panda/DeviceMemory.h"
#include "panda/test/DeviceMemoryTester.h"


namespace ska {
namespace panda {
namespace test {


DeviceMemoryTest::DeviceMemoryTest()
    : BaseT()
{
}

DeviceMemoryTest::~DeviceMemoryTest()
{
}

void DeviceMemoryTest::SetUp()
{
}

void DeviceMemoryTest::TearDown()
{
}

TEST_F(DeviceMemoryTest, test_cpu)
{
    DeviceMemory<panda::Cpu, unsigned> memory(10);
    ASSERT_EQ(memory.size(), std::size_t(10));

    // test begin end oprators
    std::fill(memory.begin(), memory.end(), 100);
    for(auto const& val : memory) {
        ASSERT_EQ((unsigned)100, val);
    }

    // test const begin and const end operators
    DeviceMemory<panda::Cpu, unsigned> const& mem_ref = memory;
    for(auto const& val : mem_ref) {
        ASSERT_EQ((unsigned)100, val);
    }

    // test cbegin and cend
    auto it = mem_ref.cbegin();
    while(it != mem_ref.cend()) {
        ASSERT_EQ((unsigned)100, *it);
        ++it;
    }
}

TEST_F(DeviceMemoryTest, test_cpu_with_Device_constructor)
{
    Device<panda::Cpu> cpu_device(1);
    DeviceMemory<panda::Cpu, float> memory(10, cpu_device);
    ASSERT_EQ(memory.size(), std::size_t(10));
}

typedef ::testing::Types<test::DeviceMemoryTesterTraits<DeviceMemory<Cpu, float>>> DeviceMemoryTraitsTypes;
INSTANTIATE_TYPED_TEST_SUITE_P(BasicCpuDeviceMemory, DeviceMemoryTester, DeviceMemoryTraitsTypes);

} // namespace test
} // namespace panda
} // namespace ska
