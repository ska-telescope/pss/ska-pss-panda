/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "ErrorTest.h"
#include "panda/Error.h"


namespace ska {
namespace panda {
namespace test {


ErrorTest::ErrorTest()
    : ::testing::Test()
{
}

ErrorTest::~ErrorTest()
{
}

void ErrorTest::SetUp()
{
}

void ErrorTest::TearDown()
{
}

TEST_F(ErrorTest, test_bool_operator)
{
    Error e1;
    ASSERT_FALSE(e1);

    Error e2("ohoh");
    ASSERT_TRUE(e2);

    std::exception e;
    Error e3(e);
    ASSERT_TRUE(e3);

    Error e4(boost::system::error_code(boost::system::errc::broken_pipe, boost::system::system_category()));
    ASSERT_TRUE(e4);
}

TEST_F(ErrorTest, test_compare)
{
    {
        Error e1;
        Error e2;
        ASSERT_EQ(e1, e2);
    }
    {
        Error e1("a");
        Error e2("a");
        Error e3("b");
        ASSERT_EQ(e1, e2);
        ASSERT_NE(e1, e3);
    }
    {
        Error e1(boost::system::error_code(boost::system::errc::broken_pipe, boost::system::system_category()));
        Error e2(boost::system::error_code(boost::system::errc::broken_pipe, boost::system::system_category()));
        Error e3(boost::system::error_code(boost::system::errc::device_or_resource_busy, boost::system::system_category()));
        ASSERT_EQ(e1, e2);
        ASSERT_NE(e1, e3);
    }
}

TEST_F(ErrorTest, test_operator)
{
    Error e("A");
    e << 1;
    ASSERT_EQ(std::string("A1"), std::string(e.what()));
    e << "B";
    ASSERT_EQ(std::string("A1B"), std::string(e.what()));
}

TEST_F(ErrorTest, test_copy_msg)
{
    Error a("A");
    Error b(a);
    ASSERT_EQ(a, b);
    Error c;
    c = a; // assignment
    ASSERT_EQ(c, a);
}

TEST_F(ErrorTest, test_copy_error_code)
{
    Error a(boost::system::error_code(boost::system::errc::broken_pipe, boost::system::system_category()));
    Error b(a);
    ASSERT_EQ(a, b);
    Error c;
    c = a; // assignment
    ASSERT_EQ(c, a);
}

} // namespace test
} // namespace panda
} // namespace ska
