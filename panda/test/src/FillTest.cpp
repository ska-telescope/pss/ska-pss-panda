/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "FillTest.h"
#include "panda/test/TestArch.h"
#include "panda/Fill.h"
#include <vector>


namespace ska {
namespace panda {
namespace test {


FillTest::FillTest()
    : BaseT()
{
}

FillTest::~FillTest()
{
}

void FillTest::SetUp()
{
}

void FillTest::TearDown()
{
}

TEST_F(FillTest, test_cpu)
{
    std::vector<unsigned> vec(10, 0);
    panda::fill(vec.begin(), vec.end(), 22);
    ASSERT_EQ(vec.size(), 10);
    for(float val : vec ) {
        ASSERT_EQ(val, 22);
    }
}

TEST_F(FillTest, test_default_impl)
{
    // setup an initialised vector on the device
    std::vector<unsigned> vec(10, 0);
    Device<TestArch> device(1);
    DeviceMemory<TestArch, int> dev_vec(10, device);
    panda::copy(vec.begin(), vec.end(), dev_vec.begin());

    // perform the fill
    // We do not fill to the end so we can test for overwrites
    panda::fill(dev_vec.begin(), dev_vec.end() -1, 22);

    // return the result to host so we can verify them
    panda::copy(dev_vec.begin(), dev_vec.end(), vec.begin());

    for(unsigned i=0; i<vec.size()-1; ++i)
    {
        ASSERT_EQ(vec[i], 22);
    }

    // ensure no overflow has occured
    ASSERT_EQ(vec.back(), 0);
}

} // namespace test
} // namespace panda
} // namespace ska
