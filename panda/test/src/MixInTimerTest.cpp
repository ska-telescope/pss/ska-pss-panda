/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "MixInTimerTest.h"
#include "panda/MixInTimer.h"


namespace ska {
namespace panda {
namespace test {


MixInTimerTest::MixInTimerTest()
    : ::testing::Test()
{
}

MixInTimerTest::~MixInTimerTest()
{
}

void MixInTimerTest::SetUp()
{
}

void MixInTimerTest::TearDown()
{
}

struct TestTypeVoid {
    void operator()() { has_run = true;};

    bool has_run = false;
};

struct TestTypeInt {
    public:
        TestTypeInt(int val=101) { _val=val; };
        int operator()() { return _val; }

    private:
        int _val;
};

template<typename VarType>
struct TestTypeVoidArgs {
    void operator()(VarType const& val) {
        has_run = true;
        passed = val;
    };
    virtual ~TestTypeVoidArgs() {}

    VarType passed = VarType();
    bool has_run = false;
};


template<typename VarType>
struct TestTypeIntArgs {
    VarType operator()(VarType const& val) {
        has_run = true;
        passed = val;
        return passed;
    };
    virtual ~TestTypeIntArgs() {}

    VarType passed = VarType();
    bool has_run = false;
};

template<typename VarType>
struct TestTypeVirtualVoidArgs {
    virtual void operator()(VarType const& val) {
        has_run = true;
        passed = val;
    };
    virtual ~TestTypeVirtualVoidArgs() {}

    VarType passed = VarType();
    bool has_run = false;
};

TEST_F(MixInTimerTest, test_mixin_default_void_zero_arg)
{
    panda::MixInTimer<TestTypeVoid> timed;
    timed();
    ASSERT_EQ(true, timed.has_run);
}

TEST_F(MixInTimerTest, test_mixin_default_int_zero_arg)
{
    panda::MixInTimer<TestTypeInt> timed;
    int val = timed();
    ASSERT_EQ(101, val);
}

TEST_F(MixInTimerTest, test_mixin_default_int_zero_arg_constructor_takes_args)
{
    panda::MixInTimer<TestTypeInt> timed(99);
    int val = timed();
    ASSERT_EQ(99, val);
}

TEST_F(MixInTimerTest, test_mixin_default_void_one_arg)
{
    panda::MixInTimer<TestTypeVoidArgs<int>> timed;
    timed(100);
    ASSERT_EQ(true, timed.has_run);
    ASSERT_EQ(100, timed.passed);
}

TEST_F(MixInTimerTest, test_mixin_default_virtual_void_one_arg)
{
    // test the base class' virtual get overriden
    panda::MixInTimer<TestTypeVirtualVoidArgs<int>> timed;
    timed(100);
    ASSERT_EQ(true, timed.has_run);
    ASSERT_EQ(100, timed.passed);

    // test the base class' virtual get overriden when stroed as the base class type
    {
        std::unique_ptr<TestTypeVirtualVoidArgs<int>> timed(new panda::MixInTimer<TestTypeVirtualVoidArgs<int>>());
        (*timed)(100);
        ASSERT_EQ(true, timed->has_run);
        ASSERT_EQ(100, timed->passed);
    }
}

TEST_F(MixInTimerTest, test_mixin_default_int_one_arg)
{
    panda::MixInTimer<TestTypeIntArgs<int>> timed;
    int val = timed(109);
    ASSERT_EQ(true, timed.has_run);
    ASSERT_EQ(109, timed.passed);
    ASSERT_EQ(109, val);
}


} // namespace test
} // namespace panda
} // namespace ska
