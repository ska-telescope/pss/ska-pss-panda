/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 The SKA organisation
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "OutputFileStreamerTest.h"
#include "panda/OutputFileStreamer.h"
#include "panda/test/TestDir.h"

namespace ska {
namespace panda {
namespace test {


OutputFileStreamerTest::OutputFileStreamerTest()
    : ::testing::Test()
{
}

OutputFileStreamerTest::~OutputFileStreamerTest()
{
}

void OutputFileStreamerTest::SetUp()
{
}

void OutputFileStreamerTest::TearDown()
{
}

namespace {
    struct TestFormattingTraits {
        public:
            TestFormattingTraits() 
                : _headers_called(false)
                , _footers_called(false)
                , _incompatible(false)
                , _write_called(0U)
            {
            }

            bool consistent_with_existing_file(int) {
                return !_incompatible;
            }

            void headers(std::ostream&, int) {
                _headers_called = true;
            }

            void footers(std::ostream&) {
                _footers_called = true;
                _headers_called = false;
            }

            bool headers_called() const
            {
                return _headers_called;
            }

            bool footers_called() const
            {
                return _footers_called;
            }

            void set_incompatible_next_call()
            {
                _incompatible = true;
            }

            void write(std::ostream&, int) {
                ++_write_called;
            }

            unsigned write_called() const {
                return _write_called;
            }

        private:
            bool _headers_called;
            bool _footers_called;
            bool _incompatible;
            unsigned _write_called;
    };

    template<typename DataType>
    class TestOutputFileStreamer : public OutputFileStreamer<DataType, TestFormattingTraits>
    {
            typedef OutputFileStreamer<DataType, TestFormattingTraits> BaseT;

        public:
            TestOutputFileStreamer(boost::filesystem::path const& dir)
                : BaseT(dir) 
            {
                this->extension(".testfile");
            }

            // expose the traits object
            TestFormattingTraits& traits() { return this->_format_traits; }

        private:
    };

} // namepsapce

TEST_F(OutputFileStreamerTest, traits_callbacks)
{
    TestDir dir;
    dir.create();
    {
        int data=1;
        TestOutputFileStreamer<int> streamer(dir.path());
        auto& traits = streamer.traits();
        streamer << data;
        ASSERT_TRUE(traits.headers_called());
        ASSERT_EQ(1U, traits.write_called());
        ASSERT_FALSE(traits.footers_called());
        streamer << data;
        ASSERT_FALSE(traits.footers_called());
        ASSERT_EQ(2U, traits.write_called());
        traits.set_incompatible_next_call();
        streamer << data;
        ASSERT_TRUE(traits.footers_called());
        ASSERT_TRUE(traits.headers_called());
        ASSERT_EQ(3U, traits.write_called());
    }

    // -- ensure files have been created
    sync(); // ensure the OS actually writes to disc
    auto it = boost::filesystem::directory_iterator(dir.path());
    ASSERT_EQ(2U, std::distance(it, boost::filesystem::directory_iterator()));
}

} // namespace test
} // namespace panda
} // namespace ska
