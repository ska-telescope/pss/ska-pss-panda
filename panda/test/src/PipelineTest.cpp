/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "PipelineTest.h"
#include "panda/Pipeline.h"
#include "panda/test/TestProducer.h"
#include "panda/test/TestChunk.h"

#include <functional>

namespace ska {
namespace panda {
namespace test {


PipelineTest::PipelineTest()
    : ::testing::Test()
{
}

PipelineTest::~PipelineTest()
{
}

void PipelineTest::SetUp()
{
}

void PipelineTest::TearDown()
{
}

TEST_F(PipelineTest, test_run)
{
    typedef panda::test::TestProducer<TestChunk<char>> Producer;
    Producer producer;
    TestChunk<char> test_chunk("test_chunk");

    producer << test_chunk;

    bool has_run = false;;
    Pipeline<Producer> p(producer,
                             [&](TestChunk<char>& chunk)
                             {
                                p.stop();
                                has_run = true;
                                ASSERT_EQ(test_chunk, chunk);
                             }
                            );
    ASSERT_EQ(0, p.exec());
    ASSERT_TRUE(has_run);
}

TEST_F(PipelineTest, test_multi_types)
{
    typedef panda::test::TestProducer<TestChunk<char>, TestChunk_A> Producer;
    Producer producer;
    TestChunk<char> test_chunk("test_chunk");
    TestChunk_A test_chunk_a("test_chunk_a");

    producer << test_chunk << test_chunk_a;

    bool has_run = false;
    Pipeline<Producer> p(producer,
                             [&](TestChunk<char> const& chunk, TestChunk_A const& chunk_a)
                             {
                                p.stop();
                                has_run = true;
                                ASSERT_EQ(test_chunk, chunk);
                                ASSERT_EQ(test_chunk_a, chunk_a);
                             }
                            );
    ASSERT_EQ(0, p.exec());
    ASSERT_TRUE(has_run);
}

TEST_F(PipelineTest, test_multi_types_with_service_data)
{
    typedef panda::test::TestProducer<panda::ServiceData<TestChunk<char>>, TestChunk_A> Producer;
    Producer producer;
    TestChunk<char> test_chunk("test_chunk");
    TestChunk_A test_chunk_a("test_chunk_a");
    producer << test_chunk << test_chunk_a;
    bool has_run = false;
    Pipeline<Producer> p(producer,
                             [&](TestChunk<char> const& chunk, TestChunk_A const& chunk_a)
                             {
                                p.stop();
                                has_run = true;
                                ASSERT_EQ(test_chunk, chunk);
                                ASSERT_EQ(test_chunk_a, chunk_a);
                             }
                            );
    ASSERT_EQ(0, p.exec());
    ASSERT_TRUE(has_run);
}

TEST_F(PipelineTest, test_abort)
{
    typedef panda::test::TestProducer<TestChunk<char>> Producer;
    Producer producer;
    TestChunk<char> test_chunk("test_chunk");

    producer << test_chunk;
    producer.set_process_return(1); // abort signal

    bool has_run = false;;
    Pipeline<Producer> p(producer,
                             [&](TestChunk<char>& chunk)
                             {
                                has_run = true;
                                ASSERT_EQ(test_chunk, chunk);
                             }
                            );
    ASSERT_EQ(0, p.exec());
    ASSERT_TRUE(has_run);
}

TEST_F(PipelineTest, test_throw)
{
    typedef panda::test::TestProducer<TestChunk<char>> Producer;
    Producer producer;
    TestChunk<char> test_chunk("test_chunk");

    producer << test_chunk;
    panda::Error e("test exception");
    producer.set_throw(e); // abort signal

    bool has_run = false;;
    Pipeline<Producer> p(producer,
                             [&](TestChunk<char>&)
                             {
                                has_run = true;
                             }
                            );
    ASSERT_EQ(1, p.exec());
    ASSERT_FALSE(has_run);
}

} // namespace test
} // namespace panda
} // namespace ska
