/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "PoolConfigTest.h"
#include "panda/PoolConfig.h"
#include "panda/test/TestSystem.h"


namespace ska {
namespace panda {
namespace test {


PoolConfigTest::PoolConfigTest()
    : ::testing::Test()
{
}

PoolConfigTest::~PoolConfigTest()
{
}

void PoolConfigTest::SetUp()
{
}

void PoolConfigTest::TearDown()
{
}

TEST_F(PoolConfigTest, test_count_single_device_in_tree)
{
    boost::property_tree::ptree pt_root;
    boost::property_tree::ptree pt_a;
    boost::property_tree::ptree pt_b;

    pt_root.put_child("ArchA", pt_a);
    pt_root.add_child("ArchB", pt_b);

    PoolConfig<ArchA> pc;
    ASSERT_EQ(0U, pc.device_count<ArchA>());

    boost::program_options::variables_map vm;
    pc.parse_property_tree(pt_root, vm);
    boost::program_options::notify(vm);
    ASSERT_EQ(1U, pc.device_count<ArchA>());
}

TEST_F(PoolConfigTest, test_count_single_multi_device_in_tree)
{
    boost::property_tree::ptree pt_root;
    boost::property_tree::ptree pt_a;
    boost::property_tree::ptree pt_a_2;
    boost::property_tree::ptree pt_b;

    pt_root.put_child("ArchA", pt_a);
    pt_root.add_child("ArchA", pt_a_2);
    pt_root.add_child("ArchB", pt_b);

    PoolConfig<ArchA> pc;
    ASSERT_EQ(0U, pc.device_count<ArchA>());

    boost::program_options::variables_map vm;
    pc.parse_property_tree(pt_root, vm);
    boost::program_options::notify(vm);
    ASSERT_EQ(2U, pc.device_count<ArchA>());
}

TEST_F(PoolConfigTest, test_total_count)
{
    boost::property_tree::ptree pt_root;
    boost::property_tree::ptree pt_a;
    boost::property_tree::ptree pt_b;

    pt_root.put_child("ArchA", pt_a);
    pt_root.add_child("ArchB", pt_b);

    PoolConfig<ArchA, ArchB, ArchC> pc;
    ASSERT_EQ(0U, pc.total_device_count());

    boost::program_options::variables_map vm;
    pc.parse_property_tree(pt_root, vm);
    boost::program_options::notify(vm);
    ASSERT_EQ(2U, pc.total_device_count());
}

TEST_F(PoolConfigTest, test_specific_concurrent_jobs)
{
    for(unsigned concurrency=1; concurrency<5; ++concurrency)
    {
        boost::property_tree::ptree pt_root;
        pt_root.put<unsigned>("concurrent_jobs", concurrency);

        PoolConfig<ArchA, ArchB, ArchC> pc;
        boost::program_options::variables_map vm;
        pc.parse_property_tree(pt_root, vm);
        boost::program_options::notify(vm);
        ASSERT_EQ(concurrency, pc.engine_config().number_of_threads());
    }
}

TEST_F(PoolConfigTest, test_unlimited_concurrent_jobs)
{
    for(unsigned number_of_A =0; number_of_A < 5; ++number_of_A)
    {
        boost::property_tree::ptree pt_root;
        pt_root.put<unsigned>("concurrent_jobs", 0); // 0 = adapt to number of devices
        boost::property_tree::ptree pt_a;
        pt_a.put<unsigned>("devices", number_of_A);
        boost::property_tree::ptree pt_b;
        pt_root.put_child("ArchA", pt_a);
        pt_root.add_child("ArchB", pt_b);

        PoolConfig<ArchA, ArchB, ArchC> pc;
        boost::program_options::variables_map vm;
        pc.parse_property_tree(pt_root, vm);
        boost::program_options::notify(vm);
        ASSERT_EQ(pc.total_device_count(), pc.engine_config().number_of_threads()) << " device of type A=" << number_of_A;
    }
}
} // namespace test
} // namespace panda
} // namespace ska
