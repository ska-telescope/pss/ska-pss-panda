/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "PoolManagerTest.h"
#include "panda/PoolManager.h"
#include "panda/Cpu.h"
#include "panda/test/TestSystem.h"

namespace ska {
namespace panda {

namespace test {


PoolManagerTest::PoolManagerTest()
    : ::testing::Test()
{
}

PoolManagerTest::~PoolManagerTest()
{
}

void PoolManagerTest::SetUp()
{
}

void PoolManagerTest::TearDown()
{
}

TEST_F(PoolManagerTest, test_default_pool_no_reservations)
{
    TestSystem system;
    panda::PoolManagerConfig<TestSystem> config;
    panda::PoolManager<TestSystem> pm(system, config);

    auto pool = pm.default_pool();
    ASSERT_EQ(system.system_count<ArchA>(), pool.free_resources_count<ArchA>());
    ASSERT_EQ(system.system_count<ArchB>(), pool.free_resources_count<ArchB>());
}

TEST_F(PoolManagerTest, test_default_pool_with_reservation)
{
    TestSystem system;

    std::string pool_id("pool_id");
    boost::property_tree::ptree pool_manager_config_tree;
    boost::property_tree::ptree pool_config_a;
    boost::property_tree::ptree device_config_a;
    boost::property_tree::ptree device_config_b;
    boost::property_tree::ptree device_config_b_2;
    pool_config_a.put_child("ArchA", device_config_a);
    pool_config_a.put_child("ArchB", device_config_b);
    pool_config_a.add_child("ArchB", device_config_b_2);
    pool_config_a.put<std::string>("id", pool_id);
    pool_manager_config_tree.put_child("pool", pool_config_a);

    panda::PoolManagerConfig<TestSystem> config;
    boost::program_options::variables_map vm;
    config.parse_property_tree(pool_manager_config_tree, vm);

    // add a single pool to the configration
    PoolManager<TestSystem> pm(system, config);
    boost::program_options::notify(vm);
    config.reserve(pool_id);

    auto pool = pm.default_pool();
    ASSERT_EQ(system.system_count<ArchA>() - 1, pool.free_resources_count<ArchA>());
    ASSERT_EQ(system.system_count<ArchB>() - 2, pool.free_resources_count<ArchB>());
}

TEST_F(PoolManagerTest, test_get_pool)
{

    std::string pool_id("pool_id");
    boost::property_tree::ptree pool_manager_config_tree;
    boost::property_tree::ptree pool_config_a;
    boost::property_tree::ptree device_config_a;
    boost::property_tree::ptree device_config_b;
    boost::property_tree::ptree device_config_b_2;
    pool_config_a.put_child("ArchA", device_config_a);
    pool_config_a.put_child("ArchB", device_config_b);
    pool_config_a.add_child("ArchB", device_config_b_2);
    pool_config_a.put<std::string>("id", pool_id);
    pool_manager_config_tree.put_child("pool", pool_config_a);

    panda::PoolManagerConfig<TestSystem> config;
    boost::program_options::variables_map vm;
    config.parse_property_tree(pool_manager_config_tree, vm);
    boost::program_options::notify(vm);

    // add a single pool to the configration
    {   // without reservation
        TestSystem system;
        PoolManager<TestSystem> pm(system, config);

        auto& pool = pm.pool(pool_id);
        auto& pool2 = pm.pool(pool_id);
        ASSERT_EQ(&pool, &pool2) << "expecting the same object";
        ASSERT_EQ(1U, pool.free_resources_count<ArchA>());
        ASSERT_EQ(2U, pool.free_resources_count<ArchB>());
        pm.wait(); // nobody is using the pool so should return
    }
    {   // with reservation
        TestSystem system;
        PoolManager<TestSystem> pm(system, config);
        boost::program_options::notify(vm);

        config.reserve(pool_id);

        auto& pool = pm.pool(pool_id);
        ASSERT_EQ(1U, pool.free_resources_count<ArchA>());
        ASSERT_EQ(2U, pool.free_resources_count<ArchB>());
        pm.wait(); // nobody is using the pools so should return
    }
}

TEST_F(PoolManagerTest, test_get_empty_pool)
{
    std::string pool_id("pool_id");
    boost::property_tree::ptree pool_manager_config_tree;
    boost::property_tree::ptree pool_config_a;
    pool_config_a.put<std::string>("id", pool_id);
    pool_manager_config_tree.put_child("pool", pool_config_a);

    panda::PoolManagerConfig<TestSystem> config;
    boost::program_options::variables_map vm;
    config.parse_property_tree(pool_manager_config_tree, vm);
    boost::program_options::notify(vm);

    TestSystem system;
    PoolManager<TestSystem> pm(system, config);

    ASSERT_THROW( pm.pool(pool_id), panda::Error);
}

TEST_F(PoolManagerTest, test_get_unspecified_pool)
{
    std::string pool_id("pool_id");
    boost::property_tree::ptree pool_manager_config_tree;

    panda::PoolManagerConfig<TestSystem> config;
    boost::program_options::variables_map vm;
    config.parse_property_tree(pool_manager_config_tree, vm);
    boost::program_options::notify(vm);

    TestSystem system;
    PoolManager<TestSystem> pm(system, config);

    ASSERT_THROW( pm.pool(pool_id), panda::Error);
}

TEST_F(PoolManagerTest, test_cpu_provisioning)
{
    std::string pool_id("pool_id");
    boost::property_tree::ptree pool_manager_config_tree;
    boost::property_tree::ptree pool_config_tree;
    boost::property_tree::ptree device_config_cpu;
    boost::property_tree::ptree device_config_cpu_2;
    device_config_cpu_2.put<std::string>("affinity", "2");
    device_config_cpu_2.add<std::string>("affinity", "5");
    pool_config_tree.put_child(DeviceConfig<Cpu>::tag_name(), device_config_cpu);
    pool_config_tree.add_child(DeviceConfig<Cpu>::tag_name(), device_config_cpu_2);
    pool_config_tree.put<std::string>("id", pool_id);
    pool_manager_config_tree.put_child("pool",pool_config_tree);

    panda::PoolManagerConfig<System<Cpu>> config;
    boost::program_options::variables_map vm;
    config.parse_property_tree(pool_manager_config_tree, vm);
    boost::program_options::notify(vm);

    System<Cpu> system;
    PoolManager<System<Cpu>> pm(system, config);

    // Initialize the DeviceConfig

    // Get the PoolConfig by pool id
    PoolConfig<Cpu> const& pc = config.pool_config(pool_id);
    ASSERT_EQ(2U, pc.device_count<Cpu>());

    std::vector<unsigned> cpu0_affinities; // no affinities set
    std::vector<unsigned> cpu1_affinities = {2,5};
    PoolManager<System<Cpu>>::PoolType& pool = pm.pool(pool_id);

    ASSERT_EQ(2U, pool.free_resources<Cpu>().size());
    PoolResource<Cpu> const& pr_0 = *(pool.free_resources<Cpu>()[0]);
    PoolResource<Cpu> const& pr_1 = *(pool.free_resources<Cpu>()[1]);

    ASSERT_EQ(cpu0_affinities, pr_0.affinities());
    ASSERT_EQ(cpu1_affinities, pr_1.affinities());
}

} // namespace test
} // namespace panda
} // namespace ska
