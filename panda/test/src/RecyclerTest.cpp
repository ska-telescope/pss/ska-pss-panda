/*
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 The SKA organisation
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "RecyclerTest.h"
#include "panda/detail/Recycler.h"
#include <vector>
#include <memory>


namespace ska {
namespace panda {
namespace test {


RecyclerTest::RecyclerTest()
    : ::testing::Test()
{
}

RecyclerTest::~RecyclerTest()
{
}

void RecyclerTest::SetUp()
{
}

void RecyclerTest::TearDown()
{
}

struct TestType {
    unsigned _a;
};

TEST_F(RecyclerTest, test_create)
{
    // good idea to run test with valgrind

    for(bool return_val : { true, false } ) {
        SCOPED_TRACE(return_val);
        std::vector<TestType*> fn_called_with;
        auto callback_fn = [&](TestType* t) mutable { fn_called_with.push_back(t); return return_val; };

        typedef Recycler<TestType, decltype(callback_fn)> RecyclerType;

        TestType* t1_ptr=nullptr;
        TestType* t2_ptr=nullptr;
        {
            std::shared_ptr<RecyclerType> r { std::make_shared<RecyclerType>(callback_fn) };
            {
                std::shared_ptr<TestType> t1 = r->create();
                t1_ptr = t1.get();
                std::shared_ptr<TestType> t2 = r->create();
                t2_ptr = t2.get();
            }
            ASSERT_EQ(2U, fn_called_with.size());
            ASSERT_EQ(fn_called_with[0], t2_ptr) << " t1_ptr=" << t1_ptr;
            ASSERT_EQ(fn_called_with[1], t1_ptr) << " t2_ptr=" << t2_ptr;
        }

        fn_called_with.clear();
        {
            // allow recyler to go out of scope before its created member
            // we expect the members to keep the recylcelr alive untit returned
            std::shared_ptr<RecyclerType> r { std::make_shared<RecyclerType>(callback_fn) };
            {
                std::shared_ptr<TestType> t1 = r->create();
                t1_ptr = t1.get();
                {
                    std::shared_ptr<TestType> t2 = r->create();
                    t2_ptr = t2.get();
                    r.reset();
                }
            }
            ASSERT_EQ(fn_called_with[0], t2_ptr);
            ASSERT_EQ(fn_called_with[1], t1_ptr);
        }
    }
}

TEST_F(RecyclerTest, test_pop)
{
    // good idea to run test with valgrind
    std::vector<TestType*> fn_called_with;
    bool return_val=true;
    auto callback_fn = [&](TestType* t) mutable { fn_called_with.push_back(t); return return_val; };

    typedef Recycler<TestType, decltype(callback_fn)> RecyclerType;

    TestType* t1_ptr=nullptr;
    std::shared_ptr<RecyclerType> r { std::make_shared<RecyclerType>(callback_fn) };
    ASSERT_FALSE(r->pop());
    {
        {
            std::shared_ptr<TestType> t1 = r->create();
            t1_ptr = t1.get();
        }
        std::shared_ptr<TestType> t2 =  r->pop();
        ASSERT_EQ(t1_ptr, t2.get());
        ASSERT_FALSE(r->pop());
        return_val = false; // t2 should now not be safe to be recylced
    }
    ASSERT_FALSE(r->pop());

}

} // namespace test
} // namespace panda
} // namespace ska
