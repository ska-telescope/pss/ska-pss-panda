/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "ResourcePoolTest.h"
#include "panda/test/TestResource.h"
#include "panda/test/TestHandler.h"
#include "panda/ResourcePool.h"

#include <array>
#include <algorithm>

namespace ska {
namespace panda {
namespace test {
struct ArchA {};
struct ArchB {};
struct ArchCapability {};
}

template<>
struct ResourceSupports<test::ArchCapability, test::ArchA> {
  static bool compatible( PoolResource<test::ArchA> const& ) {
        return false;
    }
};

namespace test {

ResourcePoolTest::ResourcePoolTest()
    : ::testing::Test()
    , _engine(ProcessingEngineConfig(1))
{
}

ResourcePoolTest::~ResourcePoolTest()
{
}

void ResourcePoolTest::SetUp()
{
}

void ResourcePoolTest::TearDown()
{
}



template<typename... ArchitectureTypes>
struct TaskTraitsBase {

    public:
        typedef std::tuple<ArchitectureTypes...> Architectures;

        TaskTraitsBase() { std::fill(_called.begin(), _called.end(), false); }

        template<typename Arch>
        void operator()(panda::PoolResource<Arch>&)
        {
            _called[panda::Index<Arch, Architectures>::value] = true;
            _handlers[panda::Index<Arch, Architectures>::value]();
        }

        template<typename Arch, typename... DataTypes>
        void operator()(panda::PoolResource<Arch>&, DataTypes&&...)
        {
            _called[panda::Index<Arch, Architectures>::value] = true;
            _handlers[panda::Index<Arch, Architectures>::value]();
        }

        template<typename Arch, typename T>
        void create_exception(const T& exception) {
            _handlers[panda::Index<Arch, Architectures>::value].create_exception(exception);
        }

        template<typename Arch>
        void wait() {
            _handlers[panda::Index<Arch, Architectures>::value].wait();
        }

        template<typename Arch>
        void reset() {
            _handlers[panda::Index<Arch, Architectures>::value].reset();
        }

        template<typename Arch>
        bool called() const {
            return _called[panda::Index<Arch, Architectures>::value];
        }

    private:
        std::array<TestHandler, std::tuple_size<Architectures>::value> _handlers;
        std::array<bool, std::tuple_size<Architectures>::value> _called;
};

// triats class that supports ArchA and ArchB
struct TaskTraits : public TaskTraitsBase<ArchA, ArchB> {
};

// triats class that supports just ArchB
struct TaskTraitsB : public TaskTraitsBase<ArchB> {
};

// triats class that supports just ArchNoResource
struct TaskTraitsDiffCapability : public TaskTraitsBase<ArchA> {
    typedef std::tuple<ArchCapability> Capabilities;
};

template<typename Arch>
struct TaskTraitsWithoutArchitectureTuple : protected TaskTraitsBase<Arch>
{
        typedef TaskTraitsBase<Arch> BaseT;

    public:
        typedef Arch Architecture;

    public:
        template<typename... DataTypes>
        void operator()(panda::PoolResource<Arch>& device, DataTypes&&... args)
        {
            static_cast<BaseT&>(*this)(device, std::forward<DataTypes>(args)...);
        }

        void wait() {
            BaseT::template wait<Arch>();
        }

        bool called() const {
            return BaseT::template called<Arch>();
        }
};

typedef detail::Task<TaskTraits> TestTask;


TEST_F(ResourcePoolTest, submit_single_resource_no_data)
{
    // Use Case:
    // Single resource
    // Sumbit a single job
    // - ensure it is sent for processsing
    // Submit a second job
    // - ensure the job is put on the queue
    // - on completion the second job is then processed
    TaskTraits job_traits;
    TaskTraits job_traits_b;
    std::shared_ptr<ResourceJob> job2;
    {
        ResourcePool<1, ArchA, ArchB> m(_engine);
        std::shared_ptr<test::TestResource> resource = std::make_shared<test::TestResource>();
        m.add_resource<ArchA>( resource );

        ASSERT_EQ( 1U, m.free_resources_count<ArchA>() );
        ASSERT_EQ( 0U, m.free_resources_count<ArchB>() );
        std::shared_ptr<ResourceJob> job1 = m.submit<0>(job_traits);
        ASSERT_EQ( 0U, m.free_resources_count<ArchA>() );
        job2 = m.submit<0>(job_traits_b);
        ASSERT_EQ( TestTask::JobStatus::Queued, job2->status() );
        do{ std::this_thread::sleep_for(std::chrono::milliseconds(1)); } while( job1->status() == TestTask::JobStatus::Queued );
        ASSERT_EQ( TestTask::JobStatus::Running, job1->status() );
        job_traits.wait<ArchA>();

        //resource->complete_job();
        ASSERT_TRUE(job_traits.called<ArchA>());
        ASSERT_FALSE(job_traits.called<ArchB>());
        do{ std::this_thread::sleep_for(std::chrono::milliseconds(1)); } while( job2->status() == TestTask::JobStatus::Queued );
        ASSERT_EQ( TestTask::JobStatus::Finished, job1->status() );
        ASSERT_EQ( TestTask::JobStatus::Running, job2->status() );
        job_traits_b.wait<ArchA>();
        resource->complete_job(); // redundant - resources no longer manage the process
        ASSERT_TRUE(job_traits_b.called<ArchA>());
        ASSERT_FALSE(job_traits_b.called<ArchB>());
        // pool goes out f scope here, but the job is still active so it should be able to cope
    }
    do{ std::this_thread::sleep_for(std::chrono::milliseconds(1)); } while( job2->status() == TestTask::JobStatus::Running );
    ASSERT_EQ( TestTask::JobStatus::Finished, job2->status() );
    // N.B our Traites objects go out of scope here. If we didn't wait for the job to complete we would end up calling an
    // non existing traits class.
}

TEST_F(ResourcePoolTest, submit_with_data)
{
    // Use Case:
    // Single resource
    // Sumbit a single job containing data
    // - ensure it is sent for processsing
    TaskTraits job_traits;
    ResourcePool<1, ArchA, ArchB> m(_engine);
    std::shared_ptr<test::TestResource> resource = std::make_shared<test::TestResource>();
    m.add_resource<ArchA>( resource );
    int data_a = 99;
    std::string data_b("data_b");

    std::shared_ptr<ResourceJob> job1 = m.submit<0>(job_traits, data_a, data_b);
    do{ std::this_thread::sleep_for(std::chrono::milliseconds(1)); } while( job1->status() == TestTask::JobStatus::Queued );
    ASSERT_EQ( TestTask::JobStatus::Running, job1->status() );
    job_traits.wait<ArchA>();
    do{ std::this_thread::sleep_for(std::chrono::milliseconds(1)); } while( job1->status() == TestTask::JobStatus::Running );
}

TEST_F(ResourcePoolTest, job_throws)
{
    // Use Case:
    // Single Resource
    // Sumbit a single job that throws
    // Expect:
    // throw to be trapped and passed in the Job info
    TaskTraits job_traits;
    ResourcePool<1, ArchA, ArchB> m(_engine);
    std::shared_ptr<test::TestResource> resource = std::make_shared<test::TestResource>();
    m.add_resource<ArchA>( resource );
    std::shared_ptr<ResourceJob> job1 = m.submit<0>(job_traits);
    do{ std::this_thread::sleep_for(std::chrono::milliseconds(1)); } while( job1->status() == ResourceJob::JobStatus::Queued );
    int error_code=1982;
    job_traits.create_exception<ArchA>( error_code );
    do{ std::this_thread::sleep_for(std::chrono::milliseconds(1)); } while( job1->status() == ResourceJob::JobStatus::Running );
    ASSERT_EQ( ResourceJob::JobStatus::Failed, job1->status() );
    ASSERT_TRUE( compare_exception(error_code, job1->error().exception()) );
}

TEST_F(ResourcePoolTest, free_resource_count_unknown_arch)
{
    ResourcePool<1, ArchA> m(_engine);
    ASSERT_EQ( 0U, m.free_resources_count<ArchB>() );
}

TEST_F(ResourcePoolTest, submit_multi_arch)
{
    TaskTraits job_traits;
    ResourcePool<1, ArchA, ArchB> m(_engine);
    std::shared_ptr<test::TestResource> resource_a = std::make_shared<test::TestResource>();
    std::shared_ptr<test::TestResource> resource_b = std::make_shared<test::TestResource>();
    m.add_resource<ArchA>( resource_a );
    m.add_resource<ArchB>( resource_b );

    ASSERT_EQ( 1U, m.free_resources_count<ArchA>() );
    ASSERT_EQ( 1U, m.free_resources_count<ArchB>() );
    std::shared_ptr<ResourceJob> job1 = m.submit<0>(job_traits);
    ASSERT_EQ( 0U, m.free_resources_count<ArchA>() ); // should take resources in order
    std::shared_ptr<ResourceJob> job2 = m.submit<0>(job_traits);
    ASSERT_EQ( 0U, m.free_resources_count<ArchA>() );
    ASSERT_EQ( 0U, m.free_resources_count<ArchB>() );
    job_traits.wait<ArchA>();
    job_traits.wait<ArchB>();
    ASSERT_TRUE(job_traits.called<ArchA>());
    ASSERT_TRUE(job_traits.called<ArchB>());

    do{ std::this_thread::sleep_for(std::chrono::milliseconds(1)); } while( job1->status() == TestTask::JobStatus::Running );
    do{ std::this_thread::sleep_for(std::chrono::milliseconds(1)); } while( job2->status() == TestTask::JobStatus::Running );
}

TEST_F(ResourcePoolTest, submit_with_priority)
{
    // Use Case:
    // Single resource
    // Sumbit jobs with differenct prioritoes
    // - ensure the lowest prioroity number is used in preference to those with higher priority numbers
    TaskTraits job_traits;
    TaskTraits job_traits_b;
    TaskTraits job_traits_c;

    std::shared_ptr<test::TestResource> resource_a = std::make_shared<test::TestResource>();

    ResourcePool<2, ArchA, ArchB> m(_engine);
    m.add_resource<ArchA>( resource_a );
    // submit first job at lower priority - its hould run as no other jobs with higher priority are available
    std::shared_ptr<ResourceJob> job1 = m.submit<1>(job_traits);

    // submit second job at same low priority
    std::shared_ptr<ResourceJob> job2 = m.submit<1>(job_traits_b);

    // now submit a job at higher priority
    std::shared_ptr<ResourceJob> job3 = m.submit<0>(job_traits_c);

    // wait for the first job to complete and ensure the next job that runs is the highest priority job
    job_traits.wait<ArchA>();
    job_traits_c.wait<ArchA>();
    job_traits_b.wait<ArchA>();

    do{ std::this_thread::sleep_for(std::chrono::milliseconds(1)); } while( job2->status() == TestTask::JobStatus::Running );
}

TEST_F(ResourcePoolTest, submit_with_architecture_subset)
{
    // Use Case:
    // subit a task that only supports a subset of the availiable architectures in the pool

    TaskTraits job_traits_a_b;
    TaskTraitsB job_traits_b;
    ResourcePool<2, ArchA, ArchB> m(_engine);

    std::shared_ptr<test::TestResource> resource_a = std::make_shared<test::TestResource>();
    std::shared_ptr<test::TestResource> resource_b = std::make_shared<test::TestResource>();
    m.add_resource<ArchA>( resource_a );

    //std::shared_ptr<ResourceJob> job1 = m.submit<0>(job_traits_b);
    ASSERT_THROW(m.submit<0>(job_traits_b), panda::Error);
    std::shared_ptr<ResourceJob> job2 = m.submit<0>(job_traits_a_b);

    job_traits_a_b.wait<ArchA>();
    //ASSERT_EQ(TestTask::JobStatus::Queued, job1->status()); // should not run

    //m.add_resource<ArchB>( resource_b );
    //job_traits_b.wait<ArchB>();
}

TEST_F(ResourcePoolTest, submit_with_architecture_superset)
{
    // Use Case:
    // subit a task that supports more than the availiable architectures in the pool

    TaskTraits job_traits_a_b;
    ResourcePool<2, ArchB> m(_engine);

    std::shared_ptr<test::TestResource> resource_b = std::make_shared<test::TestResource>();
    m.add_resource<ArchB>( resource_b );

    std::shared_ptr<ResourceJob> job1 = m.submit<0>(job_traits_a_b);
    job_traits_a_b.wait<ArchB>();

    do{ std::this_thread::sleep_for(std::chrono::milliseconds(1)); } while( job1->status() == TestTask::JobStatus::Running );
}

TEST_F(ResourcePoolTest, submit_with_architecture_but_no_compatible_resource)
{
    // Use Case:
    // submit a task where the architecture matches the pool but the resource capability does not

    TaskTraitsDiffCapability job_traits_capability;
    TaskTraitsB  job_traits_b;
    ResourcePool<2, ArchA, ArchB> m(_engine);

    std::shared_ptr<test::TestResource> resource_a = std::make_shared<test::TestResource>();
    std::shared_ptr<test::TestResource> resource_b = std::make_shared<test::TestResource>();
    m.add_resource<ArchA>( resource_a );
    m.add_resource<ArchB>( resource_b );

    std::shared_ptr<ResourceJob> job1 = m.submit<0>(job_traits_capability);
    std::shared_ptr<ResourceJob> job2 = m.submit<0>(job_traits_b);

    job_traits_b.wait<ArchB>();
    ASSERT_EQ(TestTask::JobStatus::Queued, job1->status()); // should not run as resource is not compatible
    // It should try the next job in the queue
    do{ std::this_thread::sleep_for(std::chrono::milliseconds(1)); } while( job2->status() == TestTask::JobStatus::Running );
    ASSERT_EQ(TestTask::JobStatus::Queued, job1->status()); // still should not run
}

TEST_F(ResourcePoolTest, submit_with_architecture_typedef)
{
    // Use Case:
    // submit a task where only typdef Architecture is defined (instead of the Architectures tuple)
    TaskTraitsWithoutArchitectureTuple<ArchA> job_traits;

    ResourcePool<2, ArchA, ArchB> m(_engine);
    std::shared_ptr<test::TestResource> resource_a = std::make_shared<test::TestResource>();
    std::shared_ptr<test::TestResource> resource_b = std::make_shared<test::TestResource>();
    m.add_resource<ArchA>( resource_a );
    m.add_resource<ArchB>( resource_b );

    std::shared_ptr<ResourceJob> job1 = m.submit<0>(job_traits);
    job_traits.wait();
    do{ std::this_thread::sleep_for(std::chrono::milliseconds(1)); } while( job1->status() == TestTask::JobStatus::Running );

}

} // namespace test
} // namespace panda
} // namespace ska
