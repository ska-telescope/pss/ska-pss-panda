/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "ServerTest.h"
#include "panda/test/TestClient.h"
#include "panda/Server.h"
#include <thread>
#include <type_traits>


namespace ska {
namespace panda {
namespace test {


ServerTest::ServerTest()
    : ::testing::Test()
{
}

ServerTest::~ServerTest()
{
}

void ServerTest::SetUp()
{
}

void ServerTest::TearDown()
{
}

template<typename ServerTraits>
class TestApp {
        typedef Server<ServerTraits, TestApp<ServerTraits>> ServerType;

    public: 
        TestApp() 
            : _stop_recieved(false) 
            , _disconnect_recieved(false)
            , _server(nullptr)
        {
        }

        void set_server(ServerType* server) { _server = server; }

        // functions to test are called
        void on_connect(typename ServerTraits::Session const& session)
        { 
            //Buffer<char> buf(10);
            //session->read(buf);
            //std::cout << "on_connect: got message " << std::string(buf.data(), buf.size()) << std::endl;
            _msgs.push_back(session);
        }

        void on_disconnect(typename ServerTraits::Session const&) 
        { 
            _disconnect_recieved = true;
        }

        void on_error(typename ServerTraits::Session const&, typename ServerTraits::Error) {}

        void stop() { _stop_recieved = true; }
        bool stop_received() const { return _stop_recieved; }
        bool disconnect_received() const { return _disconnect_recieved; }

    private:
        std::vector<typename ServerTraits::Session> _msgs;
        bool _stop_recieved;
        bool _disconnect_recieved;
        ServerType*  _server;
};

TEST_F(ServerTest, test_start_stop)
{
    /**
     * @details simply start the server, amd then see if it will stop cleanly
     *          with no connections having been made
     */
    typedef ServerTraits<Udp> TraitsType;
    ServerConfig settings;
    TestApp<TraitsType> app;
    typedef Server<TraitsType, TestApp<TraitsType>> ServerType;
    ServerType server(settings, app);
    app.set_server(&server);
    ASSERT_FALSE(server.is_running());
    std::thread t( [&server](){server.run();} );
    do { std::this_thread::sleep_for(std::chrono::milliseconds(1)); } while(!server.is_running());
    
    server.stop();
    ASSERT_FALSE(server.is_running());
    t.join();
}

TEST_F(ServerTest, test_udp)
{
    /**
     * @details start the server, and test the App is called apporopriately when
     *          a client connects
     */
    typedef ServerTraits<Udp> TraitsType;

    // setup the test app and get the server running
    ServerConfig settings;
    TestApp<TraitsType> app;
    typedef Server<TraitsType, TestApp<TraitsType>> ServerType;
    ServerType server(settings, app);
    app.set_server(&server);
    std::thread t( [&server](){ try {
                                    server.run();
                                } catch ( std::exception const & e) {
                                    std::cerr << "unexpected exception starting server: " << e.what() << std::endl;
                                    throw; 
                                }
                              } );
    do { std::this_thread::sleep_for(std::chrono::milliseconds(1)); } while(!server.is_running());

    // get a client and connect to the server
    TestClient<ServerType> client(server);
    ASSERT_FALSE(client.connect());
    client.send("msg");
    client.disconnect();
    ASSERT_FALSE(app.stop_received());
    //ASSERT_TRUE(app.disconnect_received());

    // close it all down
    server.stop();
    t.join();

    ASSERT_TRUE(app.stop_received());
}

} // namespace test
} // namespace panda
} // namespace ska
