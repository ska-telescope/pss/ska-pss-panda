/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "StreamerAppTest.h"
#include "panda/StreamerApp.h"
#include "panda/ConnectionTraits.h"
#include <mutex>
#include <memory>
#include <condition_variable>
#include <deque>


namespace ska {
namespace panda {
namespace test {


StreamerAppTest::StreamerAppTest()
    : ::testing::Test()
{
}

StreamerAppTest::~StreamerAppTest()
{
}

void StreamerAppTest::SetUp()
{
}

void StreamerAppTest::TearDown()
{
}

template<typename T>
struct TestAppStream {
        TestAppStream()
            : _abort(false)
        {
        }

        ~TestAppStream()
        {
            abort();
        }

        void abort() {
            _abort = true;
            _wait.notify_all();
        }

        T next() {
            // block until send() is called
            std::unique_lock<std::mutex> lk(_mutex);
            _wait.wait(lk, [this]()
                           {
                               if(_abort) throw panda::Abort();
                               return !_data.empty();
                           });

            auto data = _data[0];
            _data.pop_front();
            return data;
        }

        void send(T const& data)
        {
            {
                std::lock_guard<std::mutex> lk(_mutex);
                _data.push_back(data);
            }
            _wait.notify_one();
        }

        unsigned long interval() {
            return 100;
        }

    private:
        bool _abort;
        std::mutex _mutex;
        std::condition_variable _wait;
        std::deque<T> _data;
};

class TestUdpClient {
        typedef panda::ConnectionTraits<panda::Udp, boost::use_default, panda::Engine> ConnectionTraits;

    public:
        TestUdpClient(unsigned port=0)
            : _local_address(port, "127.0.0.1")
            , _connection(_engine, _local_address.template end_point<typename ConnectionTraits::EndPointType>())
        {
            _connection.set_remote_end_point(_connection.socket().local_endpoint());
        }

        ~TestUdpClient()
        {
            if(_remote_end_point)
                _remote_end_point->close();
        }

        std::string receive(std::size_t size)
        {
            typedef typename decltype(_connection)::BufferType BufferType;
            BufferType recv_buffer(size);
            std::lock_guard<std::mutex> lk(_mutex);
            bool data_received = false;
            _connection.receive(recv_buffer, [&data_received, size](panda::Error e, std::size_t bytes_read, BufferType buffer) {
                ASSERT_FALSE(e);
                data_received = true;
                ASSERT_EQ(size, bytes_read);
                ASSERT_EQ(size, buffer.size());
               ;}
            );
            do {
                _engine.run();
                _engine.reset();
            } while(!data_received);
            return std::string(static_cast<char*>(recv_buffer.data()), size );
        }

        std::shared_ptr<AbstractConnection> client_end_point()
        {
            if(!_remote_end_point) {
                _local_address.port(port());
                boost::system::error_code ec;
                auto endpoint = _local_address.template end_point<typename ConnectionTraits::EndPointType>();
                _remote_end_point=std::make_shared<Connection<ConnectionTraits>>(_engine);
                _remote_end_point->set_remote_end_point(endpoint);
            }
            return _remote_end_point;
        }

        unsigned port() const {
            return _connection.socket().local_endpoint().port();
        }

    private:
        panda::IpAddress _local_address;
        std::mutex _mutex;
        panda::Engine _engine;
        panda::Connection<ConnectionTraits> _connection;
        std::shared_ptr<Connection<ConnectionTraits>> _remote_end_point;
};

TEST_F(StreamerAppTest, test_fixed_client)
{
    typedef TestAppStream<std::string> Stream;

    TestUdpClient fixed_client;
    panda::Engine engine;
    Stream stream;
    std::unique_ptr<StreamerApp<Stream>> app(new StreamerApp<Stream>(stream, engine));

    PANDA_LOG_DEBUG << "client endpoint " << fixed_client.port();
    ASSERT_NE(fixed_client.port(), 0U);
    std::string data("1234567890");
    stream.send(data);
    app->add_fixed_client(fixed_client.client_end_point());

    std::thread t1([&]() { engine.run(); } );

    // expect to see a connection to that client
    std::string rdata = fixed_client.receive(data.size());
    ASSERT_EQ(data, rdata);
    engine.stop();
    app.reset();
    t1.join();
}

} // namespace test
} // namespace panda
} // namespace ska
