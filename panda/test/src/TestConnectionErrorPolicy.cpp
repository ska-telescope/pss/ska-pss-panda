/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/test/TestConnectionErrorPolicy.h"
#include "panda/Log.h"


namespace ska {
namespace panda {


TestConnectionErrorPolicy::TestConnectionErrorPolicy()
    : _connected(false)
    , _try_connect(true)
    , _connection_error_return(true)
    , _error_return(true)
    , _error(false)
    , _connection_error_count(0)
    , _error_count(0)
{
}

TestConnectionErrorPolicy::~TestConnectionErrorPolicy()
{
}

bool TestConnectionErrorPolicy::connection_error(Error const& error)
{
    ++_connection_error_count;
    _last_connection_error = error;
    _connected = false;
    if(_connection_error_return) {
        _connection_error_return=false;
        return true;
    }
    return false;
}

bool TestConnectionErrorPolicy::error(Error const& error)
{
    _error = true;
    ++_error_count;
    _last_error = error;
    _connected = false;
    return _error_return;
}

bool TestConnectionErrorPolicy::send_error(Error const& error)
{
    PANDA_LOG_ERROR << "send error";
    _error = true;
    ++_error_count;
    _last_error = error;
    _connected = false;
    return _error_return;
}

bool TestConnectionErrorPolicy::try_connect()
{
    return _try_connect;
}

void TestConnectionErrorPolicy::connected()
{
    _connected = true;
}

unsigned TestConnectionErrorPolicy::connection_errors()
{
    unsigned tmp = _connection_error_count;
    _connection_error_count = 0;
    return tmp;    
}

unsigned TestConnectionErrorPolicy::transmission_errors()
{
    unsigned tmp = _error_count;
    _error_count = 0;
    return tmp;    
}

} // namespace panda
} // namespace ska
