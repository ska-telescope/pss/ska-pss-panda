/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/test/TestFile.h"


namespace ska {
namespace panda {
namespace test {

TestFile::TestFile()
    : _path(boost::filesystem::unique_path())
{
}

TestFile::TestFile(boost::filesystem::path p)
    : _path(boost::filesystem::unique_path(std::move(p)))
{
}

TestFile::~TestFile()
{
    if(boost::filesystem::exists(_path)) {
        boost::filesystem::remove(_path);
    }
}

std::string const& TestFile::filename() const
{
    return _path.native();
}

std::string TestFile::name() const
{
    return _path.filename().string();
}

boost::filesystem::path const& TestFile::filepath() const
{
    return _path;
}

void TestFile::open()
{
    if(!_file_stream.is_open()) {
        _file_stream.open(_path.native(), std::ios_base::out);
    }
}

void TestFile::flush()
{
    if(_file_stream.is_open()) {
        _file_stream.flush();
    }
    // TODO fsync to device too (use boost::iostreams file_descriptior_sink to be able to get FH
    // boost::iostreams::stream<boost::iostreams::file_descriptor_sink> _file_stream; fsync(_file_stream.handle())
}

} // namespace test
} // namespace panda
} // namespace ska
