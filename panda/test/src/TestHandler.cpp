/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/test/TestHandler.h"
#include "panda/test/detail/TestHandlerImpl.h"

namespace ska {
namespace panda {
namespace test {

TestHandler::TestHandler(bool blocking)
    : _pimpl(std::make_shared<TestHandlerImpl>(blocking))
{
}

TestHandler::~TestHandler()
{
}

void TestHandler::operator()()
{
    _pimpl->operator()();
}

void TestHandler::wait() const
{
    _pimpl->wait();
}

bool TestHandler::wait(std::chrono::seconds const& timeout) const
{
    return _pimpl->wait(timeout);
}

void TestHandler::reset()
{
    _pimpl->reset();
}

void TestHandler::set_verbose(bool v)
{
    _pimpl->set_verbose(v);
}

bool TestHandler::executed() const
{
    return _pimpl->executed();
}

std::thread::id const& TestHandler::caller_thread_id() const
{
    return _pimpl->caller_thread_id();
}

} // namespace test
} // namespace panda
} // namespace ska
