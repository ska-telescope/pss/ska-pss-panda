/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "panda/test/TestSocket.h"


namespace ska {
namespace panda {


TestSocketBase::TestSocketBase(Engine& engine)
    : _engine(engine)
    , _max_bytes_to_send(-1)
    , _max_bytes_to_recv(-1)
    , _connected(false)
    , _open(false)
{
}

TestSocketBase::~TestSocketBase()
{
}

void TestSocketBase::close()
{
    _connected = false;
    _open = false;
}

bool TestSocketBase::is_open() const
{
    return _open;
}

void TestSocketBase::send_all()
{
    _max_bytes_to_send = -1;
}

void TestSocketBase::recv_all()
{
    _max_bytes_to_recv = -1;
}

void TestSocketBase::send_max_bytes(unsigned max)
{
    _max_bytes_to_send = (long)max;
}

void TestSocketBase::recv_max_bytes(unsigned max)
{
    _max_bytes_to_recv = (long)max;
}

void TestSocketBase::reset_connection_error()
{
    _connect_error = boost::system::error_code();
}

void TestSocketBase::set_connection_error(boost::system::error_code const& code)
{
    _connect_error = code;
}

void TestSocketBase::set_open_error(boost::system::error_code const& code)
{
    _open_error = code;
}

void TestSocketBase::reset_open_error()
{
    _open_error = boost::system::error_code();
}

void TestSocketBase::reset_send_error()
{
    _send_error = boost::system::error_code();
}

void TestSocketBase::reset_recv_error()
{
    _recv_error = boost::system::error_code();
}

void TestSocketBase::set_send_error(boost::system::error_code const& code)
{
    _send_error = code;
}

void TestSocketBase::set_recv_error(boost::system::error_code const& code)
{
    _recv_error = code;
}

void TestSocketBase::set_on_connect_function(OnConnectFunction const& fn)
{
    _on_connect_function = fn;
}

void TestSocketBase::set_on_send_function(OnConnectFunction const& fn)
{
    _on_send_function = fn;
}

void TestSocketBase::set_on_recv_function(OnConnectFunction const& fn)
{
    _on_recv_function = fn;
}

} // namespace panda
} // namespace ska
