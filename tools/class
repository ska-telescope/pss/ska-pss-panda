#!/usr/bin/perl
# Class script
# Given a classname will return the location
# of the appropriate header and the cpp file
# If the files do not exist it will create them with
# a standard template
# -----------------------------------------------
# copyright Chirs Williams 2003-2009
# -----------------------------------------------
use FileHandle;
use Cwd;
use File::Basename;
use strict;
use FindBin;

sub usage {
    print "Funtion:\n";
    print "  Return the filenames of the specified class.\n";
    print "  If the files do not exist then they will be generated.\n";
    print "Usage:\n";
    print "  class [options] module/path/classname\n\n";
    print "Options:\n",
          "  -t template\t: construct with the specified template\n",
          "                 Templates available:\n",
          "                   gtest\n",
          "                   cppunit\n",
          "                   QWidget\n",
          "                   QDialog\n",
          "  -i baseclass\t\n",
          "  --licence licence-file\t: file path to alternative license file\n",
          "  --license licence-file\t: same as --licence",
          "\n\n";
    print "Examples:\n";
    print "    >class GasSensor/GasSensorCore/MyClass\n\n";
    print "  Will create files MyClass.cpp MyClass.h\n";
    print "  in the module GasSensor/GasSensorCore\n";
}

# parse options
my @namespaces=("ska", "panda");
my $baseclass="";
my $headtext="";
my $cpptext="";
my $includes="";
my $cppincludes="";
my $cppincludesNamespace="";
my $constructSig="";
my $constructHeadSig="";
my $indents=4; # default tab size indents

# license file name location
my $license_filename = $FindBin::Bin . "/../LICENSE";

while ( $ARGV[0] =~/^-(.+)/ )
{
    shift @ARGV;
    my $opt=$1;
    if ( $opt =~/indent/ ) {
        $indents = shift;
    }
    elsif ( $opt =~/^-+help/ ) {
        usage();
        exit 0;
    }
    elsif ( $opt eq "i" ) {
        $baseclass = shift;
        next;
    }
    elsif ( $opt eq "t" ) {
        # templates
        my $template = shift;
        if ( $template eq "gtest" ) {
            push @namespaces, "test";
            $includes = "#include <gtest/gtest.h>\n";
            $baseclass = "::testing::Test";
            $headtext = '${i}protected:'."\n".
                        '${i}${i}'.'void SetUp() override;'."\n".
                        '${i}${i}'.'void TearDown() override;'."\n\n";
            $cpptext = 'void ${classname}::SetUp()'."\n".
                       "{\n}\n\n".
                       'void ${classname}::TearDown()'."\n".
                       "{\n}\n\n".
                       'TEST_F(${classname}, test_something)'."\n".
                       "{\n}\n\n";
            next;
        } elsif ( $template eq "cppunit" ) {
            $includes = "#include <cppunit/extensions/HelperMacros.h>\n";
            $cppincludesNamespace='CPPUNIT_TEST_SUITE_REGISTRATION( ${classname} );';
            $baseclass = "CppUnit::TestFixture";
            $headtext = '${i}public:'."\n".
                        '${i}${i}'.'CPPUNIT_TEST_SUITE( ${classname} );'."\n".
                        '${i}${i}'.'CPPUNIT_TEST( test_method );'."\n".
                        '${i}${i}'.'CPPUNIT_TEST_SUITE_END();'."\n\n".
                        '${i}public:'."\n".
                        '${i}${i}'.'void setUp();'."\n".
                        '${i}${i}'.'void tearDown();'."\n\n".
                        '${i}${i}// Test Methods'."\n".
                        '${i}${i}'.'void test_method();'."\n\n";
            $cpptext = 'void ${classname}::setUp()'."\n".
                       "{\n}\n\n".
                       'void ${classname}::tearDown()'."\n".
                       "{\n}\n\n".
                       'void ${classname}::test_method()'."\n".
                       "{\n}\n\n";
            next;
        }
        elsif ( $template eq "QWidget" )
        {
            $headtext = '${i}Q_OBJECT'."\n\n";
            $includes = "#include <QWidget>\n";
            $baseclass="QWidget";
            $constructSig="QWidget* parent";
            $constructHeadSig="QWidget* parent=0";
        }
        elsif ( $template eq "QDialog" )
        {
            $headtext = '${i}Q_OBJECT'."\n\n";
            $includes = "#include <QDialog>\n";
            $baseclass="QDialog";
            $constructSig="QDialog* parent";
            $constructHeadSig="QDialog* parent=0";
        }
        else {
            print "Unknown template specified \"$template\"\n";
            exit 1;
        }
        next;
    }
    elsif ( $opt eq "-license" || $opt eq "-licence") {
        $license_filename = shift;
        next;
    }
    else {
        print "Unknown option $opt\n";
        usage();
        exit 1;
    }
}

if ( $#ARGV != 0 ) {
    print "Expecting different number of arguments. Got : ( @ARGV )\n";
    usage();
    exit 1;
}
my $verbose = 0;
my $i=" " x $indents;
my $class=shift @ARGV;
my $classname=basename($class);
if ( $constructSig ne "" && $constructHeadSig eq "" )
{
    $constructHeadSig = $constructSig;
}
if ( $constructHeadSig ne "" && $constructSig eq "" )
{
    $constructSig = $constructHeadSig;
}

# find filenames
my $short_header_file=$classname.".h";
my $path=dirname($class);
my $base=cwd()."/".$path;
my $src_file = $base;
my $header_file = $base;
if ( -d $base."/include" ) {
     $header_file=$base."/include";
}
if ( -d $base."/src" ) {
     $src_file=$base."/src";
}
$src_file.="/".$classname.".cpp";
$header_file.="/".$short_header_file;
if ( $verbose ) {
    print "Checking for Class=",$class," in directory ", $base."\n";
}

# ------- write out the cpp file
{
    # expand the cpptext variables
    no strict 'refs';
    $cpptext =~ s/(\$\{\w+\})/$1/eeg;
    $cppincludes =~ s/(\$\{\w+\})/$1/eeg;
    $cppincludesNamespace =~ s/(\$\{\w+\})/$1/eeg;
}
if ( ! -f $src_file ) {
    if ( $verbose ) {
        print "Creating file $src_file\n";
    }
    my $fh=FileHandle->new(">".$src_file);
    print $fh licence_text($license_filename);
    print $fh "#include \"", "panda/".$short_header_file, "\"\n\n";
    print $fh $cppincludes, "\n";
    foreach my $namespace ( @namespaces ) {
        print $fh "namespace $namespace {\n";
    }
    print $fh $cppincludesNamespace, "\n";
    print $fh "\n";
    print $fh $classname,"::", $classname, "($constructSig)\n",
              (( $baseclass ne "")?"    : BaseT()\n":"" ),
              "{\n",
              "}\n\n",
              $classname, "::~", $classname, "()\n{\n",
              "}\n\n",
              $cpptext;
    foreach my $namespace ( reverse @namespaces ) {
        print $fh "} // namespace $namespace\n", if ( defined $namespace );
    }
}
# ------ write out the header file
if ( ! -f $header_file ) {
    if ( $verbose ) {
        print "Creating file $header_file\n";
    }
    my $fh=FileHandle->new(">".$header_file);
    my $defname=(join("_",@namespaces,$classname))."_H";
    {
       # expand the header text variables
       no strict 'refs';
       $headtext =~ s/(\$\{\w+\})/$1/eeg;
    }
    $defname=~tr/a-z/A-Z/;
    print $fh licence_text($license_filename);
    print $fh "#ifndef ", $defname, "\n",
              "#define $defname\n\n";
    print $fh $includes."\n";
    if ( $baseclass ne "" && $includes eq "" ) {
        (my $bclass = $baseclass)=~s/::/\//g;
        print $fh "#include \"$bclass.h\"\n\n";
    }
    foreach my $namespace ( @namespaces ) {
        print $fh "namespace $namespace {\n", if ( defined $namespace );
    }
    print $fh "\n/**\n",
              " * \@brief\n",
              " *\n",
              " * \@details\n",
              " *\n",
              " */\n";
    print $fh "class $classname", (($baseclass eq "")?"":" : public $baseclass");
    print $fh  "\n{\n";
    if($baseclass ne "") {
        print $fh $i x 2, "typedef $baseclass BaseT;\n\n";
    }
    print $fh  $headtext;
    print $fh  $i,"public:\n",
               $i x 2, $classname, "($constructHeadSig);\n",
               $i x 2,"~",$classname, "();\n",
               "\n",
               $i,"private:\n",
              "};\n\n";
    foreach my $namespace ( reverse @namespaces ) {
        print $fh "} // namespace $namespace\n";
    }
    print $fh "\n#endif // $defname\n";
}
if ( ! $verbose ) {
    # default is to print out filenames of the class to be piped
    # to editors etc.
    print $header_file, " ", $src_file, "\n";
}

sub licence_text {
    my $license_filename=shift;
    my $text = "";
    open( my $lfh, '<', $license_filename ) or die "Can't open $license_filename: $!";
    while ( my $line = <$lfh> ) {
        if($line !~ /^\s*$/)
        {
            $text = $text . " * " . $line;
        } else {
            $text = $text . " *\n";
        }
    }
    close $lfh or die "can't close $license_filename: $!";
    chomp $text;

    my $license = "/*\n" . $text . "\n */\n";

    return $license;
}
